create_clock -period 100MHz  -name clkintopb_p clkintopb_p
create_clock -period 250MHz  -name jesd_device_clk jesd_device_clk

set_clock_groups -asynchronous -group {jesd_device_clk} -group {clkintopb_p}

#create_generated_clock -source clkintopb_p [get_ports fx3_pclk_o] -name fx3_pclk_o

create_generated_clock -invert -name fx3_pclk_o -source [get_pins {fx3_clk_out_buf|ALTDDIO_OUT_component|auto_generated|ddio_outa[0]|muxsel}] [get_ports {fx3_pclk_o}]

derive_pll_clocks -use_tan_name

derive_clock_uncertainty

#I2C

set_false_path -from [get_ports fx3_i2c_sclk_io]
set_false_path -from [get_ports fx3_i2c_sda_io]
set_false_path -to [get_ports fx3_i2c_sda_io]

#GPIF II

set_output_delay -add_delay -max -clock fx3_pclk_o  2 [get_ports {fx3_data_io[*]}]
set_output_delay -add_delay -min -clock fx3_pclk_o  -0.5 [get_ports {fx3_data_io[*]}]

set_input_delay -add_delay -max -clock fx3_pclk_o  8 [get_ports {fx3_data_io[*]}]
set_input_delay -add_delay -min -clock fx3_pclk_o  2 [get_ports {fx3_data_io[*]}]

set_output_delay -add_delay -max -clock fx3_pclk_o  2 [get_ports {fx3_slwr_n_o}]
set_output_delay -add_delay -min -clock fx3_pclk_o  -0.5 [get_ports {fx3_slwr_n_o}]

set_output_delay -add_delay -max -clock fx3_pclk_o  2 [get_ports {fx3_slrd_n_o}]
set_output_delay -add_delay -min -clock fx3_pclk_o  -0.5 [get_ports {fx3_slrd_n_o}]

#set_false_path -to [get_ports {fx3_slrd_n_o}]

set_false_path -to [get_ports {fx3_sloe_n_o}]

set_false_path -to [get_ports {fx3_pktend_n_o}]

set_false_path -to [get_ports {fx3_slcs_n_o}]

set_output_delay -add_delay -max -clock fx3_pclk_o  3 [get_ports {fx3_adrs_o[*]}]
set_output_delay -add_delay -min -clock fx3_pclk_o  -1 [get_ports {fx3_adrs_o[*]}]

set_input_delay -add_delay -max -clock fx3_pclk_o  7 [get_ports {fx3_flaga_i}]
set_input_delay -add_delay -min -clock fx3_pclk_o  2 [get_ports {fx3_flaga_i}]

set_input_delay -add_delay -max -clock fx3_pclk_o  7 [get_ports {fx3_flagb_i}]
set_input_delay -add_delay -min -clock fx3_pclk_o  2 [get_ports {fx3_flagb_i}]

set_input_delay -add_delay -max -clock fx3_pclk_o  7 [get_ports {fx3_flagc_i}]
set_input_delay -add_delay -min -clock fx3_pclk_o  2 [get_ports {fx3_flagc_i}]

set_input_delay -add_delay -max -clock fx3_pclk_o  7 [get_ports {fx3_flagd_i}]
set_input_delay -add_delay -min -clock fx3_pclk_o  2 [get_ports {fx3_flagd_i}]

set_multicycle_path -from [get_ports {fx3_data_io[*]}] -setup -end 2
set_multicycle_path -from [get_ports {fx3_flag*_i}] -setup -end 2

#JESD204
create_clock -period 125MHz -name jesd_link_clk_virtual
set_input_delay -add_delay -max -clock jesd_link_clk_virtual  0 [get_ports {jesd_sysref}]
set_input_delay -add_delay -min -clock jesd_link_clk_virtual  0 [get_ports {jesd_sysref}]
set_false_path -to [get_ports {jesd_rx_syncn}]
set_false_path -to [get_ports {jesd_rx_syncn(n)}]

#LED indicators
set_false_path -to [get_ports {user_led*}]

#Trigger input
create_clock -period 125MHz -name trig_in_clk_virtual
set_input_delay -add_delay -max -clock trig_in_clk_virtual  0 [get_ports {smaj3_trig_in}]
set_input_delay -add_delay -min -clock trig_in_clk_virtual  0 [get_ports {smaj3_trig_in}]

#Trigger output
create_clock -period 125MHz -name trig_out_clk_virtual
set_output_delay -add_delay -max -clock trig_out_clk_virtual  2 [get_ports {trig_out[0]}]
set_output_delay -add_delay -min -clock trig_out_clk_virtual  1 [get_ports {trig_out[0]}]

#Cutting psths between domains with different clock frequencies
set_false_path -from [get_clocks *pll_afi_clk]  -to [get_clocks *clkintopb_p]
set_false_path -from [get_clocks qubit_daq_qsys:qubit_daq_qsys_inst|jesd_pll_w_reconfig:jesd_pll|jesd_avgz_pll:jesd_pll_inst|jesd_avgz_pll_0002:jesd_avgz_pll_inst|altera_pll:altera_pll_i|altera_arriavgz_pll:arriavgz_pll|divclk[0]] -to [get_clocks *clkintopb_p]
set_false_path -from [get_clocks clkintopb_p] -to [get_clocks qubit_daq_qsys:qubit_daq_qsys_inst|jesd_pll_w_reconfig:jesd_pll|jesd_avgz_pll:jesd_pll_inst|jesd_avgz_pll_0002:jesd_avgz_pll_inst|altera_pll:altera_pll_i|altera_arriavgz_pll:arriavgz_pll|divclk[0]]
set_false_path -from [get_clocks clkintopb_p] -to [get_clocks qubit_daq_qsys_inst|ddr3|pll0|pll_afi_clk]
set_false_path -from [get_clocks qubit_daq_qsys:qubit_daq_qsys_inst|jesd_pll_w_reconfig:jesd_pll|jesd_avgz_pll:jesd_pll_inst|jesd_avgz_pll_0002:jesd_avgz_pll_inst|altera_pll:altera_pll_i|altera_arriavgz_pll:arriavgz_pll|divclk[0]] -to [get_clocks qubit_daq_qsys_inst|ddr3|pll0|pll_afi_clk]
set_false_path -from [get_clocks qubit_daq_qsys_inst|ddr3|pll0|pll_afi_clk] -to [get_clocks qubit_daq_qsys:qubit_daq_qsys_inst|jesd_pll_w_reconfig:jesd_pll|jesd_avgz_pll:jesd_pll_inst|jesd_avgz_pll_0002:jesd_avgz_pll_inst|altera_pll:altera_pll_i|altera_arriavgz_pll:arriavgz_pll|divclk[0]]

#Overconstraining QR to HR clock domain
set ver_info $::TimeQuestInfo(nameofexecutable)
#if { [string notequal "quartus_sta" $::TimeQuestInfo(nameofexecutable)] }
if {$ver_info != "quartus_sta"} {
set_max_delay -from [get_keepers *qr_to_hr\|dataout*] -to [get_keepers *hr_to_fr*] 1
}

#DDR3 reset issue
set_clock_groups -asynchronous -group {clkintopb_p} -group {qubit_daq_qsys_inst|ddr3|pll0|pll_p2c_read_clk}
set_clock_groups -asynchronous -group {clkintopb_p} -group {qubit_daq_qsys_inst|ddr3|pll0|pll_avl_clk}
set_clock_groups -asynchronous -group {clkintopb_p} -group {qubit_daq_qsys_inst|ddr3|pll0|pll_config_clk}

