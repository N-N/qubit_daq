/*Triger capture module
*
*Depending on cap_edge it can capture on positive or negative edge of the clk.
*
*An external trigger signal can have an unpredictable delay, so, it's possible it'll violate setup and hold
*of the capturing flipflop. The ability to choose between positive and negative capturing clock edges can solve
*this problem. I you see jitter problen in captured data then switch to another edge.
*/

module trigger_capture
(
    input       clk,
    input       rst,
    
    input       trig_i,        //External trigger input
    input       soft_trig_i,   //Software trigger input 
    output reg  trig_o,        //Trigger output
    
    input       cap_posedge,   //External trigger capture edge 1 - positive, 0 - negative 
    input[31:0] delay,         //Trigger delay
    input       enable,        //Enable 
    input       ext_trig_en    //External trigger enable

);
    reg n_trig_o;
    reg trig_r_pos, trig_r_neg, trig_arm_pos, trig_arm_neg;
    reg [31:0] cnt, n_cnt;
    reg trig;
    
    enum int unsigned {IDLE=0, DELAY} state, n_state;
    
    always @(*) begin
        n_state = state;
        n_trig_o = 0;
        n_cnt = cnt;
        
        if(ext_trig_en) begin
            if (cap_posedge)
                trig = trig_r_pos;
            else
                trig = trig_r_neg;
        end else begin
            trig = soft_trig_i;
        end
        
        case(state)
            IDLE: begin
                n_cnt = delay;
                if(trig & enable) begin
                    if( cnt != 0 ) begin
                        n_cnt = cnt-1;
                        n_state = DELAY;
                    end else begin
                        n_trig_o = 1;
                    end
                end    
            end
            DELAY: begin
                n_cnt = cnt - 1;
                if(cnt==0) begin
                    n_trig_o = 1;
                    n_state = IDLE;
                end    
            end
        endcase
    
    end
    
    always @(posedge clk or posedge rst) begin
        if(rst) begin
            state <= IDLE;
            trig_o <= 0;
        end else begin
            state <= n_state;
            cnt <= n_cnt;
            trig_o <= n_trig_o;
        end    
    end

    always @(posedge clk) begin
        trig_arm_pos <= ~trig_i;
        if(trig_arm_pos)
            trig_r_pos <= trig_i;
        else
            trig_r_pos <= 0;
    end
    
    always @(negedge clk) begin
        trig_arm_neg <= ~trig_i;
        if(trig_arm_neg)
            trig_r_neg <= trig_i;
        else
            trig_r_neg <= 0;
    end
       
endmodule