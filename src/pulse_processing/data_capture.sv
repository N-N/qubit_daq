//AVST writing DMA
//for unaligned adressing using AVST address units
//Burstcount is 1
//N_N

module data_capture #( parameter 	AVST_DATA_WIDTH=256, 
								AVMM_ADDR_WIDTH = 32, 
								AVMM_DATA_WIDTH=512,
								AVMM_BURST_WIDTH=4, //Burstcount port width
								FIFO_DEPTH = 128,	//In avst words
                                BURSTCOUNT = 8
								)
(

	input wire 			avst_clk,
	input wire 			avst_rst,
	
	input wire 			avmm_clk,
	input wire 			avmm_rst,
	
	//AVST sink
	input wire 							avst_valid,
	input wire [AVST_DATA_WIDTH-1:0] 	avst_data,
	
	//AVMM master
	input wire 							avmm_waitrequest,
	output reg 						    avmm_write,
	output wire [AVMM_DATA_WIDTH-1:0] 	avmm_writedata,
	output wire [AVMM_ADDR_WIDTH-1:0] 	avmm_address,	
	output reg [AVMM_BURST_WIDTH-1:0]	avmm_burstcount,
    output reg [AVMM_DATA_WIDTH/8-1:0]  avmm_byteenable,
	
	//Control signals
	input    							start,      //Data capture start trigger    
	input      [31:0]	                start_addr, //Memory start address for data in AVMM words
    input                               segm_start, //Start of segment trigger
    input                               abort,      //Data capture abort signal
    input      [31:0]					len,        //Data length in AVST words
    input      [31:0]                   nsegm,
    
    output reg                          segm_stop,  //Segmend end marker    
	output reg 							busy,       //Data capture in progress
    output reg                          ready,      //Ready for segm_start trigger
    
    output reg [$clog2(FIFO_DEPTH)-1:0] wrusedw_max //Max FIFO utilization level
);

localparam ADDR_FIFO_DEPTH = FIFO_DEPTH/(AVMM_DATA_WIDTH/AVST_DATA_WIDTH);

//AVMM
assign avmm_burstcount = BURSTCOUNT;
reg n_avmm_write;
reg [AVMM_ADDR_WIDTH-1:0] avmm_addr, n_avmm_addr;

//Data counters
reg [31:0] avst_data_cnt, n_avst_data_cnt;
reg [$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH):0] pad_start_cnt, n_pad_start_cnt;
reg [$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH):0] pad_end_cnt, n_pad_end_cnt;
reg [31:0] segm_cnt, n_segm_cnt;
reg [AVMM_BURST_WIDTH-1:0] burst_cnt, n_burst_cnt;

reg avst_word_en;

//Control
reg n_busy;
reg n_ready;
reg n_segm_stop;

wire [$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH)-1:0] pad_start; 
assign pad_start = avmm_addr[$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH)-1:0];

//FIFO
wire fifo_wrfull, fifo_rdempty, addr_fifo_wrfull, addr_fifo_rdempty;
reg fifo_wr, fifo_rd;
reg addr_fifo_wr;
wire [$clog2(FIFO_DEPTH)-1:0] wrusedw;

wire [AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1:0][AVST_DATA_WIDTH/8-1:0]avmm_byteenable_dec;
wire [AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1:0][AVST_DATA_WIDTH:0] fifo_rd_data ;
wire [AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1:0][AVST_DATA_WIDTH-1:0] avmm_data_dec;
assign avmm_byteenable = avmm_byteenable_dec;
assign avmm_writedata = avmm_data_dec;

//avmm_busy synchronizer
reg [2:0] avmm_busy_sync;
reg avmm_busy;
always @(posedge avst_clk) begin
    avmm_busy_sync <= {avmm_busy, avmm_busy_sync[2:1]};
end

//AVST state machine
enum int unsigned {AVST_IDLE = 0, AVST_TRIG_WAIT, AVST_PAD_START, AVST_WR, AVST_PAD_END} avst_state, n_avst_state;
always @(*) begin
	n_avst_state = avst_state;
	n_busy = busy;
	fifo_wr = 0;
    
    n_avst_data_cnt = avst_data_cnt;
    n_pad_start_cnt = pad_start_cnt;
    n_pad_end_cnt = pad_end_cnt;
    n_segm_cnt = segm_cnt;
    n_burst_cnt = burst_cnt;
    
    n_segm_stop = 0;
    
    avst_word_en = 1;
    n_avmm_addr = avmm_addr;
    addr_fifo_wr = 0;
    n_ready = 0;
	
	case (avst_state)
		AVST_IDLE: begin
            n_avmm_addr = start_addr<<$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH);
            n_segm_cnt = nsegm;
            n_burst_cnt = BURSTCOUNT-1;
            n_avst_data_cnt = len;
            n_pad_end_cnt = AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1;
			if (start) begin
				n_busy = 1;    
                n_pad_start_cnt = pad_start;
                if( pad_start==0 )
                    n_avst_state = AVST_TRIG_WAIT;
                else    
                    n_avst_state = AVST_PAD_START;
			end else begin
                n_busy = (|wrusedw) | avmm_busy_sync[0];
            end
		end
        
        AVST_PAD_START:begin
            if ((!fifo_wrfull) && (!addr_fifo_wrfull)) begin
                fifo_wr=1'b1;
                avst_word_en = 0;
                n_pad_start_cnt = pad_start_cnt - 1;
                n_pad_end_cnt = pad_end_cnt - 1;
                if (pad_start_cnt == 1 )
                    n_avst_state = AVST_TRIG_WAIT;
			end	
        end
        
        AVST_TRIG_WAIT: begin
            n_ready = 1;
            if(abort) begin
                n_avst_state = AVST_PAD_END;
            end else if( segm_start ) begin
                if(avst_data_cnt==1)
                    n_segm_stop = 1;
                n_avst_state = AVST_WR;
                n_segm_cnt = segm_cnt - 1;
            end
        end

		AVST_WR: begin
            if (abort) begin
                n_avst_state = AVST_PAD_END;
                n_segm_stop = 1;
            end else begin    
                if(avst_data_cnt == 0) begin
                    //if( segm_cnt == 0 ) begin
                    //    n_avst_state = AVST_PAD_END;
                    //end else begin
                    n_avst_data_cnt = len;
                    //n_avst_state = AVST_TRIG_WAIT;
                    n_avst_state = AVST_PAD_END;
                    //end    
                end else if ((!fifo_wrfull) && (!addr_fifo_wrfull)) begin
                    if (avst_valid) begin
                        fifo_wr=1'b1;
                        if( avst_data_cnt==2 )
                            n_segm_stop = 1;
                        n_avst_data_cnt = avst_data_cnt - 1;
                        n_avmm_addr = avmm_addr+1;
                        if(pad_end_cnt == 0) begin
                            n_pad_end_cnt = AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1;
                            addr_fifo_wr = 1;
                            if(burst_cnt == 0)
                                n_burst_cnt = BURSTCOUNT-1;
                            else
                                n_burst_cnt = burst_cnt - 1;
                        end else begin
                            n_pad_end_cnt = pad_end_cnt - 1;
                        end       
                    end
                end
            end    
		end
        
        AVST_PAD_END: begin
            if( pad_end_cnt == AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1 & burst_cnt == BURSTCOUNT-1) begin
                if( segm_cnt != 0 )
                    n_avst_state = AVST_TRIG_WAIT;
                else
                    n_avst_state = AVST_IDLE;
            end else if ((!fifo_wrfull) && (!addr_fifo_wrfull)) begin
                fifo_wr=1'b1;
                avst_word_en = 0;
                n_avmm_addr = avmm_addr+1;
                if (pad_end_cnt == 0 ) begin
                    addr_fifo_wr = 1;
                    n_pad_end_cnt = AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1;
                    if(burst_cnt == 0)
                        n_burst_cnt = BURSTCOUNT-1;
                    else
                        n_burst_cnt = burst_cnt - 1;
                end else begin
                    n_pad_end_cnt = n_pad_end_cnt - 1;
                end    
			end	
		end
	endcase
end

always @(posedge avst_clk or posedge avst_rst)
begin
	if (avst_rst) begin
		avst_state		<= AVST_IDLE;
		busy			<= 0;
        ready           <= 0;
        wrusedw_max     <= 0;
	end else begin
		avst_state		<= n_avst_state;
		busy			<= n_busy;
        ready           <= n_ready;
        segm_stop       <= n_segm_stop;
        avst_data_cnt   <= n_avst_data_cnt;
        pad_start_cnt   <= n_pad_start_cnt;
        pad_end_cnt     <= n_pad_end_cnt;
        segm_cnt        <= n_segm_cnt;
        burst_cnt       <= n_burst_cnt;
        avmm_addr       <= n_avmm_addr;
        if(wrusedw>wrusedw_max)
            wrusedw_max     <= wrusedw;
        else
            wrusedw_max     <= wrusedw_max;
	end
end

//FIFO
dcfifo_mixed_widths	wr_fifo (
				.aclr 		(avst_rst),
				.data 		({avst_data, avst_word_en}),
				.rdclk 		(avmm_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avst_clk),
				.wrreq 		(fifo_wr),
				.q 			(fifo_rd_data),
				.rdempty 	(fifo_rdempty),
				.wrusedw 	(wrusedw),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	(fifo_wrfull));
	defparam
		wr_fifo.intended_device_family = "Arria V GZ",
		wr_fifo.lpm_numwords = FIFO_DEPTH,
		wr_fifo.lpm_showahead = "OFF",
		wr_fifo.lpm_type = "dcfifo_mixed_widths",
		wr_fifo.lpm_width = AVST_DATA_WIDTH+1,
		wr_fifo.lpm_widthu = $clog2(FIFO_DEPTH),
		wr_fifo.lpm_widthu_r = $clog2(FIFO_DEPTH*AVST_DATA_WIDTH/AVMM_DATA_WIDTH),
		wr_fifo.lpm_width_r = AVMM_DATA_WIDTH+AVMM_DATA_WIDTH/AVST_DATA_WIDTH,
		wr_fifo.overflow_checking = "ON",
		wr_fifo.rdsync_delaypipe = 5,
		wr_fifo.read_aclr_synch = "OFF",
		wr_fifo.underflow_checking = "ON",
		wr_fifo.use_eab = "ON",
		wr_fifo.write_aclr_synch = "OFF",
		wr_fifo.wrsync_delaypipe = 5;
        
dcfifo_mixed_widths	addr_fifo (
				.aclr 		(avst_rst),
				.data 		(avmm_addr>>$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH)),
				.rdclk 		(avmm_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avst_clk),
				.wrreq 		(addr_fifo_wr),
				.q 			(avmm_address),
				.rdempty 	(addr_fifo_rdempty),
				.wrusedw 	(),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	(addr_fifo_wrfull));
	defparam
		addr_fifo.intended_device_family = "Arria V GZ",
		addr_fifo.lpm_numwords = ADDR_FIFO_DEPTH,
		addr_fifo.lpm_showahead = "OFF",
		addr_fifo.lpm_type = "dcfifo_mixed_widths",
		addr_fifo.lpm_width = AVMM_ADDR_WIDTH,
		addr_fifo.lpm_widthu = $clog2(ADDR_FIFO_DEPTH),
		addr_fifo.lpm_widthu_r = $clog2(ADDR_FIFO_DEPTH),
		addr_fifo.lpm_width_r = AVMM_ADDR_WIDTH,
		addr_fifo.overflow_checking = "ON",
		addr_fifo.rdsync_delaypipe = 5,
		addr_fifo.read_aclr_synch = "OFF",
		addr_fifo.underflow_checking = "ON",
		addr_fifo.use_eab = "ON",
		addr_fifo.write_aclr_synch = "OFF",
		addr_fifo.wrsync_delaypipe = 5;
		
genvar i;
generate for (i=0; i< AVMM_DATA_WIDTH/AVST_DATA_WIDTH; i=i+1) begin: avmm_wiring
    assign avmm_data_dec[i] = fifo_rd_data[i]>>1;
    assign avmm_byteenable_dec[i] = {AVST_DATA_WIDTH/8{fifo_rd_data[i][0]}};
end endgenerate

wire fifo_nempty;
assign fifo_nempty = (!fifo_rdempty) && (!addr_fifo_rdempty);

//AVMM state machine
enum int unsigned {AVMM_IDLE=0, AVMM_WR =1} avmm_state, n_avmm_state;
always @(*) begin
    n_avmm_state = avmm_state;
    n_avmm_write = 0;
    fifo_rd = 0;
    avmm_busy = 0;
    
    case(avmm_state)
        AVMM_IDLE: begin
            if( fifo_nempty ) begin
                fifo_rd = 1;
                n_avmm_write = 1;
                n_avmm_state = AVMM_WR;
            end
        end	
        
        AVMM_WR: begin
            avmm_busy = 1;
            if(!avmm_waitrequest) begin
                if( !fifo_nempty ) begin
                    n_avmm_write = 0;
                    n_avmm_state = AVMM_IDLE;
                end else begin
                    fifo_rd = 1;
                    n_avmm_write = 1;
                end    
            end else begin
                n_avmm_write = 1;
            end
        end
    endcase    
end

always @(posedge avmm_clk or posedge avmm_rst)
begin
	if(avmm_rst) begin	
		avmm_state <= AVMM_IDLE;
        avmm_write <= 0;
	end else begin
        avmm_state <= n_avmm_state;
        avmm_write <= n_avmm_write;
	end
end	
		
endmodule