module pulse_processing_top
#(
    //Data stream
    parameter DATA_WIDTH = 256,     //Samples are packed in DATA_WIDTH bus
    parameter SAMPLE_WIDTH = 16,  
    //Feature memory
    parameter MEM_DATA_WIDTH = 32,
    parameter FEATURE_MEM_DEPTH = 8192, //In MEM_DATA_WIDTH-bit words
    //Dot product
    parameter DOT_PROD_WIDTH = 64,
    //Channels   
    parameter NUM_CHAN = 4,
	//Averaging
	parameter AVER_SAMPLE_WIDTH = 32,
	parameter AVER_MEM_DEPTH = 8192*2, 	//In AVER_SAMPLE_WIDTH samples
    //DDR3 memory 
    parameter DDR_DATA_WIDTH = 512,
    parameter DDR_ADDR_WIDTH = 26,
    parameter DDR_BURSTC_WIDTH = 4 
)
(
    //Control clock domain    
    input                           control_clk,
    input                           control_rst,
    //Data clock domain   
    input                           data_clk,
    input                           data_rst,
    //DDR3 memory clock domain
    input                           ddr_clk,
    input                           ddr_rst,
    //AVST data stream
    input   [DATA_WIDTH-1 :0]       avst_data,
    
    //AVMM binary classifier feature memory interface
    input   [$clog2(MEM_DATA_WIDTH*FEATURE_MEM_DEPTH*NUM_CHAN/8)-1:0]    ram_address,
    input                           ram_read,
    output                          ram_readdatavalid,
    output  [MEM_DATA_WIDTH-1:0]    ram_readdata,
    input   [MEM_DATA_WIDTH-1:0]    ram_writedata,
    input                           ram_write,
	
	//AVMM averaging memory interface
    input   [AVER_MEM_ADDR_WIDTH-1:0]   aver_address,
    input                               aver_read,
    output  [AVER_MEM_DATA_WIDTH-1:0]   aver_readdata,
	output                              aver_readdatavalid,
    
    //AVMM slave control interface
    input                           csr_write,
    input                           csr_read,
    input   [15:0]                  csr_address,
    input   [31:0]                  csr_datain,
    output                          csr_readdatavalid,
    output  [31:0]                  csr_dataout,
    
    //AVMM master DDR3 memory interface for dot product
	input                           ddr_waitrequest,
	output                          ddr_write      ,
	output  [DDR_DATA_WIDTH-1:0]    ddr_writedata  ,
	output  [DDR_ADDR_WIDTH-1:0]    ddr_address    ,	
	output  [DDR_BURSTC_WIDTH-1:0]  ddr_burstcount ,
    output  [DDR_DATA_WIDTH/8-1:0]  ddr_byteenable ,
    
    //AVMM master DDR3 memory interface for data
	input                           ddr_data_waitrequest,
	output                          ddr_data_write      ,
	output  [DDR_DATA_WIDTH-1:0]    ddr_data_writedata  ,
	output  [DDR_ADDR_WIDTH-1:0]    ddr_data_address    ,	
	output  [DDR_BURSTC_WIDTH-1:0]  ddr_data_burstcount ,
    output  [DDR_DATA_WIDTH/8-1:0]  ddr_data_byteenable ,
    
    //External trigger
    input                           ext_trig
); 
    
    localparam AVER_MEM_DATA_WIDTH = DATA_WIDTH/SAMPLE_WIDTH * AVER_SAMPLE_WIDTH;
    localparam AVER_MEM_ADDR_WIDTH = $clog2(AVER_MEM_DEPTH*AVER_SAMPLE_WIDTH/8);

    wire [31:0] len;
    wire signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] threshold;
    wire signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] dot_prod_ave;
    
    //DMA stuff
    wire dma_busy, dma_ready;
    wire [31:0] dma_fifo_lvl;
    
    wire cap_start, cap_busy, trig, segm_start, segm_stop, cap_abort, ext_trig_en, soft_trig;
    wire cap_ready, trig_cap_edge;
    wire [31:0] nsegm, cap_len, cap_fifo_wrusedw_max, trig_dly;
    
    //Control and status registers      
    pulse_processing_csr #( .NUM_CHAN(NUM_CHAN) )
    pulse_processing_csr_inst (
        .clk                (data_clk),
        .reset              (data_rst),

        //AVMM control interface clock
        .avmm_clk           (control_clk),
        .avmm_reset         (control_rst),
        
        //AVMM slave control interface
        .avmm_write         (csr_write),
        .avmm_read          (csr_read),
        .avmm_address       (csr_address),
        .avmm_datain        (csr_datain),
        .avmm_readdatavalid (csr_readdatavalid),
        .avmm_dataout       (csr_dataout),
        
        //Control signals
        .cap_start          (cap_start),        //Data capture start
        .soft_trig          (soft_trig),        //Soft trigger
        .cap_abort          (cap_abort),        //Data capture abort
        .ext_trig_en        (ext_trig_en),      //Enable external trigger
        .data_len           (cap_len),          //Data length to be captured
        .segment_number     (nsegm),            //Number of segments of data_len length to be captured     
        .trig_dly           (trig_dly),         //Delay from trigger to capture start in data_clk cycles
        .trig_cap_edge      (trig_cap_edge),    //Edge of data_clk at which external trigger is captured
        
        
        .threshold          (threshold),        //Threshold output for binary classifier
        .dot_prod_ave       (dot_prod_ave), //Average dot product per segments input
        
        //Data capture module status signals
        .cap_fifo_wrusedw_max  (cap_fifo_wrusedw_max), //Maximal data capture FIFO utilization
        .cap_busy              (cap_busy),     //Data capture in progress status
        .cap_ready             (cap_ready),    //Data capture module is ready for trigger 
         
        //Status signals from DMA for dot product saving
        .res_dma_busy          (dma_busy),
        .res_dma_ready         (dma_ready),
        .res_dma_fifo_lvl      (dma_fifo_lvl)
    );
    
    //DMA channel for capturing data into DDR3 memory
    data_capture #(
        .AVST_DATA_WIDTH (DATA_WIDTH), 
        .AVMM_DATA_WIDTH (DDR_DATA_WIDTH),
        .AVMM_ADDR_WIDTH (DDR_ADDR_WIDTH),
        .AVMM_BURST_WIDTH(DDR_BURSTC_WIDTH),    
        .FIFO_DEPTH (128)
		)
    data_capture_inst    
    (
        .avst_clk           (data_clk), //JESD link clock
        .avst_rst           (data_rst),

        .avmm_clk           (ddr_clk),	//DDR3 clock
        .avmm_rst           (ddr_rst),

        .avst_valid         (1),
        .avst_data          (avst_data),
        //DDR3 memory interface
        .avmm_waitrequest    (ddr_data_waitrequest),
        .avmm_write          (ddr_data_write),
        .avmm_writedata      (ddr_data_writedata),
        .avmm_address        (ddr_data_address),	
        .avmm_burstcount     (ddr_data_burstcount),
        .avmm_byteenable     (ddr_data_byteenable),
        
        //Control inputs
        .start              (cap_start),    //Data capture start trigger    
        .start_addr         ('h3D090),       //DDR3 memory start address for data in AVMM words
        .segm_start         (segm_start),   //Start of segment trigger, must be asserted 1 cycle before the first sample of a segment
        .abort              (cap_abort),    //Data capture abort signal
        .len                (cap_len),      //Data segment length in AVST words
        .nsegm              (nsegm),        //Numbr of data segments to capture
        
        //Control outputs
        .segm_stop          (segm_stop),    //End of segment trigger, coicides with the last sample of a segment
        .busy               (cap_busy),     //Data capture in progress status
        .ready              (cap_ready),    //Ready for segm_start trigger status
        .wrusedw_max        (cap_fifo_wrusedw_max), //Maximal data FIFO utilization level
	);
    
    wire [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] dot_prod_res;
    wire dot_prog_res_valid;
    
    //Qubit state descrimination module
    binary_classifier    
    #( 
        .DATA_WIDTH         (DATA_WIDTH       ),
        .SAMPLE_WIDTH       (SAMPLE_WIDTH     ),
        .MEM_DATA_WIDTH     (MEM_DATA_WIDTH   ),
        .FEATURE_MEM_DEPTH  (FEATURE_MEM_DEPTH),
        .DOT_PROD_WIDTH     (DOT_PROD_WIDTH   ),
        .NUM_CHAN           (NUM_CHAN         )
    )
    binary_classifier_inst
    (    
        //Control domain    
        .control_clk        (control_clk),
        .control_rst        (control_rst),
        //Data domain   
        .data_clk           (data_clk),
        .data_rst           (data_rst),
        //AVST Source: data stream.
        .avst_data          (avst_data),
        //AVMM Slave fixed wait state: memory
        .ram_address        (ram_address  ),
        .ram_read           (ram_read     ),
        .ram_readdatavalid  (ram_readdatavalid),
        .ram_readdata       (ram_readdata ),
        .ram_writedata      (ram_writedata),
        .ram_write          (ram_write    ),
        
        .threshold          (threshold),
        .dot_prod_ave       (dot_prod_ave),         //Average per segment (cummulative sum) 
        
        .start              (cap_start), 
        .segm_start         (segm_start),    
        .segm_stop          (segm_stop),
        
        //Dot product
        .dot_prod_res       (dot_prod_res      ),
        .dot_prog_res_valid (dot_prog_res_valid),
        //Classification result
        .result             (),
        .result_valid       ()
    );
	
	averaging 
	#(
		.MEM_DEPTH			(AVER_MEM_DEPTH),
	    .DATA_WIDTH			(DATA_WIDTH),
	    .SAMPLE_WIDTH		(SAMPLE_WIDTH),
	    .AVER_SAMPLE_WIDTH	(AVER_SAMPLE_WIDTH)
	)
	averaging_inst
	(
		.data_clk			(data_clk),
		.data_rst			(data_rst),
    
		//Data stream from ADC
		.data				(avst_data),
    
		//AVMM Slave for memory reading, ctrl_clk domain
		.avmm_address		(aver_address),
		.avmm_read			(aver_read),
		.avmm_readdata		(aver_readdata),
		.avmm_readdatavalid	(aver_readdatavalid),
    
		//Control inputs
		.start				(cap_start),     
		.abort				(cap_abort),     
		.segm_start			(segm_start),
		.segm_stop			(segm_stop)  
	);
	 
    //DMA channel for writing dot products from the binary_classifier module 
    //into DDR3 memory 
    avst_dcdma_wr 
    #(            
        .AVST_DATA_WIDTH    (DOT_PROD_WIDTH*NUM_CHAN), 
        .AVMM_ADDR_WIDTH    (DDR_ADDR_WIDTH), 
        .AVMM_DATA_WIDTH    (DDR_DATA_WIDTH),
        .AVMM_BURST_WIDTH   (DDR_BURSTC_WIDTH),//Burstcount port width
        .FIFO_DEPTH         (128)	                //In avst words
	)
    result_wr_dma
    (
        .avst_clk           (data_clk),
        .avst_rst           (data_rst | cap_abort),
        
        .avmm_clk           (ddr_clk),
        .avmm_rst           (ddr_rst),
        
        //AVST sink
        .avst_valid         (dot_prog_res_valid),
        .avst_data          (dot_prod_res),
        .avst_ready         (),
        
        //AVMM master
        .avmm_waitrequest    (ddr_waitrequest ),
        .avmm_write          (ddr_write       ),
        .avmm_writedata      (ddr_writedata   ),
        .avmm_address        (ddr_address     ),	
        .avmm_burstcount     (ddr_burstcount  ),
        .avmm_byteenable     (ddr_byteenable  ),
        
        //Control signals
        .start              (cap_start),        //Start trigger    
        .start_addr         (0),                //Start address in AVST words
        .len                (nsegm),            //Data length in AVST words
        .busy               (dma_busy),         //AVMM transfer is not done
        .ready              (dma_ready),        //Ready to start
        
        .wrusedw            (dma_fifo_lvl)      //FIFO write level
    );
    
    //Trigger capture module
     trigger_capture trig_cap_inst
    (
        .clk            (data_clk),
        .rst            (data_rst),
        .trig_i         (ext_trig),
        .soft_trig_i    (soft_trig),
        .trig_o         (segm_start),
        .cap_posedge    (trig_cap_edge),
        .delay          (trig_dly),
        .enable         (cap_ready),
        .ext_trig_en    (ext_trig_en)
    );
endmodule