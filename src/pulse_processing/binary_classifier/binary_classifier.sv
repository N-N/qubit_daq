//This module calculates dot product between incoming data stream and feature data preloaded into memory,
//it has NUM_CHAN separate channels with independent memories. Process starts with trigger 
//and continue until "len" DATA_WIDTH-bit words processed. The result is stored in memory. It also
//has "result" outputs which set high if dot product is higher then threshold stored in coontrol registers. 
//
//All the memory arranged to have contineous address space. 
//Feature memory occupy addresses from 0 to NUM_CHAN*FEATURE_MEM_DEPTH-1.

module binary_classifier 
#( 
    //Data stream
    parameter DATA_WIDTH = 256,     //Samples are packed in DATA_WIDTH bus
    parameter SAMPLE_WIDTH = 16,  
    //Memory
    parameter MEM_DATA_WIDTH = 32,
    parameter FEATURE_MEM_DEPTH = 8192,   //In MEM_DATA_WIDTH-bit words
    //Dot product
    parameter DOT_PROD_WIDTH = 64,
    //Channels   
    parameter NUM_CHAN = 4
)
(    
    //Control domain    
    input     control_clk,
    input     control_rst,
    //Data domain   
    input     data_clk,
    input     data_rst,
    //AVST Source: data stream.
    input   wire [DATA_WIDTH-1 :0]      avst_data,
    //AVMM Slave fixed wait state: memory
    input       [$clog2(MEM_DATA_WIDTH*FEATURE_MEM_DEPTH*NUM_CHAN/8)-1:0]   ram_address,
    input                               ram_read,
    output reg  [MEM_DATA_WIDTH-1:0]    ram_readdata,
    output reg                          ram_readdatavalid,
    input       [MEM_DATA_WIDTH-1:0]    ram_writedata,
    input                               ram_write,
    //CSR signals
    input       signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] threshold,
    output reg  signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] dot_prod_ave, //Average per segment (cummulative sum) 
    
    //Inputs
    input       start,      //Data capture start pulse
    input       segm_start, //Start of segment pulse
    input       segm_stop,  //End of segment pulse

    //Dot product output
    output reg  [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0]  dot_prod_res,
    output                                          dot_prog_res_valid,
    //Classification result
    output reg  [NUM_CHAN-1:0]  result,
    output                      result_valid
);
    //Memory depths from data domain
    localparam FEATURE_MEM_DEPTH_B = FEATURE_MEM_DEPTH*MEM_DATA_WIDTH/DATA_WIDTH;
    
    wire       [$clog2(FEATURE_MEM_DEPTH*NUM_CHAN)-1:0]    ram_address_w;
    assign ram_address_w = ram_address >> $clog2(MEM_DATA_WIDTH/8);
    
    //AVMM Slave: access to individual memory blocks
    reg [MEM_DATA_WIDTH-1:0] ram_readdata_decoded [0:2*NUM_CHAN-1];
    reg  ram_write_decoded [NUM_CHAN-1:0];

    //Feature data from memory
    wire    [DATA_WIDTH-1:0]  feature_data    [0:NUM_CHAN -1];
    
    //Feature memory address b
    reg    [$clog2(FEATURE_MEM_DEPTH_B)-1:0] feature_address_reg, n_feature_address_reg;
    
    //Control signals
    reg [$clog2(NUM_CHAN*2):0]  memsel, memsel_r;
    
    //Product and dot product stuff
    wire signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] dot_prod ;
    reg  signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] n_dot_prod_ave ;
    reg  signed [NUM_CHAN-1:0][DOT_PROD_WIDTH-1:0] n_dot_prod_res ;
    reg dot_prod_en;
    
    //STOP state delay
    reg [1:0] stop_dly, n_stop_dly;
    
    //Classification result
    reg [NUM_CHAN-1:0] n_result;
    reg result_ready_reg, n_result_ready_reg;
    assign result_valid = result_ready_reg;
    assign dot_prog_res_valid = result_ready_reg;
    
    //Main state machine
    enum int unsigned {IDLE = 0, CALC = 1, STOP = 2, SAVE = 3} state, n_state;
    
    //Memory read multiplexer
    assign ram_readdata = ram_readdata_decoded[memsel_r];
    always @(posedge control_clk) begin
        memsel_r <= memsel;
        if(ram_read)
            ram_readdatavalid <= 1;
        else
            ram_readdatavalid <= 0;
    end

    //Memory address decoder
    int j;
    always @(*) begin
        memsel = 0;
        for(j=0; j<NUM_CHAN; j++) begin
            if( (ram_address_w >= FEATURE_MEM_DEPTH*j)
                && (ram_address_w < FEATURE_MEM_DEPTH*(j+1)) ) begin
                memsel = j;
                break;
            end
        end    
    end

    genvar i;
    generate for (i=0; i< NUM_CHAN ; i=i+1) begin: gen_chan
                
                //Memory write multiplexer
                always @(*) begin
                    ram_write_decoded[i] = 0;
                    if (memsel == i)
                        ram_write_decoded[i] = ram_write;
                end
                //Feature memory
                feature_memory #( 
                    .DATA_WIDTH_B   (DATA_WIDTH), 
                    .NUMWORDS_B     (FEATURE_MEM_DEPTH_B)
                    )
                feature_memory_inst (
                    .address_a  (ram_address_w),
                    .address_b  (feature_address_reg),
                    .clock_a    (control_clk),
                    .clock_b    (data_clk),
                    .data_a     (ram_writedata),
                    .data_b     ( {DATA_WIDTH{1'b0}} ),
                    .wren_a     (ram_write_decoded[i]),
                    .wren_b     (1'b0),
                    .q_a        (ram_readdata_decoded[i]),
                    .q_b        (feature_data[i])
                );
                //Main state machine
                always @(*) begin
                    n_dot_prod_ave[i] = dot_prod_ave[i];
                    n_result[i] = result[i];
                    n_dot_prod_res[i] = dot_prod_res[i];
                    case(state)
                        IDLE: begin
                            if(start)
                                n_dot_prod_ave[i] = 0;
                            end
                        SAVE: begin
                            if (dot_prod[i] > threshold[i])
                                n_result[i] = 1;
                            else
                                n_result[i] = 0;
                            n_dot_prod_res[i] = dot_prod[i];    
                            n_dot_prod_ave[i] = dot_prod_ave[i] + dot_prod[i];
                            end
                    endcase    
                end
                
                always @(posedge data_clk or posedge data_rst) begin
                    if (data_rst) begin
                        dot_prod_ave[i] <= {DOT_PROD_WIDTH{1'b0}};
                        result[i] <= 0;
                    end else begin
                        dot_prod_ave[i] <= n_dot_prod_ave[i];
                        result[i] <= n_result[i];
                        dot_prod_res[i] <= n_dot_prod_res[i];
                    end
                end
                //Dot product calculator    
                dot #( 
                    .DATA_WIDTH(DATA_WIDTH), 
                    .SAMPLE_WIDTH(SAMPLE_WIDTH), 
                    .ACC_WIDTH(DOT_PROD_WIDTH)
                     )
                dot_inst (
                    .clk        (data_clk),
                    .rst        (data_rst|result_ready_reg),
                    .enable     (dot_prod_en),
                    .x          (feature_data[i]),
                    .y          (avst_data),
                    .dot_prod   (dot_prod[i])
                );
            end
    endgenerate
    
    //Main state machine
    always @(*) begin
        n_state = state;
        n_feature_address_reg = feature_address_reg;
        n_result_ready_reg = 0;
        dot_prod_en = 0;
        n_stop_dly = stop_dly>>1;
        
        case(state)
            IDLE: begin

                if(segm_start ) begin
                    n_state = CALC;
                    n_feature_address_reg = 1;
                end else begin
                    n_feature_address_reg = 0;
                end
                
            end
            CALC: begin
                dot_prod_en = 1;
                if(segm_stop) begin
                    n_stop_dly[1] = 1;
                    n_state = STOP;
                end else begin
                    n_feature_address_reg = feature_address_reg+1;
                end    
            end
            STOP: begin
                dot_prod_en = 1;
                //if(stop_dly[0])
                    n_state = SAVE;
            end
            SAVE: begin
                n_state = IDLE;          
                n_result_ready_reg = 1;
            end
        endcase    
    end
    
    always @(posedge data_clk or posedge data_rst) begin
        if (data_rst) begin
            state <= IDLE;
            feature_address_reg <=  0;
            result_ready_reg    <=  0;
        end else begin
            state <= n_state;
            feature_address_reg <= n_feature_address_reg;
            result_ready_reg <= n_result_ready_reg;
            stop_dly <= n_stop_dly;
        end
    end
        
endmodule        

//This module takes two DATA_WIDTH-bit words and calculats dot product between them, splitting them into SAMPLE_WIDTH-bit samples
//Uses Arria V GZ DSP blocks in 2-channel mode to multiply 2 samples and accumulate ACC_WIDTH-bit result,
//parallel adder then used to finally compute dot product.
module dot #(parameter DATA_WIDTH = 256, SAMPLE_WIDTH = 16, ACC_WIDTH = 64, NDSP = 2)
(
    input   wire    clk,
    input   wire    rst,
    input   wire    enable,
    input   wire    [DATA_WIDTH/SAMPLE_WIDTH-1:0][SAMPLE_WIDTH-1:0] x,
    input   wire    [DATA_WIDTH/SAMPLE_WIDTH-1:0][SAMPLE_WIDTH-1:0] y,
    output  wire signed  [ACC_WIDTH-1:0] dot_prod
);

    wire signed [DATA_WIDTH/SAMPLE_WIDTH/2-1:0][ACC_WIDTH-1:0] prod_acc;
    
    genvar i;
    generate for (i = 0; i < DATA_WIDTH/SAMPLE_WIDTH/NDSP; i=i+1) begin: gen_mult
        //Arria V GZ DSP blocks: 2 multipliers + accumulator
        mult_add mult_add_inst (
            .result     (prod_acc[i]),
            .dataa_0    (x[i*NDSP]),
            .dataa_1    (x[i*NDSP+1]),
            .datab_0    (y[i*NDSP]),
            .datab_1    (y[i*NDSP+1]),
            .clock0     (clk),
            .ena0       (enable),
            .aclr0      (rst)
        );
        
    end
    endgenerate
    
    //Parallel adder megafunction
    parallel_add    parallel_add_component (
                .data   (prod_acc),
                .result (dot_prod),
                // synopsys translate_off
                .aclr (),
                .clken (),
                .clock ()
                // synopsys translate_on
                );
    defparam
        parallel_add_component.msw_subtract = "NO",
        parallel_add_component.pipeline = 0,
        parallel_add_component.representation = "SIGNED",
        parallel_add_component.result_alignment = "LSB",
        parallel_add_component.shift = 0,
        parallel_add_component.size = DATA_WIDTH/SAMPLE_WIDTH/NDSP,
        parallel_add_component.width = ACC_WIDTH,
        parallel_add_component.widthr = ACC_WIDTH;  

endmodule 
