//Control and status registers for binary classifier module
//
//Control interface details:
//Avmm read: no wait cycles
//Avmm write: no wait cycles
//N_N
module binary_classifier_csr #(parameter NUM_CHAN = 4)(

    //AVMM control interface clock
	input avmm_clk,
	input avmm_rst,
	
	//AVMM slave control interface: used to read/write Registers
	input wire			avmm_write,
	input wire			avmm_read,
	input wire  [15:0]	avmm_address,
 	input wire  [31:0]	avmm_datain,
	output reg			avmm_readdatavalid,
	output reg  [31:0]	avmm_dataout,
	
	//Control signals
	output wire     [31:0] 	len,                      //Number of valid data words, stored in memory
	output signed	[NUM_CHAN-1:0][63:0]  threshold , //Binary classifier threshold
    
    //Status signals
    input  signed   [NUM_CHAN-1:0][63:0] dot_prod_ave, //Average dot product, must be registred outside
    input busy,
    
    input           dma_busy,
    input           dma_ready,
    input [31:0]    dma_fifo_lvl
	
    );

//Register map
localparam CTRL_REG_ADDR 	    = 16'd0;  //R	
localparam LEN_REG_ADDR 	    = 16'd4;  //R/W Length of data to be covariated
localparam THRESHOLD_REG_BASE   = 16'd8; //R/W Binary classifier threshold, contains NUM_CHAN*2 32 bit registers
localparam FIFO_LVL             = 16'd12; //R
localparam DOT_AVERAGE_BASE     = 16'd72;   //R Average dot product, contains NUM_CHAN*2 32 bit registers

//Control register bits
localparam DMA_READY = 	0; 	//R
localparam DMA_BUSY  =	1; 	//R

//Registers
reg [1:0] ctrl_reg, n_ctrl_reg;
reg [31:0] fifo_max_lvl, n_fifo_max_lvl;

reg [31:0] len_reg, n_len_reg;
reg [NUM_CHAN*2-1:0][31:0] threshold_reg, n_threshold_reg;

wire [NUM_CHAN*2-1:0][31:0] dot_prod_ave1;

assign dot_prod_ave1 = dot_prod_ave;

//Control outputs assignment
assign len = len_reg;

assign threshold = threshold_reg;

reg n_avmm_readdatavalid; 
reg [31:0] n_avmm_dataout;

integer j;

always @(*) begin
	n_avmm_readdatavalid = 1'b0;
	n_avmm_dataout = avmm_dataout;
	
	n_len_reg = len_reg; 
	n_threshold_reg = threshold_reg;
    
    n_ctrl_reg = ctrl_reg;
    n_ctrl_reg[DMA_READY] = dma_ready;
    n_ctrl_reg[DMA_BUSY] = dma_busy;
    
    n_fifo_max_lvl = fifo_max_lvl;
	
    //Read
	if(avmm_read) begin
        //Threshold
		if ((avmm_address >= THRESHOLD_REG_BASE) && (avmm_address < THRESHOLD_REG_BASE+NUM_CHAN*2*4)) begin
			n_avmm_dataout = threshold_reg[(avmm_address - THRESHOLD_REG_BASE)>>2];
			n_avmm_readdatavalid = 1'b1;
            end
        //Dot product average        
        else if ((avmm_address >= DOT_AVERAGE_BASE) && (avmm_address < DOT_AVERAGE_BASE+NUM_CHAN*2*4)) begin
			n_avmm_dataout = dot_prod_ave1[(avmm_address - DOT_AVERAGE_BASE)>>2];
			n_avmm_readdatavalid = 1'b1;
            end      
        else begin
			case(avmm_address)
                //Data length
				LEN_REG_ADDR: begin
					n_avmm_dataout = len_reg;
					n_avmm_readdatavalid = 1'b1;
				end
                CTRL_REG_ADDR: begin
                    n_avmm_dataout = ctrl_reg;
					n_avmm_readdatavalid = 1'b1;
                end
                FIFO_LVL: begin
                    n_avmm_dataout = fifo_max_lvl;
					n_avmm_readdatavalid = 1'b1;
                end    
			endcase
		end
	end
    //Write
	if(avmm_write & ~busy ) begin
        //Threshold
		if ((avmm_address >= THRESHOLD_REG_BASE) && (avmm_address < THRESHOLD_REG_BASE+NUM_CHAN*2*4)) begin
			n_threshold_reg[(avmm_address - THRESHOLD_REG_BASE)>>2] = avmm_datain;
            end
		else begin
			case(avmm_address)
                //Data length
                LEN_REG_ADDR: begin
                    n_len_reg = avmm_datain;
                    end
			endcase
		end
	end
    
    if(dma_fifo_lvl > fifo_max_lvl)
        n_fifo_max_lvl = dma_fifo_lvl;
end

always @(posedge avmm_clk or posedge avmm_rst) begin
	if(avmm_rst) begin		 
		len_reg	    <= 0;
		for (j=0; j<NUM_CHAN*2; j=j+1) begin 
			threshold_reg[j] <= 0;
		end
		avmm_readdatavalid  <= 1'b0;
        fifo_max_lvl        <= 0;
    end else begin
        len_reg 	        <= n_len_reg;	
        threshold_reg       <= n_threshold_reg;
		avmm_dataout        <= n_avmm_dataout;
		avmm_readdatavalid  <= n_avmm_readdatavalid;
        ctrl_reg            <= n_ctrl_reg;
        fifo_max_lvl        <= n_fifo_max_lvl;
    end
end

endmodule