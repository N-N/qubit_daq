module pulse_processing_dummy
#(
    //Data stream
    parameter DATA_WIDTH = 256,     //Samples are packed in DATA_WIDTH bus
    parameter SAMPLE_WIDTH = 16,  
    //Feature memory
    parameter MEM_DATA_WIDTH = 32,  
    parameter FEATURE_MEM_DEPTH = 8192,   //In MEM_DATA_WIDTH-bit words
    //Dot product
    parameter DOT_PROD_WIDTH = 64,
    //Channels   
    parameter NUM_CHAN = 4,
	//Averaging
	parameter AVER_SAMPLE_WIDTH = 32,
	parameter AVER_MEM_DEPTH = 8192*2, 	//In AVER_SAMPLE_WIDTH samples
    parameter AVER_MEM_DATA_WIDTH = 512,
    parameter AVER_MEM_ADDR_WIDTH = 16,
    //DDR3 memory 
    parameter DDR_DATA_WIDTH = 512,
    parameter DDR_ADDR_WIDTH = 26,
    parameter DDR_BURSTC_WIDTH = 4
)
(
    //Control domain    
    input                           control_clk,
    input                           control_rst,
    //Data domain   
    input                           data_clk,
    input                           data_rst,
    
    input                           ddr_clk,
    input                           ddr_rst,
    //AVST data stream
    input   [DATA_WIDTH-1 :0]       avst_data,
    //AVMM feature memory interface
    input   [16:0]                  ram_address,
    input                           ram_read,
    output                          ram_readdatavalid,
    output  [MEM_DATA_WIDTH-1:0]    ram_readdata,
    input   [MEM_DATA_WIDTH-1:0]    ram_writedata,
    input                           ram_write,
    
    //AVMM averaging memory interface (data_clk)
    input   [AVER_MEM_ADDR_WIDTH-1:0]   aver_address,
    input                               aver_read,
    output  [AVER_MEM_DATA_WIDTH-1:0]   aver_readdata,
	output                              aver_readdatavalid,
    
    //AVMM slave control interface
    input                           csr_write,
    input                           csr_read,
    input   [15:0]                  csr_address,
    input   [31:0]                  csr_datain,
    output                          csr_readdatavalid,
    output  [31:0]                  csr_dataout,
    
    //AVMM of DDR memory for results
	input                           ddr_waitrequest,
	output                          ddr_write      ,
	output  [DDR_DATA_WIDTH-1:0]    ddr_writedata  ,
	output  [DDR_ADDR_WIDTH-1:0]    ddr_address    ,	
	output  [DDR_BURSTC_WIDTH-1:0]  ddr_burstcount ,
    output  [DDR_DATA_WIDTH/8-1:0]  ddr_byteenable ,
    
     //AVMM of DDR memory for data
	input                           ddr_data_waitrequest,
	output                          ddr_data_write      ,
	output  [DDR_DATA_WIDTH-1:0]    ddr_data_writedata  ,
	output  [DDR_ADDR_WIDTH-1:0]    ddr_data_address    ,	
	output  [DDR_BURSTC_WIDTH-1:0]  ddr_data_burstcount ,
    output  [DDR_DATA_WIDTH/8-1:0]  ddr_data_byteenable ,
    
    input                           ext_trig
);

    pulse_processing_top
    #(
        .DATA_WIDTH         (DATA_WIDTH       ),    
        .SAMPLE_WIDTH       (SAMPLE_WIDTH     ),
        .MEM_DATA_WIDTH     (MEM_DATA_WIDTH   ),
        .FEATURE_MEM_DEPTH  (FEATURE_MEM_DEPTH),  
        .DOT_PROD_WIDTH     (DOT_PROD_WIDTH   ),
        .NUM_CHAN           (NUM_CHAN         ),
        .AVER_SAMPLE_WIDTH  (AVER_SAMPLE_WIDTH),
        .AVER_MEM_DEPTH     (AVER_MEM_DEPTH   ),
        .DDR_DATA_WIDTH     (DDR_DATA_WIDTH   ),
        .DDR_ADDR_WIDTH     (DDR_ADDR_WIDTH   ),
        .DDR_BURSTC_WIDTH   (DDR_BURSTC_WIDTH )
    )
    pulse_processing_top_inst
    (   
        .control_clk             (control_clk          ),
        .control_rst             (control_rst          ),
        .data_clk                (data_clk             ),
        .data_rst                (data_rst             ),
        .ddr_clk                 (ddr_clk              ),
        .ddr_rst                 (ddr_rst              ),
        .avst_data               (avst_data            ),
        .ram_address             (ram_address          ),
        .ram_read                (ram_read             ),
        .ram_readdatavalid       (ram_readdatavalid    ),
        .ram_readdata            (ram_readdata         ),
        .ram_writedata           (ram_writedata        ),
        .ram_write               (ram_write            ),
		.aver_address			 (aver_address		   ),
		.aver_read				 (aver_read			   ),
		.aver_readdata			 (aver_readdata		   ),
		.aver_readdatavalid		 (aver_readdatavalid   ),
        .csr_write               (csr_write            ),
        .csr_read                (csr_read             ),
        .csr_address             (csr_address          ),
        .csr_datain              (csr_datain           ),
        .csr_readdatavalid       (csr_readdatavalid    ),
        .csr_dataout             (csr_dataout          ),
        .ddr_waitrequest         (ddr_waitrequest      ),
        .ddr_write               (ddr_write            ),
        .ddr_writedata           (ddr_writedata        ),
        .ddr_address             (ddr_address          ),	
        .ddr_burstcount          (ddr_burstcount       ),
        .ddr_byteenable          (ddr_byteenable       ),
        .ddr_data_waitrequest    (ddr_data_waitrequest ),
        .ddr_data_write          (ddr_data_write       ),
        .ddr_data_writedata      (ddr_data_writedata   ),
        .ddr_data_address        (ddr_data_address     ),	
        .ddr_data_burstcount     (ddr_data_burstcount  ),
        .ddr_data_byteenable     (ddr_data_byteenable  ),
        .ext_trig                (ext_trig             )
    );

endmodule 