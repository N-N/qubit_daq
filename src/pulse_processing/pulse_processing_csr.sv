//Control and status registers for data_capture module
//Avmm read: no wait cycles
//Avmm write: no wait cycles
//N_N
module pulse_processing_csr #(parameter NUM_CHAN = 4)
(

	input clk,
	input reset,
	
	input avmm_clk,
	input avmm_reset,
	
	//AVMM
	input   			avmm_write,
	input   			avmm_read,
	input     [15:0]	avmm_address,
 	input     [31:0]	avmm_datain,
	output reg			avmm_readdatavalid,
	output reg  [31:0]	avmm_dataout,
	
	//Control signals
	output reg			cap_start,      //Data capture start
    output reg          soft_trig,      //Soft trigger
	output reg			cap_abort,      //Data capture abort
	output       		ext_trig_en,    //Enable external trigger
	output       [31:0] data_len,       //Data length to be captured
	output       [31:0]	segment_number, //Number of segments of data_len length to be captured     
    output reg   [31:0] trig_dly,       //Delay from external trigger to capture start in clk cycles
    output              trig_cap_edge,
    
    output signed	    [NUM_CHAN-1:0][63:0]    threshold ,     //Binary classifier threshold
    input  signed       [NUM_CHAN-1:0][63:0]    dot_prod_ave,   //Average dot product, must be registred outside

	//Data capture DMA status
	input [6:0] 	cap_fifo_wrusedw_max, //Maximal data capture FIFO utilization
	input 		    cap_busy,             //Data capture is progress status
    input           cap_ready,            //Data capture module is ready for trigger
    
    //Dot product saving DMA state
    input           res_dma_busy,
    input           res_dma_ready,
    input   [31:0]  res_dma_fifo_lvl
    
	);

//Register map	
localparam CTRL_REG_ADDR = 0; //Control register

//Control register bits
localparam CAP_START 	 =   0; //R/W	Start data capture. Self cleaning.
localparam CAP_BUSY	     =   1; //R		Data capture in progress. All control bits except ABORT are locked when asserted.
localparam ABORT 	     =   2;	//R/W	Abort data capture. Self cleaning.
localparam EX_TRIG_EN    =   3;	//R/W 	Enable external trigger.
localparam RES_DMA_BUSY  =   4;
localparam RES_DMA_READY =   5;
localparam TRIG_CAP_EDGE =   6;
localparam SOFT_TRIG     =   7;
localparam READY         =   8; 

localparam LEN_REG_ADDR          = 4;	//R/W	Capture data lenght in words. Locked when CAP_BUSY.
localparam FIFO_REG_ADDR         = 8;	//R		Peak FIFO usage in words.
localparam SEGM_NUM_ADDR         = 12;  //Segment mode register 
localparam RES_DMA_FIFO_REG_ADDR = 16;
localparam TRIG_DLY_REG_ADDR     = 20;
localparam THRESHOLD_REG_BASE    = 24;  //R/W Binary classifier threshold, contains NUM_CHAN*2 32 bit registers
localparam DOT_AVERAGE_BASE      = 68;  //R Average dot product, contains NUM_CHAN*2 32 bit registers

//Controls register bit access. 0 is read only
localparam CTRL_WR_MASK = 1<<CAP_START | 1<<EX_TRIG_EN | 1<<ABORT | 1<<SOFT_TRIG;
localparam CTRL_BUSY_WR_MASK = 1<<ABORT | 1<<SOFT_TRIG;

reg [31:0] ctrl_reg, n_ctrl_reg;//Control register
reg [31:0] len_reg, n_len_reg;	//Data lenght in words R/W
reg [31:0] segm_num, n_segm_num; //Segment number R/W
reg [31:0] n_feature_len;
reg signed [NUM_CHAN*2-1:0][31:0] n_threshold_r, threshold_r;
reg [31:0] res_dma_fifo_lvl_max, n_res_dma_fifo_lvl_max;
reg [31:0] n_trig_dly;

reg n_avmm_readdatavalid; 
reg [31:0] n_avmm_dataout;

wire signed [NUM_CHAN*2-1:0][31:0] dot_prod_ave_w;

wire feedback_avmm_clk;

//clk domain
wire cap_start_clk, cap_abort_clk;
reg feedback, n_feedback;
reg cap_start_clk_r, cap_abort_clk_r, soft_trig_clk_r;
reg n_cap_start, n_cap_abort, n_soft_trig;

assign threshold = threshold_r;
assign dot_prod_ave_w = dot_prod_ave;

assign data_len 	= 	len_reg;
//Segmented mode is enabled only with external trigger
assign segment_number	= segm_num;

assign ext_trig_en	= 	ctrl_reg[EX_TRIG_EN];
assign trig_cap_edge =  ctrl_reg[TRIG_CAP_EDGE];	

always @(*) begin
	n_avmm_readdatavalid = 0;
	n_avmm_dataout = avmm_dataout;
	
	n_ctrl_reg = ctrl_reg;
    n_ctrl_reg[CAP_BUSY]        = cap_busy;
    n_ctrl_reg[RES_DMA_READY]   = res_dma_ready;
    n_ctrl_reg[RES_DMA_BUSY]    = res_dma_busy;
    n_ctrl_reg[READY]           = cap_ready;
    
	n_len_reg = len_reg;
	n_segm_num = segm_num;
    
    n_threshold_r = threshold_r;
    
    n_res_dma_fifo_lvl_max = res_dma_fifo_lvl_max;
    
    n_trig_dly = trig_dly;
	
    //Read
	if(avmm_read) begin
		if(avmm_address == CTRL_REG_ADDR) begin
		  		n_avmm_dataout = ctrl_reg;
				n_avmm_readdatavalid = 1;
		end else if(avmm_address == LEN_REG_ADDR) begin
		  		n_avmm_dataout = len_reg;
				n_avmm_readdatavalid = 1;
		end else if(avmm_address == FIFO_REG_ADDR) begin
		  		n_avmm_dataout = cap_fifo_wrusedw_max;
				n_avmm_readdatavalid = 1;
		end else if(avmm_address == SEGM_NUM_ADDR) begin
		  		n_avmm_dataout = segm_num;
				n_avmm_readdatavalid = 1;
		end else if(avmm_address == RES_DMA_FIFO_REG_ADDR) begin
		  		n_avmm_dataout = res_dma_fifo_lvl_max;
				n_avmm_readdatavalid = 1;
        end else if(avmm_address == TRIG_DLY_REG_ADDR) begin
		  		n_avmm_dataout = trig_dly;
				n_avmm_readdatavalid = 1;		            
        //Threshold        
        end else if ((avmm_address >= THRESHOLD_REG_BASE) && (avmm_address < THRESHOLD_REG_BASE+NUM_CHAN*2*4)) begin
			n_avmm_dataout = threshold_r[(avmm_address - THRESHOLD_REG_BASE)>>2];
			n_avmm_readdatavalid = 1;
        //Average dot product    
        end else if ((avmm_address >= DOT_AVERAGE_BASE) && (avmm_address < DOT_AVERAGE_BASE+NUM_CHAN*2*4)) begin
			n_avmm_dataout = dot_prod_ave_w[(avmm_address - DOT_AVERAGE_BASE)>>2];
			n_avmm_readdatavalid = 1;
            end      
	end
    //Write
	if(avmm_write) begin
		if(avmm_address == CTRL_REG_ADDR) begin
			if(cap_busy)
				n_ctrl_reg = ctrl_reg | (avmm_datain & CTRL_BUSY_WR_MASK);
			else	
				n_ctrl_reg = avmm_datain & CTRL_WR_MASK;
		end else if( (!cap_busy) && (avmm_address == LEN_REG_ADDR) ) begin
            n_len_reg = avmm_datain;
		end else if( (!cap_busy) && (avmm_address == SEGM_NUM_ADDR)) begin
			n_segm_num = avmm_datain;
        end else if( (!cap_busy) && (avmm_address == TRIG_DLY_REG_ADDR)) begin
			n_trig_dly = avmm_datain;    
        end else if ( (!cap_busy) && (avmm_address >= THRESHOLD_REG_BASE) && (avmm_address < THRESHOLD_REG_BASE+NUM_CHAN*2*4)) begin
			n_threshold_r[(avmm_address - THRESHOLD_REG_BASE)>>2] = avmm_datain;
        end      
	end
	
    //Pulse generation
	if(feedback_avmm_clk)
		n_ctrl_reg = ctrl_reg & ~( (1<<CAP_START) | (1<<ABORT) | (1<<SOFT_TRIG) );
        
    if(res_dma_fifo_lvl > res_dma_fifo_lvl_max)
        n_res_dma_fifo_lvl_max = res_dma_fifo_lvl;    
end

always @(posedge avmm_clk or posedge avmm_reset) begin
	if(avmm_reset) begin		 
		ctrl_reg	<= 0;
		len_reg		<= 0;
		segm_num	<= 0;
		res_dma_fifo_lvl_max <= 0;
        trig_dly    <= 0;
        
		avmm_readdatavalid  <= 0;
	end else begin
		ctrl_reg 	<= n_ctrl_reg;
		len_reg 	<= n_len_reg;	
		segm_num	<= n_segm_num;
        threshold_r   <= n_threshold_r;
        res_dma_fifo_lvl_max <= n_res_dma_fifo_lvl_max;
        trig_dly <= n_trig_dly;
		
		avmm_dataout        <= n_avmm_dataout;
		avmm_readdatavalid  <= n_avmm_readdatavalid;
	end
end

data_sync start_sync(
	.data		(ctrl_reg[CAP_START]),
	.dest_clk	(clk),
	.dest_rstn	(~reset),
	.data_sync	(cap_start_clk)
);	

data_sync abort_sync(
	.data		(ctrl_reg[ABORT]),
	.dest_clk	(clk),
	.dest_rstn	(~reset),
	.data_sync	(cap_abort_clk)
);

data_sync soft_trig_sync(
	.data		(ctrl_reg[SOFT_TRIG]),
	.dest_clk	(clk),
	.dest_rstn	(~reset),
	.data_sync	(soft_trig_clk)
);

//Feedback
data_sync fb_sync(
	.data		(feedback),
	.dest_clk	(avmm_clk),
	.dest_rstn	(~avmm_reset),
	.data_sync	(feedback_avmm_clk)
);	

always @(*) begin
	n_cap_start = 0;
	n_cap_abort = 0;
    n_soft_trig = 0;
	n_feedback = feedback;

	//Positive edge detection
	if( ~cap_start_clk_r & cap_start_clk )
		n_cap_start = 1;
	if( ~cap_abort_clk_r & cap_abort_clk )
		n_cap_abort = 1;
    if( ~soft_trig_clk_r & soft_trig_clk )
		n_soft_trig = 1;
	//Feedback
	if( (cap_start_clk & cap_start_clk_r) | (cap_abort_clk_r & cap_abort_clk) | (soft_trig_clk_r & soft_trig_clk) )
		n_feedback = 1;
	//Negative edge detection. Feedback deassertion.
	if( (cap_start_clk_r & ~cap_start_clk) | (cap_abort_clk_r & ~cap_abort_clk) | (soft_trig_clk_r & ~soft_trig_clk) )
		n_feedback = 0;
end		

always @(posedge clk or posedge reset)
begin
	if(reset) begin
		cap_start_clk_r <= 0;
		cap_abort_clk_r <= 0;
		
		cap_start		<= 0;
		cap_abort 		<= 0;
        soft_trig       <=0;
		feedback		<= 0;
	end else begin
		cap_start		<= n_cap_start;
		cap_abort 		<= n_cap_abort;
        soft_trig       <= n_soft_trig;
		feedback 		<= n_feedback;
		
        soft_trig_clk_r <= soft_trig_clk;
		cap_start_clk_r <= cap_start_clk;
		cap_abort_clk_r <= cap_abort_clk;
	end
end

endmodule