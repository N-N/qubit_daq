module averaging 
#(
     parameter MEM_DEPTH = 8192*2, //Samples
     parameter DATA_WIDTH  = 256,
     parameter SAMPLE_WIDTH = 16,
     parameter AVER_SAMPLE_WIDTH = 32
)
(
    input data_clk,
    input data_rst,
    
    //Data stream from ADC
    input [DATA_WIDTH-1:0] data,
    
    //AVMM Slave for memory reading, ctrl_clk domain
    input  [$clog2(MEM_NUMWORDS*MEM_DATA_WIDTH/8)-1:0]      avmm_address,
    input                       avmm_read,
    output [MEM_DATA_WIDTH-1:0] avmm_readdata,
    output                      avmm_readdatavalid,
    
    //Control inputs
    input start,        //Data capture start pulse
    input abort,        //Abort data capture
    input segm_start,   //Segment capture start trigger
    input segm_stop     //Segment capture stop pulse
);

    localparam SAMP_PER_CLK = DATA_WIDTH/SAMPLE_WIDTH;
    localparam MEM_DATA_WIDTH = SAMP_PER_CLK * AVER_SAMPLE_WIDTH;
    localparam MEM_NUMWORDS = MEM_DEPTH/SAMP_PER_CLK;
    localparam DATA_DLY = 1;
    localparam AVMM_VALID_DLY = 2;    

    reg signed [DATA_DLY-1:0][SAMP_PER_CLK-1:0][SAMPLE_WIDTH-1:0] data_dly;
    reg [$clog2(MEM_NUMWORDS)-1:0] address_a, address_b, n_address_a, n_address_b, address_a_wire;
    reg signed [SAMP_PER_CLK-1:0][AVER_SAMPLE_WIDTH-1:0] data_b_in, data_a_out;
    reg wren_b_dly, n_wren_b_dly;
    reg [1:0] start_r, n_start_r;
    reg [AVMM_VALID_DLY-1:0] avmm_readdatavalid_dly, n_avmm_readdatavalid_dly;

    assign data_split = data;
    assign avmm_readdata = data_a_out;
    assign avmm_readdatavalid = avmm_readdatavalid_dly[0];
    
    //The input data must be delayed in order to be added to the data from memory because
    //memory has address to data delay
    always@(posedge data_clk) begin
        //data_dly <= {data, data_dly[DATA_DLY-1:1]};
        data_dly <= data;
    end
    
    enum int unsigned {IDLE = 0, CALC} state, n_state;
    
    always@(*) begin
        n_state = state;
        n_address_a = address_a;
        n_address_b = address_b;
        n_start_r = start_r;
		n_wren_b_dly = 0;
        address_a_wire = address_a;
        n_avmm_readdatavalid_dly =  avmm_readdatavalid_dly>>1;
        
        case(state)
            IDLE: begin
                //Start accumulation on "trig"
                if( segm_start ) begin
                    n_state = CALC;
                    n_address_a = 1;
                    n_address_b = 0;
                    n_start_r = start_r>>1;
                end else begin
                    n_address_a = 0;
                end
                //Reset the accumulator on "start"
                if(start) begin
                    n_start_r = 'b11;
                end
                //AVMM access
                if( avmm_read ) begin
                    address_a_wire = avmm_address>>$clog2(MEM_DATA_WIDTH/8);
                    n_avmm_readdatavalid_dly[AVMM_VALID_DLY-1] = 1;
                end    
            end
            CALC: begin
                n_wren_b_dly = 1;
                if(wren_b_dly)
                    n_address_b = address_b+1;
                    
                if(segm_stop) begin
                    n_state = IDLE;
                end else begin
                    n_address_a = address_a+1;
                end    
            end
        endcase   
    end
    
    genvar i;
    generate for(i=0; i<SAMP_PER_CLK; i++) begin: accumulate
        always@(*) begin
            if(start_r[0])
                //If there was a "start" pulse then reset the accumulator
                //on the first "trig" pulse after start
                data_b_in[i] = { {SAMPLE_WIDTH{data_dly[0][i][SAMPLE_WIDTH-1]}} , data_dly[0][i] };
            else
                //On the next "trig" pulses accumulate samples
                data_b_in[i] = { {SAMPLE_WIDTH{data_dly[0][i][SAMPLE_WIDTH-1]}} , data_dly[0][i] } + data_a_out[i];
        end
    end endgenerate

    aver_mem 
    #(.DATA_WIDTH(MEM_DATA_WIDTH), .NUMWORDS(MEM_DEPTH/SAMP_PER_CLK))
    aver_mem_inst
    (
        .aclr        (0),
        .address_a   (address_a_wire),
        .address_b   (address_b),
        .clock       (data_clk),
        .data_a      (),
        .data_b      (data_b_in),
        .wren_a      (0),
        .wren_b      (wren_b_dly),
        .q_a         (data_a_out),
        .q_b         ()
    );
    
    always@(posedge data_clk or posedge data_rst) begin
        if(data_rst) begin
            state <= IDLE;
        end else begin
            state <= n_state;
            address_a <= n_address_a;
            address_b <= n_address_b;
            wren_b_dly <= n_wren_b_dly;
            start_r <= n_start_r;
            avmm_readdatavalid_dly <= n_avmm_readdatavalid_dly;
        end
    end
    
endmodule