//A dual port single clk RAM for the averaging core

// synopsys translate_off
`timescale 1 ps / 1 ps
// synopsys translate_on
module aver_mem #(parameter DATA_WIDTH = 256, NUMWORDS = 512)
(
	aclr,
	address_a,
	address_b,
	clock,
	data_a,
	data_b,
	wren_a,
	wren_b,
	q_a,
	q_b);

	input	  aclr;
	input	[$clog2(NUMWORDS)-1:0]  address_a;
	input	[$clog2(NUMWORDS)-1:0]  address_b;
	input	  clock;
	input	[DATA_WIDTH-1:0]  data_a;
	input	[DATA_WIDTH-1:0]  data_b;
	input	  wren_a;
	input	  wren_b;
	output	[DATA_WIDTH-1:0]  q_a;
	output	[DATA_WIDTH-1:0]  q_b;

	tri0	  aclr;
	tri1	  clock;
	tri0	  wren_a;
	tri0	  wren_b;

	wire [DATA_WIDTH-1:0] sub_wire0;
	wire [DATA_WIDTH-1:0] sub_wire1;
	wire [DATA_WIDTH-1:0] q_a = sub_wire0[DATA_WIDTH-1:0];
	wire [DATA_WIDTH-1:0] q_b = sub_wire1[DATA_WIDTH-1:0];

	altsyncram	altsyncram_component (
				.aclr0 (aclr),
				.address_a (address_a),
				.address_b (address_b),
				.clock0 (clock),
				.data_a (data_a),
				.data_b (data_b),
				.wren_a (wren_a),
				.wren_b (wren_b),
				.q_a (sub_wire0),
				.q_b (sub_wire1),
				.aclr1 (1'b0),
				.addressstall_a (1'b0),
				.addressstall_b (1'b0),
				.byteena_a (1'b1),
				.byteena_b (1'b1),
				.clock1 (1'b1),
				.clocken0 (1'b1),
				.clocken1 (1'b1),
				.clocken2 (1'b1),
				.clocken3 (1'b1),
				.eccstatus (),
				.rden_a (1'b1),
				.rden_b (1'b1));
	defparam
		altsyncram_component.address_reg_b = "CLOCK0",
		altsyncram_component.clock_enable_input_a = "BYPASS",
		altsyncram_component.clock_enable_input_b = "BYPASS",
		altsyncram_component.clock_enable_output_a = "BYPASS",
		altsyncram_component.clock_enable_output_b = "BYPASS",
		altsyncram_component.indata_reg_b = "CLOCK0",
		altsyncram_component.intended_device_family = "Arria V GZ",
		altsyncram_component.lpm_type = "altsyncram",
		altsyncram_component.numwords_a = NUMWORDS,
		altsyncram_component.numwords_b = NUMWORDS,
		altsyncram_component.operation_mode = "BIDIR_DUAL_PORT",
		altsyncram_component.outdata_aclr_a = "CLEAR0",
		altsyncram_component.outdata_aclr_b = "CLEAR0",
		altsyncram_component.outdata_reg_a = "CLOCK0",
		altsyncram_component.outdata_reg_b = "CLOCK0",
		altsyncram_component.power_up_uninitialized = "FALSE",
		altsyncram_component.read_during_write_mode_mixed_ports = "DONT_CARE",
		altsyncram_component.read_during_write_mode_port_a = "NEW_DATA_NO_NBE_READ",
		altsyncram_component.read_during_write_mode_port_b = "NEW_DATA_NO_NBE_READ",
		altsyncram_component.widthad_a = $clog2(NUMWORDS),
		altsyncram_component.widthad_b = $clog2(NUMWORDS),
		altsyncram_component.width_a = DATA_WIDTH,
		altsyncram_component.width_b = DATA_WIDTH,
		altsyncram_component.width_byteena_a = 1,
		altsyncram_component.width_byteena_b = 1,
		altsyncram_component.wrcontrol_wraddress_reg_b = "CLOCK0";


endmodule