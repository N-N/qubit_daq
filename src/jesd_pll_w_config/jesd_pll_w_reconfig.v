//N_N
//PLL with reconfiguration controller for jesd204B

module jesd_pll_w_reconfig
(
	input wire clk,
	input wire rst,
	
	input wire reconfig_clk,
	input wire reconfig_rst,
	
	output wire 		mgmt_waitrequest,
	output wire 		mgmt_readdatavalid,
	input wire 			mgmt_read,         
	input wire 			mgmt_write,       
	output wire [31:0] 	mgmt_readdata,     
	input wire 	[7:0]	mgmt_address,      
	input wire 	[31:0] 	mgmt_writedata,
	
	output wire jesd_link_clk0,//same freq
	output wire jesd_link_clk1,//same freq
	output wire trig_clk, //90 deg phase shift, same freq
	
	//Deasserts when pll locked
	output wire reset_out,
	
	//Exported signals
	output wire jesd_link_clk0_exp,
	output wire jesd_link_clk1_exp
);

wire [63:0] reconfig_to_pll, reconfig_from_pll;
wire		pll_locked;

assign jesd_link_clk0_exp = jesd_link_clk0;
assign jesd_link_clk1_exp = jesd_link_clk1;

jesd_avgz_pll jesd_pll_inst(
		.refclk				(clk),            
		.rst				(rst),               
		.outclk_0			(jesd_link_clk0),  
		.outclk_1			(jesd_link_clk1),
		.outclk_2			(trig_clk),	
		.locked				(pll_locked),            
		.reconfig_to_pll 	(reconfig_to_pll),   
		.reconfig_from_pll 	(reconfig_from_pll)  
	);
	
pll_reconfig  jesd_pll_reconfig(
		.mgmt_clk 			(reconfig_clk),         
		.mgmt_reset			(reconfig_rst), 
		
		.mgmt_waitrequest	(mgmt_waitrequest), 
		.mgmt_read         	(mgmt_read),
		.mgmt_write       	(mgmt_write),
		.mgmt_readdata     	(mgmt_readdata),
		.mgmt_address      	(mgmt_address[7:2]),//2 LSB of avmm address are not used due to 32-bit data size 
		.mgmt_writedata   	(mgmt_writedata),
		
		.reconfig_to_pll 	(reconfig_to_pll),   
		.reconfig_from_pll	(reconfig_from_pll) 
	);
	
avmm_valid_from_waitrequest jesd_pll_reconfig_valid_gen(
		.clk		(reconfig_clk),
		.rst		(reconfig_rst),
		.waitrequest(mgmt_waitrequest),
		.valid		(mgmt_readdatavalid)
	);	
	
reset_sync #(.SYNC_RST_POLARITY("ACTIVE_HIGH"))
	reset_sync_1 (
		.clk 			(jesd_link_clk0),
		.rstn 			(pll_locked),
		.clk_rst_sync	(reset_out)
	);	
	
endmodule	