module reset_sync #(parameter SYNC_RST_POLARITY = "ACTIVE_LOW") (

	input        wire            clk,
	input        wire            rstn,

	output       reg             clk_rst_sync
);


reg clk_rst_sync_1;
reg clk_rst_sync_2;
reg clk_rst_sync_3;


always @(posedge clk or negedge rstn)
begin
	if(!rstn)
	begin
		if(SYNC_RST_POLARITY == "ACTIVE_HIGH")
		begin
       		clk_rst_sync   <= 1'b1;
       		clk_rst_sync_1 <= 1'b1; //Active high 
       		clk_rst_sync_2 <= 1'b1; //Active high 
       		clk_rst_sync_3 <= 1'b1; //Active high 
		end
		else
		begin
       		clk_rst_sync   <= 1'b0;
       		clk_rst_sync_1 <= 1'b0; //Active low 
       		clk_rst_sync_2 <= 1'b0; //Active low 
       		clk_rst_sync_3 <= 1'b0; //Active low 
		end

	end
	else
	begin
		if(SYNC_RST_POLARITY == "ACTIVE_HIGH")
		begin
       		clk_rst_sync_1 <= 1'b0;
       		clk_rst_sync_2 <= clk_rst_sync_1;
       		clk_rst_sync_3 <= clk_rst_sync_2;
       		clk_rst_sync   <= clk_rst_sync_3;
		end
		else
		begin
       		clk_rst_sync_1 <= 1'b1;
       		clk_rst_sync_2 <= clk_rst_sync_1;
       		clk_rst_sync_3 <= clk_rst_sync_2;
       		clk_rst_sync   <= clk_rst_sync_3;
		end

	end
end
endmodule
