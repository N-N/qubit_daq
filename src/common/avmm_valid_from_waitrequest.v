//A simlple valid generator for Avalom mm

module avmm_valid_from_waitrequest(
input wire clk,
input wire rst,
input wire waitrequest,
output reg valid
);

reg valid_d;

always @(posedge clk)
begin
	if(rst)
	begin
		valid <= 1'b0;
		valid_d <= 1'b0;
	end
	else if (waitrequest)
	begin
		valid_d <=1'b1;
		valid <= 1'b0;
		end
	else if(valid_d & !waitrequest)
	begin
		valid <=1'b1;
		valid_d <= 1'b0;
	end	
	else
	begin
		valid <= 1'b0;
		valid_d <= 1'b0;
	end
end

endmodule