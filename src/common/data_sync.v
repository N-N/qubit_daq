//This module assumes the input data is the a registered output

module data_sync(

	input    wire    data,

	input    wire    dest_clk,
	input    wire    dest_rstn,
	output   reg     data_sync
);

reg data_d1;

always @(posedge dest_clk or negedge dest_rstn)
begin
	if(!dest_rstn)
	begin
		data_d1 <= 1'b0;
		data_sync <= 1'b0;
	end
	else
	begin
		data_d1 <= data;
		data_sync <= data_d1;
	end
end

endmodule
