//N_N

//These are real parameters for LMFS 8224 mode of the ADC
//L = 8		Number of lanes
//M = 2 	Number of converters
//F = 2 	Octects per frame per lane
//K = 16 	Frames per multiframe
//S = 4 	Samples per converter per frame
//N = 14 	Converter resolution
//N' = 16 	Tranmitted bits per sample

module jesd204_rx_top_qsys #(parameter L = 8, F = 2, K = 16, FRAMES_PER_CLK = 2)
//FRAMES_PER_CLK - Number of frames received per link clock cycle
(
	//Clock and reset signals
	input wire	link_clk, 		//Link clock for FPGA fabric	
	input wire	cdr_ref_clk, 	//Reference clock for PHY hardware modules. 
	//They shold be of the same freq, but keep them separate and use double channe pll 
	//Fitter won't route them instead.
	
	input wire	link_rstn, 			//synchroneous reset
	
	input wire	reconfig_clk,
	input wire 	reconfig_rst,
	
	//AV-ST source data output 
	output    wire [L*FRAMES_PER_CLK*F*8-1:0]	rx_data,
	output    wire								rx_data_valid,
	
	//jesd204B signals                                        
    output	wire			sync_n,
	input	wire [L-1:0]	rx_serial_data,
	input	wire			sysref,
	                                                         
    //JESD204B link layer control interface                       
    input    wire[7:0]		link_csr_address,
	input    wire			link_csr_read,
	input    wire			link_csr_write,
	input    wire[31:0]		link_csr_writedata,
	output   wire[31:0]		link_csr_readdata,
	output   wire			link_csr_readdatavalid,
	output	wire			link_csr_waitrequest,
                                                             
    //jesd204B phy reconfiguration interface                         
    input wire [8:0]		phy_mgmt_address,   
    input wire				phy_mgmt_write,     
    input wire [31:0]		phy_mgmt_writedata, 
    input wire				phy_mgmt_read,      
    output wire [31:0]		phy_mgmt_readdata,  
    output wire				phy_mgmt_waitrequest,
	output wire				phy_mgmt_readdatavalid,
	
	//jesd204B phy reconfiguration controller RAM interface
	output wire [31:0]  	phy_mif_address,      
	output wire         	phy_mif_read,         
	input  wire [15:0]  	phy_mif_readdata,     
	input  wire         	phy_mif_waitrequest,
	
	output 	reg 	rxled //link data valid
);

jesd204_rx_top  
	#( .L(L), .F(F), .K(K), .FRAMES_PER_CLK(FRAMES_PER_CLK) )
	jesd204_rx_top_inst
	(
	//Clock and reset signals
	.link_clk		(link_clk	), 			
	.cdr_ref_clk	(cdr_ref_clk), 	 
	.link_rstn		(link_rstn	), 
	.reconfig_clk	(reconfig_clk),
	.reconfig_rst	(reconfig_rst),
	
	//AV-ST source data output 
	.rx_data		(rx_data),
	.rx_data_valid	(rx_data_valid),
	
	//jesd204B signals                                        
    .sync_n			(sync_n			),
	.rx_serial_data	(rx_serial_data	),
	.sysref			(sysref			),
	                                                         
    //JESD204B link layer control interface                       
    .link_csr_address		(link_csr_address		),
	.link_csr_read			(link_csr_read			),
	.link_csr_write			(link_csr_write			),
	.link_csr_writedata		(link_csr_writedata		),
	.link_csr_readdata		(link_csr_readdata		),
	.link_csr_readdatavalid	(link_csr_readdatavalid),
	.link_csr_waitrequest	(link_csr_waitrequest),
                                                             
    //jesd204B phy reconfiguration interface                         
    .phy_mgmt_address		(phy_mgmt_address		),   
    .phy_mgmt_write			(phy_mgmt_write			),     
    .phy_mgmt_writedata		(phy_mgmt_writedata		), 
    .phy_mgmt_read			(phy_mgmt_read			),      
    .phy_mgmt_readdata		(phy_mgmt_readdata		),  
    .phy_mgmt_waitrequest	(phy_mgmt_waitrequest	),
	.phy_mgmt_readdatavalid	(phy_mgmt_readdatavalid),
	
	//jesd204B phy reconfiguration controller RAM interface
	.phy_mif_address		(phy_mif_address	),      
	.phy_mif_read			(phy_mif_read		),         
	.phy_mif_readdata		(phy_mif_readdata	),     
	.phy_mif_waitrequest	(phy_mif_waitrequest),
	
	.rxled	(rxled) //link data valid
	);	
	
endmodule	