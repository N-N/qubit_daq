module transport_tb ();

parameter L = 8, M= 2, F = 2, FRAMES_PER_CLK=2, N = 14, N_PRIME = 16;

wire [L*FRAMES_PER_CLK*F/2-1:0][15:0] data_in;

wire [L*FRAMES_PER_CLK*F/2-1:0][15:0] data_out;

assign data_in = 	{16'd256, 16'd257, 
					16'd258, 16'd259,
					16'd260, 16'd261, 
					16'd262, 16'd263,

					16'd264, 16'd265,
					16'd266, 16'd267, 
					16'd268, 16'd269,
					16'd270, 16'd271};

	jesd204_transport dut
(
	.data_in	(data_in),
	.data_out	(data_out)
);

endmodule