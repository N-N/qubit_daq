vlib	work
vmap       work     work


set QSYS_SIMDIR ./

vlog $QSYS_SIMDIR/jesd204_transport.sv
vlog $QSYS_SIMDIR/transport_tb.sv
# #
# # Set the top-level simulation or testbench module/entity name, which is
# # used by the elab command to elaborate the top level.
# #
set TOP_LEVEL_NAME transport_tb.sv
# #
# # Set any elaboration options you require.
# set USER_DEFINED_ELAB_OPTIONS <elaboration options>
# #
# # Call command to elaborate your design and testbench.
#elab
# #
# # Run the simulation.
# run -a
# #
# # Report success to the shell.
# exit -code 0
# #
# # TOP-LEVEL TEMPLATE - END