/* ================================================================================
-- (c) 2009 Altera Corporation. All rights reserved.
-- Altera products are protected under numerous U.S. and foreign patents, maskwork
-- rights, copyrights and other intellectual property laws.
-- 
-- This reference design file, and your use thereof, is subject to and governed
-- by the terms and conditions of the applicable Altera Reference Design License
-- Agreement (either as signed by you, agreed by you upon download or as a
-- "click-through" agreement upon installation andor found at www.altera.com).
-- By using this reference design file, you indicate your acceptance of such terms
-- and conditions between you and Altera Corporation.  In the event that you do
-- not agree with such terms and conditions, you may not use the reference design
-- file and please promptly destroy any copies you have made.
-- 
-- This reference design file is being provided on an "as-is" basis and as an
-- accommodation and therefore all warranties, representations or guarantees of
-- any kind (whether express, implied or statutory) including, without limitation,
-- warranties of merchantability, non-infringement, or fitness for a particular
-- purpose, are specifically disclaimed.  By making this reference design file
-- available, Altera expressly does not recommend, suggest or require that this
-- reference design file be used in combination with any other product not
-- provided by Altera.
-- ================================================================================ */

// JESDCON Constants Package       


package jesdcon_pkg;

// enumerate the K-codes and D-codes used by the protocol
   parameter  [7:0] K285 = 8'hBC; // K28.5 : COM
   parameter  [7:0] K287 = 8'hFC; // K28.7 : 'F'
   parameter  [7:0] K283 = 8'h7C; // K28.3 : 'A'
   parameter  [7:0] K280 = 8'h1C; // K28.0 : 'R'
   parameter  [7:0] K284 = 8'h9C; // K28.4 : 'Q'
   parameter  [7:0] D287 = 8'hFC; // D28.7
   parameter  [7:0] D283 = 8'h7C; // D28.3
//Error mask
   parameter             ERR_DISPARITY   = 0;
   parameter             ERR_NOTINTABLE  = 1;
   parameter             ERR_UNEXPCTRL   = 2;
   parameter             ERR_CODEGROUP   = 3;
endpackage : jesdcon_pkg
