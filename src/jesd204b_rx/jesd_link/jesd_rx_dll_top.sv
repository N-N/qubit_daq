//JESD204B receiver data link layer top module
//N_N

module jesd_rx_dll_top
#(
	parameter L = 8,
	parameter F =4, //Valid values are 1, 2 and 4
	parameter K = 16,
	parameter FRAMES_PER_CLK = 2//F*FRAMES_PER_CLK must be 4
     
) 
(
	input wire csr_clk,
	input wire csr_rstn,
	
	input wire clk,
	input wire rstn,
	
	input wire sysref,
	output wire syncn,
	
	//Control
	input wire			csr_write,
	input wire			csr_read,
	input wire [7:0]	csr_address,
 	input wire [7:0]	csr_writedata,
	output wire			csr_readdatavalid,
	output wire [7:0]	csr_readdata,
	output wire			csr_waitrequest,
	
	//Input signals from PHY
	input	wire [L*FRAMES_PER_CLK*F*8-1:0] 	dec_data,       	//8b/10b decoded data
	input	wire [FRAMES_PER_CLK*F*L-1:0]		dec_data_valid,     //length is data_width/8
	input	wire [FRAMES_PER_CLK*F*L-1:0]		dec_kchar,      	//control chacareter derected flag
	input	wire [FRAMES_PER_CLK*F*L-1:0]		dec_rd_err,			//8b/10b decoder error flag
	input	wire [FRAMES_PER_CLK*F*L-1:0]		dec_patterndetect,	//K28.5 detected flag

	//Output signals to the transport Layer 
	output	wire [FRAMES_PER_CLK*F*L*8-1:0]		rx_dll_data_out,    //data out from the data link layer
	output	wire [L-1:0]						rx_dll_data_valid,
	output	wire [L-1:0]						rx_dll_lane_sync   //Received the lane alignment character  

);


localparam DATA_WIDTH = F*FRAMES_PER_CLK;
localparam MOCT_NUM = K*F/DATA_WIDTH;

wire [L-1:0] syncn_bus /* synthesis keep */;
wire [7:0] rdb_offset;

reg [7:0] oct_cnt, n_oct_cnt; //octect counter
reg sysref_d1, sysref_d2;
reg somf, n_somf;

wire sync_req, char_rep_en, scramble_en, clear_lane_error;
wire [3:0] err_mask;
wire [L-1:0][3:0] lane_error;

wire [L-1:0][FRAMES_PER_CLK*F-1:0][7:0] dec_data_array, rx_dll_data_out_array;
wire [L-1:0][FRAMES_PER_CLK*F-1:0] dec_data_valid_array, dec_kchar_array, dec_rd_err_array, dec_patterndetect_array;

assign dec_data_array = dec_data;
assign dec_data_valid_array = dec_data_valid;  	
assign dec_kchar_array = dec_kchar;
assign dec_rd_err_array = dec_rd_err;
assign dec_patterndetect_array = dec_patterndetect;
assign rx_dll_data_out = rx_dll_data_out_array;

assign syncn = (|syncn_bus);

genvar i;
generate for(i=0;i<L;i=i+1)
	begin: rx_dll
		jesd_rx_dll_lane #(
			.SAMPLES_PER_CLK(FRAMES_PER_CLK),
			.F(F))
		jesd_rx_dll_lane_0 
		(
		
			.clk				(clk),
			.rstn				(rstn),
			.rx_dll_syncn		(syncn_bus[i]),
			
			.buffer_release		(somf),   	//start of multiframe
		
		//Data and control signals from PHY
			.dec_data_out		(dec_data_array[i]),       //8b/10b decoded data
			.dec_data_valid		(dec_data_valid_array[i]),     //length is data_width/8
			.dec_kchar_out		(dec_kchar_array[i]),      //control chacareter derected flag
			.dec_rd_err			(dec_rd_err_array[i]),			//8b/10b decoder error flag
			.dec_patterndetect	(dec_patterndetect_array[i]),	//K28.5 detected flag	
														
		//input from control register                      
			.sync_req			(sync_req),
			.error_mask			(err_mask),           
			.char_rep_en		(char_rep_en),		//chacaracter replacement
			.scramble_en		(scramble_en),
			.clear_lane_error	(clear_lane_error),
		
		//output signals to the transport Layer 
			.rx_dll_data_out	(rx_dll_data_out_array[i]),    //data out from the data link layer
			.rx_dll_data_valid	(rx_dll_data_valid[i]),
			
			.rx_dll_lane_sync	(rx_dll_lane_sync[i]),   //Received the lane alignment character 
															
		//status register                                  
			.lane_error			(lane_error[i]),
			.gen_sync_req 		()//generate sync request incase of error 
		);
	end	
endgenerate

jesd_rx_csr #(.L(L))
jesd_rx_csr_inst
(
	//clock and reset signals
	.avmm_clk	(csr_clk),
	.avmm_rstn	(csr_rstn),
	
	.clk	(clk),
	.rstn	(rstn),

	//AV_MM Interface
    .avmm_write			(csr_write),
	.avmm_read			(csr_read),
	.avmm_address		(csr_address),
 	.avmm_writedata		(csr_writedata),
	.avmm_readdatavalid (csr_readdatavalid),
	.avmm_readdata		(csr_readdata),
	.avmm_waitrequest	(csr_waitrequest),
	
	//Control outputs
	.sync_req			(sync_req), //pulse
	.error_mask			(err_mask),           
	.char_rep_en		(char_rep_en),
	.scramble_en		(scramble_en),
	.clear_lane_error	(clear_lane_error),//pulse
	.rdb_offset			(rdb_offset),
	
	//Status inputs
	.cgs_done			(syncn_bus),
	.frm_aligned		(rx_dll_lane_sync),
	.err_flag			(lane_error)
);

always @(*)
begin
	n_oct_cnt = oct_cnt;
	n_somf = 1'b0;	
	
	if(oct_cnt < MOCT_NUM-1)
    	n_oct_cnt = oct_cnt + 'd1;
	else
	begin
    	n_oct_cnt = 8'd0;
		n_somf = 1'b1;
	end	
		
end

always @(posedge clk)
begin
	if(!rstn)
	begin
		oct_cnt  <= 'd0;
		sysref_d1 <= 1'b0;
		sysref_d2 <= 1'b0;
		somf <= 1'b0;
	end
	else
	begin
		//Sysref capture
		if (!sysref_d2)
		begin
			sysref_d1 <= sysref;
			sysref_d2 <= sysref_d1;
		end
		else
			sysref_d2<= 1'b0;
			
		if(sysref_d2)
    		oct_cnt <= rdb_offset;
		else
			oct_cnt <= n_oct_cnt;

		somf <= n_somf;
	end
end

endmodule 