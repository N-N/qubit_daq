//L = 8 Number of Lanes
//M = 2 Number of Converters
//F = 2 Number of octect per frame per Lane
//K = 16 Number of frames per multi frame
//S = 4 Samples per converter per frame
//N = 16 Converter resolution
//N' = 16 Tranmitted bits per sample

module jesd_rx_dll_lane #(
    parameter [3:0] SAMPLES_PER_CLK = 2,//Frames per clock cycle, F*SAMPLES_PER_CLK must be 4
    parameter [7:0] F =2) //Valid values are 1, 2 and 4
(

	input      wire								clk,
	input      wire								rstn,                                       
	output     reg 								rx_dll_syncn,
	
	input  	wire								buffer_release,   	//release data buffer

 //Data and control signals from PHY
	input	wire [SAMPLES_PER_CLK*F-1:0][7:0]	dec_data_out,       //8b/10b decoded data
	input	wire [SAMPLES_PER_CLK*F-1:0]		dec_data_valid,     //length is data_width/8
    input	wire [SAMPLES_PER_CLK*F-1:0]		dec_kchar_out,      //control chacareter detected flag
    input	wire [SAMPLES_PER_CLK*F-1:0]		dec_rd_err,			//8b/10b decoder error flag
	input	wire [SAMPLES_PER_CLK*F-1:0]		dec_patterndetect,	//K28.5 detected flag	
                                                   
  //input from control register                      
	input      wire                              sync_req,          //sync req though register interface
	input      wire [3:0]                        error_mask,           
	input      wire                              char_rep_en,		//enable character replacement and frame alignment monitoring
	input      wire                              scramble_en,
    input      wire [3:0]                        clear_lane_error,

	//Frame aligned data
	output wire [SAMPLES_PER_CLK*F-1:0][7:0] aligned_data_out,
	output wire [2:0] ilas_cnt,	//Ilas multiframe counter. Capture config data in second multiframe if needed. 
	
  //output signals to the transport Layer 
	output     wire [SAMPLES_PER_CLK*F-1:0][7:0] 	rx_dll_data_out,    //data out from the data link layer
	output     wire                              	rx_dll_data_valid,
	
                                                     
  //status register 
	output     reg                              	rx_dll_lane_sync,   //Received the lane alignment character 
	output     reg  [3:0]                            lane_error,
    output     reg                                   gen_sync_req 		//generate sync request incase of error 
);
import jesdcon_pkg::*;

localparam [1:0] CS_INIT  = 2'b00,
                 CS_CHECK = 2'b01,
			     CS_DATA  = 2'b11;

localparam       FS_INIT = 1'b0,
                 FS_DATA = 1'b1;
	            
localparam [1:0] LANE_SYNC_INIT = 2'b00,
                 BUFFER_STATE   = 2'b01,
			     PASS_DATA      = 2'b11; 
				 
localparam  [2:0] DATA_WIDTH = 4; //F*SAMPLES_PER_CLK

localparam [5:0] BUFFER_DEPTH = 5'b10000;

integer i;
reg [DATA_WIDTH-1:0][7:0] dec_data_out_latched;
reg [DATA_WIDTH-1:0] dec_kchar_out_latched;
reg [DATA_WIDTH-1:0] dec_data_err_latched;
reg [DATA_WIDTH-1:0] dec_data_err;

//CGS--------------------------------------
reg [1:0] code_sync_state, n_code_sync_state;
reg [7:0] kcount, n_kcount;
reg [7:0] vcount, n_vcount;
reg [7:0] icount, n_icount;
reg       n_rx_dll_syncn;
//-------------------------------------------

//Frame alignment----------------------------------------
reg [DATA_WIDTH-1:0] shift_cnt, n_shift_cnt;
reg shift_found,n_shift_found;

reg [DATA_WIDTH-1:0][7:0] n_frm_aligned_data, frm_aligned_data;
reg [DATA_WIDTH-1:0]      n_frm_aligned_kchar, frm_aligned_kchar;
reg [DATA_WIDTH-1:0][7:0] aligned_data; 
reg [DATA_WIDTH-1:0]      aligned_kchar; 
reg [DATA_WIDTH-1:0]      aligned_data_err; 
reg n_frm_aligned_data_valid, frm_aligned_data_valid;
reg n_frm_aligned_data_err, frm_aligned_data_err;
reg frm_aligned_data_valid_d1;
reg n_frm_align_state, frm_align_state;
reg [DATA_WIDTH-1:0] n_previous_AF_position, previous_AF_position;
wire frm_aligned_data_valid_pulse;

assign aligned_data_out = frm_aligned_data;
//ILAS--------------------------------------------------
reg [2:0] n_ilas_mfrm_cnt, ilas_mfrm_cnt;
reg       n_lane_data_phase, lane_data_phase;
assign ilas_cnt = ilas_mfrm_cnt;
//Buffer----------------------------------------------------------------------
reg [BUFFER_DEPTH-1:0][DATA_WIDTH-1:0][7:0] lane_buffer_data, n_lane_buffer_data;
reg [BUFFER_DEPTH-1:0]lane_buffer_data_valid, n_lane_buffer_data_valid;
reg [2:0] n_buffer_ptr, buffer_ptr;
reg [1:0] n_lane_sync_state, lane_sync_state;
reg       n_rx_dll_lane_sync;
//--------------------------------------------------------------------------------

//Error signals
reg  n_code_group_err, code_group_err;
reg  n_unexpected_ctrl_char, unexpected_cntrl_char;
reg  n_not_in_table_err, not_in_table_err;
reg  n_rd_status, rd_status;
reg  n_code_group_err_status,code_group_err_staus;
reg  n_gen_sync_req; //Generate sync_req incase of error

reg [7:0] last_eof_data;
wire [DATA_WIDTH-1:0]  eof;

assign eof = (F==4) ? 4'b1000 : (F==2)? 4'b1010 : 4'b1111;

/****************************************************************************************
*  Code Group Synchronization
****************************************************************************************/
always @(*)
begin
	n_code_sync_state    = code_sync_state;
	n_kcount             = kcount;
	n_rx_dll_syncn       = rx_dll_syncn;
    n_code_group_err     = code_group_err;
	
	case(code_sync_state)
	 CS_INIT: //add sync enable control signal
	 begin
		  n_rx_dll_syncn = 1'b0; //Deasserts the sync signal
		  if(sync_req)
			  n_kcount = 0;
		  else if( (&dec_kchar_out) && (&dec_data_valid) && (&dec_patterndetect) && kcount<4) //K28.5 detect
			n_kcount = kcount + DATA_WIDTH ;   
          
		  if( kcount >= 4 )	//if Number of K character is 4 then go the CS_CHECK state		
		  begin
		 	n_rx_dll_syncn = 1'b1; //Assert the sync signal
			n_code_sync_state = CS_DATA;
            n_code_group_err = 1'b0;
		  end
	 end
	 
     CS_DATA:
	 begin
		 if(!(&dec_data_valid))
			n_code_group_err = 1'b1;
		 if(sync_req)
		 begin
			n_code_sync_state = CS_INIT;
			n_rx_dll_syncn = 1'b0; 
			n_kcount = 0;
		 end
	 end
	 default:
			n_code_sync_state = CS_INIT;

	endcase
end

/****************************************************************************************
*
*  Initial Frame Alignment
*
****************************************************************************************/

assign dec_data_err = |dec_rd_err  & (!(&dec_data_valid));

//Assuming DATA_WIDTH = 4 octects
always @(*)
begin
   aligned_data =  dec_data_out_latched;
   aligned_data_err = dec_data_err_latched;
   aligned_kchar    = dec_kchar_out_latched;

   case (shift_cnt)
   	0: 
       begin
           aligned_data =  dec_data_out_latched;
           aligned_data_err =  dec_data_err_latched;
           aligned_kchar    = dec_kchar_out_latched;
       end
   	1: 
       begin
           aligned_data = {dec_data_out[0],dec_data_out_latched[3:1]};
           aligned_data_err = {dec_data_err[0],dec_data_err_latched[3:1]};
           aligned_kchar = {dec_kchar_out[0],dec_kchar_out_latched[3:1]};
       end
   	2: 
       begin
           aligned_data = {dec_data_out[1:0],dec_data_out_latched[3:2]};
           aligned_data_err = {dec_data_err[1:0],dec_data_err_latched[3:2]};
           aligned_kchar = {dec_kchar_out[1:0],dec_kchar_out_latched[3:2]};
       end
   	3: 
       begin
           aligned_data = {dec_data_out[2:0],dec_data_out_latched[3]};
           aligned_data_err = {dec_data_err[2:0],dec_data_err_latched[3]};
           aligned_kchar = {dec_kchar_out[2:0],dec_kchar_out_latched[3]};
       end
   	default:
       begin
   	   	aligned_data =  dec_data_out_latched;
   	   	aligned_data_err =  dec_data_err_latched;
        aligned_kchar    = dec_kchar_out_latched;
       end
   endcase
end   

always @(*)
begin
	n_shift_found                 = shift_found;
	n_shift_cnt                   = shift_cnt;
	n_previous_AF_position        = previous_AF_position;
	n_frm_align_state             = frm_align_state;
	n_frm_aligned_data            = frm_aligned_data;
	n_frm_aligned_data_valid      = 1'b0;
	n_frm_aligned_data_err        = frm_aligned_data_err;
    n_frm_aligned_kchar           = frm_aligned_kchar;

	case(frm_align_state)
		//FS_CHECK state is not implement as this design supports only single
		//Conveter Device.
	FS_INIT:
	begin
		n_frm_aligned_data  = 0;
		n_shift_found = 1'b0;
		n_shift_cnt = shift_cnt;
		if(rx_dll_syncn && !sync_req) 
		begin
    	 	if(dec_data_out != {DATA_WIDTH{K285}}) //Initial Frame Alignmemt
		 	begin
		 	   if(dec_data_out[0] != K285) //first character is K280 for multiple lane configuration
		 	   	n_shift_cnt = 0;
		 	   else if(dec_data_out[1] != K285)
		 	   	n_shift_cnt = 1;
		 	   else if(dec_data_out[2] != K285 )
		 	   	n_shift_cnt = 2;
		 	   else if(dec_data_out[3] != K285 )
		 	   	n_shift_cnt = 3;
		 	   n_shift_found = 1'b1;
			   n_frm_align_state    = FS_DATA;
		 	end
		end
	end
	FS_DATA:
	begin
		if(shift_found)
		begin
        n_frm_aligned_data = aligned_data; 
        n_frm_aligned_kchar = aligned_kchar; 
		n_frm_aligned_data_valid = 1'b1;
		n_frm_aligned_data_err = |aligned_data_err;
        last_eof_data = frm_aligned_data[DATA_WIDTH-1];

		for( i=0;i<DATA_WIDTH;i=i+1) 
	   	begin
		   if((char_rep_en & lane_data_phase))
           begin
             case (scramble_en)
		   	   0 ://No scrambling
               begin
                 if(n_frm_aligned_kchar[i] == 1'b1 && (n_frm_aligned_data[i] == K283 || n_frm_aligned_data[i] == K287) )
                 begin
                    n_previous_AF_position = i; 
                    if(eof[i] == 1'b1)
                      n_frm_aligned_data[i] = last_eof_data; //chacareter replacement 
	  	           //CHECK ALIGNMENT, change the shift_cnt if frame alignemnt character is found somehwehre other than at eof 
                    else if(i == previous_AF_position)
                    begin
                        if(shift_cnt == F-1)
                            n_shift_cnt = 0;
                        else
                            n_shift_cnt = shift_cnt + 1'b1;
                    end
                 end
                 if(eof[i] == 1'b1)
                     last_eof_data = n_frm_aligned_data[i];
               end
               1: //With scrambling
               begin
                  if(n_frm_aligned_kchar[i] ==1'b1)
                  begin 
                      case (n_frm_aligned_data[i])
                          K283: n_frm_aligned_data[i] = D283;
                          K287: n_frm_aligned_data[i] = D287;
                         default:
                              n_frm_aligned_data = aligned_data; 
                      endcase
                  end
               end
               default:
                     n_frm_aligned_data[i] = frm_aligned_data[i];
             endcase
           end
		end
		//find the frame boundary 
		//Frame aligned data
		if(!rx_dll_syncn)
	   	begin
		  	n_shift_found = 1'b0;
	      	n_frm_align_state = FS_INIT;
			n_frm_aligned_data_valid = 1'b0;
		    n_frm_aligned_data_err = 1'b0;
	   	end
	  end
	end
	default:
	begin
	     n_frm_align_state = FS_INIT;
		 n_frm_aligned_data_valid = 1'b0;
		 n_frm_aligned_data_err = 1'b0;
	end
 endcase
end
//-------------------------------------------
//ILAS
//--------------------------------------------
always @ (*)
begin
	n_ilas_mfrm_cnt = ilas_mfrm_cnt;
	n_lane_data_phase = lane_data_phase;
	
	if(sync_req)
	begin
		n_ilas_mfrm_cnt = 2'd0;
		n_lane_data_phase = 1'b0;
	end
	else
	begin
		if(frm_aligned_data[DATA_WIDTH-1] == K283 && frm_aligned_kchar[DATA_WIDTH-1] && ilas_mfrm_cnt <4) //Only count the Frame number for initial lane alignment sequence 
		begin	
			n_ilas_mfrm_cnt = ilas_mfrm_cnt + 1'b1;
		end		
	end
	
	if((ilas_mfrm_cnt == 3) && (frm_aligned_data [DATA_WIDTH-1] == K283)) //last multi frame of the ILAS
	begin
		n_lane_data_phase = 1'b1;
	end
end

/****************************************************************************************
*  Multi Lane Synchronization using elastic buffer
****************************************************************************************/
//Do the lane alignment on the 2nd lane alignment sequence

always @ (*)
begin
    n_lane_buffer_data  = lane_buffer_data;
    n_lane_buffer_data_valid = lane_buffer_data_valid;
	n_buffer_ptr = buffer_ptr;
	n_lane_sync_state = lane_sync_state;
	n_rx_dll_lane_sync = rx_dll_lane_sync;

	case(lane_sync_state)
    LANE_SYNC_INIT:
	begin
    	n_lane_buffer_data  = 0;
    	n_lane_buffer_data_valid = 0;
		n_buffer_ptr = 3'd0;
		n_rx_dll_lane_sync = 1'b0;
		if(rx_dll_syncn)
		begin
			if(frm_aligned_data[DATA_WIDTH-1] == K283 && frm_aligned_kchar[DATA_WIDTH-1] ) //after detecting the A character start buffering the data
				n_lane_sync_state = BUFFER_STATE;
		end
	end
	BUFFER_STATE:
	begin 
		//fill the buffer
        n_lane_buffer_data[buffer_ptr]  = frm_aligned_data;
        n_lane_buffer_data_valid[buffer_ptr] = frm_aligned_data_valid && {DATA_WIDTH{lane_data_phase}};
		n_buffer_ptr = buffer_ptr + 1'b1;
		n_rx_dll_lane_sync = 1'b1;//received the first lane alignment chacter of the 2nd frame

		if(buffer_release) //alignement achieved, starts shifting data from the elastic buffer
		begin
            n_lane_buffer_data[BUFFER_DEPTH-2:0]  = lane_buffer_data[BUFFER_DEPTH-1:1];
            n_lane_buffer_data_valid[BUFFER_DEPTH-2:0] = lane_buffer_data_valid[BUFFER_DEPTH-1:1];
			n_lane_sync_state = PASS_DATA; //pass the data through sync buffer 
        	n_lane_buffer_data[buffer_ptr-1]  = frm_aligned_data;
        	n_lane_buffer_data_valid[buffer_ptr-1] = frm_aligned_data_valid && {DATA_WIDTH{lane_data_phase}};
			n_buffer_ptr = buffer_ptr - 1'b1;
		end
		else if (buffer_ptr == BUFFER_DEPTH -1)//alignment is not achieved in this multiframe
		begin
			//initialize the buffer to 0, start resync again
    		n_lane_buffer_data  = 0;
    		n_lane_buffer_data_valid = 0;
			n_buffer_ptr = 0;
			n_lane_sync_state = LANE_SYNC_INIT;
		end
		
		if(!rx_dll_syncn || sync_req)
			n_lane_sync_state = LANE_SYNC_INIT;
	end
	PASS_DATA:
	begin
        n_lane_buffer_data[BUFFER_DEPTH-2:0]  = lane_buffer_data[BUFFER_DEPTH-1:1];
        n_lane_buffer_data_valid[BUFFER_DEPTH-2:0] = lane_buffer_data_valid[BUFFER_DEPTH-1:1];
		n_lane_buffer_data[buffer_ptr]  = frm_aligned_data;
        n_lane_buffer_data_valid[buffer_ptr] = frm_aligned_data_valid && {DATA_WIDTH{lane_data_phase}};

		if(!rx_dll_syncn || sync_req)
		begin
			n_lane_sync_state = LANE_SYNC_INIT;
    		n_lane_buffer_data  = 0;
    		n_lane_buffer_data_valid = 0;
			n_buffer_ptr = 0;
			n_rx_dll_lane_sync = 1'b0;
		end
	end
	default:
			n_lane_sync_state = LANE_SYNC_INIT;
	endcase
end

/****************************************************************************************
* assign the rx_dll data
****************************************************************************************/
assign	rx_dll_data_out = lane_buffer_data[0];
assign	rx_dll_data_valid = lane_buffer_data_valid[0];

/****************************************************************************************
* Registered the signals 
****************************************************************************************/
always @(posedge clk or negedge rstn)
begin
	if(!rstn)
	begin
		rx_dll_syncn         <= 1'b0;
		code_sync_state      <= 3'd0;
		kcount               <= 8'd0;
		vcount               <= 8'd0;
		icount               <= 8'd0;
	    dec_data_out_latched <= {F*8{1'b0}};
        
		dec_kchar_out_latched <= {DATA_WIDTH{1'b0}};
        dec_data_err_latched  <= {DATA_WIDTH{1'b0}};
		
	    shift_cnt            <= {DATA_WIDTH{1'b0}};
	    shift_found          <= 1'b0;	
		frm_align_state      <= 1'b0;
		previous_AF_position <= F-1;
		frm_aligned_data     <= {DATA_WIDTH*8{1'b0}};
		frm_aligned_data_err <= {DATA_WIDTH{1'b0}};
		frm_aligned_kchar   <= {DATA_WIDTH{1'b0}};
	    frm_aligned_data_valid <= 1'b0;	
	    frm_aligned_data_valid_d1 <= 1'b0;	
		//ILAS-----------------------------------
		lane_data_phase      <= 1'b0;
		ilas_mfrm_cnt		<= 1'b0;
		//-----------------------------------------
    	lane_buffer_data     <=  {BUFFER_DEPTH*DATA_WIDTH*8{1'b0}};
    	lane_buffer_data_valid    <=  {BUFFER_DEPTH{1'b0}};
		buffer_ptr           <=  3'd0;
		lane_sync_state      <=  2'd0;

		rx_dll_lane_sync     <= 1'b0;
		rx_dll_syncn         <= 1'b0;

		ilas_mfrm_cnt             <= 2'd0;

        code_group_err       <= 1'b0;
        not_in_table_err     <= 1'b0;
        code_group_err_staus <= 1'b0;
        unexpected_cntrl_char<= 1'b0;
        rd_status            <= 1'b0; 
        gen_sync_req         <= 1'b0;

	end
	else 
	begin
		rx_dll_syncn             <= n_rx_dll_syncn;
		code_sync_state          <= n_code_sync_state  ;
		kcount                   <= n_kcount           ;
		vcount                   <= n_vcount           ;
		icount                   <= n_icount           ;
		dec_data_out_latched     <= dec_data_out;
	    dec_kchar_out_latched    <= dec_kchar_out;
	    dec_data_err_latched     <= dec_data_err; //running disparity errror or data is not valid

		shift_cnt                <= n_shift_cnt;
	    shift_found              <= n_shift_found;
	    frm_align_state          <= n_frm_align_state;
		previous_AF_position     <= n_previous_AF_position;
		frm_align_state          <= n_frm_align_state;
		frm_aligned_data         <= n_frm_aligned_data;
		frm_aligned_data_err     <= n_frm_aligned_data_err;
		frm_aligned_kchar        <= n_frm_aligned_kchar;
		frm_aligned_data_valid   <= n_frm_aligned_data_valid;
	    frm_aligned_data_valid_d1 <= frm_aligned_data_valid;	

		//ILAS------------------------------------------------
		ilas_mfrm_cnt		<= 	n_ilas_mfrm_cnt;
		lane_data_phase		<= 	n_lane_data_phase;
		
		//lane sync------------------------------------------- 
    	lane_buffer_data         <= n_lane_buffer_data;
    	lane_buffer_data_valid   <= n_lane_buffer_data_valid;
		buffer_ptr               <= n_buffer_ptr;
		lane_sync_state          <= n_lane_sync_state;

		rx_dll_lane_sync         <= n_rx_dll_lane_sync; 
		rx_dll_syncn             <= n_rx_dll_syncn; 

		
       code_group_err            <= n_code_group_err;
       not_in_table_err          <= n_not_in_table_err; 
       code_group_err_staus      <= n_code_group_err_status;
       unexpected_cntrl_char     <= n_unexpected_ctrl_char;
       rd_status                 <= n_rd_status; 
       gen_sync_req              <= n_gen_sync_req;
	end
end

/****************************************************************************************
* Errors and Recovery
* Running Disparity Error
* Not in Table Error
* Unexpected controll character Error
* Code Group Error
****************************************************************************************/
assign lane_error[ERR_DISPARITY] = rd_status;
assign lane_error[ERR_NOTINTABLE] = not_in_table_err;
assign lane_error[ERR_UNEXPCTRL] = unexpected_cntrl_char;
assign lane_error[ERR_CODEGROUP] = code_group_err_staus;

//Running Disparity Error
always @(*)
begin
    n_rd_status = rd_status; 
    if(|dec_rd_err)
        n_rd_status = 1'b1;
    if(sync_req || clear_lane_error[0])
        n_rd_status = 1'b0;
end

//code group error
always @(*)
begin
    n_code_group_err_status = code_group_err_staus; 
    if(code_group_err)
        n_code_group_err_status = 1'b1; 
    if(sync_req || clear_lane_error[3])
        n_code_group_err_status = 1'b0; 
end

//Unexpected K char Error - K characters other than K280,K283,K284,K285,K287
always @(*)
begin
   n_unexpected_ctrl_char = unexpected_cntrl_char; 
   if(frm_aligned_kchar)
   begin
     case(DATA_WIDTH)
       1: 
       begin
           if(!((frm_aligned_data[0] == K283) || (frm_aligned_data[0] == K284) || (frm_aligned_data[0] == K285)|| (frm_aligned_data[0] == K280) || (frm_aligned_data[0] == K287)))
              n_unexpected_ctrl_char = 1'b1; 
       end
       2: 
       begin
           if(!((frm_aligned_data[0] == K283) || (frm_aligned_data[0] == K284) || (frm_aligned_data[0] == K285)|| (frm_aligned_data[0] == K280) || (frm_aligned_data[0] == K287) ||
                (frm_aligned_data[1] == K283) || (frm_aligned_data[1] == K284) || (frm_aligned_data[1] == K285)|| (frm_aligned_data[1] == K280) || (frm_aligned_data[1] == K287)))
              n_unexpected_ctrl_char = 1'b1; 
       end
       4:
       begin
           if(!((frm_aligned_data[0] == K283) || (frm_aligned_data[0] == K284) || (frm_aligned_data[0] == K285)|| (frm_aligned_data[0] == K280) || (frm_aligned_data[0] == K287) ||
                (frm_aligned_data[1] == K283) || (frm_aligned_data[1] == K284) || (frm_aligned_data[1] == K285)|| (frm_aligned_data[1] == K280) || (frm_aligned_data[1] == K287) ||
                (frm_aligned_data[2] == K283) || (frm_aligned_data[2] == K284) || (frm_aligned_data[2] == K285)|| (frm_aligned_data[2] == K280) || (frm_aligned_data[2] == K287) ||
                (frm_aligned_data[3] == K283) || (frm_aligned_data[3] == K284) || (frm_aligned_data[3] == K285)|| (frm_aligned_data[3] == K280) || (frm_aligned_data[3] == K287)))
              n_unexpected_ctrl_char = 1'b1; 
       end
       default:
              n_unexpected_ctrl_char = 1'b0; 

     endcase   
   end       
   if(sync_req || clear_lane_error[2])
     n_unexpected_ctrl_char = 1'b0; 
end

/****************************************************************************************
*Not in table Error 
*****************************************************************************************/
//Not in table Error
// K284 is the transceiver not-in-table character
always @(*)
begin
   n_not_in_table_err = not_in_table_err;
   if(!(ilas_mfrm_cnt == 1) && !(|dec_rd_err))
   begin
     case(DATA_WIDTH)
       1: 
       begin
           if(frm_aligned_data[0] == K284) 
               n_not_in_table_err = 1'b1;
       end
       2: 
       begin
          if (frm_aligned_data[0] == K284 || frm_aligned_data[1] == K284)
               n_not_in_table_err = 1'b1;
       end
       4:
       begin
          if (frm_aligned_data[0] == K284 || frm_aligned_data[1] == K284 || (frm_aligned_data[2] == K284 || frm_aligned_data[3] == K284))
               n_not_in_table_err = 1'b1;
       end
       default:
               n_not_in_table_err = 1'b0;
     endcase   
   end       
   if(sync_req || clear_lane_error[1])
      n_not_in_table_err = 1'b0;
end

/****************************************************************************************
* Generate SYNC signal in case of error
*****************************************************************************************/
always @(*)
begin
    // generate SYNC after unmasked errors or button press
    n_gen_sync_req = gen_sync_req;
    if ( error_mask[ERR_DISPARITY]  && lane_error[ERR_DISPARITY]     ||
         error_mask[ERR_NOTINTABLE] && lane_error[ERR_NOTINTABLE]    ||
         error_mask[ERR_UNEXPCTRL]  && lane_error[ERR_UNEXPCTRL]     ||
         error_mask[ERR_CODEGROUP]  && lane_error[ERR_CODEGROUP]     )
         
           n_gen_sync_req = 1'b1; //sync request generated in case of error
    if(sync_req) 
           n_gen_sync_req = 1'b0; //sync request generated in case of error
end

endmodule