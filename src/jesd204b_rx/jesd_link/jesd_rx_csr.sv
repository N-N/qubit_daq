module jesd_rx_csr #(
	L = 8,
	CHAR_REP_EN = 0,	//Character replacement enable
	SCRAMBLE_EN = 0,	//Scrsmbler enable
	//Error monitoring enable
	ERR_DISPARITY_EN = 1,
	ERR_NOTINTABLE_EN = 1,
	ERR_UNEXPCTRL_EN = 1,
	ERR_CODEGROUP_EN = 1,
	
	RDB_OFFSET='d0,
	
	ADDR_WIDTH = 8,
	DATA_WIDTH = 8
	)
(
	//clock and reset signals
	input    wire			avmm_clk,
	input    wire			avmm_rstn,
	
	input    wire			clk,
	input    wire			rstn,

	//AV_MM Interface
    input wire						avmm_write,
	input wire						avmm_read,
	input wire  [ADDR_WIDTH-1:0]	avmm_address,
 	input wire  [DATA_WIDTH-1:0]	avmm_writedata,
	output reg						avmm_readdatavalid,
	output reg  [DATA_WIDTH-1:0]	avmm_readdata,
	output reg						avmm_waitrequest,
	
	//Control outputs
	output wire						sync_req, //pulse
	output wire	[3:0] 				error_mask,           
	output wire						char_rep_en,
	output wire						scramble_en,
	output wire 					clear_lane_error,//pulse
	output wire [DATA_WIDTH-1:0]	rdb_offset,
	
	//Status inputs
	input wire [L-1:0]		cgs_done,
	input wire [L-1:0]		frm_aligned,
	input wire [L-1:0][3:0]	err_flag
);

import jesdcon_pkg::*;

//Control and status registers
   parameter CTRL_REG = 'd0; //R/W
   
   parameter CTRL_SYNC_REQ		=	0; //selfcleaning
   parameter CTRL_ERR_CLR 		=	1;
   parameter CTRL_SCR_EN		=	2;
   parameter CTRL_CHAR_REP_EN 	=	3;
   parameter CTRL_ERR_MASK		=	4; //next 4 bits
   
   parameter RDB_OFFSET_REG = 'd4; //R/W
   
   parameter STATUS_REG = 'd8; //R One reg per lane starting from here 
   
   parameter STATUS_CGS_DONE	=	0;
   parameter STATUS_FRAME_ALIGN	=	1;
   parameter STATUS_ERR			=	2; //next 4 bits
   
   
   
reg [DATA_WIDTH-1:0] ctrl_reg, n_ctrl_reg;
reg [DATA_WIDTH-1:0] rdb_offset_reg, n_rdb_offset_reg;
reg [L-1:0][31:0] status_reg;

assign sync_req			=	ctrl_reg[CTRL_SYNC_REQ];
assign clear_lane_error	=	ctrl_reg[CTRL_ERR_CLR];
assign char_rep_en		=	ctrl_reg[CTRL_CHAR_REP_EN];
assign scramble_en		= 	ctrl_reg[CTRL_SCR_EN];
assign error_mask		=	ctrl_reg[CTRL_ERR_MASK+3:CTRL_ERR_MASK];

assign rdb_offset = rdb_offset_reg;

reg [1:0] state, n_state;
parameter IDLE='d0, READ_VALID='d1, WRITE_VALID='d2;

reg [2:0] avmm_state, n_avmm_state;
parameter AVMM_READ='d1, AVMM_READ_DATA_VALID='d2, AVMM_WRITE='d3, AVMM_WRITE_DLY = 'd4;

reg [DATA_WIDTH-1:0] n_avmm_readdata;

reg valid, n_valid;
wire valid_avmm_clk;

wire avmm_read_clk, avmm_write_clk;

integer i;

always @(*)
begin
	n_state = state;
	n_avmm_readdata = avmm_readdata;
	n_ctrl_reg = ctrl_reg;
	n_rdb_offset_reg = rdb_offset_reg;
	
	n_valid = valid;
	
	for(i=0;i<L;i=i+1)
	begin
		status_reg[i] = (cgs_done[i]<<STATUS_CGS_DONE)|
					(frm_aligned[i]<<STATUS_FRAME_ALIGN)|
					(err_flag[i]<<STATUS_ERR);
	end			
	
	case(state)
	IDLE:
	begin
		//Read
		n_avmm_readdata = 'd0;
		if(avmm_read_clk)
		begin
			case(avmm_address)
				CTRL_REG:		n_avmm_readdata = ctrl_reg;
				RDB_OFFSET_REG:	n_avmm_readdata = rdb_offset_reg; 
				default:
				begin
					for(i=0; i<L*4; i=i+4)
					begin
						if(avmm_address == STATUS_REG+i)	
							n_avmm_readdata = status_reg[i/4];
					end
				end
			endcase
			n_valid = 1'b1;
			n_state = READ_VALID;
		end
		
		//Write
		else if(avmm_write_clk)
		begin
			case(avmm_address)
				CTRL_REG:		n_ctrl_reg = avmm_writedata;
				RDB_OFFSET_REG:	n_rdb_offset_reg = avmm_writedata;
			endcase
			n_valid = 1'b1;
			n_state = WRITE_VALID;
		end
	end	
		
	READ_VALID:
	begin
		if(!avmm_read_clk)
		begin
			n_state = IDLE;
			n_valid = 1'b0;
		end	
	end
	
	WRITE_VALID:
	begin
		if(!avmm_write_clk)
		begin
			n_state = IDLE;
			n_valid = 1'b0;
		end	
	end
		
	endcase	
		
	//Pulse generation
	if(ctrl_reg & ('d1<<CTRL_SYNC_REQ) )
		n_ctrl_reg = ctrl_reg & ~('d1<<CTRL_SYNC_REQ);
	if(ctrl_reg & ('d1<<CTRL_ERR_CLR))
		n_ctrl_reg = ctrl_reg & ~('d1<<CTRL_ERR_CLR);
end

always @(posedge clk or negedge rstn)
begin
	if(!rstn)
	begin
		//Set nonzero default values
		ctrl_reg <= 32'd0 | 
		(CHAR_REP_EN<<CTRL_CHAR_REP_EN) |
		(SCRAMBLE_EN<<CTRL_SCR_EN) |
			//Error mask
		(((ERR_DISPARITY_EN<<ERR_DISPARITY)|
		(ERR_NOTINTABLE_EN<<ERR_NOTINTABLE)|
		(ERR_UNEXPCTRL_EN<<ERR_UNEXPCTRL)|
		(ERR_CODEGROUP_EN<<ERR_CODEGROUP))<<CTRL_ERR_MASK);
		
		rdb_offset_reg <= RDB_OFFSET;
		
		state			<=	IDLE;
		valid			<=	1'b0;
		avmm_readdata	<=	'd0;
	end
	else
	begin
		ctrl_reg		<= n_ctrl_reg;
		rdb_offset_reg	<= n_rdb_offset_reg;
		valid 			<= n_valid;
		state			<= n_state;
		avmm_readdata	<= n_avmm_readdata;
	end
end	

data_sync sync_rd(
	.data		(avmm_read),
	.dest_clk	(clk),
	.dest_rstn	(rstn),
	.data_sync	(avmm_read_clk)
);

data_sync sync_valid(
	.data		(valid),
	.dest_clk	(avmm_clk),
	.dest_rstn	(avmm_rstn),
	.data_sync	(valid_avmm_clk)
);

data_sync sync_wr(
	.data		(avmm_write),
	.dest_clk	(clk),
	.dest_rstn	(rstn),
	.data_sync	(avmm_write_clk)
);

always @(*)
begin
	avmm_waitrequest = 1'b0;
	avmm_readdatavalid = 1'b0;
	n_avmm_state = avmm_state;
	
	case(avmm_state)
		IDLE:
		begin
			if(avmm_read)
			begin
				avmm_waitrequest = 1'b1;
				n_avmm_state = AVMM_READ;
			end
			if(avmm_write)
			begin
				avmm_waitrequest = 1'b1;
				n_avmm_state = AVMM_WRITE;
			end
		end
		
		AVMM_READ:
		begin
			avmm_waitrequest = 1'b1;
			if(valid_avmm_clk)
				n_avmm_state = AVMM_READ_DATA_VALID;
		end
		
		AVMM_READ_DATA_VALID:
		begin
			avmm_readdatavalid = 1'b1;
			n_avmm_state = IDLE;
		end
		
		AVMM_WRITE:
		begin
			avmm_waitrequest = 1'b1;
			if(valid_avmm_clk)
				n_avmm_state = AVMM_WRITE_DLY;
		end
		
		AVMM_WRITE_DLY:
		begin
			avmm_waitrequest = 1'b0;
			n_avmm_state = IDLE;
		end
	
	endcase	
end

always @(posedge avmm_clk or negedge avmm_rstn)
begin
	if(!avmm_rstn)
	begin
		avmm_state<=IDLE;
	end
	else
	begin
		avmm_state<=n_avmm_state;
	end
end	

   
endmodule