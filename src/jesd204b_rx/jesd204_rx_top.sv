//N_N

//These are real parameters for LMFS 8224 mode of the ADC
//L = 8		Number of lanes
//M = 2 	Number of converters
//F = 2 	Octects per frame per lane
//K = 16 	Frames per multiframe
//S = 4 	Samples per converter per frame
//N = 14 	Converter resolution
//N' = 16 	Tranmitted bits per sample

module jesd204_rx_top #(parameter L = 8, F = 2, K = 16, FRAMES_PER_CLK = 2, N = 14, N_PRIME = 16)
//FRAMES_PER_CLK - Number of frames received per link clock cycle
(
	//Clock and reset signals
	input wire	link_clk, 		//Link clock for FPGA fabric	
	input wire	cdr_ref_clk, 	//Reference clock for PHY hardware modules. 
	//They shold be of the same freq, but keep them separate and use double channe pll 
	//Fitter won't route them instead.
	
	input wire	link_rstn, 			//synchroneous reset
	
	input wire	reconfig_clk,
	input wire 	reconfig_rst,
	
	//AV-ST source data output 
	output    wire [L*FRAMES_PER_CLK*F*8-1:0]	rx_data,
	output    wire								rx_data_valid,
	
	//jesd204B signals                                        
    output	wire			sync_n,
	input	wire [L-1:0]	rx_serial_data,
	input	wire			sysref,
	                                                         
    //JESD204B link layer control interface                       
    input    wire[7:0]		link_csr_address,
	input    wire			link_csr_read,
	input    wire			link_csr_write,
	input    wire[31:0]		link_csr_writedata,
	output   wire[31:0]		link_csr_readdata,
	output   wire			link_csr_readdatavalid,
	output	wire			link_csr_waitrequest,
                                                             
    //jesd204B phy reconfiguration interface                         
    input wire [8:0]		phy_mgmt_address,   
    input wire				phy_mgmt_write,     
    input wire [31:0]		phy_mgmt_writedata, 
    input wire				phy_mgmt_read,      
    output wire [31:0]		phy_mgmt_readdata,  
    output wire				phy_mgmt_waitrequest,
	output wire				phy_mgmt_readdatavalid,
	
	//jesd204B phy reconfiguration controller RAM interface
	output wire [31:0]  	phy_mif_address,      
	output wire         	phy_mif_read,         
	input  wire [15:0]  	phy_mif_readdata,     
	input  wire         	phy_mif_waitrequest,
	
	output 	reg 	rxled //link data valid
	
	);
	
//PHY reset controller	
wire [L-1:0] rx_analogreset, rx_digitalreset, rx_cal_busy, rx_is_lockedtodata, rx_ready;

//jesd phy to link interface	
wire [L-1:0][FRAMES_PER_CLK*F-1:0][7:0] rx_parallel_data;
wire [L-1:0][FRAMES_PER_CLK*F-1:0] rx_datak, rx_syncstatus, rx_errdetect, rx_patterndetect;
wire [L-1:0] rx_dll_data_valid;

//PHY to reconfiguration controller interface
wire [L*46-1:0]	reconfig_from_xcvr;         
wire [L*70-1:0]	reconfig_to_xcvr;  

//Link layer to transport layer interface
wire [L*FRAMES_PER_CLK*F*8-1:0] rx_link_data;

assign	rx_data_valid = |rx_dll_data_valid;

//RX LED
always @ (posedge link_clk or negedge link_rstn)
	begin
		if(!link_rstn)
			rxled <= 1'b0;
		else
		begin
			if( rx_data_valid )
				rxled<=1'b1;
			else
				rxled<=1'b0;
		end		
	end

//Reset controller
phy_rst rst_ctrl(
	.clock				( link_clk ), 
	.reset				( !link_rstn ),  
	//Inputs
	.rx_is_lockedtodata	(rx_is_lockedtodata),
	.rx_cal_busy		(rx_cal_busy),
	//Otputs	
	.rx_analogreset		(rx_analogreset),
	.rx_digitalreset	(rx_digitalreset),  
	.rx_ready			(rx_ready)         
	);	
//JESD204 PHY
jesd_rx_phy rx_phy(
		.rx_analogreset				(rx_analogreset),          
		.rx_digitalreset			(rx_digitalreset),  
		
		.rx_cdr_refclk				(cdr_ref_clk),           
		.rx_pma_clkout				(),           
		.rx_serial_data				(rx_serial_data),          
		.rx_is_lockedtoref			(),       
		.rx_is_lockedtodata			(rx_is_lockedtodata),      
		
		.rx_std_coreclkin			({L{link_clk}}),
		
		.rx_cal_busy				(rx_cal_busy), 
		
		.rx_parallel_data			(rx_parallel_data),
		.rx_datak 					(rx_datak),
		.rx_syncstatus				(rx_syncstatus),
		
		.rx_errdetect				(rx_errdetect),   	//8/10b decoder error         
		.rx_disperr					(),    	//8/10b decoder disparity error          
		.rx_runningdisp				(),   //       
		.rx_patterndetect			(rx_patterndetect),	//Alignment pattern detected (double K285)
		
		.reconfig_to_xcvr	(reconfig_to_xcvr),
		.reconfig_from_xcvr	(reconfig_from_xcvr)
	);
	
//JESD204B PHY reconfiguration controller
jesd_enc_avgx_reconfig_cntrl phy_reconfig(
		.reconfig_busy				(),             
		.mgmt_clk_clk				(reconfig_clk),              
		.mgmt_rst_reset				(reconfig_rst),            
		
		.reconfig_mgmt_address		(phy_mgmt_address[8:2]),//2 LSB of avmm address are not used due to 32-bit data size    
		.reconfig_mgmt_read 		(phy_mgmt_read),      
		.reconfig_mgmt_readdata 	(phy_mgmt_readdata),  
		.reconfig_mgmt_waitrequest 	(phy_mgmt_waitrequest),
		.reconfig_mgmt_write		(phy_mgmt_write),
		.reconfig_mgmt_writedata	(phy_mgmt_writedata),
		
		.reconfig_mif_address		(phy_mif_address),      
		.reconfig_mif_read			(phy_mif_read),         
		.reconfig_mif_readdata		(phy_mif_readdata),     
		.reconfig_mif_waitrequest	(phy_mif_waitrequest),  
		
		.reconfig_to_xcvr			(reconfig_to_xcvr),          
		.reconfig_from_xcvr			(reconfig_from_xcvr)         
	);	
	
avmm_valid_from_waitrequest phy_mgmt_valid_gen(
	.clk(reconfig_clk),
	.rst(reconfig_rst),
	.waitrequest(phy_mgmt_waitrequest),
	.valid(phy_mgmt_readdatavalid)
	);	
//-----------------------------------------------------------	
//RX Link layer
//-----------------------------------------------------------	
jesd_rx_dll_top  #(.L(L), .F(F), .K(K), .FRAMES_PER_CLK(FRAMES_PER_CLK) )
	rx_dll	(

	.clk					(link_clk),
	.rstn					(link_rstn & (& rx_ready)),

	.sysref 				(sysref),
	.syncn					(sync_n),		//sync output
	
	.csr_clk				(reconfig_clk),
	.csr_rstn				(~reconfig_rst),

	//Data from PHY
	.dec_data				(rx_parallel_data), //8b/10b decoded data
	.dec_data_valid			(rx_syncstatus),    //Octet valid, length is data_width/8
    .dec_kchar				(rx_datak),       	//Control character flags for octets, length is data_width/8
    .dec_rd_err				(rx_errdetect),		
    .dec_patterndetect		(rx_patterndetect),

	//Output signals to the transport Layer
	.rx_dll_data_out		(rx_link_data),
	.rx_dll_data_valid		(rx_dll_data_valid),
	
	.rx_dll_lane_sync		(),	//Received the lane alignment character

	//control
	.csr_write				(link_csr_write),
	.csr_read				(link_csr_read),
	.csr_address			(link_csr_address),
 	.csr_writedata			(link_csr_writedata),
	.csr_readdatavalid		(link_csr_readdatavalid),
	.csr_readdata			(link_csr_readdata),
	.csr_waitrequest		(link_csr_waitrequest)
	
);

//Transport layer
jesd204_transport #(.FRAMES_PER_CLK(FRAMES_PER_CLK), .N(N), .N_PRIME(N_PRIME))
	rx_transport
(
	.data_in	(rx_link_data),
	.data_out	(rx_data)
);

endmodule