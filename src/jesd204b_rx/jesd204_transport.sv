//N_N
//Simple JESD204B transport layer
//For ADS54J40 with LMFS 8224 only
//FRAMES_PER_CLK, N and N_PRIME may be changed

module jesd204_transport #(L = 8, M= 2, F = 2, S = 4, FRAMES_PER_CLK=2, N = 14, N_PRIME = 16)
(
	input	wire [L*FRAMES_PER_CLK*F*8-1:0]	data_in,
	output	wire [L*FRAMES_PER_CLK*F*8-1:0]	data_out
);

wire [L-1:0][FRAMES_PER_CLK-1:0][F-1:0][7:0] data_in_array;
reg  [L-1:0][FRAMES_PER_CLK-1:0][F-1:0][7:0] hard_remaped;
reg	[L*FRAMES_PER_CLK*F/4-1:0][M-1:0][15:0] transport_remaped;
reg [L*FRAMES_PER_CLK*F/2-1:0][15:0] raw_samples, swaped_samples, shifted_samples; 

assign data_in_array = data_in;
assign data_out = shifted_samples;

integer f, frm, m, i;

always @(*)
begin
	//Hardware rx lenes mapping (on the PCB)
	/*
		FPGA	ADC
	hsma_rx_p
		0		B1
		1		B3
		2		B2
		3		B0
		4		A1
		5		A3
		6		A2
		7		A0
	*/
	hard_remaped[0] = data_in_array[7];
	hard_remaped[1] = data_in_array[4];
	hard_remaped[2] = data_in_array[6];
	hard_remaped[3] = data_in_array[5];
	hard_remaped[4] = data_in_array[3];
	hard_remaped[5] = data_in_array[0];
	hard_remaped[6] = data_in_array[2];
	hard_remaped[7] = data_in_array[1];
	
	//Map samples into sequencial form according to ADC datasheet
	for (m=0; m<M; m=m+1)
	begin
		for(frm=0;frm<FRAMES_PER_CLK;frm=frm+1)
		begin
			transport_remaped[3+frm*S][m] = hard_remaped[0+m*S][frm];
			transport_remaped[2+frm*S][m] = hard_remaped[1+m*S][frm];
			transport_remaped[0+frm*S][m] = hard_remaped[2+m*S][frm];
			transport_remaped[1+frm*S][m] = hard_remaped[3+m*S][frm];
		end	
	end	
	//Change byte order and remove tralling zeros from LSBs
	raw_samples = transport_remaped;
	for(i=0;i<L*FRAMES_PER_CLK*F/2;i=i+1)
	begin
		swaped_samples[i] = { raw_samples[i][7:0], raw_samples[i][15:8] };
		shifted_samples[i] = { {(N_PRIME-N){swaped_samples[i][15]}},  swaped_samples[i][15:(N_PRIME-N)] };//Remember that samples are signed
	end 
end

endmodule