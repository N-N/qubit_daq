// Copyright (C) 2017  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel MegaCore Function License Agreement, or other 
// applicable license agreement, including, without limitation, 
// that your use is for the sole purpose of programming logic 
// devices manufactured by Intel and sold by Intel or its 
// authorized distributors.  Please refer to the applicable 
// agreement for further details.

module qubit_daq 
(
// {ALTERA_ARGS_BEGIN} DO NOT REMOVE THIS LINE!

	clkintopb_p,
	rzqin_b_1_5v,
	ddr3b_casn,
	ddr3b_clk_n,
	ddr3b_clk_p,
	ddr3b_rasn,
	ddr3b_resetn,
	ddr3b_wen,
	cpu_resetn,
	fx3_i2c_sclk_io,
	fx3_i2c_sda_io,
	fx3_pclk_o,
	fx3_pktend_n_o,
	fx3_slcs_n_o,
	fx3_sloe_n_o,
	fx3_slrd_n_o,
	fx3_slwr_n_o,
	fx3_flaga_i,
	fx3_flagb_i,
	fx3_flagc_i,
	fx3_flagd_i,
	smaj3_trig_in,
	rx_cmos_sync,
	bin_ch_data,
	ddr3b_a,
	ddr3b_ba,
	ddr3b_dm,
	ddr3b_dq,
	ddr3b_dqs_n,
	ddr3b_dqs_p,
	user_led,
	ddr3b_csn,
	ddr3b_cke,
	ddr3b_odt,
	fx3_adrs_o,
	fx3_data_io,
	trig_out,
	jesd_rx_syncn,
	hsma_rx_p,
	jesd_sysref,
	jesd_device_clk
// {ALTERA_ARGS_END} DO NOT REMOVE THIS LINE!

);

// {ALTERA_IO_BEGIN} DO NOT REMOVE THIS LINE!
input			clkintopb_p;


input		wire				jesd_device_clk;	// JESD device clock from which the frame clock is generated
input							jesd_sysref;		// JESD Realted Signals - SYSREF (Source Synchronous to Device Clock)

input		wire [7:0]		hsma_rx_p;
output	wire        	jesd_rx_syncn;        // received JESD 2-lane bitstream

input			cpu_resetn;

inout			fx3_i2c_sclk_io;
inout			fx3_i2c_sda_io;

output		fx3_pclk_o;
output		fx3_pktend_n_o;
output		fx3_slcs_n_o;
output		fx3_sloe_n_o;
output		fx3_slrd_n_o;
output		fx3_slwr_n_o;
input			fx3_flaga_i;
input			fx3_flagb_i;
input			fx3_flagc_i;
input			fx3_flagd_i;

output  [14:0] ddr3b_a;           //SSTL15  //Address
output  [2:0]  ddr3b_ba;          //SSTL15  //Bank Address
output         ddr3b_casn;        //SSTL15  //Column Address Strobe
output         ddr3b_clk_n;        //SSTL15  //Diff Clock - Neg
output         ddr3b_clk_p;        //SSTL15  //Diff Clock - Pos
output  [1:0]  ddr3b_cke;         //SSTL15  //Clock Enable
output  [1:0]  ddr3b_csn;         //SSTL15  //Chip Select
output  [7:0]  ddr3b_dm;          //SSTL15  //Data Write Mask
inout   [63:0] ddr3b_dq;          //SSTL15  //Data Bus
inout   [7:0]  ddr3b_dqs_n;       //SSTL15  //Diff Data Strobe - Neg
inout   [7:0]  ddr3b_dqs_p;       //SSTL15  //Diff Data Strobe - Pos
output  [1:0]  ddr3b_odt;         //SSTL15  //On-Die Termination Enable
output         ddr3b_rasn;        //SSTL15  //Row Address Strobe
output         ddr3b_resetn;        //SSTL15  //Reset
output         ddr3b_wen;         //SSTL15  //Write Enable
input	  			rzqin_b_1_5v;  
	
input				smaj3_trig_in;
input				rx_cmos_sync;
input	[15:4]	bin_ch_data;

output	[1:0]	fx3_adrs_o;
inout		[31:0]fx3_data_io;
output	[2:0]	trig_out;
output	[7:0]	user_led;

// {ALTERA_IO_END} DO NOT REMOVE THIS LINE!
// {ALTERA_MODULE_BEGIN} DO NOT REMOVE THIS LINE!
// {ALTERA_MODULE_END} DO NOT REMOVE THIS LINE!

wire sys_rstn;
reg [7:0] rst_cnt;
reg rst_reg;
reg [28:0] cnt;
reg [28:0] afi_cnt;
wire afi_clk;

wire ddr3_init_done, ddr3_cal_success, ddr3_cal_fail;
wire jesd_data_valid;
wire jesd_link_clk0;
wire jesd_link_clk1;


//Hardware rx lenes mapping
//Handled by JESD204 transport layer
/*
	FPGA	ADC
hsma_rx_p
	0		B1
	1		B3
	2		B2
	3		B0
	4		A1
	5		A3
	6		A2
	7		A0
*/

//Power up self reset
//++++++++++++++++++++++++++++++++++++++
always @(posedge clkintopb_p)
begin
	if(rst_cnt!= 8'h170)	
		rst_cnt <= rst_cnt+8'd1;
	else
		rst_reg<=1'b1;
end
assign sys_rstn = rst_reg && cpu_resetn;
//+++++++++++++++++++++++++++++++++

always @(posedge jesd_link_clk0)
	cnt <= cnt+1;

//Status LEDs	
assign user_led[0] = 1'b1;
assign user_led[1] = 1'b1;		
assign user_led[2] = jesd_rx_syncn;
assign user_led[3] = cnt[28];
assign user_led[4] = ~jesd_data_valid;
assign user_led[5] = ~ddr3_init_done;
assign user_led[6] = ~ddr3_cal_success;
assign user_led[7] = ~ddr3_cal_fail;

//tese signal source
//reg [13:0]	frame_clk_devider;

//always @(posedge jesd_link_clk0)
//begin
	//frame_clk_devider <= frame_clk_devider+1'b1;
//end

//assign trig_out[1]=frame_clk_devider[13];	//syncroneous trigger
//assign trig_out[0]=frame_clk_devider[4];	//test signal 3.84 MHz

//Module's reset wires
wire sys_clk_reset;
wire jesd_clk_reset;

//Reset sources
wire soft_reset;
wire jesd_reset;

//Clock inverter for GPIF II
ddr_out_buf_1bit	fx3_clk_out_buf(
	.datain_h ( 1'b0 ),
	.datain_l ( 1'b1 ),
	.outclock ( clkintopb_p),
	.dataout ( fx3_pclk_o )
	);

//assign fx3_pclk_o = clkintopb_p;

qubit_daq_qsys qubit_daq_qsys_inst 
(
	.sys_clk_clk			(clkintopb_p),
	.sys_rst_reset		    (~sys_rstn), 
	
	.jesd_ref_clk_clk		(jesd_device_clk), 
	
	.ddr3_mem_a          (ddr3b_a),      
	.ddr3_mem_ba         (ddr3b_ba),     
	.ddr3_mem_ck         (ddr3b_clk_p),  
	.ddr3_mem_ck_n       (ddr3b_clk_n),  
	.ddr3_mem_cke        (ddr3b_cke),    
	.ddr3_mem_cs_n       (ddr3b_csn),    
	.ddr3_mem_dm         (ddr3b_dm),     
	.ddr3_mem_ras_n      (ddr3b_rasn),   
	.ddr3_mem_cas_n      (ddr3b_casn),   
	.ddr3_mem_we_n       (ddr3b_wen),    
	.ddr3_mem_reset_n    (ddr3b_resetn), 
	.ddr3_mem_dq         (ddr3b_dq),     
	.ddr3_mem_dqs        (ddr3b_dqs_p),
	.ddr3_mem_dqs_n      (ddr3b_dqs_n), 
	.ddr3_mem_odt        (ddr3b_odt),
	.ddr3_oct_rzqin      (rzqin_b_1_5v),
    .ddr3_status_local_init_done   (ddr3_init_done),
	.ddr3_status_local_cal_success (ddr3_cal_success),
	.ddr3_status_local_cal_fail    (ddr3_cal_fail),
	
	.i2c_bus_scl		(fx3_i2c_sclk_io),        
	.i2c_bus_sda		(fx3_i2c_sda_io),

	.gpif_ii_fx3_data		   (fx3_data_io),     
	.gpif_ii_fx3_pktend_n	(fx3_pktend_n_o), 
	.gpif_ii_fx3_addr		   (fx3_adrs_o),     
	.gpif_ii_fx3_slcs_n		(fx3_slcs_n_o),   
	.gpif_ii_fx3_sloe_n		(fx3_sloe_n_o),   
	.gpif_ii_fx3_slrd_n		(fx3_slrd_n_o),   
	.gpif_ii_fx3_slwr_n		(fx3_slwr_n_o),   
	.gpif_ii_fx3_flaga		(fx3_flaga_i),   
	.gpif_ii_fx3_flagb		(fx3_flagb_i),   
	.gpif_ii_fx3_flagc		(fx3_flagc_i),   
	.gpif_ii_fx3_flagd		(fx3_flagd_i),
	
	.jesd204_sync_n         (jesd_rx_syncn),
	.jesd204_rx_serial_data (hsma_rx_p),
	.jesd204_sysref         (jesd_sysref), 
	.jesd204_status_rxled   (jesd_data_valid),
	
	.ext_trig_ext_trig  (smaj3_trig_in),
	//.data_capture_0_dec_trig_dec_trig (trig_out[2]), 

	.jesd_clk0_clk          (jesd_link_clk0),
	.jesd_clk1_clk          (jesd_link_clk1),
	
	.trig_src_out           (trig_out[0]) 
	
);

endmodule
