//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:47:59 12/11/2014 
// Design Name: 
// Module Name:    I2C_Slave 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module i2c_slave(input clk,			//device clk
					input reset_n,		//reset
					inout sda,			//serial data from i2c master
					input sclk,			//serial clk from i2c master	
		//////pins given to memory block as input to memory/////			
//					output d_req,		//dataready
					output avmm_wr_en,
					output [31:0]avmm_wrdata,
					output [31:0]avmm_addr,
					input [31:0] avmm_rd_data,
					input rd_data_valid,
					output avmm_rd_en,
					output avmmstart,
					output avmmdone,
					output wr_addr_incr
										
					);

reg rea;							//read enable
reg rx_flag;					//rxr flag for setting 1 during data reception
reg sclk_r;						//sclk with one clk delay
reg sclk_rr;					//sclk with two clks delay
reg [7:0]count;				//count reg
reg [7:0]pulse_count;		//array count for storing data
reg pulse_count_en;
reg pulse_count_en_r;
reg pulse_count_en_rr;


reg [3:0]data_pul_cnt;
reg data_pul_cnt_en;

reg [31:0] avmm_rxshft;			//incoming data shift reg
reg [31:0] avmm_txshft;			//outgoing data from shift reg


reg [6:0]dev_addr;			//device address reg for storing incoming device address

reg [1:0]bytecnt;				// for byte counting

reg [15:0] datalen;			// data length
reg [15:0] byte4cnt;			// counter for 32 bit word


reg cnt8rise;
reg cnt8rise_r;
reg byte4det_r;
reg byte4det_rr;


wire byteincr,cnt8fall;

wire extraclkpul;

wire byte4det,avmstpul;
reg byte4fall_r,byte4fall_rr,avmstpul_r,avmstpul_rr;


reg [31:0]avm_mem_addr;			// reg for memory address

reg [15:0]d_outr;				//data out reg
reg Tristate;							//tristating register

reg ms_ack;

reg sda_sl_r;					//reg for above wire
reg wr_en_r;					//write enable register
reg d_req_r;					//data ready register
reg on_r;						//device on reg
reg sda_r;						//sda with one clk delay
reg sda_rr,avmmrd_r;						//sda with 2 clk delay
wire rd_en_r;
wire avmmrd;				



wire sda_m;						//output of inout sda buffer
wire sda_sl;					//input of sda inout buffer


assign d_req=d_req_r;		//assigning data_ready register to data ready output
assign sda_sl=sda_sl_r;		//assigning serial data out reg to sda
//assign wr_en=wr_en_r;		//assigning write enable register to write enable
//assign data2mem=avmm_rxshft;	//assigning data_in reg to output
//assign addr=avm_mem_addr;		//assigning memory address reg to address output

parameter ONE_MHz_count =8'd80;//For 100MHz 8'd80;For 50 MHz 8'd40
parameter FOUR_HUN_KHz_count=8'd200;//For 100MHz 8'd200;//For 50MHz 8'd100


//****************************************************************
//IOBUF for the inout sda line
/*
IOBUF #(
.DRIVE(12),
.IBUF_DELAY_VALUE("0"),
.IFD_DELAY_VALUE("AUTO"),
.IOSTANDARD("DEFAULT"),
.SLEW("FAST")
)
IOBUF_inst
(
.O(sda_m),
.IO(sda),
.I(sda_sl),
.T(Tristate)
);*/

/****************************************************************
//IOBUF for the inout sda line
i2c_sda i2c_sda_inst
	( 
	.datain(sda_sl),
	.dataio(sda),
	.dataout(sda_m),
	.oe(!Tristate)) ;


//**********************************************************************/
assign sda = !Tristate ? sda_sl:1'bz;
assign sda_m = sda;
//registers used for edge detection

wire sclk_rise,sclk_fall,sda_rise,sda_fall,byte4rise,byte4fall;

assign sclk_rise=(!sclk_rr) & sclk_r;
assign sclk_fall= sclk_rr & (!sclk_r);
assign sda_rise = sda_r & (!sda_rr); 
assign sda_fall = (!sda_r) & sda_rr;
assign byte4det = bytecnt[0]& bytecnt[1];
assign byte4rise = byte4det_r&&(!byte4det_rr);
assign byte4fall = (!byte4det_r)&&byte4det_rr;
assign cnt8fall	= (!cnt8rise)&cnt8rise_r;
assign avmstpul=&pulse_count[5:0];

always@(posedge clk or negedge reset_n)				// reg with one delay
	begin
		if(!reset_n)
			begin
			sclk_r<=1;
			sda_r<=1;
			cnt8rise<=1'b0;
			cnt8rise_r<=1'b0;
			byte4det_r<=1'b0;
			byte4det_rr<=1'b0;
			byte4fall_r<=1'b0;
			pulse_count_en_r<=1'b0;
			pulse_count_en_rr<=1'b0;
			byte4fall_rr<=1'b0;
			avmstpul_r<=1'b0;
			avmstpul_rr<=1'b0;
			avmmrd_r<=1'b0;
			end
		else
			begin
			sclk_r<=sclk;
			sda_r<=sda_m;
			cnt8rise<=data_pul_cnt[3:3];
			cnt8rise_r<=cnt8rise;
			byte4det_r<=byte4det;
			byte4det_rr<=byte4det_r;
			byte4fall_r<=byte4fall;
			byte4fall_rr<=byte4fall_r;
			pulse_count_en_r<=pulse_count_en;
			pulse_count_en_rr<=pulse_count_en_r;
			avmstpul_r<=avmstpul;
			avmstpul_rr<=avmstpul_r;
			avmmrd_r<=avmmrd;
			end
	end
always@(posedge clk or negedge reset_n)			// reg with two delay
	begin
		if(!reset_n)
			begin
			sclk_rr<=1;
			sda_rr<=1;
		
			end
		else
			begin
			sclk_rr<=sclk_r;
			sda_rr<=sda_r;
		
			end
	end
	
	
	
//*********************************
//always@(posedge clk or negedge reset_n)			
//	begin
//		if(!reset_n)
//		d_outr<=0;
//		else
//		d_outr<=data_tx;
//	end
	
////////////////////counter for frequency//////////////
always@(posedge clk or negedge reset_n)	// counter reg for pecision of sampling
	begin
		if(!reset_n)
			count<=8'd0;
		else if(sclk_fall)			//resetting counter at the neg edge of sclk
			count<=8'd0;
		else
			count<=count+1;
	end
	
	
	
///////////////////////recieve flag  will be one during operation/////////////
always@(posedge clk or negedge reset_n)	// setting and resetting flag which acts as start and stop signal
	begin
		if(!reset_n)
			rx_flag<=0;
		else if(sda_fall & sclk)
			rx_flag<= 1;
		else if(!ms_ack&&data_pul_cnt_en)
			rx_flag<=0;
		else if(sda_rise & sclk)
			rx_flag<= 0;
		else if((pulse_count==6'd9))
			rx_flag<=(0 || (dev_addr==7'd127)); //checking if device address matches
		end
	
	
//////////////dev_on signal////////////////////
always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)
			on_r<=0;
		else if(pulse_count==8'd8 && (count==ONE_MHz_count))
			on_r<=(0 || (dev_addr==7'd127)); 		 //checking if device addr matches
	end
	
	
	
////////////Slave and register addersss counter//////////////////
always@(posedge clk or negedge reset_n)// 
	begin
		if(!reset_n)
			pulse_count<=0;
		else if(!pulse_count_en)							//resetting at 29th sclk pulse
			pulse_count<=0;
		else if(sclk_fall && pulse_count_en)	//counting SCL pulses
			pulse_count<=pulse_count+1;
	end
	
//***********************Slave address and register address counter enable**************
always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)
			pulse_count_en<=1'b0;
		else if(sda_fall & sclk)
			pulse_count_en<=1'b1;
		else if (!rx_flag)
			pulse_count_en<=1'b0;
		else if((pulse_count==8'd63)&&sclk_fall)
			pulse_count_en<=1'b0;
		
	end
	
//***********************Data transaction counter enable**************	
always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)
			data_pul_cnt_en<=1'b0;
		else if (!rx_flag)
			data_pul_cnt_en<=1'b0;
		else if((pulse_count==8'd63)&&sclk_fall)
			data_pul_cnt_en<=1'b1;
		else if(sda_rise & sclk)
			data_pul_cnt_en<=1'b0;
	end	

//***********************Counter dedicated for data transcation**************
always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)	
			data_pul_cnt<=4'd0;
		else if (!data_pul_cnt_en)
			data_pul_cnt<=4'd0;
		else if((data_pul_cnt==4'd8)&&sclk_fall) 
			data_pul_cnt<=4'd0;
		else if(sclk_fall && data_pul_cnt_en)
			data_pul_cnt<=data_pul_cnt+4'd1;
	end
		
//***********************Sampling operation**************

always@(posedge clk or negedge reset_n)	// storing the received device address
	begin
		if(!reset_n )
			dev_addr<=7'd0;
		else if(sda_fall & sclk)
			dev_addr<=7'd0;
		else if(pulse_count<6'd8 && (count==ONE_MHz_count)&&pulse_count_en)
			dev_addr<={dev_addr[5:0],sda_m};
	end
	
	
//***********************storing the datalen
always@(posedge clk or negedge reset_n)	
	begin
		if(!reset_n )
			datalen<=16'd0;
		else if(sda_fall & sclk)
			datalen<=16'd0;
		else if((pulse_count>8'd9) && (pulse_count<8'd18) && (count==ONE_MHz_count))
			datalen<={datalen[14:0],sda_m};
		else if((pulse_count>8'd18) && (pulse_count<8'd27) && (count==ONE_MHz_count))
			datalen<={datalen[14:0],sda_m};
	end
	
	
//***********************storing the recieved avmm register address	
always@(posedge clk or negedge reset_n)	
	begin
		if(!reset_n )
			avm_mem_addr<=31'd0;
		else if(sda_fall & sclk)
			avm_mem_addr<=31'd0;
		else if((pulse_count>8'd27) && (pulse_count<8'd36) && (count==ONE_MHz_count))
			avm_mem_addr<={avm_mem_addr[30:0],sda_m};
		else if((pulse_count>8'd36) && (pulse_count<8'd45) && (count==ONE_MHz_count))
			avm_mem_addr<={avm_mem_addr[30:0],sda_m};
		else if((pulse_count>8'd45) && (pulse_count<8'd54) && (count==ONE_MHz_count))
			avm_mem_addr<={avm_mem_addr[30:0],sda_m};
		else if((pulse_count>8'd54) && (pulse_count<8'd63) && (count==ONE_MHz_count))
			avm_mem_addr<={avm_mem_addr[30:0],sda_m};		
		end

//************read or write enable from the		
always@(posedge clk or negedge reset_n)//storing the read or write bit which is received following the device address
	begin
		if(!reset_n)
			rea<=0;
		else if((pulse_count==6'd8) && (count==ONE_MHz_count))
			rea<=sda_m;
		else if(!rx_flag)
			rea<=0;
	end
	
	
always@(posedge clk or negedge reset_n)//tristating the inout buffer at proper timings
	begin
		if(!reset_n)
			Tristate<=1;
		else if(!rea && ((pulse_count==8'd9)|(pulse_count==8'd18)|(pulse_count==8'd27)|(pulse_count==8'd36)
		|(pulse_count==8'd45)|(pulse_count==8'd54)|(pulse_count==8'd63)|(data_pul_cnt==4'd8)))//untristating to send the ack
			Tristate<=0;
		else if(!rea && ((pulse_count==6'd8)|(pulse_count==6'd17)|(pulse_count==6'd26)|(pulse_count==6'd35)
		|(pulse_count==8'd44)|(pulse_count==8'd53)|(pulse_count==8'd62)|(data_pul_cnt==4'd8))&& sclk_fall && rx_flag)//untristating to send the ack
			Tristate<=0;
		else if(rea && ((pulse_count==6'd9)|(pulse_count==6'd18)|(pulse_count==8'd27)
							|(pulse_count==8'd36)|(pulse_count==8'd45)|(pulse_count==8'd54)|(pulse_count==8'd63)))//untristating to send the ack
			Tristate<=0;
		else if((rea && ((pulse_count==6'd8)|(pulse_count==6'd17)|(pulse_count==6'd26)
					|(pulse_count==6'd35)|(pulse_count==8'd44)|(pulse_count==8'd53)|(pulse_count==8'd62))&& sclk_fall && rx_flag))//untristating to send the ack
			Tristate<=0;
		else if((data_pul_cnt>=4'd0)&& (data_pul_cnt<=4'd7)&&rea&&data_pul_cnt_en)
			Tristate<=0;
		else if(((pulse_count<6'd64) && rea)  || (!rea && rx_flag))//tristating when we have to recieve data
			Tristate<=1;
		else 
			Tristate<=1;
	end
	
//**************Monitoring master acknowledge******
always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)
			ms_ack<=1'b1;
		else if (sda_fall & sclk)	
			ms_ack<=1'b1;
		else if(rea&&(data_pul_cnt==4'd8)&&(count==ONE_MHz_count))
			ms_ack<=~sda_m;
	end




//**************Sending out data through SDA**********

always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)
			sda_sl_r<=1;
		else if(((pulse_count==8'd8)|(pulse_count==8'd17)|(pulse_count==8'd26)|(pulse_count==8'd35)
		                       |(pulse_count==8'd44)|(pulse_count==8'd53)|(pulse_count==8'd62)|(data_pul_cnt==4'd7))&& sclk_fall && rx_flag)
			sda_sl_r<=0;
		else if((dev_addr==7'd127) && (pulse_count==8'd9))//sending ack for if recieved device addr matches
			sda_sl_r<=0;
		else if((pulse_count==8'd18)|(pulse_count==8'd27)|(pulse_count==8'd36)|(pulse_count==8'd45)|(pulse_count==8'd54)|(pulse_count==8'd63))						//ack for reg addr
			sda_sl_r<=0;
		else if(data_pul_cnt==4'd8)			//ack for data if write
			sda_sl_r<=0;
		else if(rea && data_pul_cnt_en&&(data_pul_cnt<=4'd7))//sending data during 3rd byte of read cycle
			sda_sl_r<=avmm_txshft[31:31] ;
		else
			sda_sl_r<=1;
	end
	
//*************************Outgoing data shift register****************	
always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)
			avmm_txshft<=32'd0;
		else if(sda_fall & sclk)
			avmm_txshft<=32'hdeaddead;
		else if(rd_data_valid&&rea)
			avmm_txshft<=avmm_rd_data;
		else if(rea && data_pul_cnt_en && sclk_fall && (data_pul_cnt<=4'd7)) 
			avmm_txshft<={avmm_txshft[30:0],1'b0};
	end
	
		
//*************************Receiving in data through SDA****************

always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n )//sampling and storing incoming data
			avmm_rxshft<=32'd0;
		else if(sda_fall & sclk)
			avmm_rxshft<=32'd0;
		else if(!rea && data_pul_cnt_en && (count==ONE_MHz_count)&&(data_pul_cnt<=4'd7))//sampling in 3rd byte ie data
			avmm_rxshft<={avmm_rxshft[30:0],sda_m};
	end
	
//************************ word counter****************	


	

//********************Control Signals to be sent into memory****************

always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)					//write enable for writing data into memory
			wr_en_r<=0;
		else if( (sda_rise & sclk)&& !rea && (count==ONE_MHz_count))
			wr_en_r<=1;
		else 
			wr_en_r<=0;
	end

//********************I2C Byte counter****************	
	always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)					
			bytecnt<=2'd0;
		else if (sda_fall & sclk)
			bytecnt<=2'd0;
		else if (!rx_flag)
			bytecnt<=2'd0;
		else if (byteincr)
			bytecnt<=bytecnt+2'd1;
		
	end
//********************I2C 32bit word counter****************		
	always@(posedge clk or negedge reset_n)
	begin
		if(!reset_n)					
			byte4cnt<=16'd0;
		else if (sda_fall & sclk)
			byte4cnt<=16'd0;
		else if (!rx_flag)
			byte4cnt<=16'd0;
		else if (byte4fall)
			byte4cnt<=byte4cnt+16'd1;
	end
	
assign extraclkpul=(!pulse_count_en)&&(((avm_mem_addr[31:16]>=16'h0004)&&(avm_mem_addr[31:16]<=16'h0007))||((avm_mem_addr[31:16]>=16'h0044)&&(avm_mem_addr[31:16]<=16'h0047))||(avm_mem_addr[23:8]==16'h8011)||(avm_mem_addr[27:4]==24'h200000));	
assign avmmrd=rea&&cnt8fall&&((byte4cnt!=(datalen>>2))&&(bytecnt==2'd0));
assign avmm_wrdata=avmm_rxshft;	
assign byteincr=cnt8rise&(!cnt8rise_r);
assign avmm_wr_en = (byte4fall|(byte4fall_r&&extraclkpul))&&(!rea);
assign rd_en_r=(!pulse_count_en_r)&&pulse_count_en_rr &&rea;
assign avmm_rd_en =((!pulse_count_en)&&pulse_count_en_r&&rea)||(rd_en_r&&extraclkpul)||avmmrd||(avmmrd_r&&extraclkpul); 
assign wr_addr_incr= byte4fall_rr;
assign avmmdone=sda_rise & sclk;
assign avmm_addr=avm_mem_addr>>2;
assign avmmstart=avmstpul_r&&(!avmstpul_rr);//(!pulse_count_en)&&pulse_count_en_r;

			
endmodule




