
module i2c_top #
(
    parameter DEV_ADDR = 'h7F,
    parameter FILTER_LEN = 4,
    parameter AVMM_DATA_WIDTH = 32,                  // width of data bus in bits (8, 16, 32, or 64)
    parameter AVMM_ADDR_WIDTH = 32,                  // width of address bus in bits
    parameter AVMM_BE_WIDTH = (AVMM_DATA_WIDTH/8)  // width of word select bus (1, 2, 4, or 8)
)
(
    input clk,
    input rst,
    /*
     * I2C interface
     */
    inout scl,
    inout sda,
    /*
     * AVMM interface
     */
    output [AVMM_ADDR_WIDTH-1:0]   avmm_address,       
    input  [AVMM_DATA_WIDTH-1:0]   avmm_readdata,     
    input                          avmm_readdatavalid,
    output [AVMM_DATA_WIDTH-1:0]   avmm_writedata,     
    output                         avmm_write,        
    output [AVMM_BE_WIDTH-1:0]     avmm_byteenable,   
    input                          avmm_waitrequest,  
    output                         avmm_read         
    
);

wire i2c_scl_i;
wire i2c_scl_o;
wire i2c_scl_t;

wire i2c_sda_i;
wire i2c_sda_o;
wire i2c_sda_t;


//Open drain bidirectional buffers
/**********************************/
i2c_io	scl_io_buf (
	.datain     ( i2c_scl_o ),
	.oe         ( ~i2c_scl_t ),
	.dataio     ( scl ),
	.dataout    ( i2c_scl_i )
	);
    
i2c_io	sda_io_buf (
	.datain     ( i2c_sda_o ),
	.oe         ( ~i2c_sda_t ),
	.dataio     ( sda ),
	.dataout    ( i2c_sda_i )
	);
/*********************************/

i2c_slave_avmm #
(
   .FILTER_LEN      (FILTER_LEN     ),
   .AVMM_DATA_WIDTH (AVMM_DATA_WIDTH),                  
   .AVMM_ADDR_WIDTH (AVMM_ADDR_WIDTH),
   .AVMM_BE_WIDTH   (AVMM_BE_WIDTH  )  
) 
i2c_slave_avmm_inst
(
    .clk                (clk),
    .rst                (rst),

    /*
     * I2C interface
     */
    .i2c_scl_i          (i2c_scl_i),
    .i2c_scl_o          (i2c_scl_o),
    .i2c_scl_t          (i2c_scl_t),
    .i2c_sda_i          (i2c_sda_i),
    .i2c_sda_o          (i2c_sda_o),
    .i2c_sda_t          (i2c_sda_t),

    /*
     * AVMM interface
     */
    .avmm_address       (avmm_address      ),      
    .avmm_readdata      (avmm_readdata     ),     
    .avmm_readdatavalid (avmm_readdatavalid),
    .avmm_writedata     (avmm_writedata     ),     
    .avmm_write         (avmm_write        ),        
    .avmm_byteenable    (avmm_byteenable   ),   
    .avmm_waitrequest   (avmm_waitrequest  ),  
    .avmm_read          (avmm_read         ),         

    /*
     * Status
     */
    .busy               (),
    .bus_addressed      (),
    .bus_active         (),

    /*
     * Configuration
     */
    .enable             (1),
    .device_address     (DEV_ADDR)
);

endmodule