`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Soliton Technologies
// Engineer: Reniflal
// 
// Create Date:    04/24/2014 
// Design Name: 		I2c reg configuration
// Module Name:    reg_config 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Top module of i2c slave read and write
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fx3_i2c_main(
					input clk,			//device clk
					input reset_n,		//reset
					inout sda,			//serial data from i2c master
					input sclk,			//serial clk from i2c master
					input wire master_waitrequest,
					output wire [31:0] master_address,
					output wire master_write,
					output wire [3:0] master_byteenable,
					output wire [31:0] master_writedata,
					output wire master_read,
					 input wire [31:0] master_readdata,
					 input wire master_readdatavalid

    );
wire wr_en;
wire [15:0]data2mem;
wire [7:0]addr;
wire [15:0]data_out;
wire avmm_wr_en,avmmstart ,avmmdone,wr_addr_incr;
wire [31:0]avmm_wrdata,avmm_addr,avmm_rd_data;


//---------------------------------------------------------------------//
//i2c slave module	
//-------------------------------------------------------------------------
	
i2c_slave i2cslave0(
					.clk(clk),				//device clk
					.reset_n(reset_n),	//reset
					.sda(sda),				//serial data from i2c master
					.sclk(sclk),			//serial clk from i2c master	
					.avmm_wr_en(/*avmm_wr_en*/master_write),
					.avmm_wrdata(/*avmm_wrdata*/master_writedata),
					.avmm_addr(avmm_addr),
				   .avmm_rd_data(master_readdata/*|fx3ctrl_regdata*/),
					.rd_data_valid(master_readdatavalid/*|fx3ctrl_rdvalid*/),
					.avmm_rd_en(master_read),
					.avmmstart(avmmstart),
					.avmmdone(avmmdone),
					.wr_addr_incr(wr_addr_incr)
					);
					
					
 write_master avmminst (
	.clk(clk),
	.reset(!reset_n),
	
	// control inputs and outputs
	.control_fixed_location(1'b0),
	.control_write_base(avmm_addr),
	.increment_address(wr_addr_incr),
	.control_go(avmmstart),
	.control_done(avmmdone),
		
	// master inputs and outputs
	.master_address(master_address),
	.master_write(/*master_write*/),
	.master_byteenable(master_byteenable),
	.master_writedata(/*master_writedata*/),
	.master_waitrequest(master_waitrequest)
);

endmodule
