//Simple square wave generator with adjustable pulse width and period
//N_N
module square_gen
(
	input  wire		clk, 		
	input  wire		rst,
	
	input  wire		csr_clk, 		
	input  wire		csr_rst,

	output reg		out,

	// AVMM for csr
	input 	wire			csr_write,
	input	wire			csr_read,
	input	wire  [15:0]	csr_address,
 	input	wire  [31:0]	csr_datain,
	output	wire			csr_readdatavalid,
	output	wire  [31:0]	csr_dataout
	
	);
	
	reg [63:0] counter;
	wire [63:0] period;
	wire [63:0] width;
	wire update;
	reg [63:0] period_r;
	reg [63:0] width_r;
	
	square_gen_csr square_gen_csr_inst(
	.clk 	(clk),
	.reset	(rst),
	
	.avmm_clk (csr_clk),
	.avmm_reset (csr_rst),
	
	//Interface
	.avmm_write			(csr_write),
	.avmm_read			(csr_read),
	.avmm_address		(csr_address),
 	.avmm_datain		(csr_datain),
	.avmm_readdatavalid	(csr_readdatavalid),
	.avmm_dataout		(csr_dataout),
	
	//Control outputs
	.period	(period),
	.width	(width),
	
	.update (update) //pulse
	
	);	
	
	always @(posedge clk or posedge rst)
	begin
		if(rst)
		begin
			out			<=	1'b0;
			counter		<=	64'b0;
			period_r	<=	64'b0;
			width_r		<=	64'b0;
		end
		else
		begin
			if (update)
			begin
				period_r	<=	period;
				width_r		<=	width;
				counter 	<= 	64'b0;
				out 		<= 	1'b0;
			end
			else
			begin
				if( (period_r!=0) & (width_r!=0) )
				begin
					if(counter == period_r-1)
					begin
						counter 	<= 	64'b0;
						out 		<= 	1'b1;
					end		
					else
					begin
						counter <= counter + 64'b1;
						if(counter == width_r-1)
						begin
							out <= 1'b0;
						end
					end	
					
				end
			end	
		end	
	end
	
endmodule	