//Fake module for qsys

module trig_gen_top
(
	input  wire		clk, 		
	input  wire		rst,
	
	input  wire		csr_clk, 		
	input  wire		csr_rst,

	output reg		out,

	// AVMM for csr
	input 	wire			csr_write,
	input	wire			csr_read,
	input	wire  [15:0]	csr_address,
 	input	wire  [31:0]	csr_datain,
	output	wire			csr_readdatavalid,
	output	wire  [31:0]	csr_dataout
	
	);
	
	square_gen square_gen_inst(
	
	.clk				(clk				), 		
	.rst				(rst				),
	                   
	.csr_clk			(csr_clk			), 		
	.csr_rst			(csr_rst			),
                        
	.out				(out				),
                      
	.csr_write			(csr_write			),
	.csr_read			(csr_read			),
	.csr_address		(csr_address		),
 	.csr_datain			(csr_datain			),
	.csr_readdatavalid	(csr_readdatavalid	),
	.csr_dataout		(csr_dataout		)
	);
	
endmodule	