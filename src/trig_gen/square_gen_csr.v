//Control and status registers 
//Avmm read: no wait cycles
//Avmm write: no wait cycles
//N_N
module square_gen_csr
(
	input clk,
	input reset,
	
	input avmm_clk,
	input avmm_reset,
	
	// Avalon Memory Mapped Slave Interface: used to read/write Registers
	input wire			avmm_write,
	input wire			avmm_read,
	input wire  [15:0]	avmm_address,
 	input wire  [31:0]	avmm_datain,
	output reg			avmm_readdatavalid,
	output reg  [31:0]	avmm_dataout,
	
	//Control signals
	output reg	[63:0]	period,
	output reg	[63:0]	width,

	output reg update //Pulse in clk domain
	);

//Register map
localparam CTRL_REG = 16'd0;	
localparam PERIOD_REG_LO = 16'd4, PERIOD_REG_HI = 16'd8;
localparam WIDTH_REG_LO = 16'd12, WIDTH_REG_HI = 16'd16;

//CTRL_REG bits
localparam UPDATE	= 	0; 	//W	Self cleaning.

reg [0:0] ctrl_reg, n_ctrl_reg;
reg [63:0] n_period, n_width;

reg n_avmm_readdatavalid;
reg [31:0] n_avmm_dataout;

wire feedback_avmm_clk;

//clk domain
wire update_clk;
reg update_clk_r;
reg n_update;

reg feedback, n_feedback;	

always @(*)
begin
	n_avmm_readdatavalid = 1'b0;
	n_avmm_dataout = avmm_dataout;
	
	n_ctrl_reg = ctrl_reg;
	n_period = period;
	n_width = width;
	
	//Read
	if(avmm_read)
	begin
		case(avmm_address)
			CTRL_REG:
			begin
					n_avmm_dataout = ctrl_reg;
					n_avmm_readdatavalid = 1'b1;
			end
			PERIOD_REG_LO:
			begin
					n_avmm_dataout = period[31:0];
					n_avmm_readdatavalid = 1'b1;
			end
			PERIOD_REG_HI:
			begin
					n_avmm_dataout = period[63:32];
					n_avmm_readdatavalid = 1'b1;
			end
			WIDTH_REG_LO:
			begin
					n_avmm_dataout = width[31:0];
					n_avmm_readdatavalid = 1'b1;
			end
			WIDTH_REG_HI:
			begin
					n_avmm_dataout = width[63:32];
					n_avmm_readdatavalid = 1'b1;
			end
		endcase
	end
 //Write
	if(avmm_write) 
	begin
		case(avmm_address)
			CTRL_REG:
			begin
				n_ctrl_reg = avmm_datain[0];
			end
			PERIOD_REG_LO:
			begin
				n_period[31:0] = avmm_datain;
			end	
			PERIOD_REG_HI:
			begin
				n_period[63:32] = avmm_datain;
			end
			WIDTH_REG_LO:
			begin
				n_width[31:0] = avmm_datain;
			end
			WIDTH_REG_HI:
			begin
				n_width[63:32] = avmm_datain;
			end
		endcase	
	end
		
	if(feedback_avmm_clk)
		n_ctrl_reg = ctrl_reg & ~(32'b1<<UPDATE);
end

always @(posedge avmm_clk or posedge avmm_reset)
begin
	if(avmm_reset)
	begin		 
		ctrl_reg	<= 0;
		period		<= 64'b0;
		width		<= 64'b0;
	
		avmm_dataout <= 32'd0;
		avmm_readdatavalid <= 1'b0;
	end
	else
	begin
		ctrl_reg	<= n_ctrl_reg;
		period 		<= n_period;	
		width		<= n_width;
		
		avmm_dataout      <= n_avmm_dataout;
		avmm_readdatavalid <= n_avmm_readdatavalid;
	end
end

//clk domain

data_sync update_sync(
	.data		(ctrl_reg[UPDATE]),
	.dest_clk	(clk),
	.dest_rstn	(~reset),
	.data_sync	(update_clk)
);	

//Feedback
data_sync fb_sync(
	.data		(feedback),
	.dest_clk	(avmm_clk),
	.dest_rstn	(~avmm_reset),
	.data_sync	(feedback_avmm_clk)
);	

always @(*)
begin
	n_update = 1'b0;
	n_feedback = feedback;
	//Positive edge detection
	if( ~update_clk_r & update_clk )
		n_update = 1'b1;
	//Feedback
	if( update_clk & update_clk_r )
		n_feedback = 1'b1;
	//Negative edge detection. Feedback deassertion.
	if( update_clk_r & ~update_clk )
		n_feedback = 1'b0;
end		

always @(posedge clk or posedge reset)
begin
	if(reset)
	begin
		update_clk_r	<= 1'b0;
		
		update			<= 1'b0;
		feedback		<= 1'b0;
	end
	else
	begin
		update_clk_r	<= update_clk;
		
		update			<= n_update;
		feedback		<= n_feedback;
	end
end

endmodule