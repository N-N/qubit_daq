//Interface to Cypress EZ-USB FX3 chip
//N_N
module fx3_intf
(
    input clk,
    input reset,
    //DDR memory afi clk and reset:
    input ddr_clk,
    input ddr_rst,
    
    // Control and status AVMM interface
    input           csr_write,
    input           csr_read,
    input  [4:0]    csr_address,
    input  [31:0]   csr_datain,
    output          csr_readdatavalid,
    output [31:0]   csr_dataout,
    
    //AVMM interface for DDR memory
    input           ddr_waitrequest,
    output          ddr_read,
    input  [511:0]  ddr_readdata,
    input           ddr_readdatavalid,
    output [25:0]   ddr_address,
    output [7:0]    ddr_burstcount,
    output [511:0]  ddr_writedata,
    output          ddr_write,
	 output	[63:0]	ddr_byteenable,
    
    //AVMM interface for onchip memory
    output          onchip_read,
    input  [31:0]   onchip_readdata,
    input           onchip_readdatavalid,
    input           onchip_waitrequest,
    output [31:0]   onchip_address,
    output [31:0]   onchip_writedata,
    output          onchip_write,

    //FX3 GPIF II interface
    output [1:0]    fx3_addr,
    inout  [31:0]   fx3_data,
    output          fx3_pktend_n,
    output          fx3_slcs_n,
    output          fx3_sloe_n,
    output          fx3_slrd_n,
    output          fx3_slwr_n,
    input           fx3_flagA,
    input           fx3_flagB,
    input           fx3_flagC,
    input           fx3_flagD,
    
    output soft_reset
    );

    wire wr, rd;
    
    wire [31:0] lenght, base_address;
    
    wire busy_read, busy_write, busy_onchip_read, busy_onchip_write;
    
    wire pathsel; //AVMM data path selector
    localparam DDR = 0;         //DDR3
    localparam ONCHIP_MEM = 1;  //onchip memory
    
    reg rd_ddr, wr_ddr, rd_onchip, wr_onchip;
    
    wire [7:0]  ddr_burstcount_read, ddr_burstcount_write; //  from/to memory
    wire [31:0] ddr_address_read,    ddr_address_write;
    
    assign ddr_burstcount =  ddr_write ?  ddr_burstcount_write : ddr_burstcount_read;
    assign ddr_address = ddr_write ? ddr_address_write : ddr_address_read;
    
    wire [31:0] onchip_address_rd , onchip_address_wr;
    assign onchip_address = onchip_address_rd | onchip_address_wr;
    
    wire avst_sink_ready, avst_sink_valid, avst_sink_valid_ddr, avst_sink_valid_onchip;
    reg [31:0] avst_sink_data;
    wire [31:0] avst_sink_data_ddr, avst_sink_data_onchip;
    
    wire avst_src_ready, avst_src_valid, avst_src_ready_ddr, avst_src_ready_onchip;
    wire [31:0] avst_src_data;
    
    wire abort, abort_n_ddr_clk;
    
    wire [31:0] avst_to_avmm_ddr_wrusedw, ddr_rd_rdusedw;
    
    wire pktend;
    
    //Data path mux
    always @(*) begin
        rd_ddr = 0;
        wr_ddr = 0;
        rd_onchip = 0;
        wr_onchip = 0;
        case (pathsel)
            DDR: begin
                rd_ddr = rd;
                wr_ddr = wr;
            end
            ONCHIP_MEM: begin
                rd_onchip = rd;
                wr_onchip = wr;
            end
        endcase
    end
    
    //Control and status registers
    fx3_intf_csr    csr
    (
        .clk                 (clk),
        .reset               (reset),
        //AVMM control interface
        .avmm_write          (csr_write),
        .avmm_read           (csr_read),
        .avmm_address        (csr_address),
        .avmm_datain         (csr_datain),
        .avmm_readdatavalid  (csr_readdatavalid),
        .avmm_dataout        (csr_dataout),
        //Control signals
        .rd                  (rd),            //Memory read start pulse
        .wr                  (wr),            //Memory write start pulse
        .mem_addr            (base_address),    
        .lenght              (lenght),
        .soft_reset          (soft_reset), 
        .pathsel             (pathsel),       //Data path selector
        .abort               (abort),    
        //Status signals
        .ddr_wr_busy         (busy_write),
        .ddr_rd_busy         (busy_read), //| busy_onchip_read | busy_onchip_write),
        .wrusedw_ddr_wr      (avst_to_avmm_ddr_wrusedw),
        .wrusedw_ddr_rd      (ddr_rd_rdusedw),
        .onchip_wr_busy      (busy_onchip_write),
        .onchip_rd_busy      (busy_onchip_read)    
        
        );

    assign avst_sink_valid = avst_sink_valid_ddr | avst_sink_valid_onchip;
    assign avst_src_ready = avst_src_ready_ddr | avst_src_ready_onchip;
    
    always @(*) begin
        avst_sink_data = avst_sink_data_ddr;
        if (avst_sink_valid_onchip) begin
            avst_sink_data = avst_sink_data_onchip;
        end
    end
    
    //GPIF II to AVST converter
    gpif_to_avst gpif_to_avst_inst
    (
        .clk                 (clk),
        .reset               (reset|abort),
        //AVST SOURCE 
        .src_data            (avst_src_data),
        .src_valid           (avst_src_valid),
        .src_ready           (avst_src_ready),
        //AVST SINK    
        .sink_data           (avst_sink_data),
        .sink_valid          (avst_sink_valid),
        .sink_ready          (avst_sink_ready),
        //FX3 GPIF II
        .fx3_data            (fx3_data),
        .fx3_addr            (fx3_addr),
        .fx3_flagA           (fx3_flagA),
        .fx3_flagB           (fx3_flagB),
        .fx3_flagC           (fx3_flagC),
        .fx3_flagD           (fx3_flagD),
        .fx3_slcs_n          (fx3_slcs_n),
        .fx3_slwr_n          (fx3_slwr_n),
        .fx3_sloe_n          (fx3_sloe_n),
        .fx3_slrd_n          (fx3_slrd_n),
        .fx3_pktend_n        (fx3_pktend_n),
        
        .pktend (pktend)
    
    ); 
    //DDR read
    avmm_to_avst_fifo avmm_to_avst_ddr
    (
    
        .avst_clk            (clk),
        .avst_rstn           ((!reset) | (!abort)),
        
        .avmm_clk            (ddr_clk),
        .avmm_rstn           ((!ddr_rst) | abort_n_ddr_clk),
        //AVST source
        .avst_valid          (avst_sink_valid_ddr),
        .avst_data           (avst_sink_data_ddr),
        .avst_ready          (avst_sink_ready),
        //AVMM master
        .avmm_waitrequest    (ddr_waitrequest),
        .avmm_read           (ddr_read),
        .avmm_readdata       (ddr_readdata),
        .avmm_readdatavalid  (ddr_readdatavalid),
        .avmm_address        (ddr_address_read),    
        .avmm_burstcount     (ddr_burstcount_read),
        //Control signals
        .start               (rd_ddr),
        .base_address        (base_address),
        .lenght              (lenght),
        .busy                (busy_read),
        .rdusedw            (ddr_rd_rdusedw)
    
    );    
    //DDR write
    avst_dcdma_wr 
    #(  .AVST_DATA_WIDTH(32), 
        .AVMM_ADDR_WIDTH(26), 
		.AVMM_DATA_WIDTH(512),
		.AVMM_BURST_WIDTH(4),
		.FIFO_DEPTH(128)	//In avst words
        )
	 avst_to_avmm_ddr
    (
        .avst_clk            (clk),
        .avst_rst           (reset | abort),
        
        .avmm_clk            (ddr_clk),
        .avmm_rst            (ddr_rst | !abort_n_ddr_clk),
        //AVST sink
        .avst_valid          (avst_src_valid),
        .avst_data           (avst_src_data),
        .avst_ready          (avst_src_ready_ddr),
        //AVMM master
        .avmm_waitrequest    (ddr_waitrequest),
        .avmm_write          (ddr_write),
        .avmm_writedata      (ddr_writedata),
        .avmm_address        (ddr_address_write),    
        .avmm_burstcount     (ddr_burstcount_write),
		.avmm_byteenable	 (ddr_byteenable),
        //Control signals
        .start               (wr_ddr),
        .start_addr          (base_address),    
        .len                 (lenght),
        .busy                (busy_write),
        .wrusedw             (avst_to_avmm_ddr_wrusedw)
    );

    //Onchip memory read
    read_master  read_onchip
    (
        .clk                (clk),
        .rst                (reset|abort),
        //AVST source
        .avst_valid         (avst_sink_valid_onchip),
        .avst_data          (avst_sink_data_onchip),
        .avst_ready         (avst_sink_ready),
        //AVMM master
        .avmm_read          (onchip_read),
        .avmm_readdata      (onchip_readdata),
        .avmm_address       (onchip_address_rd),
        .avmm_readdatavalid (onchip_readdatavalid),
        .avmm_waitrequest   (onchip_waitrequest),
        //Control signals
        .start              (rd_onchip), 
        .base_address       (base_address),
        .len                (lenght),  
        .busy               (busy_onchip_read),
        .pktend             (pktend)
    );
    //Onchip memory write
    avst_to_avmm  avst_avmm_onchip
    (
        .clk                (clk),
        .rst                (reset|abort),
        //AVST sink
        .avst_valid         (avst_src_valid),
        .avst_data          (avst_src_data),
        .avst_ready         (avst_src_ready_onchip),
        //AVMM master
        .avmm_write         (onchip_write),
        .avmm_writedata     (onchip_writedata),
        .avmm_address       (onchip_address_wr),
        //Control signals
        .start              (wr_onchip),
        .base_address       (base_address),
        .len                (lenght),
        .busy               (busy_onchip_write)
    );
    
    
    reset_sync #( .SYNC_RST_POLARITY("ACTIVE_LOW")) 
    abort_sync
    (
        .clk            (ddr_clk),
        .rstn           (!abort),
        .clk_rst_sync   (abort_n_ddr_clk)
    );

endmodule