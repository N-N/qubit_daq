//Avalon streaming sink to memory mapped master converter
//N_N

module avst_to_avmm_fifo #( parameter 	AVST_DATA_WIDTH=32, 
								AVMM_ADDR_WIDTH = 32, 
								AVMM_DATA_WIDTH=512,
								AVMM_BURST_WIDTH=4, 
								AVMM_BURSTCOUNT = 8,
								FIFO_DEPTH = 64	//In avst words
								)
(

	input wire 			avst_clk,
	input wire 			avst_rstn,
	
	input wire 			avmm_clk,
	input wire 			avmm_rstn,
	
	//AVST sink
	input wire 							avst_valid,
	input wire [AVST_DATA_WIDTH-1:0] 	avst_data,
	output reg 							avst_ready,
	
	//AVMM master
	input wire 							avmm_waitrequest,
	output wire 						avmm_write,
	output wire [AVMM_DATA_WIDTH-1:0] 	avmm_writedata,
	output reg [AVMM_ADDR_WIDTH-1:0] 	avmm_address,	
	output reg [AVMM_BURST_WIDTH-1:0]	avmm_burstcount,
	
	
	
	//Control signals
	input wire 							start, // pulse
	input wire [AVMM_ADDR_WIDTH-1:0]	base_address,
	input wire [31:0]					lenght,
	output reg 							busy

);

//assign	avmm_burstcount = AVMM_BURSTCOUNT;

//AVST state machine
parameter IDLE = 'd0, START_AVMM = 'd1, AVST_WRITE = 'd2, WAIT_AVMM_DONE = 'd3;
reg [1:0]avst_state, n_avst_state;

//AVMM state machine
parameter AVMM_WRITE = 'd1, AVMM_REST  = 'd2;
reg [1:0] avmm_state, n_avmm_state;

//Data counters
reg [31:0] avmm_data_cnt, n_avmm_data_cnt;

//AVMM
reg n_avmm_write_r, avmm_write_r;
reg [AVMM_BURST_WIDTH-1:0] n_avmm_burstcount;
reg [AVMM_ADDR_WIDTH-1:0] n_avmm_address;

assign avmm_write = avmm_write_r && (|avmm_data_cnt);//Block write if data counter is 0

//Control
reg n_busy;
reg avmm_start, n_avmm_start;
wire avmm_start_avmm_clk;

wire avmm_busy_avst_clk; 
reg avmm_busy, n_avmm_busy;

reg rest_flag, n_rest_flag;

wire [31:0] len_burst, len_rest;
//Lenght devideble by AVMM_BURSTCOUNT
assign len_burst = {lenght[31:$clog2(AVMM_BURSTCOUNT)],  {$clog2(AVMM_BURSTCOUNT){1'b0}} };
//Rest from devision lenght by AVMM_BURSTCOUNT
assign len_rest = {{(32-$clog2(AVMM_BURSTCOUNT)){1'b0}}, lenght[$clog2(AVMM_BURSTCOUNT)-1:0]};

//FIFO
wire fifo_wr_en, fifo_rd, fifo_wrfull, fifo_rdempty, fifo_aclr;
reg fifo_wr, fifo_clr, n_fifo_clr, fifo_rd_en , n_fifo_rd_en, fifo_force_rd, n_fifo_force_rd;
assign fifo_aclr = !avst_rstn | fifo_clr;

always @(*)
begin
	n_avst_state = avst_state;
	avst_ready = 1'b0;
	n_busy = busy;
	n_avmm_start = avmm_start;
	n_fifo_clr = 1'b0;
	fifo_wr=1'b0;
	
	case (avst_state)
		IDLE:
		begin
			if (start)
			begin
				//We start with fifo clear cycle
				n_busy = 1'b1;
				n_fifo_clr = 1'b1;
				n_avst_state = START_AVMM;
			end
		end
		
		START_AVMM:
		begin
			n_avmm_start = 1'b1;
			if(avmm_busy_avst_clk)
			begin	
				n_avmm_start = 1'b0;
				n_avst_state = AVST_WRITE;
			end
			else
			begin
				n_avst_state = START_AVMM;
			end
		end
		
		AVST_WRITE:
		begin
			if (!avmm_busy_avst_clk)
			begin
				fifo_wr=1'b0;
				n_busy = 1'b0;
				n_avst_state = IDLE;
			end
			else if (!fifo_wrfull)
			begin
				avst_ready = 1'b1;
				if (avst_valid)
				begin
					fifo_wr=1'b1;
				end
			end	
			
		end
	endcase
end

always @(posedge avst_clk or negedge avst_rstn)
begin
	if (!avst_rstn)
	begin
		avst_state			<= IDLE;
		busy			<= 1'b0;
		fifo_clr		<= 1'b0;
		avmm_start		<= 1'b0;
	end
	else
	begin
		avst_state		<= n_avst_state;
		busy			<= n_busy;
		fifo_clr		<= n_fifo_clr;
		avmm_start		<= n_avmm_start;
	end
end

data_sync sync0(
	.data		(avmm_start),
	.dest_clk	(avmm_clk),
	.dest_rstn	(avmm_rstn),
	.data_sync	(avmm_start_avmm_clk)
);

data_sync sync1(
	.data		(avmm_busy),
	.dest_clk	(avst_clk),
	.dest_rstn	(avst_rstn),
	.data_sync	(avmm_busy_avst_clk)
);

//FIFO
dcfifo_mixed_widths	wr_fifo (
				.aclr 		(fifo_aclr),
				.data 		(avst_data),
				.rdclk 		(avmm_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avst_clk),
				.wrreq 		(fifo_wr),
				.q 			(avmm_writedata),
				.rdempty 	(fifo_rdempty),
				.wrusedw 	(),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	(fifo_wrfull));
	defparam
		wr_fifo.intended_device_family = "Arria V GZ",
		wr_fifo.lpm_numwords = FIFO_DEPTH,
		wr_fifo.lpm_showahead = "OFF",
		wr_fifo.lpm_type = "dcfifo_mixed_widths",
		wr_fifo.lpm_width = AVST_DATA_WIDTH,
		wr_fifo.lpm_widthu = $clog2(FIFO_DEPTH),
		wr_fifo.lpm_widthu_r = $clog2(FIFO_DEPTH*AVST_DATA_WIDTH/AVMM_DATA_WIDTH),
		wr_fifo.lpm_width_r = AVMM_DATA_WIDTH,
		wr_fifo.overflow_checking = "ON",
		wr_fifo.rdsync_delaypipe = 5,
		wr_fifo.read_aclr_synch = "OFF",
		wr_fifo.underflow_checking = "ON",
		wr_fifo.use_eab = "ON",
		wr_fifo.write_aclr_synch = "OFF",
		wr_fifo.wrsync_delaypipe = 5;
		
assign fifo_rd = (((!avmm_waitrequest) || fifo_force_rd) && !fifo_rdempty) && fifo_rd_en;
	
always @ (*)
begin
	n_avmm_write_r 	= avmm_write_r;
	n_avmm_address 	= avmm_address;
	n_avmm_data_cnt = avmm_data_cnt;
	n_avmm_burstcount = avmm_burstcount;
	
	n_fifo_rd_en 	= fifo_rd_en;
	n_fifo_force_rd = fifo_force_rd;
	
	n_avmm_busy 	= avmm_busy;
	n_rest_flag = rest_flag;
	n_avmm_state 	= avmm_state;
	
	case (avmm_state)
		IDLE:
		begin
			if ( avmm_start_avmm_clk )
			begin
				n_avmm_address = base_address;
				n_avmm_burstcount = AVMM_BURSTCOUNT;
				n_avmm_busy = 1'b1;
				n_fifo_rd_en = 1'b1;
				n_avmm_data_cnt  = len_burst;
				n_avmm_state = AVMM_WRITE;
			end	
			else
			begin
				n_avmm_busy = 1'b0;
				n_avmm_address = 'd0;
			end
		end
		
		AVMM_WRITE:
		begin
			//Stop condition
			if( avmm_data_cnt==0 )
			begin
				n_avmm_state = AVMM_REST;
				n_fifo_rd_en = 1'b0;
				n_avmm_write_r = 1'b0;
			end
			else
			begin
				//If waitrequest then read just one word from fifo,
				//set write and wait 
				if(avmm_write && avmm_waitrequest)
					n_fifo_force_rd = 1'b0;
				else if (!fifo_rdempty)
					n_fifo_force_rd = 1'b1;
				else
					n_fifo_force_rd = 1'b0;
					
				//Start mem writing when any data appears in the fifo
				if( !fifo_rdempty )
					n_avmm_write_r = 1'b1;
				else
				begin
					//Even if fifo is empty it has last data on its output
					if(!avmm_waitrequest)
						n_avmm_write_r = 1'b0;
				end	
				
				if(avmm_write && !avmm_waitrequest)
				begin
					n_avmm_address = avmm_address + 32'd1;
					n_avmm_data_cnt = avmm_data_cnt-'d1;
				end	
			end
		end
		
		AVMM_REST:
		begin
			if(len_rest == 'd0 || rest_flag)
			begin
				n_avmm_state = IDLE;
				n_rest_flag = 1'b0;
			end	
			else
			begin
				n_avmm_data_cnt  = len_rest;
				n_avmm_burstcount = len_rest;
				n_rest_flag = 1'b1;
				n_fifo_rd_en = 1'b1;
				n_avmm_state = AVMM_WRITE;
			end	
		end	
		
	endcase		
end

always @(posedge avmm_clk or negedge avmm_rstn)
begin
	if(!avmm_rstn)
	begin
		avmm_state		<= IDLE;
		avmm_address	<= 32'b0;
		avmm_data_cnt	<= 32'b0;	
		avmm_write_r	<= 1'b0;
		avmm_burstcount	<= 'd0;
		fifo_rd_en		<= 1'b0;
		fifo_force_rd	<= 1'b0;
		avmm_busy		<= 1'b0;
		rest_flag		<= 1'b0;
	end
	else if (avmm_clk)
	begin
		avmm_state	<= n_avmm_state;
		avmm_address<= n_avmm_address;
		avmm_data_cnt <= n_avmm_data_cnt;
		avmm_write_r 	<= n_avmm_write_r;
		avmm_burstcount <= n_avmm_burstcount;
		avmm_busy 	<= n_avmm_busy;
		rest_flag <= n_rest_flag;
		fifo_force_rd <= n_fifo_force_rd;
		fifo_rd_en 	<= n_fifo_rd_en;
	end
end	
		
endmodule
