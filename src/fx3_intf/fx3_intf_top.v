
//Dummy module for qsys
module fx3_bridge_top
(
    // Global clk and reset:
    input clk,
    input reset,
    // DDR clk and reset:
    input ddr_clk,
    input ddr_rst,
    
    // Control and status AVMM
    input  wire          csr_write,
    input  wire          csr_read,
    input  wire [4:0]    csr_address,
    input  wire [31:0]   csr_datain,
    output wire          csr_readdatavalid,
    output wire [31:0]   csr_dataout,
    
    //AVMM for DDR memory
    input  wire          ddr_waitrequest,
    output wire          ddr_read,
    input  wire [511:0]  ddr_readdata,
    input  wire          ddr_readdatavalid,
    output wire [25:0]   ddr_address,
    output wire [7:0]    ddr_burstcount,
    output reg  [511:0]  ddr_writedata,
    output reg           ddr_write,
    output       [63:0]  ddr_byteenable,
    
    //AVMM for onchip memory
    output wire          onchip_read,
    input  wire [31:0]   onchip_readdata,
    input                onchip_readdatavalid,
    input                onchip_waitrequest,   
    output wire [31:0]   onchip_address,
    output reg  [31:0]   onchip_writedata,
    output reg           onchip_write,


    //FX3 GPIF II
    output wire [1:0]    fx3_addr,
    inout  wire [31:0]   fx3_data,
    output wire          fx3_pktend_n,
    output wire          fx3_slcs_n,
    output wire          fx3_sloe_n,
    output wire          fx3_slrd_n,
    output wire          fx3_slwr_n,
    input  wire          fx3_flagA,
    input  wire          fx3_flagB,
    input  wire          fx3_flagC,
    input  wire          fx3_flagD,
    
    output wire soft_reset
    );

  
    //Onchip memory write
    fx3_intf  fx3_bridge_inst
    (
        // Global clk and reset:
        .clk    (clk  ),
        .reset  (reset),
        // DDR clk and reset:
        .ddr_clk    (ddr_clk),
        .ddr_rst    (ddr_rst),
        
        // Control and status AVMM
        .csr_write          (csr_write        ),
        .csr_read           (csr_read         ),
        .csr_address        (csr_address      ),
        .csr_datain         (csr_datain       ),
        .csr_readdatavalid  (csr_readdatavalid),
        .csr_dataout        (csr_dataout      ),
        
        //AVMM for DDR memory
        .ddr_waitrequest    (ddr_waitrequest  ),
        .ddr_read           (ddr_read         ),
        .ddr_readdata       (ddr_readdata     ),
        .ddr_readdatavalid  (ddr_readdatavalid),
        .ddr_address        (ddr_address      ),
        .ddr_burstcount     (ddr_burstcount   ),
        .ddr_writedata      (ddr_writedata    ),
        .ddr_write          (ddr_write        ),
        .ddr_byteenable     (ddr_byteenable),
        
        //AVMM for onchip memory
        .onchip_read            (onchip_read         ),
        .onchip_readdata        (onchip_readdata     ),
        .onchip_readdatavalid    (onchip_readdatavalid),
        .onchip_waitrequest     (onchip_waitrequest  ),
        .onchip_address         (onchip_address      ),
        .onchip_writedata       (onchip_writedata    ),
        .onchip_write           (onchip_write        ),
    
        //FX3 GPIF II
        .fx3_addr       (fx3_addr    ),
        .fx3_data       (fx3_data    ),
        .fx3_pktend_n   (fx3_pktend_n),
        .fx3_slcs_n     (fx3_slcs_n  ),
        .fx3_sloe_n     (fx3_sloe_n  ),
        .fx3_slrd_n     (fx3_slrd_n  ),
        .fx3_slwr_n     (fx3_slwr_n  ),
        .fx3_flagA      (fx3_flagA   ),
        .fx3_flagB      (fx3_flagB   ),
        .fx3_flagC      (fx3_flagC   ),
        .fx3_flagD      (fx3_flagD   ),
        
        .soft_reset (soft_reset)
    );

endmodule