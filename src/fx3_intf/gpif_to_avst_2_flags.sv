//This is the GPIF II master to Avalon Sreaming interfaces converter.
//AVST ready and valid signals are used to control data transfer.
//When AVST source ready is asserted, read from the GPIF slave will be perforemed.
//When AVST sink valid is asserted, then write to the GPIF slave will be perforemed.
//Only two flags A and B are used and must be configured to indicate current thread status.
//N_N

module gpif_to_avst
#(
    parameter WR_FIFO_WORDS_LEFT = 10,
    parameter RD_FIFO_WORDS_LEFT = 6
)
(
	input               clk	,
	input               reset,
	//AVST SOURCE data out READY_LATENCY = 1
    //ready signal is not fully supported: it must be continiuosly asserted during whole data transfer.
    //If sink will became not ready at some point, it will cause data loss.
    //Use FIFO to eliminate this.
	output reg [31:0] 	src_data,
	output 			    src_valid,
	input				src_ready,
	//AVST SINK	data in READY_LATENCY = 1
	input	 [31:0]		sink_data,
	input				sink_valid,
	output  reg			sink_ready,
	//FX3 GPIF II interface
	output reg	[1:0]	fx3_addr,
	inout	    [31:0]	fx3_data,
	output reg			fx3_pktend_n,
	output				fx3_slcs_n,
	output  			fx3_sloe_n,
	output reg			fx3_slrd_n,
	output reg			fx3_slwr_n,
	input				fx3_flagA,  // Write FIFO not full, 2048 words, 3 cycles delay // Read FIFO not empty, no delay
	input				fx3_flagB,  // Write FIFO not partially full, 10 words left // Read FIFO not partially empty, 6 words left
	input				fx3_flagC,  //Not used
	input				fx3_flagD,  //Not used
    input               pktend
);

   
    localparam RD_ADDR = 2'b11, WR_ADDR = 2'b00;    //Threads addresses
    localparam SOCKET_SWITCH_DLY = 3;               //Thread (socket) switch delay
    localparam B_DLY_WR = WR_FIFO_WORDS_LEFT-5;     //Flag B extra delay WR_FIFO_WORDS_LEFT-5
    localparam B_DLY_RD = RD_FIFO_WORDS_LEFT-4;     //Flag B extra delay RD_FIFO_WORDS_LEFT-4
    localparam VALID_DLY = 5;                       //AVST source valid extra delay. Constant.
    localparam VALID_LOG_SIZE = WR_FIFO_WORDS_LEFT; //Size of sink_valid memory

    localparam RD_CUT_WORDS = 2;    //Cut first 2 words at the begining of reading. 
                                    //Somehow first one is garbage and second is the last sample from previous transaction. 
    
    reg n_fx3_sloe_n, n_fx3_slrd_n, n_fx3_slwr_n;
    
    reg n_sink_ready;
    
    reg [1:0] n_fx3_addr;
    
    //Delay register. Maximal value is SOCKET_SWITCH_DLY-1 or RD_CUT_WORDS-1 whichever is greater. 
    reg [3:0] dly_r, n_dly_r;
    
    reg [VALID_DLY-1:0] valid, n_valid; //src_valid delay
    
    reg [31:0] fx3_data_r, n_fx3_data_r, fx3_data_i_r;
    
    assign fx3_slcs_n = 1'b0;
    //assign fx3_pktend_n = 1'b1;
    
    reg n_fx3_pktend_n;
    
    reg oe, n_oe;
    assign fx3_data = (oe) ? fx3_data_r : 32'bz;
    
    reg flag_A, flag_C;
    
    //Extra flag delay shift registers
    reg [B_DLY_WR-1:0] flag_B_dly_wr;
    reg [B_DLY_RD-1:0] flag_B_dly_rd;
    
    reg [2:0] sloe_dly, n_sloe_dly;
    assign fx3_sloe_n = ~sloe_dly[0];
    
    reg [VALID_LOG_SIZE-1:0] valid_log, n_valid_log;
    reg [$clog2(VALID_LOG_SIZE+1)-1:0] wr_rest_cnt, n_wr_rest_cnt;
    
    //Register all GPIF inputs and outputs to close timing !
    always @(posedge clk) begin
        flag_A          <=  fx3_flagA;
        flag_B_dly_wr   <= {fx3_flagB, flag_B_dly_wr[B_DLY_WR-1:1]};
        flag_B_dly_rd   <= {fx3_flagB, flag_B_dly_rd[B_DLY_RD-1:1]};
        src_data        <=  fx3_data_i_r;
        fx3_data_i_r    <=  fx3_data;
    end
    
    assign src_valid = valid[0];
    
    enum int unsigned {INIT = 0, IDLE, SS_DLY_WR, CHECK_AB, RD, RD_FIN, RD_CUT, WR, SS_DLY_RD, WR_REST} state, n_state;
    
    always @(*) begin
        n_state = state;
        n_fx3_addr = fx3_addr;
        n_sink_ready = 0;
        n_fx3_slwr_n = 1;
        n_fx3_slrd_n = 1;
        n_fx3_sloe_n = 1;
        n_oe = 0;
        n_valid = {1'b0,valid[VALID_DLY-1:1] & {{(VALID_DLY-3){1'b1}},src_ready,1'b1}};
        n_fx3_data_r = fx3_data_r;
        n_sloe_dly = sloe_dly>>1;
        n_dly_r = dly_r;
        n_fx3_pktend_n = 1;
        n_valid_log = valid_log>>1;
        n_wr_rest_cnt = wr_rest_cnt;
        
        case (state)
        
            INIT: begin
                n_fx3_addr = WR_ADDR;
                n_dly_r = 1<<SOCKET_SWITCH_DLY;
                n_state = SS_DLY_WR;
            end
            
            /*2-cycle socket switch delay*/
            SS_DLY_WR: begin
                n_dly_r = dly_r >>1;
                if( dly_r[0] )
                    n_state = IDLE;
            end
        
            IDLE: begin
                n_valid_log = 0;
                n_wr_rest_cnt = VALID_LOG_SIZE;
                //Start writing    
                if ( sink_valid && sink_ready ) begin
                    n_fx3_data_r = sink_data; 
                    n_fx3_slwr_n = 0;
                    n_fx3_pktend_n = ~pktend;
                    n_oe = 1;
                    n_valid_log[VALID_LOG_SIZE-1] = 1;
                    n_wr_rest_cnt = wr_rest_cnt - 1;
                    if (!pktend)    
                        n_state = WR;
                //Start reading
                end else if ( src_ready && fx3_slwr_n) begin
                    n_fx3_addr = RD_ADDR;
                    n_dly_r = 1<<SOCKET_SWITCH_DLY;
                    n_state = SS_DLY_RD;
                end
                
                if(flag_A && (&flag_B_dly_wr) && (!src_valid) && (!sloe_dly[0])) begin
                    n_sink_ready = 1;
                end    
                
            end
            
            /*2-cycle socket switch delay*/
            SS_DLY_RD: begin
                n_dly_r = dly_r >>1;
                if( dly_r[0] )
                    n_state = CHECK_AB;
            end
            
            CHECK_AB: begin
                if (src_ready) begin
                    if( flag_A ) begin
                        n_dly_r = 1<<(RD_CUT_WORDS-1);
                        n_state = RD_CUT; 
                    end    
                end else begin
                    n_state = INIT;
                end    
            end
            
            /*
            *Throw out first two words at the beginning of read because 
            *they are garbage for some reason.
            */
            RD_CUT: begin
                 n_sloe_dly[2] = 1;
                 n_fx3_slrd_n = 0;
                 n_dly_r = dly_r>>1;
                 if(dly_r[0])    
                    n_state = RD;
            end
             
            //Reading from the GPIF
            RD: begin
                n_sloe_dly[2] = 1;
                n_fx3_slrd_n = 0;
                n_valid[VALID_DLY-1] = 1;
                if( !flag_B_dly_rd[0] )
                    n_state = RD_FIN;
            end

            //Wait for delayed words to arrive at the end of reading.
            RD_FIN: begin
                if (!valid[0])
                    n_state = INIT;
            end
           
            WR:begin
                if(sink_valid) begin 
                    n_fx3_slwr_n = 0;
                    n_fx3_pktend_n = ~pktend;
                    n_oe = 1;
                    n_fx3_data_r = sink_data;
                    n_valid_log[VALID_LOG_SIZE-1] = 1;    
                end
                
                if(!valid_log[0] & sink_valid) 
                    n_wr_rest_cnt = wr_rest_cnt-1;
                if( valid_log[0] & !sink_valid) 
                    n_wr_rest_cnt = wr_rest_cnt+1;
                
                if(!flag_B_dly_wr[0])
                    n_state = WR_REST;
                
                if(pktend)
                    n_state = IDLE;
                   
                if( flag_B_dly_wr[1] )
                    n_sink_ready = 1;  
            end
            
            WR_REST: begin
                if(sink_valid) begin 
                    n_fx3_slwr_n = 0;
                    n_fx3_pktend_n = ~pktend;
                    n_oe = 1;
                    n_fx3_data_r = sink_data;
                    n_wr_rest_cnt = wr_rest_cnt-1;    
                end
                
                if(pktend || (wr_rest_cnt == 0) )
                    n_state = IDLE;
                    
                if (wr_rest_cnt > 2)
                    n_sink_ready = 1;
                else if( !(((wr_rest_cnt == 1) && sink_valid) || (wr_rest_cnt == 0)) )
                    n_sink_ready = ~sink_ready;
            end
            
        endcase
    end
    
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            state       <= INIT;
            sink_ready  <= 0;
            valid       <= 0;
            sloe_dly    <= 0;
            oe          <= 0;
        end else begin
            state        <= n_state;
            
            sink_ready   <= n_sink_ready;
            valid        <= n_valid;
            
            fx3_addr     <= n_fx3_addr;         
            fx3_slwr_n   <= n_fx3_slwr_n;
            fx3_slrd_n   <= n_fx3_slrd_n;
            sloe_dly     <= n_sloe_dly;
            fx3_pktend_n <= n_fx3_pktend_n;
            
            fx3_data_r   <= n_fx3_data_r;
            oe           <= n_oe;
            dly_r        <= n_dly_r;
            valid_log    <= n_valid_log;
            wr_rest_cnt  <= n_wr_rest_cnt;
        end
    end
endmodule