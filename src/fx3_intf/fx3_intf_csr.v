//Control and status registers for fx3 interface
//read: no wait cycles
//write: no wait cycles
//N_N
module fx3_intf_csr #(parameter ADDR_WIDTH = 5)
(
	input clk,
	input reset,
	
	// Avalon Memory Mapped Slave Interface: used to read/write Registers
	input wire						avmm_write,
	input wire						avmm_read,
	input wire  [ADDR_WIDTH-1:0]	avmm_address,
 	input wire  [31:0]				avmm_datain,
	output reg						avmm_readdatavalid,
	output reg  [31:0]				avmm_dataout,
	
	//Control signals
	output wire	rd,//Memory read start pulse
	output wire wr,//Memory write start pulse
	output wire [31:0] mem_addr,
	output wire [31:0] lenght,
	output reg soft_reset,
    output wire pathsel,     //AVMM data path selector
                            //0 - DDR3
                            //1 - RAM inside different modules
    output abort,                        
	//Status signals
	input 	ddr_wr_busy,
    input 	ddr_rd_busy,
    input [31:0] wrusedw_ddr_wr,
    input [31:0] wrusedw_ddr_rd,
    input   onchip_wr_busy,
    input   onchip_rd_busy
	);

    //Register map	
    localparam CTRL_REG_ADDR =  'd0; //R/W Control register
    localparam LEN_REG_ADDR =   'd4;	//R/W
    localparam ADDR_REG_ADDR =  'd8;	//R
    localparam FIRMWARE_CRC32 = 'd12; //R/W Firmware identification
    localparam FIFO_LVL_DDR_WR = 'd16;  
    localparam FIFO_LVL_DDR_RD = 'd20;     
    
    //Control register bits
    localparam START	= 	0; 	//W		Start transfer. Self cleaning.
    localparam WRITE	=	1; 	//R/W	Mode 1-write, 0 - read
    localparam ABORT	=	2;	//W 	Self cleaning.
    localparam DDR_WR_BUSY		= 	3;	//R		Control locked when asserted.
    localparam DDR_RD_BUSY		= 	4;	//R		Control locked when asserted.
    localparam RESET    =   5;  //W     Soft reset
    localparam PATH     =   6;  //R/W   AVMM data path selector
    localparam ONCHIP_WR_BUSY = 7;
    localparam ONCHIP_RD_BUSY = 8;
   
    
    //Controls register bit write enable
    localparam CTRL_WR_MASK = 1<<START | 1<<WRITE | 1<<PATH | 1<<RESET | 1<<ABORT;
    //Controls register bit write enable.
    localparam CTRL_BUSY_WR_MASK = 1<<ABORT| 1<<RESET;
    
    //Register file
    reg [31:0] ctrl_reg, n_ctrl_reg;//Control register
    reg [31:0] len_reg, n_len_reg;	//Data lenght in words R/W
    reg [31:0]	addr_reg, n_addr_reg; //
    reg [31:0]	firmware_crc32, n_firmware_crc32;
    reg [31:0]	wrusedw_ddr_wr_max, n_wrusedw_ddr_wr_max;
    reg [31:0]	wrusedw_ddr_rd_max, n_wrusedw_ddr_rd_max;
    
    reg n_avmm_readdatavalid; 
    reg [31:0] n_avmm_dataout;
    //Output assignments
    assign lenght 	= len_reg;
    assign mem_addr = addr_reg;
    assign rd = ctrl_reg[START] && !ctrl_reg[WRITE];
    assign wr = ctrl_reg[START] && ctrl_reg[WRITE];
    assign pathsel  = ctrl_reg[PATH];
    assign abort = ctrl_reg[ABORT];
    wire busy;
    assign busy = ddr_wr_busy|ddr_rd_busy;
    
    always @(*) begin
        n_avmm_readdatavalid = 0;
        n_avmm_dataout = avmm_dataout;
        n_len_reg = len_reg;
        n_addr_reg = addr_reg;
        n_firmware_crc32 = firmware_crc32;
        n_wrusedw_ddr_wr_max = wrusedw_ddr_wr_max;
        n_wrusedw_ddr_rd_max = wrusedw_ddr_rd_max;
        n_ctrl_reg = ctrl_reg;
        n_ctrl_reg[DDR_WR_BUSY] = ddr_wr_busy;
        n_ctrl_reg[DDR_RD_BUSY] = ddr_rd_busy;
        n_ctrl_reg[ONCHIP_WR_BUSY] = onchip_wr_busy;
        n_ctrl_reg[ONCHIP_RD_BUSY] = onchip_rd_busy;
    
        //Read
        if(avmm_read) begin
            case(avmm_address)
                CTRL_REG_ADDR: begin
                        n_avmm_dataout = ctrl_reg;
                        n_avmm_readdatavalid = 1'b1;
                end
                LEN_REG_ADDR: begin
                        n_avmm_dataout = len_reg;
                        n_avmm_readdatavalid = 1'b1;
                end
                ADDR_REG_ADDR: begin
                        n_avmm_dataout = addr_reg;
                        n_avmm_readdatavalid = 1'b1;
                end
                FIRMWARE_CRC32: begin
                        n_avmm_dataout = firmware_crc32;
                        n_avmm_readdatavalid = 1'b1;
                end
                FIFO_LVL_DDR_WR: begin
                        n_avmm_dataout = wrusedw_ddr_wr_max;
                        n_avmm_readdatavalid = 1'b1;
                end
                FIFO_LVL_DDR_RD: begin
                        n_avmm_dataout = wrusedw_ddr_rd_max;
                        n_avmm_readdatavalid = 1'b1;
                end
            endcase
        end
        //Write
        if(avmm_write) begin
            case(avmm_address)
                CTRL_REG_ADDR: begin
                    if(busy)
                        n_ctrl_reg = avmm_datain & CTRL_BUSY_WR_MASK;
                    else	
                        n_ctrl_reg = avmm_datain & CTRL_WR_MASK;
                end
                LEN_REG_ADDR: begin
                    if(!busy)
                        n_len_reg = avmm_datain;
                end	
                ADDR_REG_ADDR: begin
                    if(!busy)
                        n_addr_reg = avmm_datain;
                end
                FIRMWARE_CRC32: begin
                    n_firmware_crc32 = avmm_datain;
                end
            endcase	
        end /*else begin
        //Status update
            n_ctrl_reg[DDR_WR_BUSY] = ddr_wr_busy;
            n_ctrl_reg[DDR_RD_BUSY] = ddr_rd_busy;
        end    */
        /*
        if(ddr_wr_busy)
            n_ctrl_reg = ctrl_reg | (1<<DDR_WR_BUSY);
        else
            n_ctrl_reg = ctrl_reg & ~(1<<DDR_WR_BUSY);
        */
        //FIFO_LVL_DDR_WR update
        if(wrusedw_ddr_wr > wrusedw_ddr_wr_max)
            n_wrusedw_ddr_wr_max = wrusedw_ddr_wr;
            
        if(wrusedw_ddr_rd > wrusedw_ddr_rd_max)
            n_wrusedw_ddr_rd_max = wrusedw_ddr_rd;    
    
        //Pulse generation
        if(ctrl_reg & (1<<START) )
            n_ctrl_reg = ctrl_reg & ~(1<<START);
        if(ctrl_reg & (1<<ABORT) )
            n_ctrl_reg = ctrl_reg & ~(1<<ABORT);
    end
    
    //Soft reset generation
    always @(posedge clk) begin
        if(ctrl_reg[RESET]) begin
            soft_reset <= 1;
        end else begin
            soft_reset <= 0;
        end
    end

    always @(posedge clk or posedge reset) begin
        if(reset) begin		 
            ctrl_reg	<= 0;
            len_reg		<= 0;
            addr_reg	<= 0;
            wrusedw_ddr_wr_max <= 0;
            wrusedw_ddr_rd_max <= 0;
        end else begin
            ctrl_reg 	<= n_ctrl_reg;
            len_reg 	<= n_len_reg;
            addr_reg	<= n_addr_reg;	
            avmm_dataout      <= n_avmm_dataout;
            avmm_readdatavalid <= n_avmm_readdatavalid;
            firmware_crc32 <= n_firmware_crc32; //Do not reset!
            wrusedw_ddr_wr_max <= n_wrusedw_ddr_wr_max;
            wrusedw_ddr_rd_max <= n_wrusedw_ddr_rd_max;
        end
    end

endmodule