//Dual clock writing DMA
//with unaligned adressing in AVST address units
//Burstcount is fixed to 1
//N_N

module avst_dcdma_wr #( parameter 	AVST_DATA_WIDTH=256, 
								AVMM_ADDR_WIDTH = 32, 
								AVMM_DATA_WIDTH=512,
								AVMM_BURST_WIDTH=4, //Burstcount port width
								FIFO_DEPTH = 128	//In avst words
								)
(

	input wire 			                avst_clk,
	input wire 			                avst_rst,
	
	input wire 			                avmm_clk,
	input wire 			                avmm_rst,
	
	//AVST sink
	input wire 							avst_valid,
	input wire [AVST_DATA_WIDTH-1:0] 	avst_data,
	output reg 							avst_ready,
	
	//AVMM master
	input wire 							avmm_waitrequest,
	output reg 						    avmm_write,
	output wire [AVMM_DATA_WIDTH-1:0] 	avmm_writedata,
	output wire [AVMM_ADDR_WIDTH-1:0] 	avmm_address,	
	output reg [AVMM_BURST_WIDTH-1:0]	avmm_burstcount,
    output reg [AVMM_DATA_WIDTH/8-1:0]  avmm_byteenable,
	
	//Control signals
	input 							    start,      //Start trigger              
	input [31:0]	                    start_addr, //Start address in AVST words
	input [31:0]					    len,        //Data length in AVST words
	output reg 							busy,       //AVMM transfer is not done
    output reg                          ready,      //Ready to start
    
    output [$clog2(FIFO_DEPTH)-1:0] wrusedw         //FIFO write level

);

localparam ADDR_FIFO_DEPTH = FIFO_DEPTH/(AVMM_DATA_WIDTH/AVST_DATA_WIDTH);

//AVMM
assign avmm_burstcount = 1;
reg n_avmm_write;
reg [AVMM_ADDR_WIDTH-1:0] avmm_addr, n_avmm_addr;

//Data counters
reg [31:0] avst_data_cnt, n_avst_data_cnt;
reg [$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH):0] pad_start_cnt, n_pad_start_cnt;
reg [$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH):0] pad_end_cnt, n_pad_end_cnt;

reg avst_word_en;

//Control
reg n_busy;
reg n_ready;

wire [$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH)-1:0] pad_start; 
assign pad_start = start_addr[$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH)-1:0];

//FIFO
wire fifo_wrfull, fifo_rdempty, addr_fifo_wrfull, addr_fifo_rdempty;
reg fifo_wr, fifo_rd;
reg addr_fifo_wr;

wire [AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1:0][AVST_DATA_WIDTH/8-1:0]avmm_byteenable_dec;
wire [AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1:0][AVST_DATA_WIDTH:0] fifo_rd_data ;
wire [AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1:0][AVST_DATA_WIDTH-1:0] avmm_data_dec;
assign avmm_byteenable = avmm_byteenable_dec;
assign avmm_writedata = avmm_data_dec;

//avmm_busy synchronizer
reg [2:0] avmm_busy_sync;
reg avmm_busy;
always @(posedge avst_clk) begin
    avmm_busy_sync <= {avmm_busy, avmm_busy_sync[2:1]};
end

//AVST state machine
enum int unsigned {AVST_IDLE = 0, AVST_PAD_START = 1, AVST_WR = 2, AVST_PAD_END = 3} avst_state, n_avst_state;
always @(*) begin
	n_avst_state = avst_state;
	avst_ready = 1'b0;
	n_busy = busy;
	fifo_wr=1'b0;
    n_avst_data_cnt = avst_data_cnt;
    n_pad_start_cnt = pad_start_cnt;
    n_pad_end_cnt = pad_end_cnt;
    avst_word_en = 1;
    n_avmm_addr = avmm_addr;
    addr_fifo_wr = 0;
    n_ready = 0;
	
	case (avst_state)
		AVST_IDLE: begin
			if (start) begin
				//We start with fifo clear cycle
				n_busy = 1'b1;
                n_avst_data_cnt = len;
                n_pad_start_cnt = pad_start;
                n_avmm_addr = start_addr>>$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH);
                n_pad_end_cnt = AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1;
                if(pad_start == 0)
                    n_avst_state = AVST_WR;
                else    
                    n_avst_state = AVST_PAD_START;
			end else begin
                n_ready = 1;
                n_busy = (|wrusedw) | avmm_busy_sync[0];
            end
		end
        
        AVST_PAD_START:begin
            if ((!fifo_wrfull) && (!addr_fifo_wrfull)) begin
                fifo_wr=1'b1;
                avst_word_en = 0;
                n_pad_start_cnt = pad_start_cnt - 1;
                n_pad_end_cnt = pad_end_cnt - 1;
                if (pad_start_cnt == 1 )
                    n_avst_state = AVST_WR;
			end	
        end

		AVST_WR: begin
            if(avst_data_cnt == 0) begin
                n_avst_state = AVST_PAD_END;
            end else if ((!fifo_wrfull) && (!addr_fifo_wrfull)) begin
				avst_ready = 1'b1;
				if (avst_valid) begin
					fifo_wr=1'b1;
                    n_avst_data_cnt = avst_data_cnt - 1;
                    if(pad_end_cnt == 0) begin
                        n_pad_end_cnt = AVMM_DATA_WIDTH/AVST_DATA_WIDTH-1;
                        n_avmm_addr = avmm_addr+1;
                        addr_fifo_wr = 1;
                    end else begin
                        n_pad_end_cnt = pad_end_cnt - 1;
                    end       
                end
			end	
		end
        
        AVST_PAD_END: begin
			if ((!fifo_wrfull) && (!addr_fifo_wrfull)) begin
                fifo_wr=1'b1;
                avst_word_en = 0;
                if (pad_end_cnt == 0 ) begin
                    n_avmm_addr = avmm_addr+1;
                    addr_fifo_wr = 1;
                    n_avst_state = AVST_IDLE;
                end else begin
                    n_pad_end_cnt = n_pad_end_cnt - 1;
                end    
			end	
		end
	endcase
end

always @(posedge avst_clk or posedge avst_rst) begin
	if (avst_rst) begin
		avst_state		<=  AVST_IDLE;
		busy			<=  0;
        ready           <=  0;
	end else begin
		avst_state		<= n_avst_state;
		busy			<= n_busy;
        ready           <= n_ready;
        avst_data_cnt   <= n_avst_data_cnt;
        pad_start_cnt   <= n_pad_start_cnt;
        pad_end_cnt     <= n_pad_end_cnt;
        avmm_addr       <= n_avmm_addr;
	end
end

//FIFO
dcfifo_mixed_widths	wr_fifo (
				.aclr 		(avst_rst),
				.data 		({avst_data, avst_word_en}),
				.rdclk 		(avmm_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avst_clk),
				.wrreq 		(fifo_wr),
				.q 			(fifo_rd_data),
				.rdempty 	(fifo_rdempty),
				.wrusedw 	(wrusedw),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	(fifo_wrfull));
	defparam
		wr_fifo.intended_device_family = "Arria V GZ",
		wr_fifo.lpm_numwords = FIFO_DEPTH,
		wr_fifo.lpm_showahead = "OFF",
		wr_fifo.lpm_type = "dcfifo_mixed_widths",
		wr_fifo.lpm_width = AVST_DATA_WIDTH+1,
		wr_fifo.lpm_widthu = $clog2(FIFO_DEPTH),
		wr_fifo.lpm_widthu_r = $clog2(FIFO_DEPTH*AVST_DATA_WIDTH/AVMM_DATA_WIDTH),
		wr_fifo.lpm_width_r = AVMM_DATA_WIDTH+AVMM_DATA_WIDTH/AVST_DATA_WIDTH,
		wr_fifo.overflow_checking = "ON",
		wr_fifo.rdsync_delaypipe = 5,
		wr_fifo.read_aclr_synch = "OFF",
		wr_fifo.underflow_checking = "ON",
		wr_fifo.use_eab = "ON",
		wr_fifo.write_aclr_synch = "OFF",
		wr_fifo.wrsync_delaypipe = 5;
        
dcfifo_mixed_widths	addr_fifo (
				.aclr 		(avst_rst),
				.data 		(avmm_addr),
				.rdclk 		(avmm_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avst_clk),
				.wrreq 		(addr_fifo_wr),
				.q 			(avmm_address),
				.rdempty 	(addr_fifo_rdempty),
				.wrusedw 	(),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	(addr_fifo_wrfull));
	defparam
		addr_fifo.intended_device_family = "Arria V GZ",
		addr_fifo.lpm_numwords = ADDR_FIFO_DEPTH,
		addr_fifo.lpm_showahead = "OFF",
		addr_fifo.lpm_type = "dcfifo_mixed_widths",
		addr_fifo.lpm_width = AVMM_ADDR_WIDTH,
		addr_fifo.lpm_widthu = $clog2(ADDR_FIFO_DEPTH),
		addr_fifo.lpm_widthu_r = $clog2(ADDR_FIFO_DEPTH),
		addr_fifo.lpm_width_r = AVMM_ADDR_WIDTH,
		addr_fifo.overflow_checking = "ON",
		addr_fifo.rdsync_delaypipe = 5,
		addr_fifo.read_aclr_synch = "OFF",
		addr_fifo.underflow_checking = "ON",
		addr_fifo.use_eab = "ON",
		addr_fifo.write_aclr_synch = "OFF",
		addr_fifo.wrsync_delaypipe = 5;
		
genvar i;
generate for (i=0; i< AVMM_DATA_WIDTH/AVST_DATA_WIDTH; i=i+1) begin: avmm_wiring
    assign avmm_data_dec[i] = fifo_rd_data[i]>>1;
    assign avmm_byteenable_dec[i] = {AVST_DATA_WIDTH/8{fifo_rd_data[i][0]}};
end endgenerate

wire fifo_nempty;
assign fifo_nempty = (!fifo_rdempty) && (!addr_fifo_rdempty);

//AVMM state machine
enum int unsigned {AVMM_IDLE=0, AVMM_WR =1} avmm_state, n_avmm_state;
always @(*) begin
    n_avmm_state = avmm_state;
    n_avmm_write = 0;
    fifo_rd = 0;
    avmm_busy = 0;
    
    case(avmm_state)
        AVMM_IDLE: begin
            if( fifo_nempty ) begin
                fifo_rd = 1;
                n_avmm_write = 1;
                n_avmm_state = AVMM_WR;
            end
        end	
        
        AVMM_WR: begin
            avmm_busy = 1;
            if(!avmm_waitrequest) begin
                if( !fifo_nempty ) begin
                    n_avmm_write = 0;
                    n_avmm_state = AVMM_IDLE;
                end else begin
                    fifo_rd = 1;
                    n_avmm_write = 1;
                end    
            end else begin
                n_avmm_write = 1;
            end
        end
    endcase    
end

always @(posedge avmm_clk or posedge avmm_rst) begin
	if(avmm_rst) begin	
		avmm_state <= AVMM_IDLE;
        avmm_write <= 0;
	end else begin
        avmm_state <= n_avmm_state;
        avmm_write <= n_avmm_write;
	end
end	
		
endmodule