//GPIF II master to Avalon Sreaming interfaces converter.
//AVST ready and valid signals are used to control GPIF data transfer.
//When AVST source ready is high, read from GPIF slave will be perforemed.
//When AVST sink valid is high, then write will be perforemed.
//N_N

module gpif_to_avst
(
	input               clk	,
	input               reset,
	//AVST SOURCE data out READY_LATENCY = 1
    //ready signal is not fully supported: it must be continiuosly asserted during whole data transfer.
    //If sink will became not ready at some point, it will cause data loss.
    //Use FIFO to eliminate this.
	output reg [31:0] 	src_data,
	output 			    src_valid,
	input				src_ready,
	//AVST SINK	data in READY_LATENCY = 1
    //valid signal is not fully supported: source must contineously send valid data whenever ready is asserted.
	input	 [31:0]		sink_data,
	input				sink_valid,
	output  reg			sink_ready,
	//FX3 GPIF II interface
	output reg	[1:0]	fx3_addr,
	inout	    [31:0]	fx3_data,
	output reg			fx3_pktend_n,
	output				fx3_slcs_n,
	output  			fx3_sloe_n,
	output reg			fx3_slrd_n,
	output reg			fx3_slwr_n,
	input				fx3_flagA,  // Write FIFO not full, 2048 words, 3 cycles delay
	input				fx3_flagB,  // Write FIFO not partially full, 10 words left
	input				fx3_flagC,  // Read FIFO not empty, no delay
	input				fx3_flagD,  // Read FIFO not partially empty, 6 words left
    
    input               pktend
);


    localparam RD_ADDR = 2'b11, WR_ADDR = 2'b00;  //Threads addresses
    localparam B_DLY = 5;//Flag B extra delay WR_FIFO_WORDS_LEFT-5
    localparam D_DLY = 3;//Flag D extra delay RD_FIFO_WORDS_LEFT-3
    localparam VALID_DLY = 4;//AVST source valid extra delay. Constant.

    localparam RD_CUT_WORDS = 2;    //Cut first 2 words at the begining of reading. 
                                    //Somehow first one is garbage and second is the last sample from previous transaction. 
    
    reg n_fx3_sloe_n, n_fx3_slrd_n, n_fx3_slwr_n;
    
    reg n_sink_ready;
    
    reg [1:0] n_fx3_addr;
    
    //Cut counter
    reg [RD_CUT_WORDS-1:0] rd_cut_r, n_rd_cut_r;
    
    reg [VALID_DLY-1:0] valid, n_valid; //src_valid delay
    
    reg [31:0] fx3_data_r, n_fx3_data_r;//, fx3_data_i_r;
    
    assign fx3_slcs_n = 1'b0;
    //assign fx3_pktend_n = 1'b1;
    
    reg n_fx3_pktend_n;
    
    reg oe, n_oe;
    assign fx3_data = (oe) ? fx3_data_r : 32'bz;
    
    reg flag_A, flag_C;
    
    //Extra flag delay shift registers
    reg [B_DLY-1:0] flag_B_dly;
    reg [D_DLY-1:0] flag_D_dly;
    
    reg [2:0] sloe_dly, n_sloe_dly;
    assign fx3_sloe_n = ~sloe_dly[0];
    
    //Register all GPIF inputs and outputs to close timing !
    always @(posedge clk) begin
        flag_A      <=  fx3_flagA;
        flag_C      <=  fx3_flagC;
        flag_B_dly  <= {fx3_flagB, flag_B_dly[B_DLY-1:1]};
        flag_D_dly  <= {fx3_flagD, flag_D_dly[D_DLY-1:1]};
        src_data    <=  fx3_data;
        fx3_data_r  <=  sink_data;
    end
    
    assign src_valid = valid[0];
    
    enum int unsigned {IDLE = 0, CHECK_CD = 1, RD = 2, RD_FIN = 3, WR = 4, RD_CUT = 5} state, n_state;
    
    always @(*) begin
        n_state = state;
        n_fx3_addr = fx3_addr;
        n_sink_ready = 0;
        n_fx3_slwr_n = 1;
        n_fx3_slrd_n = 1;
        n_fx3_sloe_n = 1;
        n_oe = 0;
        n_valid = {1'b0,valid[VALID_DLY-1:1] & {{(VALID_DLY-3){1'b1}},src_ready,1'b1}};
        n_sloe_dly = sloe_dly>>1;
        n_rd_cut_r = rd_cut_r;
        n_fx3_pktend_n = 1;
        
        case (state)
        
            IDLE: begin
                //Start writing    
                if ( sink_valid && sink_ready ) begin 
                    n_fx3_slwr_n = 0;
                    n_oe = 1;
                    n_fx3_pktend_n = ~pktend;
                    if (!pktend)  
                        n_state = WR;
                //Start reading
                end else if ( src_ready && fx3_slwr_n) begin
                    n_fx3_addr = RD_ADDR;
                    n_state = CHECK_CD;
                end else begin
                    n_fx3_addr = WR_ADDR;
                end
                
                if(flag_A && (&flag_B_dly) && (!src_valid) && (!sloe_dly[0])) begin
                    n_sink_ready = 1;
                end
                n_rd_cut_r = 0;
            end
            
            CHECK_CD: begin
                if (src_ready) begin
                    if(flag_C /*&& (&flag_D_dly)*/) begin
                        n_rd_cut_r = 1<<(RD_CUT_WORDS-1);
                        n_state = RD_CUT; 
                    end    
                end else begin
                    n_state = IDLE;
                end    
            end
            
            RD_CUT: begin
                 n_sloe_dly[2] = 1;
                 n_fx3_slrd_n = 0;
                 n_rd_cut_r = rd_cut_r>>1;
                 if(rd_cut_r[0])
                    n_state = RD;
            end
            
            RD: begin
                if( !flag_D_dly[0] ) begin
                    n_state = RD_FIN;
                end else begin
                    n_sloe_dly[2] = 1;
                    n_fx3_slrd_n = 0;
                    n_valid[VALID_DLY-1] = 1;
                end
            end
            
            RD_FIN: begin
                if (!valid[0])
                    n_state = IDLE;
            end
            
            WR:begin
                if(!sink_valid) begin
                    n_state = IDLE;  
                end else begin 
                    n_fx3_slwr_n = 0;
                    n_fx3_pktend_n = ~pktend;
                    n_oe = 1;
                    if(!flag_B_dly[0] | pktend) begin
                        n_state = IDLE;
                    end else if(flag_B_dly[1]) begin
                        n_sink_ready = 1;
                    end    
                end  
            end
        endcase
    end
    
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            state <= IDLE;
            sink_ready <= 0;
            valid      <= 0;
            sloe_dly   <= 0;
            oe <= 0;
        end else begin
            state <= n_state;
            
            sink_ready <= n_sink_ready;
            valid <= n_valid;
            
            fx3_addr <= n_fx3_addr;         
            fx3_slwr_n <= n_fx3_slwr_n;
            fx3_slrd_n <= n_fx3_slrd_n;
            sloe_dly <= n_sloe_dly;
            fx3_pktend_n <= n_fx3_pktend_n;
            
            oe          <= n_oe;
            rd_cut_r <= n_rd_cut_r;
        end
    end
endmodule