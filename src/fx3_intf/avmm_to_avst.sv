//Simple avalon memory mapped master to streaming source converter
//N_N
`timescale 1ps / 1ps

module avmm_to_avst 
#( parameter     
    DATA_WIDTH=32,
    ADDR_WIDTH = 32
)
(
    input wire             clk,
    input wire             rst,
    
    //AVST source, ready latency 1
    output reg                   avst_valid,
    output reg [DATA_WIDTH-1:0]  avst_data,
    input wire                   avst_ready,
           
    //AVMM master, fixed wait state, readWaitTime is 1    
    output reg                   avmm_read,
    input wire [DATA_WIDTH-1:0]  avmm_readdata,
    output reg [ADDR_WIDTH-1:0]  avmm_address,
           
    //Control signals      
    input wire                   start,        //Start trigger 
    input wire [ADDR_WIDTH-1:0]  base_address, //Start avmm reading here
    input wire [31:0]            len,          //How many words to read  
    output reg                   busy,         //Status  
    output reg                   pktend        //End of packet
);

    assign avst_data = avmm_readdata;
    
    //State machine
    enum int {IDLE = 0, READ = 1} state, n_state;
    
    reg [ADDR_WIDTH-1:0] n_avmm_address;
    reg [31:0] n_data_cnt, data_cnt;
    reg n_avst_valid;
    reg n_pktend;

    always @(*) begin
        n_state = state;            
        n_data_cnt = data_cnt;
        n_avmm_address = avmm_address;
        n_avst_valid = 0;
        n_pktend = 0;
        
        busy = 0;
        avmm_read = 0;
        
        case (state) 
            IDLE: begin
                n_avmm_address = 0;
                if (start && len) begin
                    n_avmm_address = base_address;
                    n_data_cnt = len;
                    n_state = READ;   
                end
            end      
            READ: begin
                busy = 1;
                if (data_cnt == 0) begin
                    n_state = IDLE;
                end else if (avst_ready) begin
                    n_avst_valid = 1;
                    avmm_read = 1;
                    n_data_cnt = data_cnt - 1;
                    n_avmm_address = avmm_address + 1;
                    if (data_cnt == 1)
                        n_pktend = 1;
                end
            end        
        endcase
    end
    
    always @(posedge clk or posedge rst) begin
        if (rst) begin
            state           <= IDLE;
            avst_valid      <= 0;
        end else begin
            state           <= n_state;
            data_cnt        <= n_data_cnt;
            avmm_address    <= n_avmm_address;
            avst_valid      <= n_avst_valid;
            pktend <= n_pktend;
        end
    end

endmodule