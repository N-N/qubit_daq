//Avalon streaming sink to memory mapped master converter.
//AVST Ready latency 1.
//No support for backpressure on AVMM.
//N_N

module avst_to_avmm 
#( parameter     
    DATA_WIDTH=32,
    ADDR_WIDTH = 32
)
(
    input   clk,
    input   rst,
    
    //AVST sink
    input                           avst_valid,
    input [DATA_WIDTH-1:0]          avst_data,
    output reg                      avst_ready,
    
    //AVMM master
    output reg                      avmm_write,
    output      [DATA_WIDTH-1:0]    avmm_writedata,
    output      [ADDR_WIDTH-1:0]    avmm_address,   //Byte address 
    
    //Control signals
    input                            start, //Start trigger
    input       [ADDR_WIDTH-1:0]     base_address,
    input       [31:0]               len,
    //Status signals
    output reg                       busy
);
   
    assign avmm_writedata = avst_data;
    assign avmm_address = avmm_address_r;

    //Data counter
    reg [31:0] avmm_data_cnt, n_avmm_data_cnt;

    //AVMM
    reg [ADDR_WIDTH-1:0] avmm_address_r, n_avmm_address_r;
    
    //State machine
    enum int {IDLE = 0, WRITE = 1} state, n_state;

    always @ (*) begin
        n_state           = state;
        n_avmm_address_r  = avmm_address_r;
        n_avmm_data_cnt   = avmm_data_cnt; 
        busy        = 0;
        avst_ready  = 0;
        avmm_write  = 0;
        
        case (state)
            IDLE: begin
                n_avmm_address_r = 0;
                //Start trigger
                if ( start && len ) begin
                    n_avmm_address_r = base_address;
                    n_avmm_data_cnt  = len;
                    n_state = WRITE;
                end
            end
            
            WRITE: begin
                busy = 1;
                //Stop condition
                if( avmm_data_cnt == 0 ) begin
                    n_state = IDLE;
                end else if(avst_valid) begin
                    avmm_write = 1;
                    n_avmm_address_r = avmm_address_r + DATA_WIDTH/8;
                    n_avmm_data_cnt = avmm_data_cnt -1;
                end
                if( avmm_data_cnt != 0 ) begin
                    avst_ready = 1;
                end    
                
            end
        endcase        
    end
    
    always @(posedge clk or posedge rst) begin
        if(rst) begin
            state           <= IDLE;
        end else begin
            state           <= n_state;
            avmm_address_r    <= n_avmm_address_r;
            avmm_data_cnt   <= n_avmm_data_cnt;
        end
    end    
        
endmodule