//Avalon memory mapped read master
//N_N
`timescale 1ps / 1ps

module read_master 
#( parameter     
    DATA_WIDTH = 32,
    ADDR_WIDTH = 32,
    FIFO_DEPTH = 16,
    MAX_PENDING_READS = 7
)
(
    input              clk,
    input              rst,
    
    //AVST source, ready latency 1
    output reg                   avst_valid,
    output reg [DATA_WIDTH-1:0]  avst_data,
    input                        avst_ready,
           
    //AVMM master    
    output reg                   avmm_read,
    input      [DATA_WIDTH-1:0]  avmm_readdata,
    input                        avmm_readdatavalid,   
    output reg [ADDR_WIDTH-1:0]  avmm_address,
    input                        avmm_waitrequest,
           
    //Control signals      
    input                        start,        //Start trigger 
    input      [ADDR_WIDTH-1:0]  base_address, //Base AVMM address
    input      [31:0]            len,          //Amount of data words to read  
    output reg                   busy,         //Proceas status  
    output                       pktend        //End of packet asserted together with the last AVST data word
);
    
    enum int {IDLE = 0, READ, STOP } state, n_state;
    
    reg [ADDR_WIDTH-1:0] n_avmm_address;
    reg [31:0] n_data_cnt, data_cnt;
    reg [31:0] n_rd_cnt, rd_cnt;
    reg [$clog2(MAX_PENDING_READS+1)-1:0] n_pend_rd_cnt, pend_rd_cnt;
    reg n_avst_valid;
    reg n_busy;
    reg n_avmm_read;
    
    reg fifo_full, fifo_empty, fifo_wr, fifo_rd;
    wire fifo_alm_full;
    
    reg pktend_i, pktend_o;

    assign pktend = pktend_o & avst_valid; 

    //Conditions for increment/decrement of pending reads counter
    assign pending_rd_dec = ~fifo_alm_full && (pend_rd_cnt !=0) && ~avmm_waitrequest;
    assign pending_rd_inc = avmm_readdatavalid && (pend_rd_cnt != MAX_PENDING_READS);    

    always @(*) begin
        n_state = state;            
        n_data_cnt = data_cnt;
        n_avmm_address = avmm_address;
        n_busy = busy;
        n_avmm_read = 0;
        n_rd_cnt = rd_cnt;
        n_pend_rd_cnt = pend_rd_cnt;
        
        //avmm_read = 0;
        fifo_wr = 0;
        pktend_i = 0;
        
        case (state) 
            IDLE: begin
                n_data_cnt = len;
                n_rd_cnt = len;
                n_pend_rd_cnt = MAX_PENDING_READS;
                n_busy = 0;
                n_avmm_address = 0;
                if (start && len) begin
                    n_avmm_address = base_address;
                    n_state = READ;
                    n_busy = 1;    
                end    
            end      
            
            READ: begin
                if (data_cnt == 0) begin
                    n_state = STOP;
                end else begin
                
                    if( ~fifo_alm_full && (rd_cnt != 0) && (pend_rd_cnt !=0) ) begin
                        n_avmm_read = 1;
                        if(~avmm_waitrequest ) begin
                            n_rd_cnt = rd_cnt-1;
                        end        
                    end else if (avmm_waitrequest)
                        n_avmm_read = 1;

                    if (!(pending_rd_inc && pending_rd_dec)) begin
                        if(pending_rd_inc)
                            n_pend_rd_cnt = pend_rd_cnt+1;
                        else if ( pending_rd_dec )
                            n_pend_rd_cnt = pend_rd_cnt - 1;
                    end        
                    
                    if( ~avmm_waitrequest & avmm_read) begin
                        n_avmm_address = avmm_address + DATA_WIDTH/8;
                    end
                
                    if(avmm_readdatavalid) begin
                        fifo_wr = 1;
                        if(data_cnt ==1)
                            pktend_i = 1;
                        n_data_cnt = data_cnt-1;
                    end
                end 
            end
            
            STOP: begin
                if( fifo_empty )
                    n_state = IDLE;
            end
        endcase
    end
    
    always @(*) begin
        n_avst_valid = 0;
        fifo_rd = 0;
    
         if(~fifo_empty & avst_ready) begin
            fifo_rd = 1;
            n_avst_valid = 1;
         end    
    end 
    
    always @(posedge clk or posedge rst) begin
        if (rst) begin
            state           <= IDLE;
            avst_valid      <= 0;
        end else begin
            state           <= n_state;
            data_cnt        <= n_data_cnt;
            avmm_address    <= n_avmm_address;
            avst_valid      <= n_avst_valid;
            avmm_read       <= n_avmm_read;
            busy            <= n_busy;
            rd_cnt          <= n_rd_cnt;
            pend_rd_cnt     <= n_pend_rd_cnt;
        end
    end
    
    wire [$clog2(FIFO_DEPTH)-1:0] usedw;
    
scfifo	scfifo_component (
				.clock          (clk),
				.data           ( { avmm_readdata, pktend_i } ),
				.rdreq          (fifo_rd),
				.sclr           (),
				.wrreq          (fifo_wr),
				.empty          (fifo_empty),
				.full           (fifo_full),
				.q              ( {avst_data, pktend_o} ),
				.usedw          (usedw),
				.aclr           (rst),
				.almost_empty   (),
				.almost_full    (fifo_alm_full),
				.eccstatus      ()
                );
	defparam
		scfifo_component.add_ram_output_register = "ON",
		scfifo_component.intended_device_family = "Arria V GZ",
		scfifo_component.lpm_numwords = FIFO_DEPTH,
		scfifo_component.lpm_showahead = "OFF",
		scfifo_component.lpm_type = "scfifo",
		scfifo_component.lpm_width = DATA_WIDTH+1,
		scfifo_component.lpm_widthu = $clog2(FIFO_DEPTH),
		scfifo_component.overflow_checking = "ON",
		scfifo_component.underflow_checking = "ON",
		scfifo_component.use_eab = "ON",
        scfifo_component.almost_full_value = FIFO_DEPTH-MAX_PENDING_READS-1;    

endmodule