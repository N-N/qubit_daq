# TCL File Generated by Component Editor 17.0
# Tue Jun 05 20:22:49 MSK 2018
# DO NOT MODIFY


# 
# onchip_memory2_1 "onchip_memory2_1" v1.0
#  2018.06.05.20:22:49
# 
# 

# 
# request TCL package from ACDS 16.1
# 
package require -exact qsys 16.1


# 
# module onchip_memory2_1
# 
set_module_property DESCRIPTION ""
set_module_property NAME onchip_memory2_1
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property AUTHOR ""
set_module_property DISPLAY_NAME onchip_memory2_1
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL onchip_memory2_1_top
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file onchip_memory2_1_top.v VERILOG PATH ../src/onchip_ram/onchip_memory2_1_top.v TOP_LEVEL_FILE


# 
# parameters
# 


# 
# display items
# 


# 
# connection point clock
# 
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true
set_interface_property clock EXPORT_OF ""
set_interface_property clock PORT_NAME_MAP ""
set_interface_property clock CMSIS_SVD_VARIABLES ""
set_interface_property clock SVD_ADDRESS_GROUP ""

add_interface_port clock clk clk Input 1


# 
# connection point reset
# 
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true
set_interface_property reset EXPORT_OF ""
set_interface_property reset PORT_NAME_MAP ""
set_interface_property reset CMSIS_SVD_VARIABLES ""
set_interface_property reset SVD_ADDRESS_GROUP ""

add_interface_port reset reset reset Input 1


# 
# connection point i2cavmm
# 
add_interface i2cavmm avalon end
set_interface_property i2cavmm addressUnits SYMBOLS
set_interface_property i2cavmm associatedClock clock
set_interface_property i2cavmm associatedReset reset
set_interface_property i2cavmm bitsPerSymbol 8
set_interface_property i2cavmm burstOnBurstBoundariesOnly false
set_interface_property i2cavmm burstcountUnits WORDS
set_interface_property i2cavmm explicitAddressSpan 0
set_interface_property i2cavmm holdTime 0
set_interface_property i2cavmm linewrapBursts false
set_interface_property i2cavmm maximumPendingReadTransactions 1
set_interface_property i2cavmm maximumPendingWriteTransactions 0
set_interface_property i2cavmm readLatency 0
set_interface_property i2cavmm readWaitTime 1
set_interface_property i2cavmm setupTime 0
set_interface_property i2cavmm timingUnits Cycles
set_interface_property i2cavmm writeWaitTime 0
set_interface_property i2cavmm ENABLED true
set_interface_property i2cavmm EXPORT_OF ""
set_interface_property i2cavmm PORT_NAME_MAP ""
set_interface_property i2cavmm CMSIS_SVD_VARIABLES ""
set_interface_property i2cavmm SVD_ADDRESS_GROUP ""

add_interface_port i2cavmm i2cavmm_address address Input 17
add_interface_port i2cavmm i2cavmm_readdata readdata Output 32
add_interface_port i2cavmm i2cavmm_readdatavalid readdatavalid Output 1
add_interface_port i2cavmm i2cavmm_waitrequest waitrequest Output 1
add_interface_port i2cavmm i2cavmm_writedata writedata Input 32
add_interface_port i2cavmm i2cavmm_write write Input 1
add_interface_port i2cavmm i2cavmm_read read Input 1
set_interface_assignment i2cavmm embeddedsw.configuration.isFlash 0
set_interface_assignment i2cavmm embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment i2cavmm embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment i2cavmm embeddedsw.configuration.isPrintableDevice 0


# 
# connection point avalon_slave_0
# 
add_interface avalon_slave_0 avalon end
set_interface_property avalon_slave_0 addressUnits WORDS
set_interface_property avalon_slave_0 associatedClock clock
set_interface_property avalon_slave_0 associatedReset reset
set_interface_property avalon_slave_0 bitsPerSymbol 8
set_interface_property avalon_slave_0 burstOnBurstBoundariesOnly false
set_interface_property avalon_slave_0 burstcountUnits WORDS
set_interface_property avalon_slave_0 explicitAddressSpan 0
set_interface_property avalon_slave_0 holdTime 0
set_interface_property avalon_slave_0 linewrapBursts false
set_interface_property avalon_slave_0 maximumPendingReadTransactions 0
set_interface_property avalon_slave_0 maximumPendingWriteTransactions 0
set_interface_property avalon_slave_0 readLatency 0
set_interface_property avalon_slave_0 readWaitTime 1
set_interface_property avalon_slave_0 setupTime 0
set_interface_property avalon_slave_0 timingUnits Cycles
set_interface_property avalon_slave_0 writeWaitTime 0
set_interface_property avalon_slave_0 ENABLED true
set_interface_property avalon_slave_0 EXPORT_OF ""
set_interface_property avalon_slave_0 PORT_NAME_MAP ""
set_interface_property avalon_slave_0 CMSIS_SVD_VARIABLES ""
set_interface_property avalon_slave_0 SVD_ADDRESS_GROUP ""

add_interface_port avalon_slave_0 readdata2 readdata Output 256
add_interface_port avalon_slave_0 address2 address Input 7
add_interface_port avalon_slave_0 byteenable2 byteenable Input 32
add_interface_port avalon_slave_0 chipselect2 chipselect Input 1
add_interface_port avalon_slave_0 write2 write Input 1
add_interface_port avalon_slave_0 writedata2 writedata Input 256
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isFlash 0
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isPrintableDevice 0

