import ftd2xx 

class ADS54J40EVM:
	def open(self, device_id):
		if type(device_id) is int:
			self.device = ftd2xx.open(device_id)
		else:
			self.device = ftd2xx.openEx(device_id)
	
	# crazy shit the driver does before doing anything else, comes in two variants: with
	# bitmode = 0xbb and with getBitMode and getQueueStatus
	# bitmode=0x07ce4500 and without getBitMode and getQueueStatus
	def rubbish_action(self, bitmode=0xbb, get_bitmode_queuestatus = True):
		ftd2xx.ftd2xx.FTD2XX.setUSBParameters(self.device,0x10000,0x10000)
		#ftd2xx.ftd2xx.FTD2XX.setEventNotification(h,0,0x0018df24)
		ftd2xx.ftd2xx.FTD2XX.setChars(self.device,0x00,0,0x00,1) 
		ftd2xx.ftd2xx.FTD2XX.setTimeouts(self.device,2000,20000)
		ftd2xx.ftd2xx.FTD2XX.setLatencyTimer(self.device,0x10)
		ftd2xx.ftd2xx.FTD2XX.setBitMode(self.device,bitmode,0x4) 
		ftd2xx.ftd2xx.FTD2XX.setBaudRate(self.device,115200*16)
		if get_bitmode_queuestatus:
			bitmode = ftd2xx.ftd2xx.FTD2XX.getBitMode(self.device)
			queuestatus = ftd2xx.ftd2xx.FTD2XX.getQueueStatus(self.device)
		
	def write_reg(self, x):
		self.rubbish_action()
		ftd2xx.ftd2xx.FTD2XX.read(self.device,0)
		ftd2xx.ftd2xx.FTD2XX.write(self.device,x)
		ftd2xx.ftd2xx.FTD2XX.read(self.device,len(x))
	
	def __init__(self, device_id=0):
		self.device_id = device_id
		self.open(device_id)
		self.device.setBaudRate(115200*16)
		self.device.resetDevice()
		self.device.close()
		self.device = ftd2xx.open()
		
		for crazy_sequence_iteration_id in range(4):
			self.rubbish_action(bitmode=0x07ce4500, get_bitmode_queuestatus = False)
			
		bitmode = ftd2xx.ftd2xx.FTD2XX.getBitMode(self.device)
		queuestatus = ftd2xx.ftd2xx.FTD2XX.getQueueStatus(self.device)
		ftd2xx.ftd2xx.FTD2XX.setBitMode(self.device,0xbb,0x4)
		bitmode = ftd2xx.ftd2xx.FTD2XX.getBitMode(self.device)
		self.queuestatus = ftd2xx.ftd2xx.FTD2XX.getQueueStatus(self.device)
		
	def load_lmk_config(self, filename=None):
		if filename == None:
			filename = r'C:\Program Files (x86)\Texas Instruments\ADS54Jxx EVM GUI\Configuration Files\LMK_Config_Onboard_983p04_MSPS.cfg'
		with open(filename, 'rb') as file:
			config = [[int (i, 16) for i in row.strip().split()[:2]] for row in file if len(row.strip().split())>1]
		x = []
		for q in config:
			y=self.decode_address(1,q[0],q[1])
			x.append(bytes(self.encode_bits(1,y)))
		for i in x:
			self.write_reg(i)		
		#print (x)
	
	def load_ads_config(self, filename=None):
		if filename == None:
			filename = r'C:\Program Files (x86)\Texas Instruments\ADS54Jxx EVM GUI\Configuration Files\ADS54J40_LMF_8224.cfg'
		with open(filename, 'rb') as file:
			config = [[int (i, 16) for i in row.strip().split()[:2]] for row in file if len(row.strip().split())>1]
			
		x = []
		for q in config[0:1]:
			y=self.decode_address(1,q[0],q[1])
			x.append(bytes(self.encode_bits(1,y)))
		for q in config[1:4]:
			y=self.decode_address(2,q[0],q[1])
			x.append(bytes(self.encode_bits(2,y)))
		for q in config[4:-1]:
			y=self.decode_address(3,q[0],q[1])
			x.append(bytes(self.encode_bits(2,y)))
		q=config[-1]
		y=self.decode_address(1,q[0],q[1])
		x.append(bytes(self.encode_bits(1,y)))
		
		for i in x:
			self.write_reg(i)

	# "device" mechanics. It;s actually n ot the line number but the device "name" in the config file, 
	# but we don't parse it because the order is the same in all the config files anyway.
	def decode_address (self, device, address, value):
		if (device==1):
			return [[address, value]]
		if (device==2):
			return ([[0x11, address//0x100], [address&0xFF, value]])
		if (device==3):
			return ([[0x4003,(address//0x100)&0xff],[0x4004, address//0x10000],[(address&0xFF)+0x6000,value],[(address&0xFF)+0x7000,value]])
			
	def encode_bits (self, device, inputt):
		b=15
		if (device==1): #LMK CASE
			byte_mask = 0xa8
		else:
			byte_mask = 0xb0
		sequence = []
		for q in inputt:
			address=[(q[0]&(2**(i))>0) for i in range(b,-1,-1)]
			data=[(q[1]&(2**(i))>0) for i in range(7,-1,-1)]
			sequence.append(address+data)
			
		output=[0xb8]
		for packet in sequence:
			for bit in packet:
				output.extend([byte_mask|(bit*2), (byte_mask+1)|(bit*2)])
			output.append(0xb8)
		return(output)
		
	def close(self):
		self.device.close()
        