import ftd2xx 

class DAC3XJ8XEVM:
	def open(self, device_id):
		if type(device_id) is int:
			self.device = ftd2xx.open(device_id)
		else:
			self.device = ftd2xx.openEx(device_id)
	
	# crazy shit the driver does before doing anything else, comes in two variants: with
	# bitmode = 0xbb and with getBitMode and getQueueStatus
	# bitmode=0x07ce4500 and without getBitMode and getQueueStatus
	def rubbish_action(self, bitmode=0xbb, get_bitmode_queuestatus = True):
		ftd2xx.ftd2xx.FTD2XX.setUSBParameters(self.device,0x10000,0x10000)
		#ftd2xx.ftd2xx.FTD2XX.setEventNotification(h,0,0x0018df24)
		ftd2xx.ftd2xx.FTD2XX.setChars(self.device,0x00,0,0x00,1) 
		ftd2xx.ftd2xx.FTD2XX.setTimeouts(self.device,2000,20000)
		ftd2xx.ftd2xx.FTD2XX.setLatencyTimer(self.device,0x10)
		ftd2xx.ftd2xx.FTD2XX.setBitMode(self.device,bitmode,0x4) 
		ftd2xx.ftd2xx.FTD2XX.setBaudRate(self.device,115200*16)
		if bitmode_queuestatus:
			bitmode = ftd2xx.ftd2xx.FTD2XX.getBitMode(self.device)
			queuestatus = ftd2xx.ftd2xx.FTD2XX.getQueueStatus(self.device)
		
	def write_reg(self, x):
		self.rubbish_action()
		#ftd2xx.ftd2xx.FTD2XX.read(self.device,0)
		ftd2xx.ftd2xx.FTD2XX.write(self.device,x)
		ftd2xx.ftd2xx.FTD2XX.read(self.device,len(x))
	
	def __init__(self, device_id=0):
		self.device_id = device_id
		self.open(device_id)
		self.device.setBaudRate(115200*16)
		self.device.resetDevice()
		self.device.close()
		self.open(self.device_id)
		
		for crazy_sequence_iteration_id in range(4):
			self.rubbish_action(bitmode=0x07ce4500, git_bitmode_queuestatus=False)
			
		bitmode = ftd2xx.ftd2xx.FTD2XX.getBitMode(h)
		queuestatus = ftd2xx.ftd2xx.FTD2XX.getQueueStatus(h)
		ftd2xx.ftd2xx.FTD2XX.setBitMode(self.device,0xbb,0x4)
		bitmode = ftd2xx.ftd2xx.FTD2XX.getBitMode(h)
		queuestatus = ftd2xx.ftd2xx.FTD2XX.getQueueStatus(h)
		
	def load_lmk_config(self, filename=None):
		if filename == None:
			filename = r'C:\Program Files (x86)\Texas Instruments\DAC3XJ8X GUI\Configuration Files\onboard_clock.cfg'
		with open(filename, 'rb') as f:
			config = [[int (i, 16) for i in row.strip().split()[:2]] for row in file if len(row.strip().split())>1]
		x = []
		for q in config:
			y=decode_address(1,q[0],q[1])
			x.append(str(bytearray(encode_bits(1,y))))
		for i in x:
			self.write_reg(x)		
		#print (x)
	
	def load_dac_config(self, filename=None):
		if filename == None:
			filename = r'C:\Program Files (x86)\Texas Instruments\DAC3XJ8X GUI\Configuration Files\default_841.cfg'
		with open(filename, 'rb') as f:
			config = [[int (i, 16) for i in row.strip().split()[:2]] for row in file if len(row.strip().split())>1]
			
		x = []
		for q in config:
			y=decode_address(2,q[0],q[1])
			x.append(str(bytearray(encode_bits(2,y))))
		for i in x:
			self.write_reg(x)		

	def decode_address (self, device, address, value):
		if (device==1): #LMK CASE
			return [(address//0x100)&0xff, address&0xff, value]]
		if (device==2): # DAC3XJ8X
			return ([address&0xff, value])
	# 
	def encode_bits (self, device, inputt):
		if (device==1): #LMK CASE
			byte_mask = 0x88
		elif (device==2): # DAC3XJ8X
			byte_mask = 0x90
		#byte_mask = 0x88
		packet = []
		for q in inputt:
			byte=[(q[0]&(2**(i))>0) for i in range(7,-1,-1)]
			packet.extend(byte)
			
		output=[0x98]
		for bit in packet:
			output.extend([byte_mask|(bit*2), (byte_mask+1)|(bit*2)])
		if (device==1): #LMK CASE
			output.append(0x99)
		output.append(0x98)
		return(output)
       
	def set_resetb(self, reset):
		if reset:
			packet = [0x98]+[0x80,0x81]*4+[0x98]
		else:
			packet = [0x98]+[0x82,0x83]*4+[0x98]
		self.write_reg(packet)