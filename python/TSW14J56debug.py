#JESD204B PHY reconfiguration interface	
	def phy_rd(self, regmap, chan, offset):
		self.write_reg(JESD_PHY_BASE, regmap["CHAN"], chan)
		self.write_reg(JESD_PHY_BASE, regmap["OFFS"], offset)
		self.write_reg(JESD_PHY_BASE, regmap["CTRL"], JESD_PHY_CTRL_MASKS["RD"])
		self.phy_check_busy(regmap)
		return self.read_reg(JESD_PHY_BASE, regmap["DATA"])
			
	def phy_wr(self, regmap, chan, offset, data):
		self.write_reg(JESD_PHY_BASE, regmap["CHAN"], chan)
		self.write_reg(JESD_PHY_BASE, regmap["OFFS"], offset)
		self.write_reg(JESD_PHY_BASE, regmap["DATA"], data)
		self.write_reg(JESD_PHY_BASE, regmap["CTRL"], JESD_PHY_CTRL_MASKS["WR"])
		self.phy_check_busy(regmap)	
			
	def phy_check_busy(self, regmap):
		n=0
		while(1):
			if (n>self.n_trys):
				raise Exception("PHY reconfiguration controller busy")
			ctrl = self.read_reg(JESD_PHY_BASE, regmap["CTRL"])
			if(ctrl & JESD_PHY_CTRL_MASKS["ERR"]):
				raise Exception("Invalid offset or channel")
			if(~(ctrl & JESD_PHY_CTRL_MASKS["BSY"])):
				break
			n+=1
			
	def scan_ph(self, chan, h):
		n_ph = 64
		er = zeros(n_ph)
		ec = zeros(n_ph)
		bc = zeros(n_ph)
		ctrl = JESD_PHY_EYEQ_CTRL0_CEN | JESD_PHY_EYEQ_CTRL0_BERBEN | JESD_PHY_EYEQ_CTRL0_EYEEN
		self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL0, ctrl)
		self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_VH, h )
		for ph in range(n_ph):
			sys.stdout.write("\r Phase {:d}\r".format(ph))
			sys.stdout.flush()
			self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_HPH, ph)
			self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL0, ctrl|JESD_PHY_EYEQ_CTRL0_RST)
			self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL0, ctrl|JESD_PHY_EYEQ_CTRL0_SNAP)
			bc0 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_BC0)
			bc1 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_BC1)
			ec0 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_EC0)
			ec1 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_EC1)
			ec[ph]=(ec1<<32)+ec0
			bc[ph]=256*((bc1<<32)+bc0)
			er[ph] = float((ec1<<32)+ec0)/float(256*((bc1<<32)+bc0))	
		return(er,ec,bc)		
		

	def eye(self, chan):
		n_ph = 64
		n_h = 64
		er = zeros([n_ph,n_h])
		ec = zeros([n_ph,n_h])
		bc = zeros([n_ph,n_h])
		ctrl = JESD_PHY_EYEQ_CTRL0_CEN | JESD_PHY_EYEQ_CTRL0_BERBEN | JESD_PHY_EYEQ_CTRL0_EYEEN
		self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL0, ctrl)
		for ph in range(n_ph):
			sys.stdout.write("\r Phase {:d}\r".format(ph))
			sys.stdout.flush()
			self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL1_VHPOL, 0)
			self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_HPH, ph)
			#d = 63
			for h in range(n_h):
				#if (h==64): 
				#	self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL1_VHPOL, 1)
				#	d=64
				#self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_VH, uint(abs(h-d)) )
				self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_VH, h )
				self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL0, ctrl|JESD_PHY_EYEQ_CTRL0_RST)
				self.phy_wr(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_CTRL0, ctrl|JESD_PHY_EYEQ_CTRL0_SNAP)
				bc0 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_BC0)
				bc1 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_BC1)
				ec0 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_EC0)
				ec1 = self.phy_rd(JESD_PHY_EYEQ_REGMAP, chan, JESD_PHY_EYEQ_EC1)
				ec[ph,h]=(ec1<<32)+ec0
				bc[ph,h]=256*((bc1<<32)+bc0)
				er[ph,h] = float((ec1<<32)+ec0)/float(256*((bc1<<32)+bc0))	
		return(er,ec,bc)		