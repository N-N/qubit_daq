from numpy import *

def to_bytes(number, len):
	return [(number>>(8*i))&0xff for i in range(len-1,-1,-1)]
	
reg_dtype = dtype(uint32).newbyteorder('>')
###############################################	
#USB vendor request direction
USB_TO_DEVICE = 0x40
USB_TO_HOST = 0xC0
#USB vendor requests
USB_VEND_REG_READ = 0xbb
USB_VEND_REG_WRITE = 0xba
#USB endpoints
USB_BULK_IN = 0x81
USB_BULK_OUT = 0x1
##############################################
#JES204 phy control:
JESD_PHY_BASE = 0x800400
#Standard control register (CTRL) masks for PHY reconfig
JESD_PHY_CTRL_MASKS = {"ERR":0x200,"BSY":0x100,"RD":0x2,"WR":0x1}
#--------PMA analog control--------
#This module use 2 LSBs in address and we shift REGMAP because avmm with 32-bit data doesn't
#If your module use these bits you have to remap its address by 2 bits up	
JESD_PHY_PMA_REGMAP = {"CHAN":0x8<<2, "CTRL":0x0A<<2, "OFFS":0x0B<<2, "DATA":0x0C<<2}
#PMA offsets. Write them to OFFS register
JESD_PHY_PMA_RX_DC_GAIN	= 	0x10	#RX equalization DC gain
JESD_PHY_PMA_RX_EQ_CTRL	= 	0x11	#RX equalization control
JESD_PHY_PMA_PRECDR_RSL	= 	0x20 	#Pre-CDR Reverse Serial Loopback
JESD_PHY_PMA_POSTCDR_RSL= 	0x21	#Post-CDR Reverse Serial Loopback

#-------Onchip signal monitoring EYEQ------
JESD_PHY_EYEQ_REGMAP = {"CHAN":0x10<<2, "CTRL":0x12<<2, "OFFS":0x13<<2, "DATA":0x14<<2}
#EyeQ offsets
JESD_PHY_EYEQ_CTRL0			=	0x0
#RST and SNAP can be used simultaneously. It does snap and then reset counters only
JESD_PHY_EYEQ_CTRL0_RST		=	0x8	#Reset everything, snapshot and counters are reset to 0.
JESD_PHY_EYEQ_CTRL0_SNAP	=	0x10#Take a snapshot. Copy the counter values into local registers for read access. 
JESD_PHY_EYEQ_CTRL0_CEN		=	0x4	#Counter Enable
JESD_PHY_EYEQ_CTRL0_BERBEN	=	0x2	#BERB Enable
JESD_PHY_EYEQ_CTRL0_EYEEN	=	0x1	#Enable Eye Monitor

JESD_PHY_EYEQ_HPH	=	0x1	#Horizontal phase [5:0] 
JESD_PHY_EYEQ_VH	=	0x2	#Vertical height [5:0] 

JESD_PHY_EYEQ_CTRL1 = 	0x3
JESD_PHY_EYEQ_CTRL1_1DEYE = 0x2000 	#Enable 1D-eye mode
JESD_PHY_EYEQ_CTRL1_VHPOL = 0x4		#Vertical hight polarity 1: + 0: -

JESD_PHY_EYEQ_BC0 = 0x5	#Bit Counter[31:0]
JESD_PHY_EYEQ_BC1 = 0x6	#Bit Counter[63:32]
JESD_PHY_EYEQ_EC0 = 0x7 #Err Counter[31:0]
JESD_PHY_EYEQ_EC1 = 0x8 #Err Conter[63:32]

#---------Adaptive equalizer AEQ----------
JESD_PHY_AEQ_REGMAP = {"CHAN":0x28<<2, "CTRL":0x2A<<2, "OFFS":0x2B<<2, "DATA":0x2C<<2}
#Offsets
JESD_PHY_AEQ_CTRL = 0x0
JESD_PHY_AEQ_CTRL_DONE = 0x100
JESD_PHY_AEQ_CTRL_PWRUP = 0x1 #One-time AEQ adaptation at powerup
JESD_PHY_AEQ_CTRL_MAN = 0x0 #Low power manual equalization mode
JESD_PHY_AEQ_RES = 0x1
##############################################
#JES204 csr:
JESD_CSR_BASE = 0x440000
#Lane control common register
JESD_CSR_LANE_CTRLCOM = 0x0 

#Lane control for lanes 0-7
JESD_CSR_LANE_CTRL = [0x4,0x8,0xC,0x10,0x14,0x18,0x1C,0x20] 
JESD_CSR_LANE_CTRL_M ={	"POL": 			1,		#Polarity
						"PDN":			1<<1,	#Powerdown
						"PAT_AL_EN":	1<<2	#Enable pattern align for all lanes, for JESD_CSR_LANE_CTRL[0] only
						}
#Errors detected in JESD204B IP core will be logged in this register
JESD_CSR_RX_ERR0 = 0x60						
JESD_CSR_RX_ERR0_M = {	"SYSREF_LMFC":		1<<1, 	#If SYSREF period does not match the LMFC period, this bit will be asserted.
						"DLL_DATA_RDY":		1<<2,	#Asserted if the RX detects data ready by the Transport Layer is 0 on the AV-ST bus when data is valid
						"LANE_DSKEW":		1<<4,	#Asserted when lane to lane deskew exceed the LMFC boundary
						"RX_LOCKED_TO_DATA":1<<5,	#Detected 1 or more lanes is not locked to data
						#If one of these two bits below is triggered Avalon-MM interface reset and link reset must be applied.
						"PCFIFO_FULL":		1<<6,	#Detected 1 or more lanes of Phase Compensation FIFO is full.  
						"PCFIFO_EMPTY":		1<<7,	#Detected 1 or more lanes of Phase Compensation FIFO is empty 
						}
#Errors detected in JESD204B IP core will be logged in this register	
JESD_CSR_RX_ERR1 = 0x64						
JESD_CSR_RX_ERR0_M = {	"CG_SYNC": 		1,	#Code group synchronization error for all lanes, indicates that the state machine has returned to the CS_INIT state.
						"FR_ALIGN":		1<<1,#Frame alignment error. End-of-frame marker (/F/ or /A/) position has misaligned. Dynamic realignment is not supported.
						"LANE_ALIGN":	1<<2,#Lane alignment error. End-of-multiframe marker (/A/) position has misaligned. Dynamic realignment is not supported.
						"UNEXP_KCHAR":	1<<3,#Unexpected /K/ character. Unexpected /A/ or /F/ character will be flagged as frame alignment error or lane alignment error.
						"NOT_IN_TAB":	1<<4,#Not in table, the received code group is not found in the 8b10b table for either disparity.
						"DISPAR":		1<<5,#Running disparity error, the received code group exists in the 8b10b table, but is not found in the proper column according to the current running disparity.
						"ILAS":			1<<6,#Missing ILAS sequence after cgs.
						"ECC_CORR":		1<<8,#ECC error corrected. Single bit error detected and corrected.
						"ECC_FATAL":	1<<9,#ECC error fatal. Double bit error detected and uncorrected.
						}

JESD_CSR_RX_ERREN = 0x74 #This is to enable the error types that will generate the interrupt.
##############################################
#Data capture module
CAP_BASE = 0x0
CAP_CTRL = 0x0
CAP_LEN = 4
CAP_SEGM_NUM = 12
CAP_START = 1
CAP_TRIG_START = 9
CAP_DONE = 0
CAP_DONE_TRIG = 8
#############################################
#FX3 usb chip interface module
FX3_BASE= 	0x10000
FX3_CTRL= 	0x0
FX3_CTRL_RD = 1 		#Memory read.
FX3_CTRL_WR = 1<<1 		#Memory write.
FX3_CTRL_RST = 1<<31 	#Reset

FX3_LEN	= 	0x4			#Number of samples to read from memory

################################################
RAM_BASE = 0x50000





