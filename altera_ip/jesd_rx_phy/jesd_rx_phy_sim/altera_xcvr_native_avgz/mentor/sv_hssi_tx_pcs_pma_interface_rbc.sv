// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:33:46 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
FlZxAi0gpq6Bb+5kKy/frwJFt1Dflxn/GS0JGR/SDSiiezVMQnj5qryOjNhpMDad
MbaFiURLqR3x51GswaBWXxtDxlGxx1i6qRLQBryxBxVa4R1L62EoLH14ZM1AZ4lx
UsUlq6Ud1azHwm+XteO9Mq0cBkSLJtuT+HOeIAVPNhM=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5920)
mbue5imBn1KscNTINR6qzCwAPv3IzAbmQNvCGw1ji9pxk5DdrolAmRruG1a55PJ+
cOldhqfB7+557XE6XfBksXsBBfSwu33dowg2q0B9oWMuUs3BIMOJSaThy92JuO54
2+rO6Z0Obs7FyTSyqGAZDSo9R5VuFSL93/x+hcnDNCjp9ui6SItR7tSpSCqlIjsB
VYp++eakkxUmEoJubckvNLQ5h9zacalZH9JygBNFvhE1y1gRsz4RYz5NQ87HxHHD
whpYPRYwVgLbsQU4GJ3LOx9zGt1ALE91HyvnAlodGW3JD5E7t/+NquNSqEFCvMfA
pp0BirYLufl6suDGzaoNIDNOgCytOyKY0LpnsrZN6T7Db8hTM3oQjyz5pSETje+T
5mpAmMch5/gvRvCyLPVynwuc3mizZypiibLSWxVzF7xGSXS8+E5pGbym5IB4328N
+8/YRRjwixaf74YBzt1YqhAJFFuA7VLhC9D2+JsW0YMAm8oErdfLfL5PCwOB28Qj
ImuNe/5Xla0k/hacXtLGskoHHmmPgxRdxZkATEtqRo1MpMsSjZMVskqNKs41m2Q2
dzS85VYZFGiYE9L5+14Y2kYfEeYcQrGFpJy9/xEfZ9se+PhN/8Mz0AwbQeQCZrzs
SZa/AoSe/vgWt9on6GUR0nhtJUvM6PoiQlm6GUhOpoHY6YoMJt/CgRYi6exdHYai
opyfscAp3e/fq3gSlrYD3DgR271WlRysRI1J7RkQcI4SVkZQdKlHtw/On9M583Lh
4in+6mCkFDtB3+8Tncg4UyROlbINn77CsQhO5ug0c++Bu2aDUoj5RThluDMRrUkh
U5lJYeQ9w7gj9oTr4Gyxpsnpcg0GBGCfR7xSGvY0IwRQAb1hRavcaeLI0Aa4xKMF
GNKHSvJw9ZkRplYEomgupCjHZ7Gxq+iApLSUU2E5dlki18nSJ3SmZLqBuizLJHj+
50MSuguMxzh/6BBGq4gGF8JSUatgNkRxr0vS4w9s16SyrOC+D1Yo/Whm1c/Ww08O
94ozLBycQ7PYM6MJFWjMQ3HKiqOYzqZ/+WEBgU0b2+foG1oIJmI7bzoYAkQgn/Fl
DSntxOduk/9Of9J5CeAK2muIoyKlcp2smwBJ3vZW9fhIixSyrDhW1brzgzEQFXqo
gqYE07hWg3/6McQ8g4XnyTsBJxpRaPwE3/I6cEaIG+MEQdCKlSUM5uiXzi4twaZF
BZYhleVpOyeJ1DsJeuiPX5BL53NPGVU3ZH3EVdEsVZizUxs81lc2EBHMZtiS8FqK
NciDMyxjVgcyJT/nDm0UJEQ1hM620Q0Cg6uwSKNxK85CfOGvSpajiNg/G263ExeE
IXg1Y5N81w26/a0thdGxL7t7c7nOeDKwB4XiMgcpo73Jf7zhUvh1R2hJzygumz70
wwT0WM0IGcnX5fb9Ha2H+nuQeavSM5mpF43XD3VSv3ScXQj6so9gFrHvevEZLo51
uLaP+I374FEkcfuA6SEo9BUkpL8nvGNNoiAYT1B9KGoadOqBEdA1B6REcWvIzNEi
YXz9NVzGs9Y0Vc1T8kjaVr1cYS78G9sfT8x6FjFeqhnEvzipun//PyxLGUGov8B7
tTUiMp5fwC+0bRmXleyDgsIJAqbxVG48tjeIleOkAK2nNzRnitVfmmKPfuZUD5ra
WD3QB3NCGM+NVEjfLwvC01e2RePuQAt5yQ2VfGg438PA5b/U6T9p4cNSTzD5DDpN
1iTKyp37JMOWKcoFNYACF9939BkLRHqJiFKGuW45ccMeZJ794mO1n63IcQw2YCpd
eSpyTnYE5qmCS7c5OU6ic6VzaEF+Z+EsDMNQoGFd9HftMilNpvJ154whMXPHo+Up
2e2dnaXc18waEo+Dyq0uEWPhzsOAznUPU7eqpr/HKVUmKfTuFJm2l+MYPa1Or7MV
/dp5TIVre2+9hxnropnJpZ/1VsR6vTv6EH4CkWXi77LfoIThppDepbPSR3eyZNw8
AjI1myeTk6oBvvgpD2cVFGfNLs/vZFEuq/JZZvQwEhT2XkMSHcY/ZIlgHQG+zEBi
UQsY6bXoUUTtqTK66zWpXOrhjzXLI4azOmwnP1AuW//w60dN0Pn3ehZkhSDR+qYB
EmUWfgsA9Xe4dQ1uY7+UAFUDP1pgfo94hguN8UV+4oELtNrNRPn5paD18Tj06ZlA
7GBFnAJ6b3zkPuiHHmuqNKzGE0pkml2ZxWYlEmkqi1wTy42xnEMgddvkvIm0foO3
HZnmc3APID4yQ7tgF5T7spS22Tia0Rjm68UawuS5cbtgZvV1mGd+9+ahC4EZJU28
BDHEiUqZuqK22fVZcOrDw97gib0sdBcjpUAK7Ie0jMwGgFnwBWd3Q8rt1sFiAzSV
Sb7lCfvRwU92MOUHz9xpBi8RRAkKts+pm2Kqqb4dr0pzGbZGeINs+/zhQHEEINDc
uYMYIs3Jn4Q63MRIJWx6POGE7kAyDtqPtip+4ztDIcwJOwxRt4VFIWpBEZlVt+8l
sdUhJmobFjfTHLN8SbC0gylwbJd1qLQMbx+KbhqL8fPo6emYRxkAA7/IGpU4uF9q
IK8W9FqC0gb5AciW9Fre5X7WOPWNM/PmjIfU0JK81IFoMAygzoNA6sq2SO0rgcLo
PMho1If7MNCI/acyruP0jjiY88JWkNryW38IXza3MzsklDEsQY18KxeQ5p3Bs3g4
1Liv2ZH/txjIHHaqm6/j9ltB29OipqhaUhglAzkRFcfnXcIUGlUiMUQqJiBIVpH7
TTc2irjskVO/KxYKqBLVJqcVxUj2JGXWY43zr05nZKJfZE0CV/0FilUfDnysftJP
bE8qMOV83Kz4tl9kYv2oEX26qY38leYm3OcqQHRGoMKd1vWOAQLDMke0hQv90N7p
mvC2FRNGX4dk/0UsyrTvYGLBI1/EMG1+Va50UFmzCFufFLJTBGZjQyML8mVCEMEU
AkoBIiE99axz/L6QTRUxoYHaTqcGRIQLMVxoMVU3oBfES8coxHin1QxBK48XOcxX
gp4BXYHECNYKO8YxdGAxNC9uK1SJOuNJUp22JcGQeqGxpkJprUx89MnC+t6lrjoU
BAjiRUJdkzWrYZg4TW1v0yg2um3XOAnbKadGzGx+ifgsQVzU/OwkAT5GrdZHtoBx
nfNXgOyVqZkx2Rrcd9jdcnsSiVijF0lCBUx2vgF6Tf+OIw4vi//ncMgPc3fAqaFj
E+UsZQYN08YOx+yT8bxIqD/FEMp3IeqrZ5hLgHuTgXuXge8MHBGM9IOJoLpR2xJc
iqZsHh+9NnoKgChSrDaIBev0Ip9/3ysH9mke4m3PGjHmvEDL7TaDdQd/s6OC43dS
YVAEr8tinJSY0k2YqYBZemzh9Aj4+p6z0n3w7fJUpmaRk2Lgdlhq2LSvrd97kCyi
WIsUYeCBkKVycElMdZwHfQgOUAcObGrBkSiGNQw+/XQFAta8Of2SRRZA8cuZD6rX
lEEj5O+PLQ0oPcrK7Jrxc5AND9Wq38HVoO1O/UYC7gzpkGMS/Wp64Ao1OXH5C0WM
UpueN06Kg5D4Z4cH292Cws3m5H6TRH2b9nOCryqG4pbtzu7f7cvWqPI0k0/i4sxW
ewbVJ/RiyvDdxT2rb/YwFL+kTqVAmHkMpw6ZDqwk6Fe+oocFZHuP9x1LrJ9FnrWw
spRxyuNeQynjSz0+4UnMw2/QfMyWWg2VIbZpn/EVj0hDMG5vj9KcI/5e4v1w3Lh/
4igKBfT4WtjpRTruOr8yQKga7N3o221X+CiW2ruC3kDJz0uJIvgxbyAp+c91oIGM
FuzOzRl6xgeqGTpg8poDOmMk85OD3d4fipGpReMRj3DCwFdpBEwPRI67svEjuDbz
q4F9ecER+bgoDj4DcLT9iif48p0mAa5fNGPHjS2bG8wmbb5ebVTtLfSDWHuF5u+7
+THt2KyDKLaFiwP4oUCpnVNSgx7j2oG0d6kH40tJNyAtpYiuasSaHPKppcevqmeu
lphNZnPLNp2BqKDRwJRTXCvjZWR+RXgLIzRCSmrIFTUn2ScSkNIrI0XrpC8Z/Hch
gzXUyndk0ARkkmxSl5pqS4Mz9lquasN0oYQ9v4FRq+WxagamPbH4RfViwGqsKf2y
OCdtRUefkpLcOGMTxvbRexoCKvaxkSne1SP03ZXSysgUOb3aXOQyv1q0D3uUgY7O
qqnviHkNC5Ds1oXpQ5cpvLhGS1Cl0buKH4/m0qKNzMErLKhB5P9Ao+/DW2z2pGB5
CUQxxg/0qa7wiP2Bhhv51OYE/GxW7gptFYj9Cei3VoRiYhlVWxc+NFtO83ceXPMi
wWdSj0dVwSwWRtzYdldpZ3I7KISwrwmhmOlWF/fe9MbnI8Lrx/5vN6j3jQKcQKhM
2os+LYi1yGm1TDa0AzjZbMYrWr6an/Z4YukVOhjvUnQei/JxFRqBDthDcrw/mtQf
a7AXQGmw1WH0kwS628oqq2zFrcLu4s822Q/7fqb4gG9dZPntPVH6Hdpb2PZHrTkf
3t1uerzsyYo4bEMU6txedRjJDNDvDyzIZaEeN2paXbDUWqU9LbbgmQMMQHixTLnk
fog8LAMiIOyfp8vk1aupMp14DfxxeLI2SZ8dkadVHaOCs9xAGnQJj31LiOBn+235
j8xLBLZWNqg43ieYn0mI+rPpowAjapfGlUeUN8lfltyyfGl5Z8wCMhsgvUZnqZpi
iAC5QRSwNN8rnlk2UbAzegeJirEuytiNtY1AqodwnSnZik6nb34Fbtoak0nqJT0I
yhzAkOzyOYGdGliY58um/t6aKu6aJE1rlewbsH0xyxmNIO97GdKQkOg3OykgQPvA
OLMqp3j2gmi85HkFInSgylxTzf9DMm9sTAzSSp/qHxx9yewxGBhY3iQP9evbEL7N
oL9ZQRq1xMeMVnGuaT+0KGkCp4hfk47dnaDxZvNAeknOJYbl01opQFla0DU+H7Yq
YCMOsS5N3dq+oQayGP0gx1tknPfqxv0taKTtMJe0d5tv2x+x3me0ZaZmQzz9Ijsm
FvtQ4GllAygYWqyYbxzvrWl/jPjbDUEfYB9oSMOMhyrIOygPthEyM0WsNRO1cODO
FP8zicR2Sn7ON2ypc1uYu2TS/2+rE1HFD3HzaoR+Eiyy51Brf4g0rdIA8YTMH73/
1zPSH6SPHaa+n8Cn1efax+j5xTkhG9YhOV73O8yc8RapgKWlV6UwhkQv+1yPD+/h
osBBi9Cx1urkwHgJIVlN3+9cSkxEkXWriTYjC8SL7lGlHkRnsj5OFspM3ZkLI4GE
aRl7XQzTFaGfJ2Y4Pg6k/fxKmrSne2QnTeOMb0cmecUN+F1WvjmHVK6mpyzcPs3j
2cDQYrV3AoILbg/22TYz+nCYnOlAeidy5u3TTExGqwkKFRgqEXvEvEgswS6cBn92
10q0BtpqV2ubfxgn75LUUVEeEQjU9tQtF6KgYgbAanwh0+2LJocyC1tHUgBbu+I3
0ATyngaQPK7oihYCm+o1RGOrNYa+b1NO+64EaW37anWu11dsBwlPXe/2pniRNpa/
dmDG716gh6eBgNEYj/IVIEZtMaXY+QLQu+X87GYA1/dULGa9gCAL7OYsQ+ZraSwc
kyKJJowUGWrK6a3um1Op2oRNqS6U9nVEZsHihyBc9AEqqakJqaQaYECjxd61wHzR
O6oYi3qizTV6whvdXQj/X3LpST3YgnAnQHvitNunSuYBL5GoNJIBdTLtPEM1k2OJ
ouUlBZoswoF8NHECOnUN/GbL4W0+akYet/l2LF4u+ZRxvZiG9LFcW495TurUX4kp
p0HSPjFFg1D6WTydiuJ1rFvbCwQH9eBgkW1OtvqhajOP/AdYrgYJ1sd/PAJn5qff
NmzQCwBAvz7CnBx7jp3Lzeez2UFKZwuf3ymrcRo+u25TZD93x5alxXofmxIJ0NTO
pUkbVmbCVg+8Lt4R8Ke6s1wLJlU94t6dclfFwS9ofn8uvrIXAjZL55qYPNeSsupo
iosoxR3QomKxg/qL4BTt5GICQiec0qIlid0mzcBZoVg6IZz4sL+yvFfam/Vt7Xg7
x83gVv1k1gd2+vxBBNFkJt1Rtl1UmqvQQYuVLPdH8NSNWfOphPx1bTAcId+6iiIR
iOV9v5DHcpW1jn/01tDrxPECkFV3t7H6qq3WpyDVweDBbk4vD6xbwCC0I1KtKkOG
cbdqk594PqyzEe4IAtQtMmgQ/RgoVBIBzUMbX/fBB8NjiZjBrmrjrRoDGodLGw60
b0ANfWGKJ/pkY94jzngDskgHXOPapL9JVSZQ2P2e/4KmpFaqmrGRDG52bOuJkXky
6E/27DSELxLJ8w77PoEYZdbCxflC+JB/oT59FuruZYnNADkw1+VOk0vKeST1ypqB
dpi09grs/oGlBd3RLTucM2U0sC1MbZh/G3TJ/amsJS+yJxF1YTDyQ9ppqWk38wj7
q+QXWxFCtuYeskNp4SNooCSdC6UO+a47Py3vujxUcOznZSpKCusbHZt0SIam00ix
JeEoHbMBBbtDweMQijBx+8XdSGkH43WCmjySdNHjHxZLjuQl5/4oXz0mzyS/0XG3
qtxrJ6lSdBI/HTVP2cEyYuQSJTl5vtPmUrs1wKSggnLFXUNyG1CNghlgHryPWJtQ
ERkLgBkFGpYcrNv/8MjWu4zVi6u2x1hDYn+j1MdYGll6OV4fQfQWUM9J8MXbrFnp
gy9PjIjYQIUrZX3bb+BIqwWHr1pM3MQilUpq5Mhc9gLRSnByZoWsTrNmYKkSl3G+
ggcsBPnX2YLLGM1G2ND+LnhL/fI9YXVxSyjpeYdjXec2Q0mjfcGfle/dIsWaGX57
kjZVAJSLj1LSxG7Qwjjn98CjfQ1X6+evD6dSjBP5eOTr67XAQagjpXya2qGVlnZl
DOJ+vL6lm3pTnx0492n8PvH7np4OcHf2iA2B5FXB3w12441wm3HDUF8Gr8MxjFSK
nIBYvlXNyFgG4d14HklePAEigqEz9Mb84oIHndZsGM50T0uLcdc0gmOZlqpT6kRb
+e18CmydDt/EJEmOxGWjGnIETSOkHo+UUvpuV2SDxg7Rf/8olzhIE5yLf43HAI7K
LD3QxGczozrx9DaTJ4mR/M70CCsbpu8SH1S1FW99c9N9eM1cnYPVO04VyA0LzU8M
CHNQgyhGFBWm0oAfuj8E5gLPm1B6qPV2N7j68WOD29xlKjIAxX0JJJqmArgGR7MU
o3T567p7Pq5V/htUAo3scE1AlaIGdDoF6Ya+C1Im9OdS0z2LBwp1emInAbmMX9nc
3EROdgHh49DCL221RSTkthCm2aGo24MpK7xEjCBOSGKjFx5+Ql04jeBAsAmx25FG
69ai3PHa7jxO9DTuWBAzRqlb8oC0EYyXKu5UvfEwxWWiEJ+pdE0PIKHH5t+yIRG1
/N6eKd6LvNBOMmWp1kywt/5l3AZUt9UzE5lrAmTDBOroVBQqMfpzFMK3XSM+n8V4
ns+S6AEuzF5b82UZfk/lmgZkqDKBXpDV1VF9l79dNofMn2cRZ/kf2WqGTKm876ug
B6Bt1l53he6xbv1zJPPuEMuh29+XyQHNbn0cY9Y4932wJpPmDIrpCOxmMyTMwc7/
NbdeVGZE8HNlWjC8ek3KsIXKM17cf5I4KIm4kuIEG2IGpUjb0yKHfeVgbB8hZChP
1yNd9YeqCR2qYNilVEcy4Y9jxtzh5Ab8POp97xV/qMC1Uw008xKbfU/txBC7T4Ea
yhUN5x1Mtu43eS9fhHbZA9VvI0Bi1HFN3GnKNsbZnBhJ/grnfX+PowHvdkYjDh0w
2VyJJfeWRguf1BdoFnt4YiDFqdNmG+vLOG+mOhQe7jAoCOIkMcVR63lG0NXn7jqa
+hbiBtfMlgI3Ak1HWsJbxL8JIVMSlLTWJcMChUn4hEayFspiNoo3mEmLprt8tLrC
Pf68Ljlub9Xqv7Zfby7cOw==
`pragma protect end_protected
