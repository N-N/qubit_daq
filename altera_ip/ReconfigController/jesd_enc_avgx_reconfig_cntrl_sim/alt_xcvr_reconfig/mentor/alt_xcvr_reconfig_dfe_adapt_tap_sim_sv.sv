// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:02 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
TbbuZI+z+ywHiAe7RY/Rm8cbQ0xvPzUzRNeTi9nHyLoXGF1T6dRut5TF7tHEa1Bd
K2+iyyf444rLKmrs5aD5wLdvkEbH0wVMvFhm4IFUjGidZPZ8GinU41/Y/CeVKmRM
HwkoQsZpT7Y3+WULymzS4Iy1BkcKYGvJL1e/NwQBTjw=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2736)
V+eRFgf3DO5kbKUz0U7pv+kPLH0QXv1YbMpvH92PgxlmDACXREqoixCichnb6EFz
YIvQtJj+gUjVZuY3csuoHtedNw46E9lVdrnwUvAJlf+X3rAPlVPUljSwBGWZKKlh
nVzdXWtwwIv+/xJfak0ehGc8yFkGj4NnjgfT5sNOgMPeEpugafgnzDR16+yktDsd
Sl+L9PyuxtjNp+z71TiJI+MP4XZHHbv17DyG10sPSPO6GkeO/m3TO8YvGuPMbo5O
3bKnq4naStmqbhGQrM+TY2/6y+M/yf4MEjQt2gudXMb4aJteYbyEJXHa3FG4cOKp
2/YKSf3CTbSi7FLqde3UAWNI5oHG70KD5m3+XbCgEbZduAgGjkRHqJ0qsMgi6xtZ
XaXztYApJ97JXhvq0UCCidujTd0i2kA3rVZEyLE3tg7SInfZrhEilg0wYIGBbLKB
a2NMhekNZ9ZBP22o6cvRgRcJSWD22bFC/g+A5AFN2gunTza6KSHASjgXYHLftSA7
L+NpcYnmkA0MV1SVgdMX4+cHB7IlRU2RKC+5mnm9XPqtoefUjj9tfZKt89ZCVPtb
5+qeyWXuvcij3GiyjrklR4KMR8M+dtGL1Y+4WAr47puqceXHMxjj/2maER2/E/qP
vWUYsEMvzTNhzkqwQ2YJ3lmEq97+S0Ac5GNckhV2jIcffNCUyaqLc6MqWeur6n2z
jHwUinApmsz+kpKtxoEFungo2fdp856FYWB4Oc+KWQPsX8k8sEMWUnUGdh5lzWX8
jmmhShd38iBYnZcFsOQeLzUeuDZzfeU3I6cix3Yb1CtXNT1kxVcICj+tlRsMJgHy
gprGg9bOjbVyR/7EF3xdDKBY62rSOjtIFmeB6q/MRqKs+8VX1jZMaEm3+szjVeDj
lKdtRWiWvPiHK3NPCUqdbE6ilUHltcwJ7B9zS0wf0zcIXIqzXQHe7L5NsCjSFMT1
q40M2qz7JpNj8EeMTJK491bnQU6jj4BKJ75YlICZZAjQZySIjDE3jWgByodQ5n6u
UPfLESLgoj3D/PvquOESvQpeTr/AkR2j7hQF/sLfAQ07XNfZwwS9s+4Lnx6cNPrj
ONkZd20HeYjfhWnCunpEa+y5DhnY6X/SmQVZJqIQ4KIaoTReKbfQawpC8h5NlVLd
innfS8MltuAOBRhMobjPB7zHKYo+l8ElOKILfz4bRPkrV6B45Sptkg2pbatCtjNI
qy4GNW5G/V5ABQq8JtJ0QGjjJUdPSjKBTEnKtO0o+dCaqe4NsvDWdDX1XIcRaIX1
IPiepSR9erCEVuLERG9//t0avC9tzzBGYGVbdccb8RZ3oeDSd/nnMidzG4EIdQDr
XfZQnIT3TEhnwXKtAr5sjbeHRG+Fkkn8mOqGJFlSBKb6HXQVmWz65y0ZY1vZoNXy
kG2AyUR2C7+C5CJ5SaIj+rgS1vOTWo1G1bj96m080AF9gFMFgWD/hjRe/m4m7ZFx
bIUnP/XBaRwIjF0UVKMfVRkMrU3B2rv9SNULfO/9radv1vM4XWkwgwY00m44A8hm
03LkpG8PJM2VEZK1hsRiUOkM5G+xelhioRtiBVd9Z6d/2wHacYAOqoMyZSXOjlKO
5MHDWEIItobrZIDu8ayhP7wcSoD0S7Rr2U4elEwTC7q7PobIcbEgSXPEbZSlAfmB
M3aETkT72rcGoG7JcakJcQBGFiWrUKPmLV1iEFIwhSQOQdXeuFmWXU7j3/5gb6+X
7Ql0hkjmtndkEpt59iLfPTed3z5+FydGl/+7LrnZlxvMfwX3yEm195muUCUQUIAf
ZbVHxw0+etJHzw5uDIZf6lZpqRTfHVvQNiyibBn50XRBdygyzX6JXAH1gkU9GVB7
nojTr1qih/c7WXxotrOnQM14vs9vv8XwBb+zoZcjb3jWZhjvU9ZxfCsQf1NG3CJc
jHEgGVn/jrgBdkVtvaBB6MI6GAr3gFP9TTAIYAqPW1zwuePHLzehWmiNeBnejra+
+lTm3NcVG/Kik7bz1nog3Flmb+dkFaywiB07nFCEgymGRAMgGWFVi1+9THc0na9/
o5Z1cpnNPWf75EfxRelOCGaOIBdEHLTyvk/lS+hoTA3X5w7Kl2qYfQ5avZW8u7fa
jwd3LifuFtfRy7Xg7/FAe0n8U3bgedskjYh3XJbVouhRVWImUe5wuy1AnRflkSAp
M6/snTMyGyO8EHlGMAZNTJSpYuKZveb9VTImKQzIRw+OgqEVXnV6C6JVI/cYW0AX
OB/y6VQv4b66OsgQQbQbx7hc6uQgEboFNW58BqKnzTsC/Tw8qqkq4DG8gd/mn448
zDC0ER4ZrU8+lL/CxfwgcuzNmnUBczNXxdenpOsyU7oCcEwpne8aFX7rIMDKJiqg
QEX2gJdDE2yfu+UBbHtnQPKVHtq+MpYq/l+fHJ2RW5geA5of4jmd6Y3pUr7+dGvn
jciKetp/IvNWh1G7QEB/3u0P3ezIgyKELliojX/SYCHYlzzEIRoWkKsfofjpZiMJ
WkDXZD8HWYx9V/VY6Yu79KO5LWFvSjMfhyu/P8CL8XqDzxlvbJT/kxZPnRsIICb3
R/AB3H44ZQ1tR3i2pXTXVTiBzi9rTFf3YSSU+oukDUhiS10yFsv1FAwerFmTlD3d
++z+N1bNRp/wUUMjEk4SBNHqSO/F8cWU+FQB+v+KBWCbTs5hAhi+Pfk+uuh/KPBe
X9RGZlA77yy3yK2I+UgjLMQsdXOJOju8XrYSKgQDPLDp+Ln3D1B5ptfS2Cj+IGYt
yHrP8DCANeaghsBQUlVODz71Wdy4seiLrRS2Y9MVGQ+6eL3SHznKCe55hUFjuj8n
I4erkrT7bDRVQemGucNOzfXqoLnNejlA0USlLZQUkZpUKaVoF7iO5ieEoOUk12kh
S4/TjZNp4Xkww+/gWDPOy9H2FlR4+PomYB+jScgG95ILTkIXqff8LurbjHYKfH9K
/W3o/cF8WsBo520oQrsya+h4fTifbehkpn/c3Y38m37Lyicpkc2rY0kpzPo4vSQU
1NK307CAOEElrN1e+Bn2smDIt5Lg1Evdcv5pBde1qlNtJ4kvQLyRJP+pg7Oxxjv/
zkWp9vB8PEr/CAf/KmodmL/zEIKUwvKdYjfmvIeCy3nhr032c3dBnr1TNNxmhTzn
XtAUOK1Ot+56WLYHyyJjk/RCyIrZfWqq0Mszbqnf+uANicew/I6z6FxUDTlnWmGL
eUciTbCawAyBX8ArKsIcMjeoyOvP74tNDlC3bti7GKZsjxm54w1TjegZcJn4lisv
EY1QkOVzJ7ZBTp/46vBoWDllG6QYa47y+CdVpuSEe3r56jJMpRWzZwK2c7SyHjBW
7nSdNpjcIMoEitD1zVv+gIxmuvQs8vIac1fqsqrsz/P1tIg9oz7EM+hIY9ddVUQN
N2ChH3Au+nNhYZtpkFEGHL87NCehiRBrf9N943yWxTgQQR2aV2q5eBsLQLQdau3y
DYwZ+3u8phK7+jULF+2Z1YzYINC/RXPYR2Cw652xxtMc1gQwZF1ZvLJQAh7pSfM8
PjtPDzl/7YSelDRCpcA/nycehrLtF5LDAlZlwlbx+0BfDh0v39Rugq896jkOC3xb
9neZnMIIxHf7aSsPJcu03zke1I2FIEjXxa+yG8aDvdlK3fLlQIY1RoElKsgjF4ZH
`pragma protect end_protected
