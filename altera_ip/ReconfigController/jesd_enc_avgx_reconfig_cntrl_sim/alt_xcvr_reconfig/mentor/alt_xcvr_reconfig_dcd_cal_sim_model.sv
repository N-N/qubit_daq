// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:01 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Tl5QocIU279/X4kL6OGKCR01BROTJhqsZINtwzMXNZygMIKwrBgCopXL8WoueHqq
UJ5TODPh30FSJn8D4EYO8QeRpR4r2K0H4duYL7MEX21gfwpkPmje/ZM5uUccVcRO
/kpkZ5svPemL7A1USseFARL82vBywDyTLQXiZBMsFnk=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2768)
mGGF+G+r6oTuGQiB4M0WBDKRsGsmPhybYd+r0x7hATb06A6orkOZEbG9tq3HvzaF
fa1Ghg/hmoiGxi6DrB6YqXvKMFrmUDd4t+0T/G320U9NO5h/ZNAjYDJrNlX8vYmJ
Zgog9CznSdGDRqV5HciYGUfcdBetYSaOTLEgiW8lay+l5YKKMy3ASKcNp2738hYa
zjFunKuhnI0myLNZEImU39UczLADxyu2QxzI6ouam+OeJ0/X7ZJjVpR+nvJ3UX0Z
w5pMOSuYbHcGHqKEJzqtZSYj2TRCR+E/5xDyT9kBWUQQIh+n+sF2MUAtGaoDEk3X
H8YjPho8utdUh/0Gyxj3y6Jt5YC1z6sWobuSLlcWwuGTQdPWVzob/uUPW9lSFBZ/
T7q+0o6+hhd7LsrgGUGR0zBTkyJ50MhWZdM11/8rQdWyab0Oo0iozi29Lypn5bWL
x5suKxTY/JpI6lQhRQs7VgKRISnYhTjvXOBx/HMUBQ+CAZ5JNZGTMbmKTHKsypWs
Fz9oT0kYd3Vee3mDSBEr3Al2jCLLwQ9BsoEudua628x6sPWVn7J8e6HqY1s09bvH
DA7Frv2RbSkPJE6IKl2+6gMIIiESiv9CtMg9bHNQpvK5+EFbT4NEUA/428ShsDuZ
uLkxESyEtV0G1+fgMG1SXuzm3F/sGLtp3iLTMeEn6BqsKywMfxP6pbWeRnntXrGP
CD/5+XK7YRguSB61eiVIhGu1veEAgbJWuD9tTnuFYP98dxpwIyKjn9g0/FvMY9LJ
vHXyDrHdxzPUthULCRHxx8WRHf+D+8dkfGf9LzQkmIou90CtwLUMfVgro9gZrx8P
3ZcRyUIjHQ/s5abJNjB3EkKV1MJ3UM+Qz8veZfOcKnODYK2gyMsuwwgrGyyNttn5
nG5FvNXrzuTcgtCASexQ6aJMnd3ZjwAMm03tIAO5qic/WjipBmJKLD24ZWKgU/5F
jFFUb8Rc0Ks1DvZfrdtm6ZPpo4/TDz+2FHR1hyV+pmxmuHmWQM3ou3tZvOGjnNoR
uBAAaon8rV44DWnGhUs8ve3qlnM4w4G5FT92a4EG6DKWIQrNxhbJq9nkc1HS9Oeh
xTA+Q3rT1DRpqKgMefkz4WF4+IIuXAogtiHyIaAx7F493rTCOek1qvSQX6VyJO+C
w/MINx0u00VV2TJPPF21ZUEpUE6MIjb82cDU621E2S3A3Kic3x115RP1EkkfiXVW
DS+in4AuYwPRj9f8AMSNKS9YsvMdGXQNILbK85rDQ75iihx2XvfpPNvAaPoa1yhp
Ri6AwQQQz004N7MirTMRbtjMt8h6KMh8dhV097ZiRvPcSJjs/xxLo3rXQG8fpcp2
D90FGAlqZqAH8dA0MgYd75umzm5+zw3nIy/zucV5lBA/oN1T4+7h3/foEOSZ2pCP
3rpPonKBDTQhEp8gAgHGYUnilDsAEE65F64yw80o4HN5lnPmA4xIIP97ynlyfdFZ
/QZ3IuyViZAA2tB5unKRGs4wIlxHZYg7FIdEnLBIsd1+A6MpRNPVv3dDo2qG8svs
db9dKSv98U2z74jgAnrKYRM5uGWU3THXwmopnZ8ULjVZvWAJTf+S8KpPdTwN99xM
vvtv+Cj+9l763JSTW6OqwUgaekcPQLuR2YKotTF94QqbKN4VXtNH4u8Wuk+c/vnG
HumUUqFfnFF3T4IFE/VQGbWVtQ5OV17eFzPlHUJyhBTSIlZiQdHoccfYv4dmxYJc
6CBcw2z54JB1yPIEOhk3XLUkHXeK7Orfysg/Ukt36IBKinETqQSahrquxw3msnz/
XRwvvk6nZXEcbNicQ4LdCgaRoYdkfZHZ/s6UTiwt0QRu6x8t+FxgYqABZvhR/Ejs
RwbRr6oQNgmTdGnBWZC8qAwMDdPdVfBSd8Vfaxl2EMhx7uZ90VdwA+lBiOFI02YM
v6fEZu0hkrfSxfZRTA663nqU05QNOBoAq2mx23sHoMq2EvVm8h8Yzii0f2waQFcU
VfMX9caYS54CvEPZlmdcTC6kD9+OpvAVR/sqRCaotDy6LOiGKePkIWSaholcsF9Y
LC6Wi5Kl2PgrBivOLgHld84Y5hlOHVlhBllp4poXY/nphFltjKelNdHIhY+bGqzQ
Vjkjgh0XiS7RrdFy1QeEYLK6GOXkfgGoOxx7q8+Hh0L7iGlvvAhKvuKUCY5/0H+H
szrmUuvG+4KBKMXBAbzUTtEN6hHRyXEVGiF7jRM9ddQHye5i0MYb1FzZ0gSgN1Fo
FHAL5CDb6H0tEaMhAIpEllG5mfZeHugtm7H3SmiMfkh4Cu7ztCWaRXq+xc0RWUOx
IBgDP5JFJMHe1utqOsjAMzT2+FAr945dqCKk7E3TbghatzMx5MDdXFKYXihANZE9
E+9vfLtlphK8jqZ7bxQCD+Ag0shmdBnGO9Z6gdZ9EEe5L58S9sITGY4MrtuSujmi
HezztR7llYyIWcyjmtNR2M+icQMpsEFKk8m/QwlpUJhEUKVR/guum6p6S2PwVrQG
GkmKqMhurWRfpsrvidVf+Kx+FkKqqb4rta6qpZAY8OZ4F6dTetSrPCkmMpej8kex
b5lp5y6erneaabC2IGTsGJUcSX85s2iVB4ntRgdtk78VqriAP5QCGr1IuUrYYt95
QcekzcQbX7K0p3oPR/sZFqVdVW9zB/3E4UCmfixLTcX06PEXErDT4HAV+O/9F62G
10Em0XnAJnwkd5ixtev4/PxoHTrxpqPj3LnbRKwSpg2GHAjNcEddyvRbQAY9UPiZ
+lIU7wDVF/b7/XJzcceWkoj9hqHsRGrh7KXWIt3CCaVQBegMHo6QKPGGWWpZ9+Z6
/p1/RJkFroj2GFKxZYOQfgGq8aQMIZ+smpx9lvUFeLCeeOvkgcWce5Dc0sumNXWA
rrfkogu+3wDP46V5FHUYa4rbsvGMwRQVzrRHsFoqdzUcERsTOXcmdG5+ZFV+KMGe
C2A+I/B/i8zXi9uzrGx69f7s0GXc/IDZipzI356BGHEYFUjBrTNTM5X3prRUffuI
TKXxKHWbhg1nWE6wUGhhhRmuXCcGKyviRs11PmQeMKCOM01ZaG8V9u6DXsh1KBm8
8c4920zPVrTDlJY1k6hnqgltp2JPuZhb/Tw86NV6FIcd1ZOYyty3JkgROrhcLYAK
AmE+ai8ydF701AwiYfOdNeIbyMW27VBceCdvDxjRlxkg1QRN4nbqgrgotgEl7DvK
N7VTI6B38a1ga7GV943Q7GtTiTLmkH++jSe4ck0kt7qr98pP+qY8sr1B/JkRtODT
NXAGXbd8t/ygHs5oiafzij+qZchaR5ssM79wOLuI8TNQhzMsLKZA21G7oPE20G4Z
r0KJ1PFCks/I+cXgQ93mr8yAulSWeN3johakytgPgKMH6rDY3V+8fYBh4aNxeeEJ
8G5N7+1WhKsnwxS7inJUa3ECgXXqGGj27qSD2NO30S2VqQhO9QWoQw4IjvL3wWhr
wIGf4/BrNlLsfHQVBJ+gLbLA+mby1ZEphpq3zidt+uu3LYuSdE6+XxrAZPE2anVR
AkufYHoqgdYqv6SFNv5nX9Md1L7omEVmttt+g6fmu9h+vtu1433M34cF9/ddoGoO
yPYKouiQKMTu8C6bayW9L/rurkYNdKZFrLk9d0qWnVtOFxZkiZiZu3wT3cudmcnM
Ji9/IESVcbrqw08IQ6M6CE9F4wbWB/YYFV8JqE+CPTY=
`pragma protect end_protected
