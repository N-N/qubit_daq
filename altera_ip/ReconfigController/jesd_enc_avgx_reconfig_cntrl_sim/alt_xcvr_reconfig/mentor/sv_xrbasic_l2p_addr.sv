// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:00 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
UeFsHTSdE4BRLkNzNhGS9kgB4u8qCdHvafbhWNReTB6nerlQKQ74TfwfyCCALweh
N/tMwnYydCsbrmRqTy/325DuduRymxo08PZCAzNkpmhDLuB/bQccU+2dbk76A8wt
Bs1S02kXg77KQt0hwDgJLIxbB/tiqmXaIN3q6P8u4M4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2192)
C5mFW73cwsbu+Di2rEOghG8irp/1/H9gO8vbKwJp6gkW7da8xoceCaa9ckKTTg2Q
jlduP27xkaA/3Nh1wCgPumiHmkvRvMg14RJF1gIo+vDJ203W5Qu23rW8yUfrvjac
Q4R/QrTXaVm3+TDnfCpy0vfChjKuKuooBiAHhGFEBj5EP9y/dLE/5C93vnlTLVWV
Bveg9s3CpbsHxkICe9lR+JtDoxVMnVlcfsV6+ekeglACV0m35Bt7Q8eHoRf6SOK5
qlaBa/zWshqZ7+p0tE+B2usJOsvsIE5Ju+y81dk1UZVDwNoES2D75atTmihdOy5K
P42gNMcg/z7sgCCZfUOatm6xTKS47j6w5CzVjiAYSJ49Q3JJqG/qRjRzC0Wh8v5G
Ftfmr8QzGF319BckB34O5X9te0oMEXvfv7TlUlVaO+5O/ki+Q0f9ivK3bVZrNv3I
+dB5TYLn7PxHwU8HtJTDE4UIq+0wh7bGQhVOxYLPsSAqLNfIiYaXIn727Rbd2Vr6
Bdr98Haau2UI27Vj/Wj196i/RMaDW1Sv+HAk1ntwcZ8MGUhKZrVH4fxrju+VQvLJ
a0ovYNqvnVbD0nesh6RMPuJyu11zkj0q+gDLyibDy8VGZUbrie/IYXQcd42jScSG
IQNshfpyBWSCGwiQhBr6KX4Era4HSR57WSunGwKTsui/hMxKVxgJARiyvd19Ivsi
yGOh1UylU77HvfqvTMefynJGkiLb+8CGotZ8fkxRjBVR1foEwif+uRbPKOrfI/m1
xnIMu9WSFBi6Xo+Ep/sPUq36skmSmwcQrCYOJDx10RelyrAB4aIYhW2KFKQVyq32
vrMapQJcIzOt+xKhOC+l42937uVutEdVxpyAy6d10Ty99qvSGNvMLAwt+7zKvoBR
kzq5Zzlui+IzXdk3ioRiA1VtWDcbZ1E5R3coyFcOcLxYNyvgKAoZnT1hWh+TbFvx
b8EAWwWEJH9kxOXSHopsf0fRBmeDjkV3pWn0DVx5tF0Z5yGZ+WM8ih04IAjS7IsV
/dl3eivScLHaxU0aZsPWFLucTsPQi8AH60nv+15Rw4y09YEt0c2HaNIRuX5Vbv7I
koqNfBsseOT6xJUI5e+VWYCft4QajUU21bOc3KHAdH4kj+abTC/L7V/ytkMsX6Hx
AhbmrQCHt+nPfiTpc0/L5ZxbHwdzqbewuVLQpKZekNrqSLz1h7V0bxNcMRabEN68
3DnkiS2pMhUFE8+yJPSjHRLKvC8vHv48f6tLu7Dd8KB84aT9jeYqOxZkSNveeQm3
W/EXTb2kwVK/bpMCfhvcIpS9AiouCrLaljstVMIZogXYGCiQvzZeMz3a+hbAn1E6
dvz5HoCeY7zScDkz9T8vjAzXYhA4uREoV3sNvgOFSThWD/IjdkuF3Cs+FPlrKUU1
n2vhp9NYQwPmWmMY+pyte2jMnV+NUSPpZYBNjW4DlNObqeTKw86MK+zwdpnGTuRm
AtudwPu6ZbLE2znc5s/noXXJEEfgqZfvGot6DdEDfTh84IsCthxLzhbkeNynX4QG
tk1TQCONGuBcmYmK49ORHZGhxfjtayhKenlmb3kLQnfFVCA8BgLfCVGqkjoqsw9N
Q+tAE6yJPMnQnAr6+LFYiAbtK46svyOVrkeAMK2Bja7muzf+oYDmOT3uXjLnhHxa
3NN9w49f3kqfxwY3Tpxp3OPFNWMepICvpO9I5SSO27rzQHdvn0Pw+mAo24+rcoMj
nXImP+c3zWKsaC+XxaT5EIVrE47Of8E+d0crTt1Oa+mFVE8IK6/tdbJP30U4Uv+L
2z9Y6TU6+uW6msEhyRa2z18gQe2W1eBqtdOVuuK4eUy4QXuwM/zVhOAWDZJ/1Yl8
NJorF7CgpTVBSCGplSJlYeKOfXQ9NVNwvh/rzBGpjhYQWphgvZq8/FWDy90cnsSF
GI1e2qj4EDPNRxRUK4J7dD5yK6etzbifRD3uPiT52Ag45kpYe99FNsEsCeqiPV33
IeYNJ6Tfr6yRPoFmc39H/0mqcg3KfZ6uMNlWniYUoW0ffiQ0gC4E+ns5JDwemeLL
smeT0wHhvCOyaXXRJfosMmwUoYfhF/0LYabCU/V1OhHreF4+qY09ccC2QEi3byEc
aXEG+kzG9XW6/KN52YQkG8vOzLUXhjw/WYs6NQD8a6ftKcySCp9bcD0bYRJYq5fR
P5uqQ978vGIEyPF0CeeC7tQ5IelSes6q7xvKtfjCtUr2PKu9xkiWqeV3VFhu4i2s
2bgdRsIzJre0jgrn/YE4mzpHXgkqDTE9Z792k9RmjK0gxYiXBxX31Cp+GfCpxrcT
a/lToGxnv8B9cMwFVVcCEN4HfAg2qbOQvD/9qC9v1p5paBGPnXXiZzlRj6Q9HwPL
1TrDIUT/I3RiflpBuJZOEd24YB3g0E2cMnh4bnhIOxNAQJ0gjRkMvCDXmM4+0Oa5
mJqQJmmhD37ETkRH/1ghUSwiA6DzdPZz/cgs3IW6cJlIKdxRsv8kGjQLg527Wv9v
lJJq557Z0E6dC+hHVXwEsKb5Zlm5Q95wi7EJsZaTCpWUApB3ICAfl6zP5MScUz5x
MM5GF2IbY72biVOBXXLwTVtI01Mp3z3bSHMtYUah6JkLMg1j+5KzhlvVffVoSiVI
4UZ3U3VEbsiX93znT6M5+rFGQYWbVOZlywwQOcqWXvdHTZ5pKr/iI+/vlXSvEg84
cjglbj463t6XHuV4lpG6Av4EFTA5PaHT1m8pas9xM++zm7192r9+VYGt1MpRi6Dz
tjr3Up2wzEx7GecAOU72fscDAYU07gnn9jUuCyLL1I9mLx7WS5uVyUj5ZtcXMfre
zuqeJ5zd12CtdgpsbYmp8RTBUomrr+Hq778yCQANjzDkSCyTOHQFKMMYar722AZd
rN8aWu043aKC98PWK5y33tf39J5gdtjNsZhAhaEYcVs=
`pragma protect end_protected
