// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:03 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
JtWwNr/DaRWpt0cjdkx3K2ibu/+G0otZRlaEk/2X3koCcD1XSpDyUTpWQRiPFlfZ
dUgL115tIoJ0RmzwoTGtuTYdNqFPUr/u8eKDB4+UZ3XbApLns8Gm/FfxmeRsy9o9
X/1aHOesFISrYhpvWhK6bBNoDaoUkXknbLQnYvespy4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1456)
6+DgAO2TwPPxPTSiDukssoGkUUfdBnjyN2bns2pp6HzMSg/jLqz19OhhUbY4qf/e
E2z0ci79alxz2X780naISJaGK626GzCIl1dNQa2dQZmSMWnISPENA1NXhjFbfO8E
5zpEXj46zBZLeuinall1mM7Jkgt6HOkRKz+QQ3/2um46w+zieJ4UdoNnmPy39lVB
+Cgs+M/Q82Ynu4od1O5mDKtpZseSeGxUg3vFO1shf6BswCjKNYDehcJPWu12Nt0M
LrTppBulQeLC+mOSOT5OT+a1eQKxu7CTVLbtmKE9nmBRGeBuQRCoSqIg97bJdvo7
ciwNpo6eGeAbImf17nlcUpuZ8JSOq8FGKeivgQWkqGUVXwKJPdVwV1EIaMVB0fFA
eJnxcNtdvwjDNtQ6Y740OMLG9zhDqx1XCg7tqXaJtSSGib5KGmNIuuArzPEuVRsh
IspRkZklfpoYoGCP7U5Iaw5OQk3hHfEOCZGh4ShVhlvciJQVaAu52oXOJ5xRRubN
D9g3kuKGxHOAPcFdb7BuRyeOeodnge5KghkbTAvo01n5FQHZMgwfPVw3MZVA/O7I
F9i+/xJgLImnFiHiZCJOV8QNdb2MWhYfPMkTmM4rlrlt0GiSvT7jaOK6KHv8pBcV
/YA/xkkxVF3hwmZtM/pW0TgVPHyK9USkFF4qrf3Mqzi0p7hkhNXwklKEtw40A141
7vpz3QuCv0vgH6w01MASblPJz7e/4uhzQxNROTIB17fjZ8bjMJe8kIWo5ZN/HTUu
5lQNDM2pDmIciSVjk31S+ceB/uJDdTrOTFS3P825P278V+zi9YIf/EqFCt+6GX2I
ZISX5Np0mHDLMGM7FD/lEoZDnVj4Y08ivveDOWILmsNM77Vaft3YV9h5tP1lYHe9
cb+wQwLG4xKpiWTal8saqXPaDvuwNKasgeg4vkSvX42JyUWjAQve4x+743Sryiq2
nn6PNOEcqcBecN9uSwppc2306fnm5gOrxsNXesCZ60blYA2/3X60BQYpKPTORIh4
ZKNWCYyc+wTKGXoUWm5er27wKl5SBna4WgXFUH2p7OPn2Pyw0tgsf8gNIEK6GoTB
gLidFKShup8qOG7Ti6MgdKd9S3lrNvY5Vgn+hSy8TgkY75eISrcRc1adFq4YttQw
SB+uFS8vv2nc2/EQuFQO3zykHH84nD74dgDgHr5O5ys2SiaYjFIDJzOme4NOwz1L
Preaia2XyYhbH2Mc/rTH0ukns4yoY/w2jhOkxmnzBkcCZDoY1g5Xj3S9W4Af9KPJ
54qJpr5I9SA+aLH6n9JF0+F/mIF906EoeJVkXcdKXAZL8+RP0E9Nz9n8EdYecK9J
jZ5V6tGteA6qS3A4L2vMSjc8ZXrKa1sNMtgw/LV5Fx5Flaq14xviND511JpVmY9t
MTUjDXrZTT2FnoDOqI7ZLQLS3VI8QASbLYp1Nhi09A0ZRqNaFVY1uL2OQTEIkWQO
M1pOkcDyKd7Wc1z2YWBpxq3yusXO7uXeXzDRvkwZY/Dc8u4zxn218+uyZQi5y9xq
TClHbL0m7mbBl/N+DEHBQQKtHS+3SSIKZ1Juqq3+xQYdYY9hENKqtqW7e7nkTtrc
BC7UzljY+niWLJj2MKLa3CS7K30MZC6vujxt5akUHZ0XUhCtp6JXuTHwSrJJGWzE
RK0JZlgYtmWU5D+PkVwUMVMiu16ctxxn+5ns3dV2QlGG6ni4upUuOuwaJdg+7UjV
gSRrs3BG2+BhvNgzX5b9UJIUl9j611dKn3RRsPzbBjxAXwG9R4QjOUb6UG9bLc4H
UXWo2xZxdeAyava/uFK8MCKjxqvzCsffixRixsPfk5YFQpQJSLyouULc8NTJqbzs
gPFyEeKu49GYVfI/yZjw0NYj4l4Y33XFDro4N/nRsJwISGcfkcAY2JKy+xNtSvWB
V3gsTnvAVOAHaVkReTNv7w==
`pragma protect end_protected
