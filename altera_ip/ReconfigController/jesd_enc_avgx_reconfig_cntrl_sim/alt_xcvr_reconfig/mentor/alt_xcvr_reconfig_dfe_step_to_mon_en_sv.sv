// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:04 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
A47mrHUmJ3QwhoorPqyKFDVC8c0GPq+c1MmuAHGYodeKPDo7QP5AkO2KaP4LQdU2
HnNO1GlJENW2Kp0zWSTQJztWGBaRVKyGM/7BnW1klgMr0g6N9RTY7N1MFsQfSMMb
4W1Cb4OPorfmq46hXwrzMkMvb7lX4Op02cpOFPpbTP4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3888)
KJg+p3MSL3YOgFfrDoHaPNIV4z9vMlOba8nIfsVxeiXKGRKMasqOJ6JuovNBKl7v
6Pw5QPfUaVbaLy49HIDQmtiYKzLVbiaRBIIHQwMsJecelsU/Ixrw2R+mA1cvhe1b
ZBvxyNdTwHvVY69NyV8J47xN7hGrYIl5E9S/1WJiUHWlPbVuWWWhzKc6/Z3SnTzu
7u2/L8vbI8xewfpYH/mTo00hi7Hmkbz9HwgZz6KjzrjTE9GbwiVjDQ2RejCm/sXl
mjsz48t9QEu2VAmt0yE6BVxfKgUqUry0mAd9dOcQg/2okcwSkaMNgqc7XcUajmfb
/b2D2f9dg1lBwVOaYh3Gsm1gCfPeH8tr2AQIaajR0LLfQZFWJz3vC41fXevX46vf
bl9oTtrd34BeSsgbYguIr8McOdZzbZW9gHnhXIOjqfyeKf5VGUyIIX1kAo/kdlMD
2ZFp/wFbnEwX2t3noxCMtE27FnlwKgr+WKHObWdcXsW0qpGRHYqLIWFoI9anhtw9
MZzhQyg1LRD7GBY2Fa5tQpdQLa4vnPjKWdQ2RMtvDXt0fAoDiqbj25NSo88+7dSd
R0xzvmQKfoVUMp8MCZdfD/aXHiQmGy5TNARY24sWKSXT/y2h+tK0B8W0er5Fe+pR
pPaRKJbzcBJXhdsnl1sO3NCWb+hTp/x6YBgbnyJFVcjSIoggodbP5N2pKi+gIw04
lh2y0dqo3yAQC6w52nGawX6vykQXv1DJR0V86kB3zbTblCRoiDOr0md/q/3eIDNe
XLZVbGJB2gaNTB4tOTge8YUO2W9qSYESi1y0qGiI+sdQUpwwalPAkc41Jdw71owz
g2NxDp4X7rl4rZoSmJxTg3gMbYTdgM2Y3xh2mnEgaBm4lDGuFPtd4m+N9oWWjCND
pGwonEmWrkMo69iAFD/dKdKsab6lvREok5DOmXr+HDlcoa7fQ1iZLU2r5doyI0I4
2uwP6st873+IuScGsvQyObiLr5Lf8Kf2c1x5F3MrLFBdB2D+Kst8laPuipkFNc2o
9AcXtU6oBHq4GtH2cMW2YaKCBKMG2z+M318Vb2elRtbL17aSwi/zU7MAmQmv8fEo
BwLoj4u6IElGFPDOcow5gD7O23FsKfB3ZvcxZM39Tn0Bm2c5Uidd2yMCUzvhBowF
O9WqHa6CJhj4+KEC45NSkrew9Qw2uemDLyyo/MfQlDgRyEcs7D4R5DhhoFHIAmBe
1VQDv3pOUFFt33VM8KbgHTiOMFP+Z5RSAGzSO/5367PdUMKcHpp1KPOTlBras+PG
SWqe0+4sxvcJCZBIB+5zdZGh75eQJ6NqgT/rmajIsAUfgoOn2SfDWx9QwtcNnftF
iR2Ccvre+3754PigYYF8Vwaz6crgVLdhtP+QcQoASbt79jdGxZM+dm2u6d6ZEfxX
zoTqf3ou/0XD6lzeKYGWeil8su9sDVUg/pGfU4LaR1CF0IYI5k9Ij6ZdwPPX9zip
bypyRsQRWys8mQWPmsxyYnwcoPRG1ibnlGuQX/WO5hXZ6VHtSNRmH14lpYe7isp1
nA8PP616JlwI9EkqXKoqZZQm3MEx0cXPQYns9uvXz9eaFsXGcbh4SpWdhFK7uFms
ioK2x5TxcKbJyxRvZKblyRgY3yt4RZH3SkP0S9grMMPi+Ck5xMmlcHWDsbtw2+Bz
b57+X0O4ivjKvr7dXyBAhaoEej9HgMbiA3DpIaHZ2gCFJ2yY6nb61B6ebmzelCMW
27E8Sa4fF8saTCiYO8RhuRF3SRzamduTUAekbyGr2grAo9z2szSFOh5kQpd6z/hg
SipGCPjCw/gJeeZsaSEeIGeWI54L0cDg/ELFgZnIJDS+y6dihMC8SYKpaELlS36k
6DhmufpHyi7OTN07s+Do2anU9JJYvKUfEYKtim1xrHyHQA6gDqYp08+bFuluGR8Z
kOrQ30AGUxvHDWBUR/HnXPskzWpZbYLb4Ur2yK8nK9yBNJWrsCoE5DE818T3NfO8
6GoVf1qfGaWXD5D/9uFn9g2VFJGdGRBnOuk1IzdleDeFDnac+YM+/+5hdV+UqyPj
M5sNQtPn50j6gB7tc7jELpxx89GNqf68vKkiAiICJ32dlhPk5iXUNXE6M38h8GFH
6mFFa4OG+drleY9ksw53xnP/z5XzvTrPvRh+hHHHs7QilP0uKQfF/WqCzaickl+C
6KeZcRfe1oVOKucvbNzwUkG9u950fWzxlZItCGnlzJHkVLlYyeJzd+36Rn2mC9tr
aOyqJlf1O89RM0HMzAO88v85k8CW3uCeY5wf5evm14ToF1zNXpVfGRmH2OEhZVaH
fHhHGV1W+Q5KVVM/X8tRwpESUiXoLtLpqXnDmCkYQFMtoJuarDAMG2hWSyF+pftj
C3HTiOHKMW11Lwqi0QGqgVcadQOEsRM0i6ti090edoVwBV2DG9FhrLpJUUHTFWyU
8YUndv16qyDeCJNInCxUlW6/YMOOILuzeJr2sGjxapcFoFQ+xU1cpIGSPcpxi8vL
nqS8G1J3i5KuxlymAuudfsmk8Kdud2Re7CzOnAU+eBgb8hMW2WoD+qtHQmUvsGFA
+PO5avR1hr4AZN5jlAT66t3mRYrSZ0RJpB99OYshCw1VC9MAp0b28vlksyehHH3f
0jOx4dZy9RhfhqdGBfwaooiBYdolySqp5sWD8AYlEiCRWQo+8H5l7FZvd4QE/lYH
SxhIa65ap2VWlaso3OeY8kCNkn6U9wR8nUbq2TuUPEHcTl8MucJm2GOck1D0MQ3F
J56UY4THmt5Bg8OR1k9O7iC7KoDiNEehFbH8B39J0x3Fe5oElvCWLMwPB/Msh1Nx
r14T7EP+R9Yj4sgeL9Dnjvi2XRBGoxYVD14CZPvx8Xv/IIg1/SRmV7vPv3e/xS1e
rFBHg922+3/wvFG+k41KBbgtG3DrVse28rGty3O4ViIoaKbflfyqV8T81r7csesR
MTc7uf1XdJQ0Wv9nWzZb/y17mRzI4cA8trM25vJWCN7+Mnst8jpiYKCW9TslDFEl
/dDfmlEu5FeNaAI61AFT/TwBqDaVFN3ewdCpVJHcNgSvF+3U0wvJOIiIABbSkRuX
gA3VNWv5NEeq/YEQUsqiwFz5lRn4HMr/RXxgyf3u4JRprjGS69dKXBdzMnDm4PCx
jq7pCYYkI1cdaQZqQq4/rOtgpCJtvOXsgijuykd19HojFX5IBKylJsmkrwfBZ28m
HdyZSQJjJ2arKvYfNNOscIxhBxd8M1baZ1olq78FDCn8I0TEoiW8FPEHBWJIs7U6
ZTr9vgiQ3L5luiK6rAI0HkGNlrqnHU9eeLfkZhxypXsncfPBZFLJEzbEawYrkhBm
7z8ljNN9yQE45iqzUmakuUdSVgZA3vWk1ao8pRqqDu4bIEbqAQvVmdXRPBuc6Udy
SDTUT2yGf0kNdgu+tkRP4TaCwzyZIo+5Qx8KnixgGnraBf+o29lVDdV1M1A+ssAB
hIa0FefvB1vWLolUj9olCTEMwl+eRAC/nV9wjT+xJCjKOmpYmdPZjy3hbpX3O896
Ri8keClbwerdiFHlfqNmG0gCc+6muAW3FLrFpTcfj9lPCTsio+cguHH8ZkNVwbBq
9CL/g57Q1ON17gwadEs0/L/7FTYHbkqM8WEa1itK/6xnlFziBi7RXfdhH53GZXi/
drzLa8mIlFqtaCVa1W3RMMSx1xswgiQSe548rTYlCmxGpkKEaaMdQlrQlbbC64bf
GX3SFPM21881p+sDcPLzJoXz2Khs30T0Ggm9R0j8iYwIk0M8QS6MbjR7DC+sn27g
kcUpI1XHZ+hKc2PKbi36ZuhZXDz0n90yUGDDnLMDkdGwtvpgmXjwcbMCP09ujsJ8
XPtPAGdCkejuDsGkD/QWKAiNmAVJ+kZYVHJIpiWzDCJHU2ETr6buFFmqAsxaMCXf
LIsOi8dLBhHY/h9LIvZT7EZUQqzLz2h1bx/GoQXEsAicmO/piy22gZaOQ8LnwChw
vRc4P/sYB9thhlHUhW3cUUlSeWagzhJfmYqXRwWeZXI+zHplyr8Zrbi2IiYaPh/R
b+XcWHTfGP7XetXS23fqc8xf7HRzVgbQJE2aLi/yT44cj08zjqq6RgGvpCwLgtn1
VYcIMUfWT8J0FkmLWNbwa1xzw4llUoa9K9/yWnl8qcJjFXegslTwKdqk7yeBYtyh
liL5uN4YfnQO1EIL8qaD9F2diLcrANIVIoPQjBUW2/p8Fl6D75HTc1iX3k/wtXfV
on3Gu0CgAfxMTWQvhPT9ro402eY1JKn7ApMrQjFSm12iSduYftd9jaoHBhXKPA9d
PoCuv6d1oni+uk6KMw1dzb/b1sl90HO8xPz1MriinqitRRrZQiWKS/7H1H0TKiM9
ESjg+6DS+McH3hKDuZx+jEHT3h5GbJpPE2NgHjDJWbAfwm6MLTqZ26J/MhefFs5U
nsA/aNPCi1hXpYiaUbi2UMIX+DZzHy8MNlnl4pL+WEUrf2jsze98gjOYuFGQcAoY
xNn8x0skkoV7Xta00PZZyvxC6LFTlgluICvS44rY1CTCPlEVQdKhog6870wyjX1t
6B39qr69Mp4V5ug8bd+dvBPZwZEvr0Uat6WlXQygjejW5gRTJ+7q2TBM+r5d9v/L
w6kgRdESXFlsh7+ApELiwMFnEV9C3XuZZp75ZQWv+jLdZKpYbSZcE1z0AcMsp3o9
uxxGWzWmaq+U38SlsrZVPS6MyawZwEx9Ou5GEjQwOEM/bJCVLUDvslZ88VFlTUOM
JwmCBbfgp+7Xi1Y66/8hxqcpihIECG8bIfh4LmrE+4xDsyv/w+c7MT0oQTj8t1v4
3ougEPI2r4kfrpiJ22r2ZIGJ1NsoenGj+EayAeGgCYT4i7xUPOTEKUGqWBsAYbol
NiMYyfhDWQnemRmr5tInVLvMhrbgU0AoIeYOOdWNEjGQj0O5VzmB90rb+8BFcEXj
hr0DmkaEeBpFaFesMfKlt5JRWDONwWXPuJHOYSiBSPNv9LV6UtAjkjX9Syy+t6rE
boI/ArohKtDFLQrKTZY4UVJ1S0xL2gxMZwYkU2CtHxCTm5cK8QcEhyuSFzYfXVz7
IGfDOJfIqFxkLxrUuxKJPr5+Z8rNzYoESUcwZkEr1WzDBUp9iQCDxbygU728TwOB
L043Dq25cGC2Oq6IpQl+vOlXZb0aEAf8pk0osN76Fy1s+BYvivU3V4pTZMkxBwIG
`pragma protect end_protected
