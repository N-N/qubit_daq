// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:03 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
WL9bH+jm3najCdVKO5DYpUYf/M7/+dDhLyw66BDuE91jsntFUttzg2pZHsfONR/5
/acfYCD9N3AyGWuOquC6owt6XmhgR2d/enJXmEyPzacRZ9ub7109tZx55cAJBa6c
VK6n9Qc5GGwGyYPFT3e6u+y1g5+uECNEg07ZzBp9jmc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3200)
/Ydc4tdQkgdoqyvPklCcES+Oqy6d/H8WjiXRGpDVOndXzr0qB/0gBeDYt5X/0n/h
OUbRroHA/WvlnB0Tur4V1fC0oRW+azXS9qda8s56ZFF/5bOEbbV6vzOQTKGTZHB+
SMkQpccMrWSrY1bBLEp5Rkd8CnLYb2B0jTaBLjbZOSqBKesnGO84mfNGKUdQ+NVl
qeb9rrR5+8r3f7nz/WkqO8f7ezCeXMaR9x1ywkwxzgukngUfEyGxdcTr925Dp2Gc
k0UNtySCr2VAqM+GRGZhduE0b0B+BhBI5DwsoSTU0YwMImKYDLNk3vHfrqHJgxor
lpJ2e5g+SGZD38xcn97RCi3JLj/8oilOiZ/8idqaFKOxUU2VbYMkDuIj1xwuWvu2
TrVOQQ+BYtZ6/kVGdMvXq7TRXsx1mJ+f/FwJ0Xt9+4BdkIcg8OPwB1woqcTNZ6PE
k/X5faxObi763+SplwMYSj8bgawVkFi8KO1XHr7A/niryooxDxnllDOln2MvsIVJ
sXJzSva2QzOLZRGVX49Tfn8oKo4pH2JjQkMUFZMLrcFctYTcxv5mc5P1hTW7AN7X
nO4HD3umVlWiT6/uXV06XgxmxsQv1oHlNFDBT0uAkTfNKRMFXggFvP83S6kk4MQj
jKPFdk54Aw6SYI4ukkFI8TC4+oVt2IAfuHWaKCKelDvMxdh6cgjx0LJ/wvG29oz9
sJUwX+AxIOwgqZ9oZin5JVH1WjDvG6qfXC4Rp+CIbqzw0FDD5zP/v3e+YLLwdgot
sSbw6G/7lvbXfuLE7yOCj3YfS0HHcpkEdNKAMpwf7XZNmM+r69dCLaDQQRn1CuDp
jEPhVMgUm4Gu2moDQyoTaKDWqnT7j+Idry3Yolw7Uni1gTyVemaeTspXLNUgGPcj
0UyeAOI6kKlLVhA6m+XTGZoJC9LlL61/miLtyU+3WI+jgIS5s2vPk3+qzrS+uG9P
aqMo1E37sLtuvT+iEhXmzil16jm3bSqfV0l6zdUuKY2LBmh4Ec4GHHtFZOdZf4Zn
lDUx5e9hOsq/odt3qFJ/o6b1v+cV0hlH75Ii6uo8oqjGthwWFY3p+s/O4GK+LZ08
iuXh15nsVxJmC7MtHl98da8AFu+dFlQvSRDY2bqbpDwZXMBNZHp04fb01d+ZEaCT
Hq4a4Of3yboees/AXk/+D+QQql+TNkIuvWi/I19eQ2P+YTeyLFFIs8r0A9Vj+N6L
O51PV3eg5hIV/yLdQmLAbLlYFOC/szysWR6n8FqezRxJ1i6mYeVYhblzqFBItvak
d5NkgDze5AZTQGlFsfWIpq1CPXEfUZ51K0GquZBr8Ul+Sc+uvhPhO6nwIAux6T92
rlvJI7/ScWimBCz4oZ6Xm7k1IoQrhKsC3Br6S0ZPoUGOPtTTBCTlK1jJGuSIs92q
NLQnt62wrnAfi8dGKHbJaBjCb53FWorQlrlC2mpq85gXOheFIx/z0eMCoxzzt3I9
sl4gWiuxp0vpy74prp3VHU0kXiAo7a6BGMvGHEUWJ/9f5TylF9PT8oaTpLN6plFA
qvtYqSABLGAS3gq7L2GtCtoLGjEMI8hFdQX0XND7xOh84iv+GocU6GcUb/9FTryb
qCI4paehTziRI1xikSZuAC1OGESAtgUVCrZzKDHIlCL/C6LqLKidp2ByV+lLz3PF
O76aP3ZVU8QKjSu5aiIdw0q/H/sfCq2/oP8rlxswwnlg4IygzjykjWq5us4qHmOf
FpJ0x/k+UdAf0HOw565S9WG8Tt9KRLQxJPdvhPyZU/7br0vpMWNj37M7ORvWLv5p
9row+z+ZF5Sr7ww1jqs8qmhu+0UEHwMtT7nFwI9xkq1l+Xs3Es1iVPJMRuJIe4Wb
Iu1n7r1W86YzMh7oyJt5c5uJ+ss5FyyMpGirYUPL2xLcn2DUjF7aqfk4oWVHyoT7
vFZqTHtQmUu9k8W5JDI8ZYNrFSMYjKiun+AfKMRa7eimNtMLBSf0O+BFXA7iHsk5
ucFYvrgDM+zl5MQ5WpfZOjoukShN9Y/PcZthjRKw1VAhe1lixTWXENv3NTPHfo3m
30DlNBazkxty2zjgECFN2hU6THrB6aRYHl3cdTC8ox8AfKDHUxuUGigNtI+HeScL
uGbmS1icYZ3gzr4ymKR7N5oyFDADlOf7BIqZ5GHmuat1cRU/8X9xgcpYAHLoz1IL
Lay5aAU4kvuwUC8eV6jQMrhfGarjK8fOxXtELheSXxZdFMmwTEBWgEr9Tu6/bhw+
+th6mq/KfmFRkXBibColyu4PE0zJlEZPJ22u/ja3HRREuuRr1/mVz3C6AeJMhoyf
N9DwMf0dIUNfsy8VJjnsxehuBBOYlzv3Y/UOzwM8yojYeqL/nFx4BbTQF86isEiH
6UoZQ8TO4gL3EXuC+YgcZvl2SZDR0lx043vn3ZVBz0qB72CzQLrpOyHGbUgjXhud
85FAjLud9RctJBzKgGXHBiUzKF7/i/IYUe5j1aZ6l6Rnbe/ucles1TwIC5wyJDj+
8IYPPwj2w9PLPNibcu1+EYTOOoAxmoy5U68b1Owv0J8LkhfYmj/x4fa27iYrsp53
SS5teLWZOLsOSau8UdJdEN+DWIN2b9R3U53w7l4anT4J3dUeBDaFMpAoxsdJ22Zk
xQ8MB3e9f2GbDDVbNNs6q5re/1baOESOfv4kjYlTf9dR7RG22GHTdsSxeZJkU/mC
FhkhVc1kklQKDELsT4zN49o6FKf5PUeTnIxlfSHlCJC5+vhcSjF69W9M+6HWAdAd
+hfHEMyb7BJ1LP3PkX4nQERngrIM7RK5FwApdu5MMnnwR0mZf2v4yRHpmABwVBKl
GDtjNM/nUyxx2Td3DAuElsE+o/2HXKIEN51cvyo2zPQesbDLCGju1W9CJsWOFpYS
XWrDJ6G8zfv3FKSJYaUBi8x9c400XC7Ee2DHu4sI1mqQ/u5mJUn5e0oaLdkXUA4j
g9mEZNksqbWO3GbXbyjng7ATJFAe0b8caFikckKCeDxnyxqEDsFnZsWaKPBbj4Fw
BA2NAnSDjHRJKqR4Ck8zFw4hFeAEsygGtRzbQh2gn2nLC7JUtgHC9yI7C9M4UpVs
/+VOvC88draMs43A8YkcY84Ev23WuZpXT01dLUHBaroUc/kKHe82JTlWm4ZXSIhI
fAhB+55BunaZh9HIZMAnMJ5Z40oQFr2FQWLNIRab0lwsxCdlD0zr1diJWt9ca5+M
7QlywMJr+NGUJOoW3tEf+vg/t9uACgAqGp4j76AGQx8y+5Ju744ukrLFU5u7nxdR
yjjSYobboTY7ViyugH4aDGDUgcFOZBdw32wUwgNVbfDRVAJkLGubx8P5UwqDG/IH
ld4+xCFGYCvL+zEse+sZ2liTr5mrPb2C2VC+lLqXbBTqgdKGLuIKGvP9aE3/tgmO
oEy4x9u75EPjUqC6VxrynYLhavomBvf1TJt/4hyud53ewX0JM1qq/bYk3KOqWLnG
cqtJtdbT1zg4qt5PxhgnnJPfgEYI86M54fBqXSpdwPJO+H6EsjHqfKrRu01jKPvo
bMnQIt0j/Uz8SkYYREZvIi/UBFtyunZGrla5bTKpHguRitXuXY37D8dw/XdsrmVI
Uddv5QhPkTHASOm3MtfSKlVw3hMvC3YkBeChmoRat1XCErZK8OnGQb92lAKuuftz
EmPd+8TDVYUY7V1wtgGaBxT9oEiF0SrcRsnBmRgwtYSf/N2uaXEGpwzKBloOACtB
qMu45Rxzn/gZKqcHIlNm6YptibhnzYValhsJVCc/tlJLQ+2uDBPq5IMkNOzXZKvJ
g8auSqglhAD5X/Jj8jGDIMtrY+HRInKZlHpiAgFXQLUNgfTneHTkFqgcna98jah+
/OiG0MwW1uzB1OKUgZfGnvrxcLj4BR1Ah3RJwq/soj15v+vtzh6vZhAgabpXcJe2
eFiessPhwnCOTkcaRDNG4lEfgtMepPCgUL4rQy6hxp2f4LTMRfXj9v8v5Ur95nkq
T8guTZfqErUTGCC0WU5ZO7QwnMmBEdHS3FM7kwsFm101A5Et87X0W2roc5fRp7Gu
Jr2WwmXkYfHV7WGaC7tAKX2N1HUWc5Yv10jryICltiB6QOQfFgvg3QRG272bKWkA
mHpPIZnIYXlOnDbeE4pZCpAra1L/dH6Dl9rRekqKT0HcvePzJ7KJ3Tq+nyTOrj8b
gM/RITUQlUpt7BhkElpidh/tHCSPX0OJEIjmRZE42jaUObIX2F5VqzEZvr6+MeUQ
GMf2Dzxri+452qJzD7d2YvAxqlRVSlvJ6qyCsO1qTfo=
`pragma protect end_protected
