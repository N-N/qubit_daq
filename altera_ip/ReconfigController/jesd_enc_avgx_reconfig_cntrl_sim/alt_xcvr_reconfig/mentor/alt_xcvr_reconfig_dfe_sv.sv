// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:37:04 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
scEf6AEbspDtpJCk4Ly4cD7lr+/MEhNHJ7NWGECxNnXu6iqkkchlLo0kajgXm/nY
JZE8erNYd/0szptLs/CaJhEs+PS6dcAwS6kqXZsyhtuhZr4zITf/LM33K39pGydN
Rj5IQrzFOwIYPDWVc6YJOhWgrxUuUCc5ee6egWztfx4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 14784)
SO0w3mTvZ1xmkQ1LMCy+v5UQAXZWJQ7nqdfGDr/lRQxKUEkZNiC3r1Lf1+zLbV8W
LW62AXr/Nt47mguzyha5/u9n2n4BTNqyYszMuurulDkjjmtuxZKqWo0VJ8FMSve2
OrG0dtJ2VTMbtr8z3FvoyaLxSNYwiB/uU4+o47N5Sn1tyaX+gOoeUHi6Ok0usJG9
HNIDF6/CO+fxctpYgL1Ey3Mzpt8IVu/yIxznwDlnB/NcRo0eDnu2hhbVoZwfXFCr
1cauSy0CTlybp0b8SD+3LoKr5FkeYjSU+l7wUy7ItGqPWLi+ld+hXlgZLeaJQ8mb
PLaqwWtOcqbxZ0SvtW78MzGLScXp38DWUIu1I1mn1Cmj6n3ouRQbYu9mmqI1b/Gl
t8KWxiDDy+8pDFjj92RVHC8aJnSxOI1yDE5HK3S2wt8cHCTGlsLIO2KSoHCGyi1u
bl1uIU86suXHlXxQMN9wR3rHFq5uM0MkoJYdI+7fk955zU4qpe9GpZBZlJPHobG0
FpSBZC+49DQpS5SF2hXPvmsaU/G439LJqDE3+v4EbXZ4Nox7ofJDdZUxL5ll1A2n
uxawh5tKITiZ5YZ+2yp45TojFVieNhhSyvOWItaVvcaI5D5mBvmxXRHwQ3/rClZD
ylL8BdC3rEC+CIuKaM2gjVJJ/czKcK2ttQrYWl7OGRXAw82BD4VqsCO6wyAlrC+Z
qt/ucRjpeprfWIW9EM56Y5WwSx2ZVgp/Y1P7SOgeR6/CLfJlz1pyLd+d9863q4sr
NOw8zRiVwdOpHY/z/z8oYxc2CxXvufQau7Y7xhcHbtaWfS0a4UIcd2nYF+8Og2Mr
+T6lT+2k6nl8MTKDa+JeCUXGLdK6zJXkl4R0LZQwsZGy2s4kxHd36eFjgBZogBUb
6bUlEg6xCDqnFOcC4yssXC2dR7EgzPPO1TKy2PXNitGJuQEV8WmQ+U5vEWoY8zUi
QJ2oQi5PZwjY1Zf+38lhudff6CcKpiOShmN70rsXSjFjdFed+UzoGfiMSHIiepYo
/Q2sK9Wt49dQbOCPdbi836pEpPPJ3hl8/+3GZ/eLl+qC2gTuXtrcS+CZ4+TmTZXD
HN2cIldMVIk4hd1BXFfRh5WhRjDpvHx9wNsHAXJTpkMg4duFOQj92F153dJc6a1x
AsVtGzGNmkNHd85GYXAVIO73l4rG0Me9f21j3q9zIJpZCNlw04WUK2KgTfJylXIg
rEH0TCz6FHRCt1eH4hWt8zdfOPBCK1rGRwPMmsoHElWRnzyr0a8HZXO78qsaUv3E
KQtQOSSnJt7DFgFvayjOPnWvZ8dIMi+uLvvWpnor1+U9F8N+GatroXiQG9liUK6V
TgR8uJHlM8xRv8qLJSAU2V6UyVH3l+2SWlGBivH5lQa5BlQrudlfWZI9CXuHnNDF
IX8z39ZAyNgqdCHPAiGXoGW8Q1fJCjM16XCfJWZot4USVZ9U3+z80OZuZAOS6v3w
3fkqkzRKLCk7yFddUYB+GdegNh1VqMNq5Ww3rD8NUQaJy5L4tCi7ojm/I032Lkre
w27fOH+bIMjAuKt7ZJlYG4PRa5bNBKRnQSJz6lAHuH0lz2i9dD9YVU9EExlcMYME
cDvGmFnlYcc3rDVO4Z+ho3uHzG/hr8O5PijVJe9yw7MZ1JJulngTjI9si2y3GYF5
pb5C39Ik406isN13NrVdVP93HfGbJyAfK1PUs9H15wIyAGBNFlGPnTZXvqiH1vEb
804JdLXBHxU84b+mgMuf9e5YTgWpAdzeJVDs70I8PqGfLEQe9HBTLwJWfLlK+RCP
To7CHf/CxkXh4HqVvhB/2kcP6erY7xPTbTY8GD53dHHxyZZZdjiboaO+1Rqulpf2
7hqMKTNjWZ8ReOrZugCWpun6xFKCmdis0F5a2sxsTDsLWBdokjKo8FZeukd5UfZJ
N4qoS8Y6twFAigGzd3PXF9LDzkKeXnBBodlqxwCBezJSSj4PFbRAq/R4C2R3B6Gd
zxAei/mKGsN1/gyqEbMBn46jhloz555H7ke9eAg/EA6ZHBY8eHBr/dUd1CvkShvq
Onhv/Qazszlk3lYetlqCwfRAVH3ukFXN/1OgRnG3ShiwB3RRVSKR+HnNOBV96QWY
GXC2pTnzQUPC7JiQz0/oQgMi8JH+zsyYiu2VdiBOsd0LtH3jvuhtcpD1EuJj0q1s
WsLGZLD+dRPQoR749Yovjn+S8IwidLnU78NnED7wibPgxP4eOMIRMlrWRck8mJPA
fcmPBklpf/3ug90JgVaGCICtMwr9fE/htaq/U86HTPlY+/aAyKyKXzXFHIcG2/k/
u8zZPPlse2vP7PCh5Qcf1XuXLOMpvn+0X8x3CQwpFXu1ky1pQfT2xn9U6tT/2JqX
ZLaTS0h+tYhTjlaJKPrr9wAvOZHjQS49PxjNjqBK5SLnOg0wJmeVbolULeWUlaMD
9aNSKxAL4BVOm2gZRsuXpTi90kgHsvOZS25cIkacBPwBGUElqxojK1A+EeLtR+mE
4WIIL7TD4tif1wRwdxVvGYYRyG0H5zYcyoEUNm7s5BfwixVnHS08TGzN9imoE8iQ
rlTXmBxgAhFYkJvh9KV2cEF529Q8/e2Lztfbn1Nh8kRhPTAiFks7jhJCctv7fkLk
ACcpeAeHaPz27i+6D26W4cXGEv6CvfULSxKvicWQMrKdO8gECjacePsvNc3wbMJF
d1UDLSRLa4UBJFmIbPq+QQk4YILItYswT4XSTxXtPrqKlpBu+iwcx3DWMU5hARaI
CSbrnEW/q9jBffnmHoOLZMYOcFvK0gQa5sS7JhOWdFaoy45XKJZTKeG46sioAz1/
qkxx8PPnlzqgThBAj/dXyqUUFTgqV9fptf10rCBjqnG8BxauD4PZz1F5+U3m9Zw6
fJUwXR2k72+sWSnf9c7mvqkzMkaD7/2Sik2XHemi7D837WVnGboE2kU/rW5uGZBz
EK2rFAVdpHeeoPS/zWUJtb3D/Sx6PWfVLRXKDeB0vzBFrgzh2BX65WXfSJM4+NVL
elx9ksA/X93zBlFKq13eq6B+QgQy7HIICF8738nxQ46KyrW7Iqccs7kwNRVo6LUz
l//Ctkk9fFneSzBMN+UaqOknKagqT8a9BaJ6GyKhxfS5dzWEmUFg6X/0aR/TRz2Z
uFA6XLrGH9bg+pBrwwTcuBQPv2UtKbuoF+y3pLBLObsLzFhfyccQDHmHOwJhypvd
OSKp1FG/ZFvPNEVjUakMFBBqLecbYPeLWM0kBGS1HBFI/OUwcghEVE5IHVBfBXO5
0+iOgELu2TJ9TPjExw8OHvY/EP0UMvJnL501iBpc6c57owGDLk9mk3x/SR7jRmOC
AulTRWdlp0Lt/S6JseOj+ThObx+KmfsyL5by2Hg8WMBphwdozIcCLc2LFqI82A9h
cW8rqopBi4zqZKgCOCBiDY9usXu5zU77c3FpnnAWAuBSC3eJy9sZQUQq39H/AJ3A
RmzH3lBivQFp9KUFYtRKjSS3Lq08VWYRqiFYS3yME76f2Vgoggma9VwNB4Rxm/Bq
NhSlelclRnjyGA1RxWiPLnKOy/7QToOOHxGMOYWqWAPNuz4iyYi8P7zfW8my68pO
Uk2aDSCGzQ+Ws80P/opq+oVCQ/zoXqQ/so5iKXn+FdtMQpHJpgjwFfRTsG4J//iE
svQCysnGadWI4JsVSXdFm/Cb/g5l6WWHRMsM+ySTy5d2cIFCFu4lbjCoVYa43rKh
cx8YzJU7jLjtIAlYYq8+Hp1ivbfQCbGOoiKpmcmLCz6L0OH90+FIHuZPS716UcdO
iHH2BPs6fpNaHMoSl6lLtjArbhGFn4IMMu27M98t/Xnvamfl3Da1TPsPjqTJaUPC
OZ/QGSz299KF0QzX1CeRjfkkf/EHMrOz+qXrx8dsIEOSsvYfgIn+2h9pe13i0pgd
xl/pEr6FBqoRktkIwOdUYrzkJcIQUIW2iUPVTLbHtBoeOIvxwBVqtJVNDIOnqCIh
y3mDHU+oCcDqj85dTAX1lRkt+Yym6xQbqdZTK8fRHkmFuxjGcViTqd5z+oiCamra
v+5ejn30X7bpx7IYsKgPZvd8kJgv0EoGM8owgLB+FualFVUPP8m1gfp7XlT7pH8A
gI2nDYjXa1+2IlL2wfOAzhRs7P0VYadv3cebjWoUA0GGC31yqLigoSta3e8tD9lt
FwJI9+BL7gK8zXWWjlamb0sURiGLztL3vHh3jgcDFQqK2XSa8TJmCC/t2ZAjwGPA
IbFPwMN7+n5ESc7hFS0nywH0LOqWkucSxLUhS0yuM3my3Pa5KfaFbxAJT4ZYk9vA
yIbvy7axf/mkin0YCyDbpizrrFCIVLKhLUcKaLYqsqNCKMz9ces/A2ktbb4SgBw3
ZijVegiTN00c8tUK9rkIzHGnPVDTykCkq6Rlf9UzcYZ+uZeO9a4R7U2W8jgWgjZ4
HAr0dgPryvBnmF1YYrdKcJsHw8XrBYzzO7bWyenmekyHTLrgfVrb1jMb5cFBHt5V
t1XRsXr9wIG8EABK+uK2XhYgiN9MPdBrqe7yy/aMTBtpZGIlqN9ERB3212T8pIUY
URCep0cqt9xhHrQI9E2SC+a9BXeP/a5hQ8YliuS0gYM0KOgWJRtIgxSNMhJ2jhye
lL/HvASHkQc8LDDKVfHOf0J8DNc36FDsDJY4Sx6AOgOz6tUyRWwmH4Lu0P7yqEEF
0fM6T91n3QS8BtmHoOwpXdIR0lMO7Jk4o2VGDCJ+phqZmLCz0lIMQ2RzWuxItSKa
CJUGXMiVqa7P9sSe5k0KZ9bHOyGcJPv1SVA52Wt5mMEKtN74KDC7bDCbChQ0xEf3
jeGk+DxtC9Bsi7BvC7EnwtePO+1XzRGhZCDh/mtj0O08nEDHRZ2qxksm8XiM91xC
/UxiKwXB50vD4edTrBfQqyMwmtmFtmGKQeUWCHGvvQdhSHCBd3jGcv8BneHVs9pD
7k2CI2A11e/D/qdRnqlzHPqcTAwcL4hKQN2rcctjg0hpdFbSZ6T69/p3IcpPYVMO
wZqMnTfQ9WbiotTg3emQcr5gsyp1ECtkRz4WWdnLyjZILtM5no5yFdm0GrIyQb69
hTRUtVTWbjmUYBzFvZHiURQPhLIflXr29kOYw9RFrWkBe/3QIDbgYTVXT8RZwT7J
KE5lZQGCt2mc9QtqwKrASCu6i+l5QN6oCc3HUF+4DsMUYLGgjK7MuC/6S9i2iqf4
L6vpNeggfhq07xnd61nEtlysaBFGDZNOTEVx0NPlU9mILNY+OARxeqWNWxG6ukYH
2u7lr5NJK+/9RKQoNsxkF5dWWE79nd0tfGKQtWrQ5NYVJxmtf++MFTQJHcR1GvIC
XxABUz/Jmud9R4x96bxW76++XdEBzE4UMKJVkuIAd5azVJY0tLkHASBBm8vaNFPd
/yg0wWezxh35x+KKMBBQlMSBMHd50e90VuqzIm1r8sjccObm3eABh1ZvJDtC7Lit
lnJstP3QA8/qxW66ZLY3HczjRE16y5vLVZtNo7p8LJzlYO4DY7t60pI2zK3+hEgH
06Z6gB5XVbmlFQ6nKWpApVR9GGeKIFkteQkGsgq9tEMY9584LQB+whEN7dpdAIlU
b5HKVSqQ6q0eZ1sm22dRumpaSQqm0a0PID45UEySMOV8KpVFYohuWJBHd2i64BHh
kJVqZ1/kVRRoFQzIX/Ju5g0+nhIBDO7zP8ERDYG0d3VJ9WlVS6a/vBiVp2zmpGmX
zWSg+wzPpR4+pQRIOMN3hHjeyLabLho/AHUsfw/rWTzvivpSTaqZLIPm45J4qL0A
PDM66jrPiltViLJi+EoY2NNzdHQVqCFAoLQyg8WknXVoGvE4lKcHbCTRIeRghlWs
skmOLJhv8ZgRTpbrtohD2iu8thKqOeM8tDNlniMF5PXyF2AJugF3+7s51rTIXd6h
7vhrEEZ4foVCx3HO12SIsBGy9lBLLlLWkMMag/d+ESggHopbs88j13wkZ8zNHn23
b+hiMMBTDAPCr+ab2LEOQMlemHBC7O6lf8EEFO4LCIg3qWSUMpiZ3PUULbbklXDC
6+hs6aQOhE/q6QCOjJYtfByFf39h52JIFXrbWSa36rE8odt45Acynnv/DKpoV04n
PPiW9+cdkUP13ywQUHFALq3gggpDCDMUalhbVAN7d0qiIJEMEyTNj/c/ji67Vxpn
Ymp94LdiP3NqPgIIFyspM86KXoz9MiC5KhEPUUGuxc/eGnVN9TlBv3fd+WLNMTt3
mhm0XEZgg9lZW+b3kB2G+VW/cCjUJgf2ZRIR6Yzjm7In9Jtg3qSOEJtqh3XDsv4c
JVjfqvZoLmFiEYg+qjcyCQZVHdLgbYKf1Zz0EAUBLLrLRMgcX/gqjKcPen8c3Eqp
iWuFkyvLABlM3w2KqIKLpTOzUxUdNlakHWbVpSgm9f7Zzitp95G/t8uS3GFKKw5f
t/IsGo3TmI3qY27CVC9sjcNJI1WfUal8Bfq798Rla9ZrOBmQtbfRi64JlW1jcFZa
hPO+JzDYrYTRL+MswLtT+RPZ8FKSws9IFrSfq0of2IhmQZXTPwTsdX0xmQR3GrX5
jIrfKw5dK0K8SoyN9tsujMBmYBhAB6PMw7oE8MY/rTaQPjC+vmN2zS/QWlhORL4h
UfImEgwyig995jghQdudPPBnGU0EyDq+qm496GquJ7TebyaZUrVE0DT7cY7LjmTO
o+VpsEQnLpXhd0or74xtqJIetQYIgjlwXi7rokJjITIeGpzT5+4MGZnFZ/HtKV3W
B/8/VvqkYIs6k0KU5VLDByVcfV78uB/5Id57gsZ5q7uYlhI0NqogTJABx7u3qFUp
/meKZYNPriC06LXQLlEOZougHHFar0hG4uOZq8L72l/4jSD4LPJnOHsCXw9Fz1up
iMkRFrDNjf4qP2BFQUFy9hjNGA4d4vyhD85WooMLm8i5R2f/EbaU6t9Zi/aPdFsZ
3Z+ihD6apDQAOeMMh8lCaL3MeDsQYvZLUH4rrRzgbShvuOQq+z530nVi979M6l0A
EdD80IP0GwdmecuFd62dc1WkgGdxAG2kRs1/u25A8kTDsO2W9RzP/8BUqzEm2dZ1
z+58pygRXYQUd8Q5/LImoTl7RvBEmuGWErUDsct8c4PzivN1W7ktosU9wQQLgsTB
CybWj/5hnb5zjoc+bozBv5SOd4wAtaKGj4A0C8oZpLJAtd4P/C6UpoCM+cin42s0
2jf46i2wyJUYHrrQ5pu4hX9CzxZrXIiNjOMi4T8Rs0Ej0x9JRp7ptz0oycpVx+0b
yqvcnssyfFGy0TMUst3unE6jDZuw5BABocVYiH3CfXjHMgMPFzfIRDQobglPI94i
igMKxa3XrUsfNsz5rD1SJYx9a82gngTDaAjOZO29a/eALvmzuTzUoNcryi3NI+RP
xb9PMrg2d9AOqnRi0hw87ZtOmPX7bKCONeqly5q0rH8CHcSd2/ONtfi/DuBWe8Op
dqCjoRt1+Ifp3H5C47QISMA5jwJkFkKSOGNWEHzYf9qzzD3sSvhDySC+stk0Cqsr
YaVFBEM8+a+yQXYA8/7/EHUJ/8Ha6LVyza9/p47Gqw6dCELDA6b04XET9oYclGGE
tDrSmvrXwbpNr7F+jewWHrBzZnC0qyA2cMok179GRuQHSrsy3H2dSWUbp/vQEsYi
zVw7q/7WigONXEpCBF+y19Qd0wZ0RDY7tN23+fv4qNZ50qbihExv0QxKTnwgxvrl
VQurTwxdLjwxn0bgwux3ISg06ANVsJJjEAdiqJ3i0scnozi62uMOXBEfVUHzkxcT
OfIG2WoIILZBotevKKUZw+lji0Y9DmjfeSD4Yq98gr+WrdKmeiz1h0i+dsfbWY3a
Ot8wOFAsR/bAiHU/fJ0uImr4Jp25kxbA5Yo5pe0svhMi0PUZ0up3lSIeo6UsYRX+
V90ie8iAREhSXzX19mINq5WgVxHzMMWg5NBKQB/nErIFjOTG/o/nuPKfG0SOgchL
FoQ4gy5rq5x2Y8lmp2NfbL+sXEMS8gG6hK7w1fSFuQc0WG29acdPWpU+jmyquWtO
Q0Wgtud7tcYXqE37gG/ZNsFllEv7V1bw/VsIuGvIM+Mhz2lJc1DLWEQSLtbt21Cn
MFg5Ky/Ir3r6ajDtB660IE45X/3VuQLZZzWNYJqxCIgZzgW+WVKAJ+NSYD3tGukJ
oQ7WRARrGSJfrqtVdlMW8GTd/A3VguKGWt2RBMcUffKgppPQt8plib46Eb1BHDtZ
wgj0o4rdimSlqx0FE34ljnW6MniQtw441ibJadrq3/id/F91v0jTYsoL/Zpn4CvY
cmwbTqvmzjrN6bqDy8V9m/MvnXoGL+uGarzRfe75kLP/GJCwZDlhuNSF0tcgSrRx
/eLH75yGGrm8EzbHV4ADYOHZdxo5J3KERAQD4GDSE1inNunjN+rQXAIPTb9Obqt4
oW7N1col1eRT20l2nrlMM1wIbViULgzGCDkH/Ny1TcwYcvzT/OSadv78tpIwjJBz
osvTydnreO3M+NSnoU/IPMQ/H4UTllBvKe5yWZ3ZbHFI3JUJdbl+SG1TEDzDinHo
4/XZvgst44TiyEmzbt78pixHdLxb/G0oHh59fmmZgaaxHo1fZ5id/9SewD4+YhLW
fBUzEyhbYoKhRgS4FP3bhWeQc2wb55TfJgp5Hwpyv2oJX87RJERyCI6MvN4allWe
Yrvcy3u3jtDOgKtn6DzMyWmIu7CNPD7/ZWCNT8Zjdt4OJACZpt9aIcKZXNobKRDU
tvP8Os5vyLO/liC5jvG9D2xel+Af7zuNhdsHuTBGptxrPAZ8Wi8xpN55QFe6gtGv
OjO25mLFaw8PB9BX3VGXEQqtGb7vZ6LxolrPXbIDLXhuL+m4e6ZsUBKFy0Hz4l07
DuH7r4FIjQ2AXO4gvADmCyfINJlsRl0l0qNUjmn3Uciz0ZiYTPuODVaD8PVv+FoU
EX6nM4d5OwiBinBUev8f66TDkUn7XQf+njF/HE6x1JYgbNbezU5PsW3lkt9ysfn1
Bo0ITHQdnG+2ugt2g3k63Tl6mTEGmYRfgPVzGLU0VmTH+Pqo9cxq/GHz6xhP0Bcz
cH0jAUuuMgIdUYdQuFSLiXfdWHdicFLSJmR01QYx99X9aMJmezKZyT89+Mk3NRvS
cX77yM9q+QBiLhySIRnXvQMc588H+8q4Ek0E0wpR90iRz2w8w5vrUjrd5DN8cp86
DgtxtYik7YOgyd7h76t4nRvl713yyUkW7F4nH+VzUPMSloBl3nz4fZoYCBa+X1UT
IjglyJPSLJ1vQdayVdvDIwEU+sonfzCSAZ2J6ubnuNBhr3gHORssUmkEXVtRsWNU
N6CbHw3RBRAlDpuMa0R9m4ZNX4AAsnT8PAwUpcrs24IJRI3gAFz6wkeKfMpojlLT
vA5qAVmYxNP7Fs6YV9HdaMLTU8lAvlGdh2dqzAOhPw5w1kpTykm0nwOQOlwtj+Hv
5gWXI95z48cwnqh014HM8IxIeb9NOKuUaCbBwuQ2DiRLlnTLQZmptp8SpM9vGSRi
d8tWRHLDrn4y4bjoLQ3C6sKau8hZyK8Sn//t7+NloYTijkBZZJqDsHbK2sCLzVgr
p0GWf0gxP6wbxcgYhoaukDcrqv7GT5inU5uZbG8w0c7FCU7PhxQvNsw6waxdJa+w
cQz0RhsalxAES4Hq+zMmApJyszA0guFBbqzI11Rni59Pim6wLVG8TrwTPdLv/hZW
Wcp0ImdK4fTHt+8rlgRRAPKL4lnJ/ZnxgsjAVZyBxRz5J0UHezVowP5QI1dW6ryT
gMWooz/9oVUsgoZpz1QYgDPttSpcyzL0pjV89SqrnDvFFCpk6DzzyAhOuOJfmirh
2mG+/Ca9fg6I5x9jZGVEBYwQi+y/5oJQK19wTY3f1VnUL+WCuCHfgusnJ4KyFwR1
GaGT7InIZJJSNQ5oWl+CW9214Fpl+JBVljMeVx3cNKh82QNT7dohgrnTeBxqGqLS
xPARhtK3TAtFaLkpQH1Qcnmk+6Pw4pOj8Y3obW63BjTUWiDVu8NHCQcarrmj4Civ
4pYw/EmzOLCwoUNmzrzmMtiiUaZjIraWd2KkpdJtJZQA+WWG7n/CKwa06wshoMgP
jN+MKwRREuRzHYmJCOm4avFZIULr8phv7ZHRzkyqICRkQ1/GFLTVci2CQSdpQUp2
KCCs95tJ2qC30Nmeq0+SMgR13TBOZQBXia0LIJON1WsayPsRO/JxvRtJJdOxS4s7
jcZacbtOXBVFVFbWj+ytfbuVPumf5Kk9Ub0t18CZ88BH3nRd9N4BlW8L2ETCtS7y
Nfu86NnGyC88c8tHg71aEtBhc81r5Kg1f2PnXHJAcY1Hbzi83ktyXpkBhPbccyJs
sdEefPEoakDKf6zmHe/B5wPhxCQf7Fx0QvU/hN9ypDft4pRqgMQlP7xNoxLo9Hbi
Uwk5ICEBw94x5ZXDkmML1nsHhDhb6T/MmMg8UCG+lBf+BsqVVzdFtdDVy6+MyE4Q
ZNJzlWs2tsFDkZE0G7L9ourK3ScKWEF5Ag8Tf35q6B9/MMlfiC/dkzUlV/i87Kxl
5NsSz+ahTMeyT8B+OhCEI8gTPvTfj092S9VjQr/Sr0r9NK7T/ebb8t4b2hvavEUv
O6DJ33EFvx5pJCgSKX3f6Pb+45vzSQJFNo6Dv57Pb1m9vC4TgYECqmVxIkxppPg1
OJ7S44gHW0Rog2x6qMGi5aPMx+AhnaVwBvi6xL/hkLYFDcve9ZQAlkuNbHm2ecPh
gMkV09DAfcO/LDmozBNwWbFjKTJ6yO8y/vGEKdxOuTupdQRsFNM++9cvQjkdqoCj
t6+KCACn68TtolJuUyYHYgVHs/HaCRO2avSOfELCPoq7bSqIjcPyWFRNc5FTQLI5
8hzGMIaTPiJAt8o4/DYtBc6hRcPKTzQSoJD1KBtw+kbV51NKfPxn9c2eXXGTo0eP
CAa0k6nWhdX4TKymTSUWbVUiaWaruSWwrZfOlSdLIaAYnofqx4RIkBGUEtD4AUqZ
uVQdxHDIeN9hR7ZqZUETt1LkffS5ZF3VjHJtbIPJ6eP5AbdBEIBXCq1Azfen5LiR
QJCR5xjQC+3FfxuKlVdWuoQ4kNRBk5ED8zz7ENHVkm/4D1EXJJXATGG1J0X9lsly
6MuKjuUvOcw7y+HU53JZpeQlBI3L3g5dP+d+iL9o9Sjw6eg3zWfartoJoq+1++qm
gEvocgOp5u9h1WxzApXnLyKy2vIYAAqcUOiUKqX0d5IhP4eXLhuzwDAxCH/H5O0Q
HCrI9h6WSynLso2MKaxeJKUaIVFP3ropopahIXZRx13NeOyyw+FDw/J8t6BNDXim
SP8f9/fEjNgK2V7jwnnP8Z0VorCLVl7lPGavZURNjULFfq1skXfx5zObKHdFGSpZ
Mkc6nXHRVr06Mn3kHNAnXYdgEFfoAcpJSKxQ1LjsGPgsrglo6h/5G+3gEAenfGG7
MGnhLUHbIvm2Cg6o42tQM7+4UeAWNfsBuoC5Jx955ztr722PjhQjp3yP0jTs9WFM
G+ZceSqjZ74LgHKMSuq4bIbFRgSRizs6AWfN+TdsoCsO+qGgxSiYSv/kTqxv1FxN
0zW9DCiu0SfSdP8OX9O5YPogyl5Bp4nRJGK1oyU9KKujifJOdh4cals2qU9Fu9O5
XQLejcdmKV6LvvDltbU3kmxqi4vsY5c3n1tN9FzXyQpmDequb6X157E4ZdM9UgG4
HK8jOuJ3l9ApU7M4QE+qkobopWr/Mva9pBdD8MEif6uhYgQCWWQmoB4dpeByKMCz
KWgEtlGIAZsqzsaR/jsElVLhEHCtu3thsDxij4zTgzbJOptTN0s7UTqlI+aX9pcq
yT29Ig70D0aeZaT1ryWWuY9bkVUA5n4ZfVeIALWz8B5plO3j7qsG8ubw0N0ze+gp
PCZimgcoSCuiPePVGa2IzeJnTCX+JihbZlQz26hs/PqGgFgxQ5eqB5Q2S5Tn4Z5j
+6Ok9+Z2NFBw9MpVtg/OKx3ypkoWF3YFefCpuXoOwGwsaCE5jXFGDQOTu9tax3AV
GBvQ/uP/X+kMP8HQ9HF43f4Ab9UESZlsLey1J0FiWznEH+YA02OrGqgMjn+8x62M
xNm1yJBaKZkAWsVpKfw7mIBVkqOVwsVnWSszBpnsfMtRr5Q81QgmB2F3UVtJg/03
hoFchbLAbL5Itsv/MDPPyve8zTtowGIpd2kkZJ1BOah0Ig1gu5NsYvbNbQNH2zfo
3OSZfS+Ff7omuWfN7aeNEn5KfhDXE0N4apZKZ+R96G7TgIkU30Iwmv+m3HNh1B/D
dxxbQ29I+GVzL3KCNpAh1khglKqPIHb9y95ZaPnDf0lSMcjMAU4TUoocIBNM4ilg
hiMqiEGGiD2LyU46949GRzrOUQCYndQiIRbNiyYXGoP7ILpZm6shalr6Ok/WcjaW
smytyMa3mOXV5bTDj9ACUOYPn+zOvfZkKiEPmHBUTI6Re+K0YMLdx55EzG3ImLZT
q+/0e/8hoGWceIhPICACY/XMrkiel2DYr0Flu0JfUUKaLH8zGVTEGYfFP32Ns2uI
H8itFBD8htmyUQqcX6jQlNQdcPqP1RQkwbq5TdjPCzDCdR/d9c2CtXLh/hdQzglp
N6G6tdSNHbBzM8NYSuP/PLfY7eI8PqgzAe/9cgoER2rdAIDV8NtP3Tk2MCVm7CxH
THV4sxYj09/87lcWuVtxlICJtZmvQyzsJ3v/VUWxqOmvxKEUgznJZLqEYfUIgqfs
OpT87Vr07MprSIKBoHveGYXo7gec8+dKUh9yqyHrzeH1Acl5NXBr8bJ9Ox5KkG2w
abhJSgnDiE4LWs1C9FXYAbrzCUMQwpw+9SNqD7ksYbjVaZAuhjZdjr+20AqzXCMw
SuhdnqMm7aRhdIH0enu8bZvg1IXFxvxoC0C6Pst2drCP1Xv+8rxrA43aS4JxOeB8
WCRpTjKmhaCh8IJ1laQI1dCsBbl130wxeK/1dK6j57jjafew/uxbsCmdJ3f11vJK
S6V9OfXlmpqvMJt/M8BzA+x03nUPCKTpEjZvekz4FUu/pjCtDai+qZL4Q5PStR+4
5FuqHD7qIt0jUJyf7wWZZvcFUSPk0y0rfjMADlUA4slu+tBZ5hObLIsG6KtuuCOT
75FyQKuRTuiOTsT3UU7ETKLaA8n9JDYeV74XvHIg20toQpm9bu7Riyw5U4+bMU7h
F37h9/fRAZMjQodp3hnIqaa9AR87KZ0vbjI2JyMuGqWz+N8RJXtdMIeARex7X/PN
N0t8+m6ZMz3tjlt/5tuIFHNQ4eGejbR5qm4uF2iTcs+Qe+ViFa8axO/Sx5DYirIB
wPwY/Xb7dCnlyZFeTZd16t+lFQI6HiSdI8aNYbqAe7G2L7XD1of5RKjd2oPQCZ4M
6o/SoU262BMRxHcxnBBABiCcxdLAuWt8WPR52/liWJqh8DNkCnEsPwwhwbDCwiWf
A4OC/M0GWUlNWi4/b8JUNvt7oRkaZa2MbX0SPhH5Sl31EZKiXzJnVxSeFIP0Uq0y
cMSLxXudVv1cY7AMGqaP+qZmlCHkvVuZY07tOkQL05zfPiMSli36vveVsc3C3CF/
R3ixmGhO1U/4+rnjhDFkC1cUKKNoN9qG0I+Wl5lj9a2hOVMFOEUuYu5S9yqsSi31
VyFvGrMK2/vN1BAxU4AelWUZykcH7uSpSSvNNBBzWnms9c3AegZHwuLtv98c+Dwf
9jOAJnfzVnoEdbo5jDbRm3ElbfSKYlmCiJP3o4qlQoiq2rKpLjMPMFs4oRq8Kp0B
FrG0QmjNTiNt0yJrBC60uKpRlJdU1d3/HRBbQgSOdXE05ivTvQXPlduVWJM7MfdV
7zQ8V7277n7Pps7qma6ssMkP+VD/xNWrvbLbhF3+xE++8NDCLFQ8No07WdP5g1eu
yCcFh2qD4IbElDh7dzRtu+9WBlMAy911ixg2N3OkJj1IACWE44sQBHQOgUbAREdL
aU0qtWWUN9R0G3VqixADThO3FR37G43nLn9feQktUopqE+LjhJtaJifAPUGHEYos
pCkVfa4fACu0NhgNQaYSPN3AUD8kl6IpIepa98FFqu/j9Q/rib+HlaBSN6OzYU60
2Cx10QmI5nD4UXXclVyF1B1Aw0eorzUqclDLkuFEGFgyYkXyYfmYi7jmg/W5Xt2D
fLtpgiK8Z1ju2e26GV8hOCdvmlDh94r/Ej8EZSBTxHyJ0U5ELz+Nloy4olypQMsR
1twUkLxiuK5Rr0ATA9vP4QsAW/59qzX20wMODrI/xeawPL8E8eZz3VGfV9JuUuj6
4A2nHLH3AjHzx7pnHmXSxLl77lK/FmSCNLaHpc0+5h/NjP9heEpbRPV+0T3fC58s
WWdKc+WYp4HQmaa0F3jn2rZJyGRR3l8tnutnt7LfG/s2To3inXZZh3nNfKHqyEra
xs+qqhoxFRMvRehEWgziSW77llT94HURqV7WJIsZvoqjHH0oUQ34XjOAQVTwdUFJ
xkobbDIsuU9AFOAnuw52mH2dYvKC/CJsGjvIuxUtVcsEFlGJ42tBUQIT5EYVT7d4
aajme7lasUW+owNE4g7XdgWD8sopmATKkToVLmnyACprRpML6G8vXhgmt75bceNL
Nw+/grMOWl2c2CuzmG0ud7DQO6j212EVdIEz+7DjxU0yWKhYtdr+lis2pdehSG61
e+YfDKVzByIxdmQk4Fy7yhtg5ed4silGA7+iXcBSXqLchvSgf1F8Fup0ai3qdJLH
+9q3XZSPD91vV9dK+rgN8Qkyx5XFPV7sovt6mUAbVaCkFm4iK1orHEdETn3yB/Mn
cNct61+GVt+iiNSPDMqgCUDQkYQwB2S3U9AehiVfntAGqztKF7L9J0hTtqZfMuer
T0AtPv/fvll9FlZTKs2Fpw//9aX2tN6XWngEOe6EYdJebRaT9SO0v4OPbapv8Ntu
zeckCENHhCiPfzH0GV7B5ptE7HJhKZnMNEMpokX5EvpHzstaevqrUObIQFlJ2BSL
1mV0DNteLSlIwpiQCJb3ZD80TD+F0t3YVqtoweo5FV9IezpDx8PbIZ1QvB5TFjPW
GWzsvtKSR1CfnY1P1CuMvxDJp+y+VBZ8wJ4vM4ns5xlQpUDHyHSRIUJN/ccvP5oY
+3fv3Vl5/FFFQMpAfwMUeWjYr18yqgZTP70xt9XPNMqXlqLmj3Hc1f8LoD0NmLDc
W6NhJfkMU6x/6nxzSE+X+NpadD39DQUBpGB0hYegN8ulQMXqUAWznJOPNmwtIECK
UFFgLunD9EkiEkmRDcl6CvkkagoLA1Q9p+kQv/CbQIROixDbr8oDkFDssSXyno+H
FV2Dhc0Q/g0/zH59yqCD6BjCU18EDjqHR/+1aVMs0MatMZRvcPMe7wC5ZocVQJRz
LR9CvoT3uK+EbehPno45SXX73IBsyGBqSr56wNTuILGCPuro2pjHYdW7m0OAtkT3
PBKZ9+WAzE0X9WMu8y6V3hy1xh4GZv01+Q7R79P2d0VngezppW489s0k7G7kY721
cCJm4EJjvWYIMRVnyl2heHzTs11ndg/+3pjc8DQWNu7t1b9EthfhxA3jgqmKeSZb
LsHRZgsZ5snYfrvf/cpOeqsGIo6YDTtT28BMacQ0Eh5nSsrMFoCeri5OWgGzkMWd
G+XoXqCYxGCQDc8RBISHmz/vouigpMF8ZBk4KRlQMTCltOldrIts7ScUkey2ctqD
HIVuZez3D6/TSj6OlSmV+TmV8litGMUORG3Ltop782Tom4Omj3Wph2b73GrzKJ9L
n7PvdSji4LCSL1L4lzaOMCD5mreKGWnFAHe7nju53V8/i2yKlAmpfaqhOo7k/mW2
3Fqeesc4mj9AKX+ljsDeW9cgZ9CcJNeI5/rpLNLbIYSYGcSmAdNkE6yOvy8SpStv
7RmZlIhjBdu54VIHs72zdq0u7gv6MalbFOcojzQcYiZj9Pyl+uQEdd6NKByJj2sI
76U/s9N5OkNWr01r69MZzBLddhqGNi+Od8Ig2VqqtFeoEKACZUxDFDSuUMnrAn2E
aeesaePC7Q7lF+L0uD8eXh8LKSVG40RHtTHuBYbZ9CIF0YEN9sR5xUVG8o65Ud1Q
ND8247zoeSe6GkQbTAs1dX0OnI0HMyEXQfLTbkYERBO8edssLLZcbvWw4TEaAEmb
F4btUmNvLpARx3ZBHhZUiH3E3aCOP96KeIBat9xiLaZqRSqr7Tvii3Jm6QZq0ZES
G33Vdyi0koNnk/Tx6OHOM8r0waF2MWJ4Q3sUJNEl/P9RToK76ya0IpaI532mksNX
FV9jRCg7EVraHD7+YtkS1DdNGvk+X9znSt6QDRRYEs1aTzRLC6cjhYRT46MPbcVN
IRpuGgs1fHlqp81qMy3YXAngbYt+mz3cTcHUAl1Wa6Ag3h958Tz9t5us01XCTC3t
VArYA5vP+txbT37j7yTlc+ln9DmydzQZEFp46M0oaKcc5uC9jlchMpHy6/AQfDbp
Bm1aapE32Yyy+XSnkmC4I7F0Vi3DnbKGiWmCiLdN940AMWErfQI7Wigae7LmKxqR
Z9sll6T5CJDoVF0tLor9bkKVRk6YXcvR6jq8ASACojBt7+RwxJkjM1kcI41Lebcq
201pbUIs2YDCCQJai83uTwYGQ5im3ISXZh+Eg8z1Y8sV0iw0C50s9nq0aqYHeDl8
jniJEzW9Oso+Q2Zde1zDl0xPI3wrK1ApHoduLyqHNIWELIP0mJbO+FNMehM+Fchf
n9XgoPvozwoel4YiNg9noLnSYV16M44lMZRSMZPUfIgQw/vCYf158AC5arNYKqkU
9SrTXTRvZoCS7A0o2+HybAfEPiOHeJt8qJwGhcYaxJDIpmCjgxOhLweWrmZN4p0e
bFS34SnGJY3/Oj/07hdJd4aWRPT3HiroA84kL3tompkNShcEaOQQa061yd2t/He4
FQEiG60NEphW4Q9ibTR2u30WdvZy5/o1s6KC9C6uQ59x7T7NzbYw3kmeVWi68xE5
G3X5HtNiQ6IKjUxanQphe3UK/Nluyalc0h8Uq6Tj2IJeeWz58IAs+CK/tTA/pjz9
UqrxYVtHebS5vVo4BlgO2X3Lg8Eg44y1LAbgglD4PWBrHTeasoi/RZCTnBrgLiC2
MjQ6z0RbHr+tc1rmwxvQA7w+FXkKZrMjm7o8rwosJuXo+oogwm/eMq23PdiJ5lOI
qqUwzluYYgqpFaC/yXrGWSS6Oyr//jashuVaZtWy9F9FBqJIdbC0EPIVvjdgAr2z
HzWQIuKp4ugh48NNn7lONq2lVSZlaslhdr2Hk4gTl7HIyMEDdauBt4PPVp/BZr3o
BoQXQEmqjVrkWSWO8krXWAL1PaJluKu7WD655o4u0pdLZQEu0FHxACum8/osS3Q+
THuql81T0l3J44j27GHT0Ily6hbwL8Cxwoj1ODakLw77NtBbvFbEaNEanOVLRXfq
DH6wSTHrH43ictJ6nbkfikq0OmZR6whBmbktJ/m6XAByB9c2LsVwe71TUz1F1Zcz
sLzcJV7vV4ff56J+qUHc6HzYOtxW9DNsvin71uxnLHo4yZZeOrfNBMMGKLjZCYyo
HnbnlC0t/vhILh3CKoiKM6I+0CxIOadUrNAsheOE7f2exV+2x0+SlojcS6RoY2NX
mW4f1RJPmD4IqjgggXFuZZ+y1fbiQZ4S0v9fgrqC9sUsYqznvKUZvKxEbbToSvan
AMUFNWwHxVzEIqF/ezCBwiA3A4UQH10HDbqUqGuT3+Ef8q09b4sFlLLM/Q2O1zC5
hwTf2Ge8pF7C0QDWv2LcTKOdpGYx+PUFvDkiNDykudBx2nE7OCfSJbI5SyqCPj3v
M7nItzV17GWE4VmcVmG+NUdJYVhqnpe+hbc+twOxXqOOjl2EhpA4mJ+6xwWCFK0W
4WKiEgrSlglX+tumnQswUP0rZrGBHbeyZfYJ5uBTJWI1Rc5rebJUKbTJYR6UwykS
7EYRtTkaARCY1my8BcaBPEs3d8TOXRXE1SwezEMpGO3X0sePn5bI5xaKEKtmg9lZ
znGMXgf4vHKttK1v9BuyA4z+ASajxCzJ0Ofmgnjl20+5BaJOyu6qnUYRdt9nxOV/
WYUtvqTsqLgX+hsaRdrTyT2lUOSEM4++r563xI+MOlNjFrujukcatcyuP48LOxmR
zNIHJ3PgsIWte/TJwvjOT0AAW3f6Tpg7z2O0AjJyXmBUH9xMFu7y6lzdikL/VA4i
NJ5e9jezpJvF6ticvTjTOpi8AM4/OiXbVx+bSjpGDGPFJ4ia7rqjWJqrksYlrpDY
OhcCNXIOqwGRkYWhJT5KTuJ22ugI3uA8OJd4WnrAjLSSI53gDhBUWnUnOTO1IFk8
gjQBv2TsKddoUzPHX0rtz4/mrYaJMcn6Y6b6Pqc1cxNpzYsFKxE7sAAdgc7RwKan
NkoHvEDHpjTP0wmIZlEX/l4YtNTeg1xYmVEocd3qn1GGSZQBuXTfaqn0TeN8bafb
WNt7PhIKYHoJzMiYJ2JQ/l78dPUifYVRCEXU6bzngGDgWbOii0+UhRpve8a0TBAl
iRtesCQ+qJrC2fqrRSZct6QiEP0FR2TdzmlU6Qq3DVB+ucw/fimTipJ/dH0KpDfK
aUz/DiQB2nyWjL7laBkUCutpz7gyhqVyGiQFHDt3QI1QGt40GgFmAnC0tTRF78UC
XXNsWQIv5qi9nS7Ubz4w/9L44/Kx9E1TqKQxNhyuqefS3prWPwnZPZ+r4KbD9L1E
7RwuO27uk8ti9FyBL7fR+p3gkR1tZ6P2o/CPbcHJlTMUkRvzkfEuhXh7EAU1nJ3v
VK48/yKUaRKou1aPIRD0+LTAzhggnifQV7spmdcEsLNJs9N80MNN+KuDHYsUY0Sa
xite9JjbcWLvkyHSqZ/F4XoTY0pqxMPr6sVAymLzcnc9u/kn07AGyLtQB/4aN1WA
cCjl4HHdwsXopS+9w45kf7lbbg7Z7TyTr0lQ+44Wpb+ChpVd/iuo6GtNTzF5rOwo
+uog20dT/DVshNHJkrXZPit+laTOVadVwUc1HGRFRvKsJXRJmfGvR7pReeZ1QdIK
2m2877OFFdFoZ0F8sePPvG5kLRh+dMxwe6X/36Y2DH2U0xerjk0FhtjtH56QuLLR
j4FudET5DSQHMWrvj1ecMxzel/aKoelKiVHRWxUAuy2U22JioTyaWz2nC/fMgS7O
YGQx5B38rEuddGbBOEJg5iU6bcIJPZpC76ntED3pQ/PnSMbEVitacZ0SO/mp1dlB
IP4/2xGS2NeDvGkdEgjjOxgOC56bsS0CNZ8C24x5G1HxN3GQp6oJZwkOogvk8ucu
mG4ACvTNhxDu6IdKORjceCuvPNOMAnOzpqSIU8pnf3E+lgVuUpdEIXmZU6YFg0Ll
OynuYd/CfHeFpaXmE2VUPiQvd55CHrPO4pwKw1orb/GgGNMFips2PNUDaDmRC8kJ
aYMBYXNBaN6K9pBTO9iS10L9TGMiO/Q5cMOHfIQ0+SkYc5VSm/Ivu9ymC/2Qvnuo
MjWFFmvgxjAVE+lC0L0qvnwYbRqqZVd9N1+NPa9hnDsQyub81eccHP02tfCPI1XT
cbis/hoEfo8OgN2wsfJiHrR2Ej3Mq6YZRndX5F1L8t8SErJC0YD+RSBtJughwzot
ltUiO+Hn46EVVS7tbQIHOePV/OKWFrBqZbPSp3c6PorrugiUVclqCC7CEOXQXDKH
gWMuxn6H0bLgjHNNtgpgZOkR7lMjEVmThwJmZ63oI1euhEEbJG+38GgAh7AmRxLX
eBOvJI4YdNSnfgDABd36Z6YMvdGJHAsjcaeM+vBJBohDu0eOcVeLK6vKDLbdk/gC
`pragma protect end_protected
