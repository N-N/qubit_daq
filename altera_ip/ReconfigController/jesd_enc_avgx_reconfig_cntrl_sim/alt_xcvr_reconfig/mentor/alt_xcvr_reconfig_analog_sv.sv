// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 13:36:58 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
XLD3hQ50skPQZfnkoYF9Mn8SUvnpjVLVFCgjL8pS9528Zm01K1190UY3maNDNT3A
8sraMiwBgdFyeML0MGgtFTININRvO6zTStUfX+5NHYCelvAcHkipM/nM0e0BKGfi
F+VVxW+d2ICWq42u1qlKvciXQ4y/ikL5itsNWsr1+HE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 9024)
x7CHe6qSlxnW5CKMC6YyoC/y2Z0u53/Ynu9h/axvDhTAG4z96lRl9To1BwEE7h31
NFARNkSV0gD8ESwENZSKB/tqCWBzOwMELXKVnUml8EQJ7Ie9GOMOlOc+kJHRZmny
CNJBFzSQVMU9z8UzqVv+sco25pgludaLxdhvQ8wOw/wzJQSmDknNJf8N96eQWL6q
b+y7c9Ot/Idk0pzCU9KEwZZQ1yE8votrywTa6IPHftolBppn6B4Un/oZelQINpLS
psnvJjg5SPPme4smvls169rx2VdzXwHELonO4NGWJcYEvo+mqF5BXc0mMK1VlipH
MVbPN8h0ynUazOt2zNw51gRm4kErAAmePSVGgWQsrqgAB0Sek6LKfg3Ss30lQM6c
krzJQcSYkUB6mySSytOlsVjtfjeTCtKpW38/3cRedA7btIfVc/J6gNUKgvqBcZC6
KYDYlcQTeXmeS/J5W5rt60+P6ktH4vFze6ie9WYWa7uzUq2RJ525ZFC10KYfSnn4
KxG+Rj/MNzo6CqpGHSIBZbhGRv1eRb+Y/2elaW7EVyl4CFzGY/QdwA7qm+kbEMb0
6v1XUpKWkfJ657/CxYv6AZXAB+P6elo9KkUqyTG0ta0BnZeNWcIq62F5drlVklcd
x+HSqi1ublFTYItwhj/V33KrfdM+16zORNMLUSG6R3SAtKzTKRZjJltr9Sy+G7vD
UA8Tm5plN6u0vETNqdsMrEw5CCunyTE95JNCEuJuhL/oDOs7pXZrok5/eBNcmKx4
dImL5ggNLwWlv3G+/jiasvIztJfxqpcekauSOe4NT2gI8COzyACXpIt47OZlnRiz
Attrg70NgEc7XMoCxeXg1PIr138RxdOVPa4rvxQ5I9MUs/GjxF2JfKIWRi1Rh8Gg
Lukis3tKeQGgHExITFi6zCytOjjuNl76oP6F0o5VAOi4fmbYttXPJmZeer81spbt
gA1UcfU2B5T4RDuRsm6Hzs1riXVCa6DknAZcdsnIajKl+u5rsf4scHGflDucZw57
grU9vXEZ34OV1kqzc/rp9Myj8QMoLluFf/Lg7QYMkaHzhweKmYsFCtd8jQ2ou9St
YjGl+VdWUzr1k903soMdlWZLwNJOoiAgeLbK28H/eYmjXgBSg7Oclvnc2CeFcTpX
l5pq3H1EkdrM1JP10B4jlDxIhWkFfomBxTc2U15fcOn0KS2g7F/HJlBGX5SK4zg9
apTeZoRIKO9pItWGmdsVxuJ2/FL01A31gCgM/0JsLlraMeqLdMVeJr/VzjQdGQuK
yOmQ2Q+TmtIraAtbhUhOwAu6jQlOQm9J9pLmOFbER9JNlmWUFpC1uzs0QzAGypOk
2pL84vxoN9WHPIX9s7POLhYUGHozT+az9ztaB+/X4wHMA99jER0nMJAzvlJRq366
kd2D4Uzub/BsQQjs8MiE1Em7T8hMXDLj4PnS7XP4nuJRanTUDTNd+qpgv2ccJbwk
AxR34vVvuDJb5GyymnsKyha4bDkyW8UxXQS8WhP56VE2CeKRYEn0jGVp/o8RHg1Z
Qkd0TDdIf8Cq6EdF3Oxx8CLTCXdn6bjzoFPrdsFktwid8yIsq4JhacvXCn9PgauE
SaRqn6tDlY8Fvz3N8v4Ni3ND3mV9UaPHu8HXzylZ0yRNpHd/ftHMLU2mopzL3z4Y
6Wpg+dqqd44j9JsoU+LqfidIBgei/b+rv3+yxzPKE9Hn7nXV876mOh18gab9YFeF
Rq0pB9r4cYrNqb3HRqARVfJ2JF+Plv6rhP+OXWHqKUI5TS6jMhHaA3OG/3K6HxRu
wLYQ9c8NqgWBhc9IljNjQAB/vBCvGMunqMPxvfiKXHdTzpz9qfQSt9sE2XW3qbKl
wj2LjPCjYpzHwe6LKvrNLI/wVHaP4tJthYziPI0MxNAya2M+N+j4DmhjXcgZPaGT
FPzDMUVdQqqQoOb4BRpYTa/+K92pJXGjntOVHXaR0XdCFBJFp9Vq4CyyecVjU51c
57csvd79GpmFsw5b2owGrVRJyHZ77+LCbPeejj7KK7YqL7pZDGqN3DBRDm+qSxN/
eJi50Rcm33O05qAGVKPLGjvKgnP3BgPaSKe+zxO4yiJx0cPq5UqqLOLplpsoFI2N
Ombj9hYbbT8BcEJHYz/7zj08XJD96mYgD/8tJiwelMXyhkjMwtS5uM/hX40vGIR9
rmb5blmUNWDm+QR41TW9JlbSsSYqOTRowWuT52OqwaQFohHx7AdTx2+WQG3AJpsb
V2qMqoLDhh0xLPRka9DhV093x91DLTE9CfxBO78GY8czbC3IKPnevPw11qVU+TrA
dqJtI1UC3JjlPKFDMpqn7b3AidTMVazoQGebA7nTo2tv4l3T21vnKAxF3OSafpkU
jOIeMKUVtINMXa57QrejtLErTmysO0os/Lc/V2InK42fLn1VgR5Q4lQsA/mNHXLC
YE+20xG6FQrRqq+VkAIgDdLm4i2STkMxUYJnEIpR6rf8KZZcIPaSr0eKO/BGnIky
EYvtq4g6Q6qd+O2oNaCHCadw9cQS1LjYxCfRuNUWDB10FBNiOBP675JkYDmPerrm
oXAVyFRBvRtr6zLihxJbNwOHNngtcbjFsqszkLa+npiTItpXRJdY5eLUpjAfa3ZF
oPOn0cGgeQDqgDJbx4C81JsXDZHyLlZwO9GXvNJv4FkIAcQ+6tXc1x2U7K9USRl/
KfQA8Dwbz1upvn875MeyEfUQs2d6mGC3dVuGcJAK3npZ3+i6AqVLIub9QfwC2qM/
XGTL/2kBm/27/4aF2ksiAON78U4t/SDv3Dxlf9mU/lXtJO60HiIRovIX8DZ4UCBM
z6BizzJRbcZkf9XoQ8wFOt3bmvEYgp8chaomXWufHRG/wdjGTQ2kXHURV9jWMOkJ
wl/wv6l8h+BXr9xapiQSKo/nVh4p3Gn7dUHO70/qXzl5Sh4+ShOLAlpEBEVUvbf0
Q2P83X5l9rJLglATnG4xAMDn8REmvL03MyvWXGry1Hid1260iubDM4RgQ9jhWAkx
sKMwwPD4zsz6DXIVs0+/IW6JRcs7/lmplwFO1rdoyTo/A28dtaKvREuRo/9HUR8C
LjhOmMDtZAH0NQ/pfqhYKVW140ymp5OBURmHlByziuD91UF69Oolykvuy0+dkRt9
wFxzMlXkRR27n9CJ8Y+VFbaQTCjDTRl6aI3yDgNuG07ZrgaNmUAWlLhOyM4DGF/H
O4jJr90NRO9EYfAxNvLCE35ojZeLe5eBZ379p9yeM31nlt2YnQYR1MPx29EcQ2WH
Rn+sa9n80I6RQgNEfL/HQMu8T90qQOWeQgD6PjTyWfco8rY+gk3ywJe4Va5+sb7e
PBHMogKHpgpIULNK7m/rHkjNbZ8eInLYJ+z9OVrtHXa5f+58aCKQbed4r872N/tE
ZGQtUmhZvXMyfJ+/4/n/0IPKy6C74WJnf3uIaOSEq6fYY48WYfSYwMyluvC/uMB2
S98j/KNVxMoi+8r/o4kCvmYDKVCawX7B/j4DzaMQyhKoVNKeZyAdulq7Edf4Jcva
gAMSRK1zoQ141SI7l9tOruruKyxwAbCiq9evUTPeulaSgSkWfwjqU+ukwLahoA1e
WKMeboaoSBcJNfP4SYbKjb8C4AnYp+/+fgtW4vwtOB86teDeKKiBp94h4lyl9k2d
XbYmpQjXcEnuwJV3vGOQPSO362QfCJwdwGISaKSCLxuJtm1xwRoQyaE/Wclxcq4J
2ubgse28emXhSitEuprXhBhRF6hBo2spf7WBjIaVfLRQSu8QZkRPgHZvokrEdkEh
48pr/mQYwT/LnysPqSp9xPiE5CkFPa+UPtuP9TO5rAMVdb/BD5450u90pVWiT8J/
QxaEvtYKONjXn0kMp9Bsvc4/j1ekiYA2I4RCK0tqB2mTr2cYGRccPSXu13ugm4TR
O+PhrvdsOqrannrduNohrvatrFF/Bj5gsOB2wfCKtq3EK5Jn3LLN75cXpAS1DGEX
nyqPGWKilLQm6BxAHa7nu7wCjQtCiQoWui2RUT67GHDK+8lVtRkBPvmiMWxvaf9i
Uwe/XYF+Jgix1C32NEKUvlV3WjQBnCCuv8HUW/s7NoRqEeFN8NfFT4YwfDOWrgH7
zqI6hFE5wW4Ak3ljLgH5+fvLT2+2nYuNyQ8y4LhsXkyqSHHsWW/RTsvGVsrspjm2
oEiI7yoO/8ftZEsdVi4xxSW6Nn0IlFgsRan/pwydYyP29JIDzUE86flU4PKAVLDt
kMUp+Hiq9KM7XIojGbY5UVSZWnEpF4/PklIxHEFa4RYXGPYbG/TN0Uzjbpt4ILP7
7EYPEq/e+SzFqpN0IKBYnL0vDr7+u8qBiRetOaCPivEI1O5QjXdU4fjreAmjdcfP
hBKG18oOTakm4NoJSKuR0l0/ACThNOk9sxF6V+cDY5Qe0F9AKGIt+zuhwTBE+eft
zyDp0xNu4m0xI6etrT5qDxWTHHgF6ZcfZSWWrdrQeIzxLb9WCgy40S3ouT1DB8Tm
WqLBJa6x8K2zRo6givo0zTji5pmAkZs/i7jhaVLBtEQ28/IPP1bwI/OehTTC/AtP
nT7IRq36rhXt6vmwG2rOJggfUqb3XkU+bjkP1wTrT/0/wJlfZHxWXjX1dWMMrOsY
aOXNT4ujd/tyZi9QYq7TErENZbPBjcAHVQrBJYzuIWpB8IiMumiJlagWvIbyP5CB
ACBB8ulGSCXl26lxlzavd8+LbhYSzNfRUjzGvJTZKmx11Myt4klSJiyiJBguMcWh
33CQnw+Xc8vOR45Y6ecr3Qa+MKyKqareA9NonG9kk/nhb1gKHsTj5+/PMCh0idU/
DmJUVygY/E4mrMancj9xR5FKnMllbWPcDgonWdNZuqmvqVtXywWcEQvEde7h5NMr
a/xoV3D/K9E8XVJ1ihv0VTEcad/Tc8L6UuXyfPTTie4U8kvDbwNKln/S2xKvLjtE
BCSYyUi5v1rmuHU9NZ4FEgk5Xplw4dWtjrRBEG3I6R9/4Y8rzxUysPssbMgjv6Km
FWTw9jNXbtuTnjzrWmQTYBAQ5pSV2c6APrgg9J873wwUuSgKjC6VuetceEix1ara
Ol8zmZE3CG9Zym9OfMHyS0AOt4cWL13P+QGDBFY4M4pd1uAe+tJa9xSOItfVG6px
lWwBkcNRHWuLavegTB/KzT6Npw9l6BKeKVbRDgzFkTV7uyVGdu5euWHPgoJ1O6az
s8+vvIZ4LWw09i6cNxKhal5E8JttR8dzaTkfL25tjTVFY9H5JCOx14/lV3+e9tCZ
EC0Dya94QARwEW3OipiYPQQ3Ck++GbJDRotjrvdksQa+E2UdX4nw1vdF0NTbzt3o
4T+0K2CSFaleXmBvsBu+Ru7mp0clTWhaoMGSGJHx9ohzjSD92AOYY+wqzPL9/BF/
hUkp3Wls4Q4unArmo7geIA7Rk/+efPZqwuKCuwSN7ZJE3cLBzxBMiD6J4iNt7kov
rYnflvHbd1dpIQlo7Yj6qMp22P+knHnNFktbkw1j/FarrBSJ1IXWO7A/pBx8ZFrH
vQ4dZldEavx3YNyF0QiUkG9P467EIwlbwhPFWQAKmsJT7MWj2a1WqF5knT2ku6bw
Zajsm47rdwj05KpM/dMshf0L8EZP9JYgwZMO0ANt0RGcK7/NojUZ2JVK/kpEEysC
4UM5wBUzWN9x6zAm4Y/dVxigQ6X6BkmjlMI9yKZKZlQU4pM8k2O7tMirmktAuOua
3CgJuxCB+/jQaCioeIO9yW4l+deZ7wW54oJ782r+PLlbJAkPh7dhYSgoMDo3vVGt
rjkDhESm9bLgsE1mVIB+sCYxVcW/kN1Wgi6O1R7nE+L9oWSJMXNeFDv3u+5iVZKA
RRFqvB2M81J6ziYDP/jm1g73diwJuuqfGvtEr07JoAZAPzwnNnbo+0/XIuZJjrzA
VUcHPjGC3rbw3wVYlhhtCwL9hYeiwV6hGLWRE/wbaT8pNDaCcPWe6r9r+zxjq0jH
N69Di8CofgqZw94oo+VT7cXSh9G0oLe/QSW3bz8yXuk6hslqeFOgrm8/bR3r+6hl
iNT0/ZReTEnPKkFUH762ZHHY8NZGh/9wvwiOcYfQHkHKKafvYtqsARrE0lIPZa+c
Pb7p5WbNc1rKv2vLq/XlZN0ekWI9sIy65SN90RLLMT2x7wJiVAEjwoNndLcMpxFh
kNJxE0urnzJ91KB1scXIPtq+RB+VIFofQLjvmwlEsudVsHkO2gSyzOeRv/MqKgCy
gNxSYoiqHOyLC7BRAUu5HojKPjySUlXREpLCk6vb/svmIKr0rQFhH9VybyiYpHi6
Cjpy1WRUfmwialTzsmULABgA3LS9PCWJ9ZzEelrZL5i8821zTl9jhITrbFFGZ7fU
W0l4uCHJXQ9Pei+kJ9o0gXok6R9UZP3mHw5oR4r6rm7tz2Spgz5vepZHIGdYUKxn
R1qbf2hWtNsc01n5lfEXHcZN+79UhSkx9kxZVNevrGcZyr6Or7o7Iz27nF9HLKdT
7LT95eV+8ZmZcGJmrepZ7VNhixud2cUL5B3kSqg4wmK0B7SRtBBF0yG/+PUoRVw6
YsRFcxB5q+aGMs22vxf6HCBqsEhG3yiMYgSTOSr66ro9On8n7lZcnzNKsi/hSaL7
/JVUwF6WtABgPzTQWvomaV/PyiulRM+Px1Nzwv8a4PjoCYNLRAQ+6C1HDNGNzYUO
+WvUt0fYG6H8v7PalaxdAIpF7uLetyZ50z+r8Hh76nqGt93vz7gOl2odUeGQCnP0
Vuf90zQk0kg0gG6zocqcO3WRZ/t1JPhZNPCjuy4Sg0XKdJT51mGXihwSqwvHW6M+
v6mxFFZ8wNFOAn2CbMBjbrvFx7Bq195G6Z6BbzJnLCi+OMqx0hdHqoywIh2ZE2ot
DqLQ//VbFu6mWbrug6RQbPqutnIvDEVmMMl971j5v4v4B5hHc/D/cFuT+V3cYrZV
qBFWYb9cEdhE/HuFV70lggLiC7TBICYxxqT+jxtn/7vpAyX6AqIZhi1qcjCjsXz/
neOTipXzd5OZiIwMR9AMhEghVnNmSXgujzxmuw4d8Tjun6uwFa/xywvzoIxqe7+p
k3RyizNwWOOy5nOTaZ8svSn5Z+nQjNNbMqnjtlj5quohblWYbOhdKp8BtZQ8Rcrk
T09Gbj8oYhUqwmgBPSCAgnTxmy8x8gaUq3skkJjt4JZZHFRhetgZpwgKx2HAuXAB
6vIzM21Y8E/+81rmqF+qkXKKC8V6WqkPvHRNABDvACrIGWTGsI0oN0S4SLXGKa9b
XnZvPjEwI1uVNfUx5CFvIju7aR9PFr8D98K165oOjQFE6kL8Rhjl4O8plSo036mE
LQi3Hf6N1voBpztkfe4dhhca+B+piN3PpirQ0tJhI+BGcS1ka2FGYikrDis0QdwF
bO9X7nzZ37bO7AWSGyv5pVCFiTdG5zkEervqxLGrKRUfj4BIuYK/saODYP6gtGCl
AMiRb7SH7YmHJZbQv00IPzo9ehDhLcma5ARA3gfJHYVpBy16mR+7yta5Bg4SDyM+
rKvw6a7Z94yy6/v8RVTIpYyxOcHKZUETph/0hjvvrhICjL6rMYZdEx//WeiWNF14
1Nq1zjIglbqlxLxYgrdeyyIVWeR0zGJnZZSekBZcm2bUqxiHIP9T6PFvLIHD9s5J
xhId5gi90m57w8QqWWyDFr6M/GQ+rCGm89XKL7ZCOJRVkwfqaE9zOLPSiQMYp9Aq
yZ8vHiUTNhVdI6HriptE2ZW1i20H+9JSmQmNiJGKVUWhufKBEzcz8R9a/scisa6+
Op3PiKBYYsgbDMC09RMvqnyf8f0r+AEOuytZsNDinX5kGR1UCBvWTZuI/8tLlGhd
5/vo1AfiGnY4DsOL7DUv+Zij3Ay1KVg0mSrqmlgL91dzgOayhZm+XQW23lj7fB+7
ua3gINw5NkDhpAtcJHF8OzKhF1auonq0kiVfE07b6nVkFn+zRfkmTG+Gbd45Lxjk
wHhmR89kMHKRHFsi7q/zRT05uaAoAxUKxwV28Xc+84Q9aQSiSGDETkRALqEQb0DS
h2wYH9xUvgeIUo412SImLcdyy80ID5bAqjJMJcbvUn1nmNSp/6OjIuUeztuldxKf
FfrU7IfYG3ufCg5+GMwPFe0lt7YFEs0tmWv/KbQoXJWABlAVEgFRFRMIHZsbfwEv
xs2G3euiSCnLglIM25VZqNphyUx6bw1JTqRz9q0wQPTEC6PtlBxjOympES11N4wq
HWUp/BGfJWj8Z5uW7EKqE+NEAF/2QyfhzDnZIp17oxGH5kTg7HK8rz2y2cAqGKje
86WMIaEHGHH9fG9+E+g4tMbONVGZ4IwlWYk7jEa6d9WhErlaswot2tSlEsSPpJFw
oFoj254/+W3TyMqZi2itcU3zRtjgwalnA26hudB9rSEZ6neYvzXd7upA67VxxEqt
tjRSVVLdRF1nl8pA8RrqzqXPlNqVcqKEZc7eKg/KxuBnldYh1PkCDs2Tqny63JwR
MUUS4sNvZserfB+Z3wmAgong6+kyzhuTsJRpRBakBiKLHSjLNxu9M4FuTgLvLLzv
Kc2EZxuNO2lJZeArXMQNPOsawwjgsXYL6uW/3QOhQ/D6A3kCE1V+zmtWFJLbfiAz
7Xsa6N07m+gzwNX87+mAT5A0kHK9NZWk0jLdS7NdfbMd0Vj0fKKVyes+v1WkpvyW
AmVyJmItaQskd5zg8ktF/6ulPkm/ml+x8TInJfyOIbvOur0jze++OatHIsb6VLAw
sc9WOEi2BQ81ytj6y7cr5qfsryCP3qKx+7ozvxxNYj8/4uFS5y2LuLfsk2SnWq6d
ftIMFIQ5hU/O7yIWWtNZX7rAlBzKeAEFYwffMvy5p0A7ENqYDYdlPIFL0Gs6xVm4
/HtUULDtEEP2tUkey09zv0Ni/ZYvzXbL5n0ME5TqRQmr1FtZs2OA5GovJ1C9sKSI
lYzd9l1C9hn2cB978pM0d/vS9k3P7YYPpWFA+Iar/kXhCQfxF+StKBq6eP3PA0UX
Vo+jPXXUMJPEmI1QCWioYjJjTHpcpsNxOnjg/xGZ0puPsKo1Yg9wQctjrWPS3O6x
izT/EAELu0fsbs56ewcEBGTzTILhS6ig8dmvxgDKdJBF/bQIkRa/0+w4OnvA2FvP
4OG1Z0pK+Jcv7iSq7hTLPhYtSpWx1brMvA6SVhjvrbFkFptIJntUdV9mPDhzYWdc
7B8l0ALMHhMjnpY9FJEpHkW10/kn3azoEW9v3IMg2gMHQgrHwumV/m9NGvplQVIt
hYtttNLVDFJF8XBp2wTWNL0+3JI2KkuvaEJSo/dUj0VnLwGCWOjWm76sYTCFtDhd
Da8jjv6F86AcIdlD08nbFaIBZ3gnyVwzR3lx2G9+CNrRykftUozo4CHd43wrTVHA
h9O0ZlGu1YUCXMNoTlCYSFgR+HsXVKGi3Nmzl10PwVbsS/wRsw0pUNFwvPHRRhHt
l4M1QvoKy27Z0u96tYaWcO4TIyUJmUVHTdmvtKDkNqbW5oM3FbG5Xhs56IA0xGbE
MNsMjnZXngZnHr0v03b3M9kdk4pBRu4V/7uE1NY+ptuG6+rSYLh0NAY5RQ9LpfHy
37sDzrcyTnVXVxZs/E1hS4AGTliIH3C4Fx9WPAC2XpHwPwso2jJ09tO3eu6w9Gop
EjMBjdrNHIBZHTgjKMO7ejagJh1zB9uRe1NAf/3/ThpyDDMnCEKsVzxVkWP2k2Wl
xT50whxi4FCx/8/Uq3Lx4It+8+GVPlml9gILi9xgTZrIwb7q1l+y6HuTgP893ncq
w3+d8pmf2lxGTl/IOTOjHzVLnUVYKu5+jzynV7s4Pu9tRNYWyib1nr3kF2TlHxso
uFMZfnyZg0P3x5Yz72hwuX9UagOm+D+rgqUDkdmrCs7fYkLBMeKAN1S+8mSZUrZe
S3oXNTWbkk4jKzSx3jRpqNZ68KT+uW25WvYl91Ps6gQS/bfrjoRfKXHj3acd4hc4
l5xFmO7gzKLGkL8Mt+yy9+bp006ltdYa/toYbrVTmt4XPDfSuJ9fnVrdzLbxC1Hq
rfU3/jPZ7m9egoPs2jW5ao28oNaISV0/f8s8VfeOcPN4X7ClEPgI6WwbMlsMN2v/
75m7hqLLHky2uXJfCSD/TvkvRnZc0Tvo735xfRiaxAQk/EWvcOqDWTKJx97DO+ow
SIF3CYEkwBxysgX5HupKSfMACFqk6BK8Jsei5Cy30hvEYy34o+NyGlmUsNAg/j/G
Z93cuRH1n0daq1p0eTAhznitcfhNwzrLmmvkvBpPL2WYvYrrW4laulc9Xldtb5xr
5EYXuPfz0JbPAUy6GAmDKWwPRtRnw521nyMR9yst1Wh3gypSUMLE/wfTXbU6bKHS
T7r/84nSfScCBUnTmf2lk+AcIlUwheA5zDtgoQrq5cAb/1GGpCyV4cvLroS4Tdou
1ILAhXpvQ5sP2JiBFqbgFwydA/ox8lwupzCi1Rvp5MUT7S/BJQjAs0zhDifUrJAy
2gXz6i8WFk/yx5q7+b7djdejEGxojoyyKsD47ST+DsHPoCdlm0DvJanMSmRDOtST
fp9aPxYlFpmyk3fkQPhyFTOASvtcaOOENuGyGxdJqhOOiSrNYB93KUKQ/dIKXn7C
rcuPyOqRG56+3dkUvSfhi3otGpBvOPBPb7Y4HKigGvLiX7HaNayroIV8cM3A8rm3
A0DklBnrm8CCWwGgr4qUg6V2xyxWiEd5yEiMoTftuJXCM+2AAQvai3L+6gO494ZG
/QrneRo9GF9AenwiqJi7js8z+w2OSF2wLogPBJng3b0EnwcdsHddj6n0b2jFtyJy
p9NKZeTGrPkw9N/CedkuOHKd4HpVHWEVOm8vgGn0p3PwnNq0G8P6V1rZJ74h37Ck
I9Jd7foG13nba68E3b9wMI4y8kqwcNwCfIGSxsgNlX/iFDQdYHgZ/NFcJzFwuqhR
ymv4lhc9FioCqxEHFCoQBvFdjEIaaiFiSBkyMGY1tcFIskkZvCxTJz5in2HyV/35
HNObUUgSxII9xtS1L/MDc4fqKNupWakKts5V0FBZZy0FyH8Um5d+0cYhlGD7A2cD
NLF2s80eBVyLAQ2ykB3p/Ky4EwvxuUanhPmvQ+fN6aPNdCKUJojTVuM1+/uBi6OD
rE3oZU5qa8GrvHaFzlR+Dr69KOQE7UQtjLSdqNgdkYNc/lTPAVbCdLNFIlL2WE8p
oVC+jCr3FUfY8/lGoXZCYlCDUvn/Uy/w69AnM0DI44uo3/6O0AvNDWhoDyj5VjnA
DfLtjQzbPB1hc7zjBBQmHeBZpGlXZhawaGLft3rxdMocGakLJ8mjoOgr+hmVnr6V
x4f8DTAX1tPIiRyOwje03I5tnpbwieCBYTO9JRTNstnUifTUX49MIEJwT+Sh5JTo
SIgFE26p+gBF+rKhYN2Jv4dDdwoszrg6JFxGMbsiL5a3buYHohqRoJ6hNIeGIftv
OuoCswG+/V2t0OQHpKd+LGIZbqe4OxSfecHrJ0y7ANgp2XpqZM+9XUyLIiBLECv1
n1AlBIhrVC8FPIjE7iaCJrMbYPEC0LY/XHXPsUd9Op7DKeTSVMK5S2DtdF1iwnUj
BEPAGNLbrBAPaulCdnNsqfOY/QQNxGppaWUv3CQ6RRLXTi4EcY7jVuHQ9eiFVKvY
aQ4HRrZuXQBZJx5IlIhDhKR0sbXYccyWd7vUA+lgPiAfdlQM3uMDzJR5cZ1XFDtG
/edbMPI4DSGqEAp17dldEOtm5KqJ645BZTiglupcNGMaUsQ7TxxNUVPfX/9hFfcK
FMEI6woFLQ4tS20gI/RcvqSrexPEhkgQP7eYLdr2hYdmofu6TaLT1or/YEk3oXqz
GPMDOBtfxRhw3ylc1iQ58DYVv3mEFo/wdDUXzIZ+WTcogNNXXa6bePDJMuBPEpb+
y8bQbqt7sC1eo/04YgL+7cDvj10gZm6ezcGOnClq2uXOqb4xPg3ZnIech/v648Aj
p12tBogyZCrsm4AnNNaMN1IteAHwBB9+8ibrjHN8FbfnDvCOMuXbjCETyC+5RJZv
OFUhHACbhQ+gdKFHVkszV8HlM0VJmMwaTV8eXZNXnv3wTyoLNsHtkvsucrnDmApe
`pragma protect end_protected
